<?php
include_once("../public/mem_check.php");
include_once("../public/web_function.php");

if(!$_SESSION['KNH_LOGIN_ID'])
{
    header("location:../index.php");
    //exit;
} 

if(is_numeric(quotes($_GET['RS_ID'])))
{
    $RS_ID = quotes($_GET['RS_ID']);
    
    $sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
    $crs = $objDB->Recordset($sql);
    $crow = $objDB->GetRows($crs);
}

if(!is_numeric(quotes($_GET['RS_Status'])))
{
  $RS_Status = 2;
}
else
{
  $RS_Status = quotes($_GET['RS_Status']);
}

$rs = $objDB->Recordset("SELECT * FROM resident WHERE RS_Status = '$RS_Status' order by RS_Date asc"); 
$row = $objDB->GetRows($rs);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title?> 歡迎頁</title>

<script language="javascript" src="../js/common.js" ></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script type="text/javascript">

$(document).ready(function(){
    
    //alert("main:ready~!");
    /*
    $("ul").click(function(){
        
        var hasRS_ID='<?php echo is_numeric(quotes($_GET['RS_ID'])); ?>';
        if(hasRS_ID!='1')
        {
            alert("ready:請先選擇住民!!!");
        }
    });*/

});

function doChange1(){
  
  //alert("function: doChange1");
  var hasRS_ID='<?php echo is_numeric(quotes($_GET['RS_ID'])); ?>';
  if(hasRS_ID!='1')
  {
      alert("請先選擇住民!!!");
  }
  else
  {
      //alert("main:else");
      $("#hide1").toggle();
  }
  
};

function doChange2(){
  
  //alert("function: doChange2");

  var hasRS_ID='<?php echo is_numeric(quotes($_GET['RS_ID'])); ?>';
  if(hasRS_ID!='1')
  {
      alert("請先選擇住民!!!");
  }
  else
  {
      //alert("main:else");
      $("#hide2").toggle();
  };
};

function doChange3(){
  
  //alert("function: doChange3");

  var hasRS_ID='<?php echo is_numeric(quotes($_GET['RS_ID'])); ?>';
  if(hasRS_ID!='1')
  {
      alert("請先選擇住民!!!");
  }
  else
  {
      //alert("main:else");
      $("#hide3").toggle();
  }
};

</script>
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="1000" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
		<!-- header starting point -->
        <?php include("../include/header.php");?>
        <!-- header ending point -->	
    </td>
  </tr>
  <tr>
    <td valign="top">      
    <table width="1000" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
       		<!--menu starting point-->
            <?php include("../include/menu.php");?>
			    <!--menu ending point-->
        </td>
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="830" valign="top"><table width="830" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td width="836" height="30" class="content">歡迎！</td>
            </tr>
            <tr>
              <td height="10" class="content_red_b"><img src="../images/spacer.gif" width="1" height="1" /></td>
            </tr>
            <tr>
              <td class="content_red_b"> <?php echo $_SESSION['KNH_LOGIN_NAME'];?>  登入</td>
            </tr>
            <tr>
              <td height="10"><img src="../images/spacer.gif" width="1" height="1" /></td>
            </tr>
            <tr>
              <td valign="top"　class="content_red_b"><p>*操作左側授權單元管理前，請先選擇住民！<br /></p>
              </td>
            </tr>
            <tr>
              <td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $crow[0]['RS_Name'];?></td>   
            </tr>
            <tr>
              <td align="right" class="content_list"><br /></td>
            </tr>
            <tr>
                <td height="10" class="content"></td>
            </tr>
            <tr>
                <td height="30"><input name="addmenu_btn" type="button" class="content" id="addmenu_btn" onclick="MM_goToURL('parent','../care/register_add.php');return document.MM_returnValue" value="新增住民"/></td>
            </tr>
            <tr>
                <td height="20" class="content"></td>
            </tr>
            <tr>
               <table width="1000" border="0" cellpadding="0" cellspacing="0">
  
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        
        <!--menu ending point-->
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top"><table width="830" border="0" cellpadding="0" cellspacing="0">      
          <tr>
            <td valign="top">
               <table width="825" border="0" cellpadding="0" cellspacing="0">
                  
                  <tr>
                    <td>
                      <table width="820" height="55" border="0" cellpadding="1" cellspacing="1" bordercolor="#777777" bgcolor="#555555">
                        <tr>                           
                          <td width="100" align="center" bgcolor="#898989" class="back_w"><a href="#">姓名</a></td>
                          <td width="125" align="center" bgcolor="#898989" class="back_w"><a href="#">登記日期</a></td>
                          <td width="60" align="center" bgcolor="#898989" class="back_w"><a href="#">狀態</a></td>
                          <td width="121" align="center" bgcolor="#898989" class="back_w"><a href="#">身分證字號</a></td>
                          <td width="100" align="center" bgcolor="#898989" class="back_w"><a href="#">出生年月日</a></td>
                          <td width="111" align="center" bgcolor="#898989" class="back_w"><a href="#">電話</a></td>
                          <td width="90" align="center" bgcolor="#898989" class="back_w"><a href="#">聯絡人</a></td>
                          <td width="90" align="center" bgcolor="#898989" class="back_w"><a href="#">功能操作</a></td>
                        </tr>
                       
                        <?php for($i=0;$i<$objDB->RecordCount($rs);$i++){?>
                        <?php if($i%2==0){$bgcolor="#EBEBEB";}else{$bgcolor="#FFFFFF";}?>
                        <tr>                           
                          <td align="center" bgcolor="<?php echo $bgcolor;?>" class="content"><?php echo $row[$i]["RS_Name"]; ?> </td>
                          <td align="center" bgcolor="<?php echo $bgcolor;?>" class="content"><?php echo $row[$i]["RS_Date"]; ?> </td>
                          <td align="center" bgcolor="<?php echo $bgcolor;?>" class="content">
                            <?php 
                                if($row[$i]["RS_Status"] == 1) 
                                    echo "待審"; 
                                elseif($row[$i]["RS_Status"] == 2)
                                    echo "入住";
                                elseif($row[$i]["RS_Status"] == 3)
                                    echo "入院保留床";
                                elseif($row[$i]["RS_Status"] == 4)
                                    echo "退住";
                                else
                                    echo "無";
                            ?> 
                          </td>
                          <td align="center" bgcolor="<?php echo $bgcolor;?>" class="content"><?php echo $row[$i]["RS_IdentityCard"]; ?> </td>
                          <td width="100" bgcolor="<?php echo $bgcolor;?>" class="content" style="width:px;word-wrap:break-word;overflow:auto" align="center"><?php echo $row[$i]["RS_Birthday"]; ?></td>
                          <td align="center" bgcolor="<?php echo $bgcolor;?>" class="content"><span class="content" style="width:px;word-wrap:break-word;overflow:auto"><?php echo $row[$i]["RS_Tel"]; ?></span></td>
                          <td width="90" bgcolor="<?php echo $bgcolor;?>" class="content" style="width:px;word-wrap:break-word;overflow:auto" align="center"><?php echo $row[$i]["RS_Contact"]; ?></td>
                          <td width="90" align="center" bgcolor="<?php echo $bgcolor;?>">
                        <?php if($row[$i]["RS_Status"] == 1){?>
                          <input name="button2" type="button" class="login" id="button2" onclick="MM_goToURL('parent','../care/register_md.php?RS_ID=<?php echo $row[$i]["RS_ID"]; ?>');return document.MM_returnValue" value="檢視" />
                        <?php }else {?>
                          <input name="button2" type="button" class="login" id="button2" onclick="MM_goToURL('parent','main.php?RS_ID=<?php echo $row[$i]["RS_ID"]; ?>');return document.MM_returnValue" value="選擇" />
                        <?php }?>
                           </td>
                        </tr>
                        <?php }?>
                        <?php if($objDB->RecordCount($rs)==0){?>
                        <div align="center" style="margin-top:5px">無資料！</div>
                        <?php }?>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                    </table></td>
                  </tr>
                </table>              
              </td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>

            </tr>
          </table>
            <br />
            <br /></td>
      </tr>
    </table>
    </td>
  </tr>
    <td height="1" bgcolor="B5B4B3" class="copyright"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td class="copyright">
      <!--footer starting point-->
      <?php include("../include/footer.php");?>
      <!--footer starting point-->
    </td>
  </tr>
</table>
</body>
</html>
