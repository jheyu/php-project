<?php 
include("../public/mem_check.php");
include("../public/web_function.php");
	/*
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}
	*/
	if(is_numeric(quotes($_GET['RS_ID']))){
		 $RS_ID = quotes($_GET['RS_ID']);
	}else{
		 ?>
     <script language="javascript">		
		location.href='../index.php';
	 </script>	
         <?php
	}	
	$sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
	$rs = $objDB->Recordset($sql);
	$row = $objDB->GetRows($rs);	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.datepicker.min.js"></script>

<script>
$(document).ready(function(){
    $(".date-pick" ).datepicker({ 
		dateFormat: 'yy-mm-dd', 
		showOn: "button",
		buttonImage: "../js/calendar.png",
		buttonImageOnly: true
	});	
 	$("#mybtn").click(function(){	
			$("form#form1").submit();
		}		
	)	
})

</script>
<link type="text/css" href="../css/ui-darkness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<!-- header starting point -->
	<?php include("../include/header.php");?>
	<!-- header ending point -->    
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?>
        <!--menu ending point-->          
        </td>            
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top">
        <table width="830" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="30" class="content">專業照護 > 評估量表 &gt; 新增</td>
              </tr>
              <tr>
                  <td height="10"></td>
              </tr>  
              <tr>
                <td height="10">
                <span class="form_title">
                     <input name="search" type="button" class="content" id="search" value="回上一頁" onclick="MM_goToURL('parent','tube_list.php?RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>
                </span></td>
              </tr>   
              <tr>
                  <td height="10"></td>
              </tr>                      
             <tr>
              	<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>   
             </tr>   
             <tr>
                <td height="30"></td>
              </tr>           
              
              <tr>
              		<td style="font-size: 13pt"><strong>新增管路</strong>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  </td>            
              </tr> 
              <tr>
                <td height="5"></td>
              </tr>
              <tr>
              	  <td ><img src="../images/blueline.jpg" /></td>
              </tr> 
               <tr>
                <td height="5"></td>
              </tr>   
            <tr>         
              <td>    
              <!--管理員管理startinging-->              
              <form name="form1" id="form1" method="post" action="tube_process.php" />
			        <input type="hidden" name="action" id="action" value="new"/>              
              <input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>" />             
             <table>
				        <tr>
                  <td width="110" align="right" class="content">管路名稱：</td>
                  <td width="705">
                  <select id="TB_Name" name="TB_Name">
                      <option id="TB_Name1" name="TB_Name" value="">請選擇</option>
                      <option id="TB_Name1" name="TB_Name" value="1" >尿管</option>
                      <option id="TB_Name2" name="TB_Name" value="2" >膀胱造口</option>
                      <option id="TB_Name3" name="TB_Name" value="3" >鼻胃管</option>
                      <option id="TB_Name4" name="TB_Name" value="4" >胃造瘻口</option>
                      <option id="TB_Name5" name="TB_Name" value="5" >氣切管</option>
                      <option id="TB_Name6" name="TB_Name" value="6" >Pigtail</option>
                      <option id="TB_Name7" name="TB_Name" value="7" >人工血管</option>
                      <option id="TB_Name8" name="TB_Name" value="8" >動/靜脈瘻管</option>
                      <option id="TB_Name9" name="TB_Name" value="9" >腹膜透折血管</option>
                      <option id="TB_Name10" name="TB_Name" value="10" >角針</option>
                      <option id="TB_Name11" name="TB_Name" value="11" >其他管路</option>
                  </select>
                  </td>  
                </tr>
                <tr>
                   <td height="10"></td> 
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">起始日期：</td>
                  <td width="705">
					<input name="TB_Start"  id="TB_Start" type="text" class="txt date-pick" style="  width:80px;"  value="<?php  echo date("Y-m-d"); ?>"  />
                  </td>
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right"  class="content">更換日期：</td>
                  <td width="705">
					<input name="TB_End"  id="TB_End" type="text" class="txt date-pick" style="  width:80px;"  value=""  />
                  </td>
                </tr>
				<tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="110" align="right" class="content"> Fr：</td>
                  <td width="705">
					        <select id="TB_Fr" name="TB_Fr">
                      <option id="TB_Fr1" name="TB_Fr" value="">請選擇</option>
                      <option id="TB_Fr1" name="TB_Fr" value="14" >14</option>
                      <option id="TB_Fr2" name="TB_Fr" value="16" >16</option>
                      <option id="TB_Fr3" name="TB_Fr" value="18" >18</option>
                      <option id="TB_Fr4" name="TB_Fr" value="20" >20</option>
                      <option id="TB_Fr5" name="TB_Fr" value="22" >22</option>
                  </select>
                  </td>  
                </tr>
				        <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="110" align="right" class="content"> 型式：</td>
                  <td width="705">
					          <select id="TB_Material" name="TB_Material">
                      <option id="TB_Material0" name="TB_Material" value="">請選擇</option>
                      <option id="TB_Material1" name="TB_Material" value="1" >silcon</option>
                      <option id="TB_Material2" name="TB_Material" value="2" >ruber</option>
                      <option id="TB_Material3" name="TB_Material" value="3" >shilley(有cuff)</option>
                      <option id="TB_Material4" name="TB_Material" value="4" >shilley(無cuff)</option>
                      <option id="TB_Material5" name="TB_Material" value="5" >bivona</option>
                      <option id="TB_Material6" name="TB_Material" value="6" >teflon</option>
                      <option id="TB_Material7" name="TB_Material" value="7" >其他</option>
                    </select>
                    </select>
                  </td>  
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>
			              <input name="mybtn" type="submit" class="form_fix" id="mybtn" value="確定送出"/> 
 		                <input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>
		              </td>
                </tr>    
             </table>
              </form>
              <!--管理員管理 ending-->              
              </td>
            </tr>
            
          </table>
          </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>
