<script type="text/javascript" src="../js/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.datepicker.min.js"></script>
<link type="text/css" href="../css/ui-darkness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<script>
$(document).ready(function(){
    
    $(".date-pick" ).datepicker({ 
		dateFormat: 'yy-mm-dd', 
		showOn: "button",
		buttonImage: "../js/calendar.png",
		buttonImageOnly: true
	});	

 	//$("#mybtn").click(function(){	
		//$("form#form1").submit();		
	//})	
})

</script>
<!--form name="form" id="form" method="post" action="layout.php?RS_ID=<?php //echo $_GET["RS_ID"];?>&t=ioresult" style="border:none;float:left" onsubmit="">
      選擇其他日期:&nbsp;
      <input name="Date"  id="Date" type="text" class="txt date-pick" style="width:80px;"  value=""  />
      <input type="hidden" name="RS_ID" id="RS_ID" value="<?php //echo $_GET["RS_ID"];?>">
      <input name="button6" type="submit"  id="button6" value="go" class="content"  />
</form-->
<?php 

	$RS_ID = $_GET["RS_ID"];
	
	
	$rs_form = $objDB->Recordset("SELECT * FROM vital WHERE RS_ID = '$RS_ID' ORDER BY VS_Date DESC");
	
	$row_form = $objDB->GetRows($rs_form);
	if($objDB->RecordCount($rs_form) > 0)
	{	
	
	//$FA_ID = $row_form[0]['FA_ID'];
?>
<table>
	<tr align="center">
	 <h2 align="left"><strong>生命徵象評估紀錄</strong></h2>
	</tr>
	<tr>
	    <td width="227" bgcolor="#777777" class="enter_wb" align="center">日期</td>
		<td width="227" bgcolor="#777777" class="enter_wb" align="center">時間</td>
        <td width="227" bgcolor="#777777" class="enter_wb" align="center">體溫</td>
		<td width="179" bgcolor="#777777" class="enter_wb" align="center">脈搏</td>
		<td width="179" bgcolor="#777777" class="enter_wb" align="center">呼吸</td>
		<td width="179" bgcolor="#777777" class="enter_wb" align="center">血壓</td>
		<td width="179" bgcolor="#777777" class="enter_wb" align="center">疼痛(0~10分)</td>
		<td width="179" bgcolor="#777777" class="enter_wb" align="center">解便次數</td>
		<td width="179" bgcolor="#777777" class="enter_wb" align="center">護理人員</td>
        <td width="222" bgcolor="#777777" class="enter_wb" align="center">編輯</td>
   </tr>
	<?php
		$DIn = $DOut = $NIn = $NOut = 0;
		for($i=0;$i<$objDB->RecordCount($rs_form);$i++)
			{
				if($i%2==0)
					$bgcolor="#EBEBEB";
				else
					$bgcolor="#FFFFFF";
				
	?>
	<tr>
		<td height="41" align="center" bgcolor="<?php echo $bgcolor;?>"  class="content" ><?php echo $row_form[$i]["VS_Date"];?></td>
		<td height="41" align="center" bgcolor="<?php echo $bgcolor;?>"  class="content" ><?php echo $row_form[$i]["VS_Time"];?></td>
		<td height="41" align="center" bgcolor="<?php echo $bgcolor;?>"  class="content" <?php if($row_form[$i]["VS_Tem"] >= 37.5 || $row_form[$i]["VS_Tem"] <= 36) echo "style=\"color:#FF0000;\""?>><?php echo $row_form[$i]["VS_Tem"];?></td>
		<td height="41" align="center" bgcolor="<?php echo $bgcolor;?>"  class="content" <?php if($row_form[$i]["VS_HR"] >= 90 || $row_form[$i]["VS_HR"] <= 60) echo "style=\"color:#FF0000;\""?>><?php echo $row_form[$i]["VS_HR"];?></td>
		<td height="41" align="center" bgcolor="<?php echo $bgcolor;?>"  class="content" <?php if($row_form[$i]["VS_RR"] <= 14 || $row_form[$i]["VS_RR"] >= 22) echo "style=\"color:#FF0000;\""?>><?php echo $row_form[$i]["VS_RR"];?></td>
		<td height="41" align="center" bgcolor="<?php echo $bgcolor;?>"  class="content" <?php if($row_form[$i]["VS_SBP"] >= 150 || $row_form[$i]["VS_SBP"] <= 120 || $row_form[$i]["VS_DBP"] >= 100 || $row_form[$i]["VS_DBP"] <= 60 ) echo "style=\"color:#FF0000;\""?>><?php echo $row_form[$i]["VS_SBP"]."/".$row_form[$i]["VS_DBP"];?></td>
		<td height="41" align="center" bgcolor="<?php echo $bgcolor;?>"  class="content" <?php if($row_form[$i]["VS_Pain"] >= 1) echo "style=\"color:#FF0000;\""?>><?php echo $row_form[$i]["VS_Pain"];?></td>
		<td height="41" align="center" bgcolor="<?php echo $bgcolor;?>"  class="content" ><?php echo $row_form[$i]["VS_BC"];if($row_form[$i]["VS_BT"] != "") echo "/".$row_form[$i]["VS_BT"];?></td>
		<td height="41" align="center" bgcolor="<?php echo $bgcolor;?>"  class="content" ><?php echo $row_form[$i]["VS_NS"];?></td>
		<td width="123" align="center" bgcolor="<?php echo $bgcolor;?>"><a href="vital_md.php?VS_ID=<?php echo $row_form[$i]["VS_ID"]; ?>&RS_ID=<?php echo $row_form[$i]["RS_ID"];?>" class="overed">編輯</a></td>
    </tr>		
	<?php } ?>
	<tr height="20">
	</tr>
</table>
<?php
	}
	else
	{
		echo "<br/>沒有記錄，請先新增記錄。";
	}
?>