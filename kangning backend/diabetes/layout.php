<?php 
include_once("../public/mem_check.php");
include_once("../public/web_function.php");

if(!$_SESSION['KNH_LOGIN_ID'])
{
		header("location:../index.php");
		exit;
}	
	
if(is_numeric(quotes($_GET['RS_ID'])))
{
		$RS_ID = quotes($_GET['RS_ID']);
}	

$sql = "SELECT * FROM resident WHERE RS_ID = '$RS_ID' order by RS_Date asc";
$rs = $objDB->Recordset($sql);
$row = $objDB->GetRows($rs);

$type = $_GET['t'];
			 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title><?php echo $html_title;?>專業照護</title>
<script src="../js/common.js" language="javascript"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.js"></script>
<script type="text/JavaScript">
/*
function changed(theselect) {
	//var am_type = theselect.selectedIndex + 1 ;
	var am_type = $('#type_slt').val();
	window.location.href="admin.php?am_type="+am_type;
}
*/
$(document).ready(function(){ 			
	$("#mybtn1").click(function(){
		var rs_id = <?php echo $RS_ID;?>;
		var form_type = '<?php echo $type;?>';
		if(form_type == 'bs'){
		window.location.href="bs_add.php?RS_ID="+rs_id;
		}else if(form_type == 'ins'){
		window.location.href="ins_add.php?RS_ID="+rs_id;
		}else if(form_type == 'dpa'){
		window.location.href="dpa_add.php?RS_ID="+rs_id;
		}else if(form_type == 'vital'){
    window.location.href="vital_add.php?RS_ID="+rs_id;
		}
		//$("form#form1").submit();		
	})			 
});   
</script>

<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>

<body onLoad="MM_preloadImages('../images/logout_r.gif','../images/logout_r.gif')">
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
    <!-- header starting point -->
    <?php include("../include/header.php");?>
    <!-- header ending point -->
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?>
        <!--menu ending point-->
        </td>        
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top">
        <table width="830" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="30" class="content">專業照護 &gt; 評估與記錄</td>
          </tr>          
          <tr>
            <td valign="top">
               <table width="825" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td>
                        <table width="830" height="25" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                  		<td height="10"></td>
                      </tr>
                      <tr>
              				<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>                           
                      </tr>
                      
                      <tr>
                        <td height="20"></td>
                      </tr>
                      <tr> 
                          <!--<form name="form1" id="form1" method="post" action="fall-add.php">
                          <input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>"/>    -->                     
                          	<td class="content" >                                                       
                              <input name="mybtn1" type="button" class="content" id="mybtn1" value="新增表單"/></span>                   
                              <input name="search" type="button" class="content" id="search" value="回上一頁" onclick="MM_goToURL('parent','people_list.php?t=<?php echo $type;?>&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>
                            </td>
                          <!--  </form>-->
                      </tr>
                          </table>                          
                          <table width="830">                                                 
                          <!-- 所有表單button --> 

                          <?php include "allformbtn.php";?>              
                      	 </table>                      
                     </td>
                  </tr>
                  <tr>
                    <td>
                    <table>
                    <!-- 從這開始 -->
                    <?php
					switch($type){
						case "":
							include "overview.php";
							break;
            case "vital":
              include "vital.php";
              break;
						case "bs":
							include "bs.php";
							break;
						case "ins":
							include "ins.php";							
							break;
						case "dpa":
							include "dpa.php";
							break;
					} 
						
					?>
                    </table>                      
                    </td>
                  </tr>
                  <tr>
                    <td>
                    <table width="830" border="0" cellspacing="0" cellpadding="0">
                    </table>
                    </td>
                  </tr>
                </table>              
                </td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td class="copyright">
      <!--footer starting point-->
      <?php include("../include/footer.php");?>
      <!--footer starting point-->
    </td>
  </tr>
</table>
</body>
</html>
