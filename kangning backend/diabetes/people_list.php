<?php 
include_once("../public/mem_check.php");
include_once("../public/web_function.php");

if(!$_SESSION['KNH_LOGIN_ID'])
{
		header("location:../index.php");
		exit;
}	
	
if(is_numeric(quotes($_GET['RS_ID'])))
{
		$RS_ID = quotes($_GET['RS_ID']);
}	

$sql = "SELECT * FROM resident WHERE RS_Status = '2' order by RS_Date asc";
$rs = $objDB->Recordset($sql);
$row = $objDB->GetRows($rs);

$type = $_GET['t'];
			 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title><?php echo $html_title;?>專業照護</title>
<script src="../js/common.js" language="javascript"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.js"></script>
<script type="text/JavaScript">
			   
</script>

<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>

<body onLoad="MM_preloadImages('../images/logout_r.gif','../images/logout_r.gif')">
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
    <!-- header starting point -->
    <?php include("../include/header.php");?>
    <!-- header ending point -->
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?>
        <!--menu ending point-->
        </td>        
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top">
        <table width="830" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="30" class="content">專業照護 &gt; 評估與記錄</td>
          </tr>          
          <tr>
            <td valign="top">
               <table width="825" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td>
                        <table width="830" height="25" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                  		   <td height="10"></td>
                      </tr>
              			  <tr align="center">
                          <h2 align="left"><strong>
                      <?php 
                            if($type == 'ins'){
                              echo "胰島素注射紀錄";
                            }
                            elseif ($type == 'bs') {
                              echo "血糖紀錄";
                            }
                            else{
                              echo "生命徵象評估紀錄";
                            }
                      ?>
                          </strong></h2>
                      </tr>
              				<!--<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>-->   
                      <tr>
                  			<td height="10"></td>
           			      </tr>
                      <tr>                           
                          <td width="100" align="center" bgcolor="#898989" class="back_w"><a href="#">姓名</a></td>
                          <td width="125" align="center" bgcolor="#898989" class="back_w"><a href="#">登記日期</a></td>
                          <td width="60" align="center" bgcolor="#898989" class="back_w"><a href="#">狀態</a></td>
                          <td width="121" align="center" bgcolor="#898989" class="back_w"><a href="#">身分證字號</a></td>
                          <td width="100" align="center" bgcolor="#898989" class="back_w"><a href="#">出生年月日</a></td>
                          <td width="111" align="center" bgcolor="#898989" class="back_w"><a href="#">電話</a></td>
                          <td width="90" align="center" bgcolor="#898989" class="back_w"><a href="#">聯絡人</a></td>
                          <td width="90" align="center" bgcolor="#898989" class="back_w"><a href="#">功能操作</a></td>
                      </tr>
                       <?php for($i=0;$i<$objDB->RecordCount($rs);$i++){?>
                        <?php if($i%2==0){$bgcolor="#EBEBEB";}else{$bgcolor="#FFFFFF";}?>
                        <tr>                           
                          <td align="center" bgcolor="<?php echo $bgcolor;?>" class="content"><?php echo $row[$i]["RS_Name"]; ?> </td>
                          <td align="center" bgcolor="<?php echo $bgcolor;?>" class="content"><?php echo $row[$i]["RS_Date"]; ?> </td>
                          <td align="center" bgcolor="<?php echo $bgcolor;?>" class="content"><?php echo "已入住";?> </td>
                          <td align="center" bgcolor="<?php echo $bgcolor;?>" class="content"><?php echo $row[$i]["RS_IdentityCard"]; ?> </td>
                          <td width="100" bgcolor="<?php echo $bgcolor;?>" class="content" style="width:px;word-wrap:break-word;overflow:auto" align="center"><?php echo $row[$i]["RS_Birthday"]; ?></td>
                          <td align="center" bgcolor="<?php echo $bgcolor;?>" class="content"><span class="content" style="width:px;word-wrap:break-word;overflow:auto"><?php echo $row[$i]["RS_Tel"]; ?></span></td>
                          <td width="90" bgcolor="<?php echo $bgcolor;?>" class="content" style="width:px;word-wrap:break-word;overflow:auto" align="center"><?php echo $row[$i]["RS_Contact"]; ?></td>
                          <td width="90" align="center" bgcolor="<?php echo $bgcolor;?>">
                        <!--input name="button4" type="button" class="login" id="button4" value="刪除" onclick="if(confirm('確定要刪除此筆資料嗎?'))MM_goToURL('parent','register_process.php?action=del&RS_ID=<?php //echo $row[$i]["RS_ID"]; ?>');return document.MM_returnValue"  
                          /-->
                        <!--input name="button2" type="button" class="login" id="button2" onclick="MM_goToURL('parent','register_md.php?RS_ID=<?php //echo $row[$i]["RS_ID"]; ?>');return document.MM_returnValue" value="檢視" 
                          /-->
                        <input name="button2" type="button" class="login" id="button2" onclick="MM_goToURL('parent','layout.php?t=<?php echo $type;?>&RS_ID=<?php echo $row[$i]["RS_ID"]; ?>');return document.MM_returnValue" value="檢視" />
                        <!--
                        <input name="button4" type="button" class="login" id="button4" value="刪除" onclick="if(confirm('確定要刪除此筆資料嗎?'))MM_goToURL('parent','admin_process.php?action=del&AM_ID=<?php echo $row[$i]["AM_ID"]; ?>');return document.MM_returnValue"  
                        />-->
                           </td>
                        </tr>
                        <?php 
                          }
                        ?>                           
                          </table>                                              
                     </td>
                  </tr>
                  <tr>
                    <td>
                    <table>
                    <!-- 從這開始 -->
                    </table>                      
                    </td>
                  </tr>
                  <tr>
                    <td>
                    <table width="830" border="0" cellspacing="0" cellpadding="0">
                    </table>
                    </td>
                  </tr>
                </table>              
                </td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td class="copyright">
      <!--footer starting point-->
      <?php include("../include/footer.php");?>
      <!--footer starting point-->
    </td>
  </tr>
</table>
</body>
</html>
