<?php 
include("../public/mem_check.php");
include("../public/web_function.php");
	/*
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}
	*/
	if(is_numeric(quotes($_GET['RS_ID']))){
		 $RS_ID = quotes($_GET['RS_ID']);
	}else{
		 ?>
     <script language="javascript">		
		location.href='../index.php';
	 </script>	
         <?php
	}	
	$sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
	$rs = $objDB->Recordset($sql);
	$row = $objDB->GetRows($rs);	
	
	if($_GET['EV_ID'] != "")
	{
		$EV_ID = $_GET['EV_ID'];
		$sql = "SELECT * FROM evaluate where EV_ID ='$EV_ID'";
		$rs_f = $objDB->Recordset($sql);
		$row_f = $objDB->GetRows($rs_f);
		$EV_ND = explode(",",$row_f[0]['EV_ND']);
	}
	else
	{
		$ND_Num = $_POST["ND_Num"];
		$EV_ND = explode(",",$ND_Num);
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.datepicker.min.js"></script>

<script>
$(document).ready(function(){
	$(".date-pick" ).datepicker({ 
		dateFormat: 'yy-mm-dd',
		showOn: "button",
		buttonImage: "../js/calendar.png",
		buttonImageOnly: true,
		
	});	
 	$("#mybtn").click(function(){	
			$("form#form1").submit();
		}		
	)	
})

</script>
<link type="text/css" href="../css/ui-darkness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<!-- header starting point -->
	<?php include("../include/header.php");?>
	<!-- header ending point -->    
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?>
        <!--menu ending point-->          
        </td>            
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top">
        <table width="830" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="30" class="content">專業照護 > 評估量表 &gt; 新增</td>
              </tr>
              <tr>
                  <td height="10"></td>
              </tr>  
              <tr>
                <td height="10">
                <span class="form_title">
                     <input name="search" type="button" class="content" id="search" value="回上一頁" onclick="MM_goToURL('parent','nd_list.php?RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>
                </span></td>
              </tr>   
              <tr>
                  <td height="10"></td>
              </tr>                      
             <tr>
              	<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>   
             </tr>   
             <tr>
                <td height="30"></td>
              </tr>           
              
              <tr>
              		<td style="font-size: 13pt"><strong>護理之家住民健康評估單</strong>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  </td>            
              </tr> 
              <tr>
                <td height="5"></td>
              </tr>
              <tr>
              	  <td ><img src="../images/blueline.jpg" /></td>
              </tr> 
               <tr>
                <td height="5"></td>
              </tr>   
            <tr>         
              <td>    
              <!--管理員管理startinging-->              
              <form name="form1" id="form1" method="post" action="nd_process.php" />
				<input type="hidden" name="action" id="action" value="new"/>              
				<input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>" />
				<input type="hidden" name="ND_Num" id="ND_Num" value="<?php if($row_f[0]['EV_ND'] != "") echo $row_f[0]['EV_ND']; else echo $EV_ND[0];?>" />     			  
             <table>
			 <tr>
                  <td width="110" align="right"  class="content">起始日：</td>
                  <td width="705">
					<input name="ND_Date"  id="ND_Date" type="text" class="txt date-pick" style="width:80px;"  value="<?php  echo date("Y-m-d"); ?>"  />
                  </td>
             </tr>
			 <?php
				if($EV_ND[0] != 0 )
				{
				for($i=0;$i<count($EV_ND);$i++)
				{
					$sql_n = "SELECT * FROM diagnosis_static where ND_Num ='$EV_ND[$i]'";
					$rs_n = $objDB->Recordset($sql_n);
					$row_n = $objDB->GetRows($rs_n);
				?>
                <tr>
                   <td height="10"></td> 
                </tr>
				<tr>
				<td></td>
				<td width="110" align="left" class="content"><strong>＃<?php echo $EV_ND[$i],$row_n[0]['ND_Name'];?></strong></td>
				<input type="hidden" name="<?php echo $row_n[0]['ND_Num']."_";?>Name" id="<?php echo $row_n[0]['ND_Num']."_";?>Name" value="<?php echo $row_n[0]['ND_Name'];?>" /> 
				</tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">導因：</td>
                  <td width="705">
					<?php 
					$ND_Reason = explode(",",$row_n[0]['ND_Reason']);
					for($j=0;$j<count($ND_Reason);$j++){?>
					<input type="checkbox" name="<?php echo $row_n[0]['ND_Num']."_";?>Reason[]" id="<?php echo $row_n[0]['ND_Num']."_Reason".$j; ?>" value="<?php echo $ND_Reason[$j];?>" ><?php echo $ND_Reason[$j];?><br/>
					<?php }?>
                  </td>  
                </tr>
				<tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">鑑定特徵：</td>
                  <td width="705">
					<?php 
					$ND_Features = explode(",",$row_n[0]['ND_Features']);
					for($j=0;$j<count($ND_Features);$j++){?>
					<input type="checkbox" name="<?php echo $row_n[0]['ND_Num']."_";?>Features[]" id="<?php echo $row_n[0]['ND_Num']."_Features".$j; ?>" value="<?php echo $ND_Features[$j];?>" ><?php echo $ND_Features[$j];?><br/>
					<?php }?>
                  </td>  
                </tr>
				<tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">護理目標：</td>
                  <td width="705">
					<?php 
					$ND_Goal = explode(",",$row_n[0]['ND_Goal']);
					for($j=0;$j<count($ND_Goal);$j++){?>
					<input type="checkbox" name="<?php echo $row_n[0]['ND_Num']."_";?>Goal[]" id="<?php echo $row_n[0]['ND_Num']."_Goal".$j; ?>" value="<?php echo $ND_Goal[$j];?>" ><?php echo $ND_Goal[$j];?><br/>
					<?php }?>
                  </td>  
                </tr>
				<tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">護理計畫：</td>
                  <td width="705">
					<?php 
					$ND_Plan = explode(",",$row_n[0]['ND_Plan']);
					for($j=0;$j<count($ND_Plan);$j++){?>
					<input type="checkbox" name="<?php echo $row_n[0]['ND_Num']."_";?>Plan[]" id="<?php echo $row_n[0]['ND_Num']."_Plan".$j; ?>" value="<?php echo $ND_Plan[$j];?>" ><?php echo $ND_Plan[$j];?><br/>
					<?php }?>
                  </td>  
                </tr>
				<?php }?>
				<?php }else {?>
				<tr>
                   <td height="10"></td> 
                </tr>
				<tr>
				<td width="110" align="right" class="content">護理診斷：</td>
				<td width="110" align="left" class="content">＃
				<input name="Num"  id="Num" type="text" class="content" size="1" />
				<input name="0_Name"  id="0_Name" type="text" class="content" size="15" /></td>
				</tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">導因：</td>
                  <td width="705">
					<textarea rows="4" cols="50" id="0_Reason" name="0_Reason"></textarea>
                  </td>  
                </tr>
				<tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">鑑定特徵：</td>
                  <td width="705">
					<textarea rows="4" cols="50" id="0_Features" name="0_Features"></textarea>
                  </td>  
                </tr>
				<tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">護理目標：</td>
                  <td width="705">
					<textarea rows="4" cols="50" id="0_Goal" name="0_Goal"></textarea>
                  </td>  
                </tr>
				<tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">護理計畫：</td>
                  <td width="705">
					<textarea rows="4" cols="50" id="0_Plan" name="0_Plan"></textarea>
                  </td>  
                </tr>
				<?php }?>
                <tr>
                  <td>&nbsp;</td>
                  <td>
			           <input name="mybtn" type="submit" class="form_fix" id="mybtn"  value="確定送出"  /> 
 		               <input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>
		          </td>
                </tr>    
             </table>
              </form>
              <!--管理員管理 ending-->              
              </td>
            </tr>
            
          </table>
          </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>
