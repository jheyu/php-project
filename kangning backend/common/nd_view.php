<?php 
include("../public/mem_check.php");
include("../public/web_function.php");
	/*
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}
	*/
	if(is_numeric(quotes($_GET['RS_ID']))){
		 $RS_ID = quotes($_GET['RS_ID']);
	}else{
		 ?>
     <script language="javascript">		
		location.href='../index.php';
	 </script>	
         <?php
	}	
	$sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
	$rs = $objDB->Recordset($sql);
	$row = $objDB->GetRows($rs);	
	
	$ND_ID = $_GET['ND_ID'];
	$sql = "SELECT * FROM diagnosis where ND_ID ='$ND_ID'";
	$rs_f = $objDB->Recordset($sql);
	$row_f = $objDB->GetRows($rs_f);
	$ND_Reason = explode(",",$row_f[0]['ND_Reason']);
	$ND_Features = explode(",",$row_f[0]['ND_Features']);
	$ND_Goal = explode(",",$row_f[0]['ND_Goal']);
	$ND_Plan = explode(",",$row_f[0]['ND_Plan']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.datepicker.min.js"></script>

<script>
$(document).ready(function(){
	$(".date-pick" ).datepicker({ 
		dateFormat: 'yy-mm-dd',
		showOn: "button",
		buttonImage: "../js/calendar.png",
		buttonImageOnly: true,
		
	});	
 	$("#mybtn").click(function(){	
			$("form#form1").submit();
		}		
	)	
})

</script>
<link type="text/css" href="../css/ui-darkness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<!-- header starting point -->
	<?php include("../include/header.php");?>
	<!-- header ending point -->    
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?>
        <!--menu ending point-->          
        </td>            
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top">
        <table width="830" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="30" class="content">專業照護 > 評估量表 &gt; 新增</td>
              </tr>
              <tr>
                  <td height="10"></td>
              </tr>  
              <tr>
                <td height="10">
                <span class="form_title">
                     <input name="search" type="button" class="content" id="search" value="回上一頁" onclick="MM_goToURL('parent','nd_list.php?RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>
                </span></td>
              </tr>   
              <tr>
                  <td height="10"></td>
              </tr>                      
             <tr>
              	<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>   
             </tr>   
             <tr>
                <td height="30"></td>
              </tr>           
              
              <tr>
              		<td style="font-size: 13pt"><strong>護理之家住民健康評估單</strong>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  </td>            
              </tr> 
              <tr>
                <td height="5"></td>
              </tr>
              <tr>
              	  <td ><img src="../images/blueline.jpg" /></td>
              </tr> 
               <tr>
                <td height="5"></td>
              </tr>   
            <tr>         
              <td>    
              <!--管理員管理startinging-->              
                   			  
             <table>
			 
				<tr>
				<td></td>
				<td width="110" align="left" class="content"><strong>＃<?php echo $row_f[0]['ND_Num'],$row_f[0]['ND_Name'];?></strong></td>
				</tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right"  class="content">起始日：</td>
                  <td width="705">
					<?php  echo $row_f[0]['ND_Start']; ?>
                  </td>
             </tr>
                <tr>
                   <td height="10"></td> 
                </tr>
				<tr>
                  <td width="110" align="right" class="content">導因：</td>
                  <td width="705">
					<?php 
					for($j=0;$j<count($ND_Reason);$j++){?>
					<?php echo $ND_Reason[$j];?><br/>
					<?php }?>
                  </td>  
                </tr>
				<tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">鑑定特徵：</td>
                  <td width="705">
					<?php 
					for($j=0;$j<count($ND_Features);$j++){?>
					<?php echo $ND_Features[$j];?><br/>
					<?php }?>
                  </td>  
                </tr>
				<tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">護理目標：</td>
                  <td width="705">
					<?php 
					for($j=0;$j<count($ND_Goal);$j++){?>
					<?php echo $ND_Goal[$j];?><br/>
					<?php }?>
                  </td>  
                </tr>
				<tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">護理計畫：</td>
                  <td width="705">
					<?php 
					for($j=0;$j<count($ND_Plan);$j++){?>
					<?php echo $ND_Plan[$j];?><br/>
					<?php }?>
                  </td>  
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>
		          </td>
                </tr>    
             </table>
              <!--管理員管理 ending-->              
              </td>
            </tr>
            
          </table>
          </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>
