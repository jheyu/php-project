
<?php 
include_once("../public/mem_check.php");
include_once("../public/web_function.php");

if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}	
	
	$RS_ID = $_GET["RS_ID"];
	$rs = $objDB->Recordset("SELECT * FROM resident WHERE RS_ID = '$RS_ID'");	
	$row = $objDB->GetRows($rs);
					 
	$rs_form = $objDB->Recordset("SELECT * FROM record WHERE RS_ID = '$RS_ID' ORDER BY CR_Date DESC");
	$row_form = $objDB->GetRows($rs_form);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title><?php echo $html_title;?>專業照護</title>
<script src="../js/common.js" language="javascript"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.js"></script>
<script type="text/JavaScript">

function changed(theselect) {
	var am_type = $('#type_slt').val();
	window.location.href="admin.php?am_type="+am_type;
}
</script>

<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>

<body onload="MM_preloadImages('../images/logout_r.gif','../images/logout_r.gif')">
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td><!-- header starting point -->
        <?php include("../include/header.php");?>
        <!-- header ending point -->
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?></td>
        <!--menu ending point-->
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top"><table width="830" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="30" class="content">專業照護 &gt; 評估與記錄</td>
          </tr>
          <tr>
            <td height="10" class="content_red_b"><img src="../images/spacer.gif" width="1" height="1" /></td>
          </tr>
          <tr>
            <td valign="top">
               <table width="825" border="0" cellpadding="0" cellspacing="0">
					
                  <tr>
                    <td>
                        <table width="830" height="25" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                  		<td height="10"></td>
						</tr>  
						<tr>
              				<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>   
						</tr>
                        <!--tr>
                        <td><?php include ("../include/searchBar.php");?></td>
                        </tr-->
                        <tr>
            					<td height="10" class="content"></td>
       					  </tr>
						<tr>				
						<td height="30"><input name="addmenu_btn" type="button" class="content" id="addmenu_btn" onclick="MM_goToURL('parent','record_add.php?RS_ID=<?php echo $RS_ID; ?>');return document.MM_returnValue" value="新增護理記錄"/></td>
						</tr>
                          <!--
                          <tr>                          
                          	<td class="content" >住民：                            
                            <select id="type_slt" name="type_slt" onChange="changed(this)">                            		
                             	<option <?php if($am_type==1){?>selected="true"<?php }?> value="1">最高</option>                                  
                                <option <?php if($am_type==2){?>selected="true"<?php }?> value="2">一般</option> 
                             </select>                       
                            </td>
                          </tr>  
                          -->                        
                          <tr>
            					<td height="5" class="content"></td>
       					  </tr>                        
                        
                      </table></td>
                  </tr>
                  <tr>
                    <td>
                      <table width="820" height="55" border="0" cellpadding="1" cellspacing="1" bordercolor="#777777" bgcolor="#555555">
                        <tr>                        	 
							<td width="227" bgcolor="#777777" class="enter_wb" align="center">日期</td>
							<td width="227" bgcolor="#777777" class="enter_wb" align="center">焦點問題</td>
							<td width="227" bgcolor="#777777" class="enter_wb" align="center">護理人員</td>
							<td width="222" bgcolor="#777777" class="enter_wb" align="center">編輯</td>
                        </tr>
                       
                        <?php for($i=0;$i<$objDB->RecordCount($rs_form);$i++){?>
                        <?php if($i%2==0){$bgcolor="#EBEBEB";}else{$bgcolor="#FFFFFF";}?>
                        <tr>                        	 
							<td  height="41" align="center" bgcolor="<?php echo $bgcolor;?>"  class="content" ><?php echo $row_form[$i]["CR_Date"];?></td>
							<td  align="center" bgcolor="<?php echo $bgcolor;?>"  class="content" ><?php echo $row_form[$i]["CR_Name"];?></td>
							<td  align="center" bgcolor="<?php echo $bgcolor;?>"  class="content" ><?php echo $row_form[$i]["CR_NS"];?></td>
							<td  align="center" bgcolor="<?php echo $bgcolor;?>">
							<input name="button2" type="button" class="login" id="button2" onclick="MM_goToURL('parent','record_view.php?CR_ID=<?php echo $row_form[$i]["CR_ID"];?>&RS_ID=<?php echo $row_form[$i]["RS_ID"]; ?>');return document.MM_returnValue" value="檢視" /></td>
                        <!--
                        <input name="button4" type="button" class="login" id="button4" value="刪除" onclick="if(confirm('確定要刪除此筆資料嗎?'))MM_goToURL('parent','admin_process.php?action=del&AM_ID=<?php echo $row[$i]["AM_ID"]; ?>');return document.MM_returnValue"  
						  />-->
                         	 </td>
                        </tr>
                        <?php }?>
                        <?php if($objDB->RecordCount($rs)==0){?>
                        <div align="center" style="margin-top:5px">無資料！</div>
                        <?php }?>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                    </table></td>
                  </tr>
                </table>              </td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td class="copyright">
      <!--footer starting point-->
      <?php include("../include/footer.php");?>
      <!--footer starting point-->
    </td>
  </tr>
</table>
</body>
</html>

