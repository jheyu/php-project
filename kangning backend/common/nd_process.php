<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php 
include_once("../public/mem_check.php");
include_once("../public/web_function.php");
include_once("../public/thumb.class.php");

	$action = $_REQUEST["action"];
	switch ($action) {
		case "new":	

			$Num = $_POST["ND_Num"];
			$ND_Num = explode(",",$Num);
			$RS_ID = quotes(trim($_POST["RS_ID"]));
			$ND_Start = quotes(trim($_POST["ND_Date"]));
			$ND_NS = $_SESSION['KNH_LOGIN_NAME'];
			for($i=0;$i<count($ND_Num);$i++)
			{	
				$ND_ID = $objDB->GetMaxID('ND_ID','diagnosis',3);
				$Name = $ND_Num[$i]."_Name";
				$ND_Name = $_POST["$Name"];
				
				$Reason = $ND_Num[$i]."_Reason";
				$N = $ND_Num[$i];
				$ND_R = $_POST["$Reason"];
				$ND_Reason = implode (',', $ND_R);
				
				$Features = $ND_Num[$i]."_Features";
				$ND_F = $_POST["$Features"];
				$ND_Features = implode (',', $ND_F);
				
				$Goal = $ND_Num[$i]."_Goal";
				$ND_G = $_POST["$Goal"];
				$ND_Goal = implode (',', $ND_G);
				
				$Plan = $ND_Num[$i]."_Plan";
				$ND_P = $_POST["$Plan"];
				$ND_Plan = implode (',', $ND_P);
				
				if($ND_Num[$i] == 0)
				{
					$N = $_POST["Num"];
					$ND_Reason = $_POST["$Reason"];
					$ND_Features = $_POST["$Features"];
					$ND_Goal = $_POST["$Goal"];
					$ND_Plan = $_POST["$Plan"];
					
				}
				$sql = "insert into diagnosis(ND_ID,RS_ID,ND_Start,ND_Reason,ND_Features,ND_Goal,ND_Plan,ND_Num,ND_Name,ND_NS)values('$ND_ID','$RS_ID','$ND_Start','$ND_Reason','$ND_Features','$ND_Goal','$ND_Plan','$N','$ND_Name','$ND_NS')";

				$objDB->Execute($sql);
			}
		?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script language="javascript">
alert('新增資料成功!');
location.href='nd_list.php?RS_ID=<?php echo $RS_ID?>';
</script>
<?php
			break;
	
		case "mdy":
			
			$ND_ID = quotes(trim($_POST["ND_ID"]));
			$RS_ID = quotes(trim($_POST["RS_ID"]));
			$ND_Appraisal = quotes(trim($_POST["ND_Appraisal"]));
			$ND_End = date("Y-m-d");
			$ND_NS = $_SESSION['KNH_LOGIN_NAME'];
			$sql = "update diagnosis set ND_Appraisal='$ND_Appraisal',ND_End = '$ND_End',ND_NS='$ND_NS' where ND_ID='$ND_ID'";  
			//echo $sql;
			//exit;
			$objDB->Execute($sql);
			
			$ED_ID = $objDB->GetMaxID('ED_ID','edit',3);
			$ED_TN = "diagnosis";
			$ED_Editor = $_SESSION['KNH_LOGIN_NAME'];
			$ED_Date=date("Y-m-d H:i:s");
			$ED_PS = $_POST["ED_PS"];
			
			$sql = "insert into edit (ED_ID,ED_TN,ED_TI,ED_Editor,ED_Date,ED_PS) values ('$ED_ID','$ED_TN','$ND_ID','$ED_Editor','$ED_Date','$ED_PS')";
			
			$objDB->Execute($sql);
			
?>			
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script language="javascript">
alert('修改資料成功!');
location.href='nd_list.php?RS_ID=<?php echo $RS_ID?>';
</script>
<?php
			break;
		case "record":
			$ND_ID = $_GET['ND_ID'];
			$sql = "SELECT * FROM diagnosis where ND_ID ='$ND_ID'";
			$rs_f = $objDB->Recordset($sql);
			$row_f = $objDB->GetRows($rs_f);
			
			$CR_ID = $objDB->GetMaxID('CR_ID','record',3);
			$CR_Name = $row_f[0]["ND_Name"];
			$CR_Text = "導因:".$row_f[0]["ND_Reason"]."<br/> 鑑定特徵:".$row_f[0]["ND_Features"]."<br/> 護理目標:".$row_f[0]["ND_Goal"]."<br/> 護理計畫:".$row_f[0]["ND_Plan"];
			$CR_NS = $_SESSION['KNH_LOGIN_NAME'];
			$RS_ID = $row_f[0]["RS_ID"];
			$CR_Date = date("Y-m-d");
			$sql = "insert into record(CR_ID,CR_Name,CR_Text,CR_NS,RS_ID,CR_Date)values('$CR_ID','$CR_Name','$CR_Text','$CR_NS','$RS_ID','$CR_Date')";
			$objDB->Execute($sql);
	
?>
<script language="javascript">
alert('修改資料成功!');
location.href='nd_list.php?RS_ID=<?php echo $RS_ID?>';
</script>
<?php
		break;
	}
?>