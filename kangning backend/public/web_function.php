<?php
//+++++++++++++各功能選項定義++++++++++++++++//

//ini_set('display_errors', 'On');
//error_reporting(E_ALL);

/* ==[ NEWS新聞發布區塊 news_list.php]== */ 
  $READ_NEWSNUM="10" ;  //新聞發布每頁分頁筆數
   
  $NEWS_THUMB_WIDTH_01 = 100; //新聞縮圖寬
  $NEWS_THUMB_HEIGHT_01 = 1000; //新聞縮圖高
  
  $FCK_InnerWidth = 600; //FCKeditor內頁圖片最大寬
  $FCK_InnerHeight = 1000; //FCKeditor內頁圖片最大高

/* ==[產品類別設定cm_list.php]== */ 
  $READ_CMNUM="20" ; //產品類別每頁分頁筆數
  $CM_THUMB_WIDTH_01 = 147; //產品類別縮圖寬
  $CM_THUMB_HEIGHT_01 = 104; //產品類別縮圖高

/* ==[產品列表設定product_list.php]== */ 
  $READ_PDTNUM="40" ; ; //產品列表每頁分頁筆數
  $P_THUMB_WIDTH_01 = 200; //產品維護-最新產品-縮圖寬
  $P_THUMB_HEIGHT_01 = 200; //產品維護-最新產品-縮圖高 

/* ==[會員管理mem_list.php]== */  
  $READ_MEMNUM="10" ;  //會員列表每頁分頁筆數

/* ==[會員管理 - 留言列表msg_list.php]== */  
$READ_MSGNUM="10" ;  //留言列表每頁分頁筆數

/* ==[訂單管理order_list.php]== */  
 $READ_ODNUM = "40";
 
//上傳路徑 "PHOTOS/"
$MAINTAIN_THUMB_WIDTH_01 = 300; //產品圖 大張縮圖寬(產品規格內) -只限定寬
$MAINTAIN_THUMB_HEIGHT_01 = 300;//產品圖 大張縮圖高(產品規格內)- 高不限
 
//+++++++++++++++++++++++++++++++++++++++++//

//防止sql注入
function quotes($content){
	if (!get_magic_quotes_gpc()) {
	  if (is_array($content)) {
		  foreach ($content as $key=>$value) {
			  $content[$key] = addslashes($value);
		  }
	  } else {
		  $content = addslashes($content);
	  }
	}
	return $content;
}

//計算分頁function
// $ALL_PAGE所有分頁數目  $page_num頁數
function PageCount($ALL_PAGE,$page_num){
          for($i=1;$i<=$ALL_PAGE;$i++)  {
           $selected=($i==$page_num)?"selected":"";  
           echo  "<option value=".$_SERVER['PHP_SELF']."?page_num=$i  $selected>第".$i."頁</option>"; 
             }    
}	

// $ALL_PAGE所有分頁數目  $page_num頁數 $Category類別名稱 $ID 參數
function PageCountType($ALL_PAGE,$page_num,$Category,$ID){
          for($i=1;$i<=$ALL_PAGE;$i++)  {
           $selected=($i==$page_num)?"selected":"";  
           echo  "<option value=".$_SERVER['PHP_SELF']."?page_num=$i&$Category=$ID  $selected>第".$i."頁</option>"; 
             }    
}

function ckselect($str1,$str2) {
	if($str1==$str2) {
		return 'selected';
	}
	return '';
}

function ckRadio($str1,$str2) {
	if($str1==$str2) {
		return 'checked="checked"';
	}
	return '';
}

function ckbox($str1,$strarray) {
			if(in_array($str1,$strarray)) {
				return 'checked="checked"';
			}
			return '';
}

function counter($str){
	if(!empty($str)){
	    return (substr_count($str, ",")+1);
	}
	else{
		return 0;
	}
}

function ConvertToArray($option1, $option2, $option3){

	$array = array($option1,$option2,$option3);
	$splitArray = implode(",", $array);

	return $splitArray;
}

function big($a, $b){
	if($a > $b)
	   return $a;
	else
	   return $b;
} 

function Combine($values) {
	$array_Q1 = array('完全沒有','輕微','中等程度','厲害','非常厲害');
    $str_Q1 = '';

    for($i=0;$i<count($array_Q1);$i++)
    {
	    if($values == $i)
	        $str_Q1 = $str_Q1 .'￭'.$array_Q1[$i];
	    else
	        $str_Q1 = $str_Q1 . '□'.$array_Q1[$i];
    }

    return $str_Q1;
}

if(!session_id())
{
    @session_start();
}

?>