<?php 
include("../public/mem_check.php");
include("../public/web_function.php");
	/*
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}
	*/
	if(is_numeric(quotes($_GET['RS_ID']))){
		 $RS_ID = quotes($_GET['RS_ID']);
	}else{
		 ?>
     <script language="javascript">		
		location.href='../index.php';
	 </script>	
         <?php
	}	
	$sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
	$rs = $objDB->Recordset($sql);
	$row = $objDB->GetRows($rs);	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.datepicker.min.js"></script>

<script>
$(document).ready(function(){
    $(".date-pick" ).datepicker({ 
		dateFormat: 'yy-mm-dd', 
		showOn: "button",
		buttonImage: "../js/calendar.png",
		buttonImageOnly: true
	});	
 	$("#mybtn").click(function(){	
		$("form#form1").submit();		
	})	
})

</script>
<link type="text/css" href="../css/ui-darkness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<!-- header starting point -->
	<?php include("../include/header.php");?>
	<!-- header ending point -->    
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?>
        <!--menu ending point-->          
        </td>            
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top">
        <table width="830" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="30" class="content">專業照護 > 護理記錄 &gt; 新增</td>
              </tr>
              <tr>
                  <td height="10"></td>
              </tr>  
              <tr>
                <td height="10">
                <span class="form_title">
                     <input name="search" type="button" class="content" id="search" value="回上一頁" onclick="MM_goToURL('parent','layout.php?t=vital&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>
                </span></td>
              </tr>   
              <tr>
                  <td height="10"></td>
              </tr>                      
             <tr>
              	<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>   
             </tr>   
             <tr>
                <td height="30"></td>
              </tr>           
              
              <tr>
              		<td style="font-size: 13pt"><strong>生命徵象評估紀錄</strong>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  </td>            
              </tr> 
              <tr>
                <td height="5"></td>
              </tr>
              <tr>
              	  <td ><img src="../images/blueline.jpg" /></td>
              </tr> 
               <tr>
                <td height="5"></td>
              </tr>   
            <tr>         
              <td>    
              <!--管理員管理startinging-->              
              <form name="form1" id="form1" method="post" action="vital_process.php" />
			   <input type="hidden" name="action" id="action" value="new"/>              
              <input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>" />             
             <table>
                <tr>
                   <td height="10"></td> 
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">日期：</td>
                  <td width="705">
					<input name="VS_Date"  id="VS_Date" type="text" class="txt date-pick" style="  width:80px;"  value="<?php  echo date("Y-m-d"); ?>"  />
                  </td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">時間：</td>
                  <td width="705">
                    <select name="VS_Time" id="VS_Time">
					<option value="">請選擇</option>
					<?php for($i=1;$i<=24;$i++){?>
					<option value="<?php echo $i?>"><?php echo $i;?></option><?php }?>
					</select>
                  </td>  
                </tr>
				<tr>
					<td width="110" align="right"  class="content">體溫：</td>
					<td width="705">
					<input name="VS_Tem"  id="VS_Tem" type="text" class="content" size="15" />
					</td>
				</tr>
				<tr>
					<td width="110" align="right"  class="content">脈搏：</td>
					<td width="705">
					<input name="VS_HR"  id="VS_HR" type="text" class="content" size="15" />
					</td>
				</tr>
				<tr>
					<td width="110" align="right"  class="content">呼吸：</td>
					<td width="705">
					<input name="VS_RR"  id="VS_RR" type="text" class="content" size="15" />
					</td>
				</tr>
				<tr>
					<td width="110" align="right"  class="content">血壓(收縮壓)：</td>
					<td width="705">
					<input name="VS_SBP"  id="VS_SBP" type="text" class="content" size="15" />
					</td>
				</tr>
				<tr>
					<td width="110" align="right"  class="content">血壓(舒張壓)：</td>
					<td width="705">
					<input name="VS_DBP"  id="VS_DBP" type="text" class="content" size="15" />
					</td>
				</tr>
				<tr>
					<td width="110" align="right"  class="content">疼痛(0~10分)：</td>
					<td width="705">
					<input name="VS_Pain"  id="VS_Pain" type="text" class="content" size="15" />
					</td>
				</tr>
				<tr>
					<td width="110" align="right"  class="content">解便狀態：</td>
					<td width="705">
					<input name="VS_BT"  id="VS_BT" type="text" class="content" size="15" />
					</td>
				</tr>
				<tr>
					<td width="110" align="right"  class="content">解便次數：</td>
					<td width="705">
					<input name="VS_BC"  id="VS_BC" type="text" class="content" size="15" />
					</td>
				</tr>
                <tr>
                  <td width="110" align="right" class="content">護理人員：</td>
                  <td width="705">
					<input name="VS_NS"  id="VS_NS" type="text" class="content" size="15" value=""/>
                  </td>  
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>
			           <input name="mybtn" type="button" class="form_fix" id="mybtn"  value="確定送出"  /> 
 		               <input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>
		          </td>
                </tr>    
             </table>
              </form>
              <!--管理員管理 ending-->              
              </td>
            </tr>
            <tr>
			<td>備註1：使用塞劑代號：D(dulcolax) G(glycerin ball)<br/>
				例如：○○○三天未解便，使用塞劑後解1次正常軟便，符號呈現1/D<br/>
				備註2：有使用止痛劑之住民若服用藥物後無疼痛問題，未必會有分數產生
			</td>
			</tr>
          </table>
          </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>
