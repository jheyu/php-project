<?php 
include_once("../public/mem_check.php");
include_once("../public/web_function.php");

$ALL_COUNT = $objDB->Recordset("SELECT * FROM shower WHERE RS_ID = '$RS_ID' ORDER BY shower_Date DESC");
$ALL_WORDS = $objDB->RecordCount($ALL_COUNT);

//分頁選單用的
$ALL_PAGE=ceil($ALL_WORDS/$READ_MEMNUM); //計算所需頁數

//判斷頁數為數字
if(is_numeric(quotes($_GET['page_num']))){
	 $page_num = quotes($_GET['page_num']);
}else{
     $page_num = 0;
}
?>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.datepicker.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){  	
   
    $("#mybtn").click(function(){		
		$("form#form1").submit();		
	})

});   

</script>
<link type="text/css" href="../css/ui-darkness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
         
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top"><table width="830" border="0" cellpadding="0" cellspacing="0">  
          <tr>
            <td height="10" colspan="2" class="content_red_b"><img src="../images/spacer.gif" width="1" height="1" /></td>
          </tr>
          <tr>
            <td width="192" height="4" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="644" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2" valign="top">
                <table width="825" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td>
					
			<?php
			   if(empty($page_num))$page_num=1;
				$start_num=$READ_MEMNUM*($page_num-1);
			?>
                        <table width="830" height="25" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td colspan="2"><span class="content_red_b">洗澡紀錄</span></td>                         
                            <td width="477" class="content" align="right">前往頁面 　 第<?php echo $page_num;?>頁，共<?php echo $ALL_PAGE; ?> 頁 ( <span class="point_red" style="font-weight:bold"><?php echo $ALL_WORDS; ?></span> <span class="content">筆資料 ) 
                              <?php
						$back=$page_num-'1';
						$next=$page_num+'1';
						if(!($back<=0)){echo "<a href=".$_SERVER['PHP_SELF']."?page_num=$back>上一頁</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ";}
						if($next<=$ALL_PAGE){echo "<a href=".$_SERVER['PHP_SELF']."?page_num=$next>下一頁</a>";}
						?>
                              
                          <select name="select" class="content" onchange="window.location.href=this.options[this.selectedIndex].value;">
                                  <?php  PageCount($ALL_PAGE,$page_num);  ?>
                                </select>                            </td>
                          </tr>
                      </table></td>
                  </tr>
                  <tr>
                    <td>
                      <form name="form1" id="form1" method="post" action="shower_process.php">
                        <input type="hidden" name="action" id="action" value="mdy"/>
                        <input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>" />
                        <input type="hidden" name="shower_ID" id="shower_ID" value="<?php echo $shower_ID;?>" /> 

                        <table width="830" height="55" border="0" cellpadding="1" cellspacing="1" bordercolor="#999999">
                        <tr>
                          <!--<td height="22" align="center" bgcolor="#898989" class="back_w"><a href="#">排序</a></td>-->
                          <td width="86" align="center" bgcolor="#898989" class="back_w"><a href="#">洗澡日期</a></td>
                          <td width="111" align="center" bgcolor="#898989" class="back_w"><a href="#">皮膚是否完整</a></td>
                          <td width="61" align="center" bgcolor="#898989" class="back_w"><a href="#">檢查日期</a></td>
                          <td width="139" align="center" bgcolor="#898989" class="back_w"><a href="#">護理師檢查</a></td>
                          <td width="61" align="center" bgcolor="#898989" class="back_w"><a href="#">功能操作</a></td>
                        </tr>
                        <?php 
						    $rs = $objDB->Recordset("SELECT * FROM shower WHERE RS_ID = '$RS_ID' ORDER BY shower_Date DESC");
						    $row = $objDB->GetRows($rs);	
                            $num = $objDB->RecordCount($rs);
					    ?>
					    <?php
                        //$Status=0表示 
						    for($i=0;$i<$objDB->RecordCount($rs);$i++){					  

							    $RS_ID = $row[$i]['RS_ID'];
							    $shower_ID = $row[$i]['shower_ID'];		
							    $shower_Date = $row[$i]['shower_Date'];
							    $shower_complete  = $row[$i]['shower_complete'];					  
							    $shower_inspection = $row[$i]['shower_inspection'];
                                
							    if($shower_inspection == '1'){
							  	    $status = '已檢查';
							  	    $ADSM_Style = 'style="color:#00F"';
							    }else{
								    $status = '尚未檢查';
								    $ADSM_Style = 'style="color:#FF0000"';
							    }
							  
                                if($i%2==0){$bgcolor="#EBEBEB";}else{$bgcolor="#FFFFFF";}
							?>
                        <tr>
                            <td height="25" align="center" bgcolor="<?php echo $bgcolor;?>" class="content"><?php echo $shower_Date; ?> 
                            </td>
                            <td height="25" align="center" bgcolor="<?php echo $bgcolor;?>" class="content"><?php 
                                if($shower_complete == '1')
                                   echo '是'; 
                                else
                                   echo '否'; ?></td> 
                            </td>
                            <td height="25" align="center" bgcolor="<?php echo $bgcolor;?>" class="content"><?php 
                                if($shower_inspection == '1')
                                    echo $row[$i]['inspectionDate']; 
                                else 
                                    echo '等待檢查'; ?></td>
                
                            <td height="25" align="center" bgcolor="<?php echo $bgcolor;?>" class="content">
                                <input name="button" type="button"  <?php echo $ADSM_Style; ?> id="button2" onclick="MM_goToURL('parent','shower_process.php?action=change&RS_ID=<?php echo $RS_ID;?>&shower_ID=<?php echo $shower_ID;?>');return document.MM_returnValue" value="<?php echo $status; ?>" />&nbsp;
                            </td>
                            <td align="center" bgcolor="<?php echo $bgcolor;?>" class="content" >                     
                                <input name="button2" type="button" class="login" id="button2" onclick="MM_goToURL('parent','shower_md.php?RS_ID=<?php echo $RS_ID;?>&shower_ID=<?php echo $shower_ID; ?>');return document.MM_returnValue" value="修改" /> &nbsp;
                            </td>                   
                        </tr>
                            <?php }?>
                            <?php if($objDB->RecordCount($rs)==0){?>
                            <div align="center" style="margin-top:5px">無資料！</div>
                            <?php }?>
                        </table>
                      </form>
                    </td>
                  </tr>
                  <tr>
                 <td>&nbsp;</td>
                </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td class="copyright">
      <!--footer starting point-->
      <?php include("../include/footer.php");?>
      <!--footer starting point-->
    </td>
  </tr>