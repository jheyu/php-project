<?php 
include("../public/mem_check.php");
include("../public/web_function.php");
	/*
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}
	*/
	if(is_numeric(quotes($_GET['RS_ID']))){
		 $RS_ID = quotes($_GET['RS_ID']);
	}else{
		 ?>
     <script language="javascript">		
		location.href='../index.php';
	 </script>	
         <?php
	}	
	$sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
	$rs = $objDB->Recordset($sql);
	$row = $objDB->GetRows($rs);	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.datepicker.min.js"></script>

<script>
$(document).ready(function(){
    $(".date-pick" ).datepicker({ 
		dateFormat: 'yy-mm-dd', 
		showOn: "button",
		buttonImage: "../js/calendar.png",
		buttonImageOnly: true
	});	
 	$("#mybtn").click(function(){	
		$("form#form1").submit();		
	})	
})

</script>
<link type="text/css" href="../css/ui-darkness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<!-- header starting point -->
	<?php include("../include/header.php");?>
	<!-- header ending point -->    
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?>
        <!--menu ending point-->          
        </td>            
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top">
        <table width="830" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="30" class="content">專業照護 > 護理記錄 &gt; 新增</td>
              </tr>
              <tr>
                  <td height="10"></td>
              </tr>  
              <tr>
                <td height="10">
                <span class="form_title">
                     <input name="search" type="button" class="content" id="search" value="回上一頁" onclick="MM_goToURL('parent','layout.php?t=overview&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>
                </span></td>
              </tr>   
              <tr>
                  <td height="10"></td>
              </tr>                      
             <tr>
              	<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>   
             </tr>   
             <tr>
                <td height="30"></td>
              </tr>           
              
              <tr>
              		<td style="font-size: 13pt"><strong>藥物紀錄</strong>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  </td>            
              </tr> 
              <tr>
                <td height="5"></td>
              </tr>
              <tr>
              	  <td ><img src="../images/blueline.jpg" /></td>
              </tr> 
               <tr>
                <td height="5"></td>
              </tr>   
            <tr>         
              <td>    
              <!--管理員管理startinging-->              
              <form name="form1" id="form1" method="post" action="med_process.php" enctype="multipart/form-data"/>
			   <input type="hidden" name="action" id="action" value="new"/>              
              <input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>" />             
             <table>
                <tr>
                   <td height="10"></td> 
                </tr>
                <!--tr>
                  <td width="110" align="right"  class="content">日期：</td>
                  <td width="705">
					<input name="MD_Date"  id="MD_Date" type="text" class="txt date-pick" style="  width:80px;"  value="<?php  //echo date("Y-m-d"); ?>"  />
                  </td>
                </tr-->
				<tr>
					<td width="110" align="right"  class="content">學名：</td>
					<td width="705">
					<input name="MD_SN"  id="MD_SN" type="text" class="content" size="15" />
					</td>
				</tr>
				<tr>
					<td width="110" align="right"  class="content">天數：</td>
					<td width="705">
					<input name="MD_Day"  id="MD_Day" type="text" class="content" size="15" />
					</td>
				</tr>
				<tr>
                    <td width="106" height="25"  align="right" bgcolor="#FFFFFF" class="content">圖片：</td>
             	    <td colspan="2" bgcolor="#FFFFFF" class="content"><input name="MD_Pic" type="file" id="MD_Pic" size="30" /></td>
               	    </tr>                  
                    <tr>
               	      <td width="106" height="25"  align="right" class="content">&nbsp;</td>
               	      <td colspan="2" class="content">上傳寬600pix、高度400pix <br />
						建議檔案上傳小檔案 50k~1mb 以內 。</td>
           	        </tr>
					<tr>
               	      <td width="106" height="25"  align="right" class="content">&nbsp;</td>
               	      <td colspan="2" class="content"><img src="../images/pic02.gif" alt="" width="193" /></td>
           	        </tr>
				<tr>
					<td width="110" align="right"  class="content">數量：</td>
					<td width="705">
					<input name="MD_NUM"  id="MD_NUM" type="text" class="content" size="15" />
					</td>
				</tr>
				<tr>
					<td width="110" align="right"  class="content">來源：</td>
					<td width="705">
					<input name="MD_Source"  id="MD_Source" type="text" class="content" size="15" />
					</td>
				</tr>
				<tr>
					<td width="110" align="right"  class="content">進食方法：</td>
					<td width="705">
					<input name="MD_Method"  id="MD_Method" type="text" class="content" size="15" />
					</td>
				</tr>
				<tr>
					<td width="110" align="right"  class="content">主要副作用：</td>
					<td width="705">
					<input name="MD_SE"  id="MD_SE" type="text" class="content" size="15" />
					</td>
				</tr>
				<tr>
					<td width="110" align="right"  class="content">用法：</td>
					 <td colspan="2" class="content" name="MD_Text" id="MD_Text" cols="50" rows="5">
					<?php
							   include "../CKEdit/ckeditor/ckeditor.php";
			
								$CKEditor = new CKEditor();
								//$CKEditor->config['width'] = 500;
								$CKEditor->config['height'] = 150;
								$CKEditor->config['toolbar'] = 'MyToolbar';
								
								$CKEditor->basePath = '../CKEdit/ckeditor/';
								$CKEditor->editor("Text");
						?> </td>
               	    </tr>
				</tr>
                <!--tr>
                  <td width="110" align="right" class="content">護理人員：</td>
                  <td width="705">
					<input name="VS_NS"  id="VS_NS" type="text" class="content" size="15" value=""/>
                  </td>  
                </tr-->
                <tr>
                  <td>&nbsp;</td>
                  <td>
			           <input name="mybtn" type="button" class="form_fix" id="mybtn"  value="確定送出"  /> 
 		               <input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>
		          </td>
                </tr>    
             </table>
              </form>
              <!--管理員管理 ending-->              
              </td>
            </tr>
          </table>
          </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>
