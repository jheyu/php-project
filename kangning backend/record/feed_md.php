<?php 
include("../public/mem_check.php");
include("../public/web_function.php");
	/*
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}
	*/
	if(is_numeric(quotes($_GET['RS_ID']))){
		 $RS_ID = quotes($_GET['RS_ID']);
	}else{
		 ?>
     <script language="javascript">		
		//location.href='../index.php';
	 </script>	
         <?php
	}	
	
	$sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
	$rs = $objDB->Recordset($sql);
	$row = $objDB->GetRows($rs);	
	
	$TF_ID=$_GET["TF_ID"];
	$sql="SELECT * FROM feed where TF_ID ='$TF_ID'";
	$rs_f = $objDB->Recordset($sql);
	$row_f = $objDB->GetRows($rs_f)
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.datepicker.min.js"></script>

<script>
$(document).ready(function(){
    $("#water").hide();
	$("#milk").hide();
    $(".date-pick" ).datepicker({ 
		dateFormat: 'yy-mm-dd', 
		showOn: "button",
		buttonImage: "../js/calendar.png",
		buttonImageOnly: true
	});	
	$("#TF_HR").change(function(){
			if($("#TF_HR").val() %4 == 1 && $("#TF_HR").val() != 5)
			{
				$("#water").show();
				$("#milk").hide();
			}
			else if($("#TF_HR").val() %4 == 3 && $("#TF_HR").val() != 3)
			{
				$("#water").hide();
				$("#milk").show();
			}
			else
			{
				$("#water").hide();
				$("#milk").hide();
			}
		});
 	$("#mybtn").click(function(){	
		$("form#form1").submit();		
	})	
})

</script>
<link type="text/css" href="../css/ui-darkness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<!-- header starting point -->
	<?php include("../include/header.php");?>
	<!-- header ending point -->    
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?>
        <!--menu ending point-->          
        </td>            
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top">
        <table width="830" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="30" class="content">專業照護 > 護理記錄 &gt; 新增</td>
              </tr>
              <tr>
                  <td height="10"></td>
              </tr>  
              <tr>
                <td height="10">
                <span class="form_title">
                     <input name="search" type="button" class="content" id="search" value="回上一頁" onclick="MM_goToURL('parent','layout.php?t=feed&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>
                </span></td>
              </tr>   
              <tr>
                  <td height="10"></td>
              </tr>                      
             <tr>
              	<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>   
             </tr>   
             <tr>
                <td height="30"></td>
              </tr>           
              
              <tr>
              		<td style="font-size: 13pt"><strong>管灌個案輸出入量監測紀錄</strong>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  </td>            
              </tr> 
              <tr>
                <td height="5"></td>
              </tr>
              <tr>
              	  <td ><img src="../images/blueline.jpg" /></td>
              </tr> 
               <tr>
                <td height="5"></td>
              </tr>   
            <tr>         
              <td>    
              <!--管理員管理startinging-->              
              <form name="form1" id="form1" method="post" action="feed_process.php" />
			   <input type="hidden" name="action" id="action" value="mdy"/>              
              <input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>" />
			  <input type="hidden" name="TF_ID" id="TF_ID" value="<?php echo $TF_ID;?>" />			  
             <table>
                <tr>
                   <td height="10"></td> 
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">管灌日期：</td>
                  <td width="705">
					<input name="TF_Date"  id="TF_Date" type="text" class="txt date-pick" style="  width:80px;"  value="<?php  echo $row_f[0]["TF_Date"]; ?>"  />
                  </td>
                </tr>
				<tr>
                  <td width="110" align="right"  class="content">建議熱量：</td>
                  <td width="705">
					<input name="TF_Cal"  id="TF_Cal" type="text" class="content" size="10" value="<?php $row_f[0]["TF_Cal"];?>"/>
                  </td>
                </tr>
				<tr>
                  <td width="110" align="right"  class="content">配方種類：</td>
                  <td width="705">
					<input name="TF_Type"  id="TF_Type" type="text" class="content" size="30" value="<?php  echo $row_f[0]["TF_Type"]; ?>"/>
                  </td>
                </tr>
				<tr>
                  <td width="110" align="right"  class="content">配法：</td>
                  <td width="705">
					<input name="TF_Spoon"  id="TF_Spoon" type="text" class="content" size="5" value="<?php  echo $row_f[0]["TF_Spoon"]; ?>"/>平匙加<input name="TF_Water"  id="TF_Water" type="text" class="content" size="5" value="<?php  echo $row_f[0]["TF_Water"] ?>"/>c.c.溫水
                  </td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">時間：</td>
                  <td width="705">
                    <select name="TF_HR" id="TF_HR">
					<option value="">請選擇</option>
					<?php for($i=1;$i<=24;$i++){?>
					<option value="<?php echo $i?>" <?php if($row_f[0]["TF_HR"]== $i){echo 'selected="selected" ';}?>><?php echo $i;?></option><?php }?>
					</select>
					<font id="water"> 水</font>
					<font id="milk"> 奶+水</font>
                  </td>  
                </tr>
				<tr>
					<td width="110" align="right"  class="content">輸入：</td>
					<td width="705">
					<input name="TF_In"  id="TF_In" type="text" class="content" size="15" value="<?php  echo $row_f[0]["TF_In"]; ?>"/>c.c.
					</td>
				</tr>
				<tr>
					<td width="110" align="right"  class="content">輸出：</td>
					<td width="705">
					<input name="TF_Out"  id="TF_Out" type="text" class="content" size="15" value="<?php  echo $row_f[0]["TF_Out"]; ?>"/>c.c.
					</td>
				</tr>
                <tr>
                  <td width="110" align="right" class="content">護理人員：</td>
                  <td width="705">
					<input name="TF_NS"  id="TF_NS" type="text" class="content" size="15" value="<?php  echo $row_f[0]["TF_NS"]; ?>"/>
                  </td>  
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>
			           <input name="mybtn" type="button" class="form_fix" id="mybtn"  value="確定送出"  /> 
 		               <input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>
		          </td>
                </tr>    
             </table>
              </form>
              <!--管理員管理 ending-->              
              </td>
            </tr>
            
          </table>
          </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>
