<script type="text/javascript" src="../js/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.datepicker.min.js"></script>
<link type="text/css" href="../css/ui-darkness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<script>
$(document).ready(function(){
    
    $(".date-pick" ).datepicker({ 
		dateFormat: 'yy-mm-dd', 
		showOn: "button",
		buttonImage: "../js/calendar.png",
		buttonImageOnly: true
	});	

 	//$("#mybtn").click(function(){	
		//$("form#form1").submit();		
	//})	
})

</script>
<form name="form" id="form" method="post" action="layout.php?RS_ID=<?php echo $_GET["RS_ID"];?>&t=ioresult" style="border:none;float:left" onsubmit="">
      選擇其他日期:&nbsp;
      <input name="Date"  id="Date" type="text" class="txt date-pick" style="width:80px;"  value=""  />
      <input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $_GET["RS_ID"];?>">
      <input name="button6" type="submit"  id="button6" value="go" class="content"  />
</form>
<?php 

	$RS_ID = $_GET["RS_ID"];
	$Today = date("Y-m-d");
	
	$rs_form = $objDB->Recordset("SELECT * FROM io WHERE IO_Date = '$Today' AND RS_ID = '$RS_ID' ORDER BY IO_Hr ASC");
	
	$row_form = $objDB->GetRows($rs_form);
	if($objDB->RecordCount($rs_form) > 0)
	{	
	
	//$FA_ID = $row_form[0]['FA_ID'];
?>
<table>
	<tr align="center">
	 <h2 align="left"><strong>攝入及排出紀錄</strong></h2>
	 <h3 align="center"><strong><?php echo $row_form[0]["IO_Date"];?></strong></h3>
	</tr>
	<tr>
		<td width="227" bgcolor="#777777" class="enter_wb" align="center">時間</td>
        <td width="227" bgcolor="#777777" class="enter_wb" align="center">輸入</td>
		<td width="179" bgcolor="#777777" class="enter_wb" align="center">輸出</td>
		<td width="179" bgcolor="#777777" class="enter_wb" align="center">護理人員</td>
        <td width="222" bgcolor="#777777" class="enter_wb" align="center">編輯</td>
   </tr>
	<?php
		$DIn = $DOut = $NIn = $NOut = 0;
		for($i=0;$i<$objDB->RecordCount($rs_form);$i++)
			{
				if($j%2==0)
					$bgcolor="#EBEBEB";
				else
					$bgcolor="#FFFFFF";
				if($row_form[$i]["IO_Hr"] > 8 && $row_form[$i]["IO_Hr"] < 20)
				{	
					$DIn += $row_form[$i]["IO_In"];
					$DOut += $row_form[$i]["IO_Out"];
				}
				else
				{
					$NIn += $row_form[$i]["IO_In"];
					$NOut += $row_form[$i]["IO_Out"];
				}
				?>
	<tr>
		<td height="41" align="center" bgcolor="<?php echo $bgcolor;?>"  class="content" style="color:<?php echo $color;?>"><?php echo $row_form[$i]["IO_Hr"];?></td>
		<td height="41" align="center" bgcolor="<?php echo $bgcolor;?>"  class="content" style="color:<?php echo $color;?>"><?php echo $row_form[$i]["IO_In"];?></td>
		<td height="41" align="center" bgcolor="<?php echo $bgcolor;?>"  class="content" style="color:<?php echo $color;?>"><?php echo $row_form[$i]["IO_Out"];?></td>
		<td height="41" align="center" bgcolor="<?php echo $bgcolor;?>"  class="content" style="color:<?php echo $color;?>"><?php echo $row_form[$i]["IO_NS"];?></td>
		<td width="123" align="center" bgcolor="<?php echo $bgcolor;?>"><a href="io_md.php?IO_ID=<?php echo $row_form[$i]["IO_ID"]; ?>&RS_ID=<?php echo $row_form[$i]["RS_ID"];?>" class="overed">編輯</a></td>
    </tr>		
	<?php } ?>
	<tr height="20">
	</tr>
	<tr>
		<td height="15" align="center" bgcolor="#FFFFFF"  class="content" style="color:<?php echo $color;?>">白班小計(8~20)</td>
		<td height="15" align="center" bgcolor="#FFFFFF"  class="content" style="color:<?php echo $color;?>"><?php echo $DIn;?></td>
		<td height="15" align="center" bgcolor="#FFFFFF"  class="content" style="color:<?php echo $color;?>"><?php echo $DOut;?></td>
		<td height="15" align="center" bgcolor="#FFFFFF"  class="content" style="color:<?php echo $color;?>"></td>
		<td width="123" align="center" bgcolor="#FFFFFF"></td>
    </tr>
	<tr>
		<td height="15" align="center" bgcolor="#FFFFFF"  class="content" style="color:<?php echo $color;?>">夜班小計(21~7)</td>
		<td height="15" align="center" bgcolor="#FFFFFF"  class="content" style="color:<?php echo $color;?>"><?php echo $NIn;?></td>
		<td height="15" align="center" bgcolor="#FFFFFF"  class="content" style="color:<?php echo $color;?>"><?php echo $NOut;?></td>
		<td height="15" align="center" bgcolor="#FFFFFF"  class="content" style="color:<?php echo $color;?>"></td>
		<td width="123" align="center" bgcolor="#FFFFFF"></td>
    </tr>
	<tr>
		<td height="15" align="center" bgcolor="#FFFFFF"  class="content" style="color:<?php echo $color;?>">小計</td>
		<td height="15" align="center" bgcolor="#FFFFFF"  class="content" style="color:<?php echo $color;?>"><?php echo $NIn+$DIn;?></td>
		<td height="15" align="center" bgcolor="#FFFFFF"  class="content" style="color:<?php echo $color;?>"><?php echo $NOut+$DOut;?></td>
		<td height="15" align="center" bgcolor="#FFFFFF"  class="content" style="color:<?php echo $color;?>"></td>
		<td width="123" align="center" bgcolor="#FFFFFF"></td>
    </tr>
</table>
<?php
	}
	else
	{
		echo "<br/>今日還沒有記錄，請先新增記錄。";
	}
?>