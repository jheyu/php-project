<?php 
include("../public/mem_check.php");
include("../public/web_function.php");
	/*
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}
	*/
	if(is_numeric(quotes($_GET['RS_ID']))){
		 $RS_ID = quotes($_GET['RS_ID']);
	}else{
		 ?>
     <script language="javascript">		
		location.href='../index.php';
	 </script>	
         <?php
	}	
	$sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
	$rs = $objDB->Recordset($sql);
	$row = $objDB->GetRows($rs);	
	
	$EL_ID = quotes($_REQUEST['EL_ID']);
	$sql = "select * from daylife where EL_ID='$EL_ID'";
	$rs=$objDB->Recordset($sql);
	$row_f = $objDB->GetRows($rs);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.datepicker.min.js"></script>

<script>
$(document).ready(function(){
    $(".date-pick" ).datepicker({ 
		dateFormat: 'yy-mm-dd', 
		showOn: "button",
		buttonImage: "../js/calendar.png",
		buttonImageOnly: true
	});	
 	$("#mybtn").click(function(){	
		$("form#form1").submit();		
	})	
})

</script>
<link type="text/css" href="../css/ui-darkness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<!-- header starting point -->
	<?php include("../include/header.php");?>
	<!-- header ending point -->    
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?>
        <!--menu ending point-->          
        </td>            
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top">
        <table width="830" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="30" class="content">專業照護 > 護理記錄 &gt; 新增</td>
              </tr>
              <tr>
                  <td height="10"></td>
              </tr>  
              <tr>
                <td height="10">
                <span class="form_title">
                     <input name="search" type="button" class="content" id="search" value="回上一頁" onclick="MM_goToURL('parent','layout.php?t=overview&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>
                </span></td>
              </tr>   
              <tr>
                  <td height="10"></td>
              </tr>                      
             <tr>
              	<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>   
             </tr>   
             <tr>
                <td height="30"></td>
              </tr>           
              
              <tr>
              		<td style="font-size: 13pt"><strong>個案日常生活照顧服務紀錄</strong>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  </td>            
              </tr> 
              <tr>
                <td height="5"></td>
              </tr>
              <tr>
              	  <td ><img src="../images/blueline.jpg" /></td>
              </tr> 
               <tr>
                <td height="5"></td>
              </tr>   
            <tr>         
              <td>    
              <!--管理員管理startinging-->              
              <form name="form1" id="form1" method="post" action="daylife_process.php" />
				<input type="hidden" name="action" id="action" value="mdy"/>              
				<input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>" />  
				<input type="hidden" name="EL_ID" id="EL_ID" value="<?php echo $EL_ID;?>" />  
             <table>
                <tr>
                   <td height="10"></td> 
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">日期：</td>
                  <td width="705">
					<input name="EL_Date"  id="EL_Date" type="text" class="txt date-pick" style="  width:80px;"  value="<?php  echo $row_f[0]["EL_Date"]; ?>"  />
                  </td>
                </tr>
				<tr>
                   <td height="10"></td> 
                </tr>
				<tr>
					<td width="110" align="right"  class="content">班別：</td>
					<td width="705">
					<select name="EL_WH" id="EL_WH">
					<option value="" >請選擇</option>
					<option value="白班" <?php if($row_f[0]["EL_WH"]=='白班'){echo 'selected="selected" ';}?>>白班(8~20)</option>
					<option value="夜班" <?php if($row_f[0]["EL_WH"]=='夜班'){echo 'selected="selected" ';}?>>夜班(21~7)</option>
					</select>
					</td>
				</tr>
				<tr>
                   <td height="10"></td> 
                </tr>
				<tr>
					<td width="110" align="right"  class="content">大便性質：</td>
					<td width="705">
					<input name="EL_PT"  id="EL_PT" type="text" class="content" size="15" value="<?php echo $row_f[0]["EL_PT"];?>"/>
					</td>
				</tr>
				<tr>
                   <td height="10"></td> 
                </tr>
				<tr>
					<td width="110" align="right"  class="content">大便次數：</td>
					<td width="705">
					<select name="EL_PC" id="EL_PC">
					<option value="" >請選擇</option>
					<?php for($i=0;$i<=10;$i++){?>
					<option value="<?php echo $i;?>" <?php if($row_f[0]["EL_PC"]==$i){echo 'selected="selected" ';}?> ><?php echo $i; ?></option><?php }?>
					</select>次
					</td>
				</tr>
				<tr>
                   <td height="10"></td> 
                </tr>
                <tr>
                  <td width="110" align="right" class="content">護理人員：</td>
                  <td width="705">
					<input name="EL_NS"  id="EL_NS" type="text" class="content" size="15" value="<?php echo $row_f[0]["EL_NS"];?>"/>
                  </td>  
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>
			           <input name="mybtn" type="button" class="form_fix" id="mybtn"  value="確定送出"  /> 
 		               <input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>
		          </td>
                </tr>    
             </table>
              </form>
              <!--管理員管理 ending-->              
              </td>
            </tr>
            
          </table>
          </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>
