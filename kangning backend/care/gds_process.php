<?php
include_once '../public/web_function.php';
include_once '../public/mem_check.php';
	
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}	
	
	$action = $_REQUEST["action"];
	
	switch ($action) {

		case "new":	

			$GDS_ID = $objDB->GetMaxID('GDS_ID','GDS',3);
			$RS_ID = $_POST["RS_ID"];	
			$GDS_Score = $_POST["GDS_Score"];	
			$GDS_Satisfaction = $_POST["GDS_Satisfaction"];
            $GDS_Tired = $_POST["GDS_Tired"];
            $GDS_Inability = $_POST["GDS_Inability"];
            $GDS_Dare = $_POST["GDS_Dare"];
            $GDS_Value = $_POST["GDS_Value"];
            $GDS_Activity = $_POST["GDS_Activity"];
            $GDS_Emptiness = $_POST["GDS_Emptiness"];
            $GDS_Spirit = $_POST["GDS_Spirit"];
            $GDS_Unfortunately = $_POST["GDS_Unfortunately"];
            $GDS_Happy = $_POST["GDS_Happy"];
            $GDS_Remember = $_POST["GDS_Remember"];
            $GDS_Live = $_POST["GDS_Live"];
            $GDS_Energy = $_POST["GDS_Energy"];
            $GDS_Hope = $_POST["GDS_Hope"];
            $GDS_Bless = $_POST["GDS_Bless"];
				
			date_default_timezone_set('Asia/Taipei');	
			$GDS_Date = date("Y-m-d H:i:s");
		
			$sql = "insert into gds (GDS_ID,RS_ID,GDS_Score,GDS_Satisfaction,GDS_Tired,GDS_Inability,GDS_Dare,GDS_Value,GDS_Activity,GDS_Emptiness,GDS_Spirit,GDS_Unfortunately,GDS_Happy,GDS_Remember,GDS_Live,GDS_Energy,GDS_Hope,GDS_Bless,GDS_Date) values ('$GDS_ID','$RS_ID','$GDS_Score','$GDS_Satisfaction','$GDS_Tired','$GDS_Inability','$GDS_Dare','$GDS_Value','$GDS_Activity','$GDS_Emptiness','$GDS_Spirit','$GDS_Unfortunately','$GDS_Happy','$GDS_Remember','$GDS_Live','$GDS_Energy','$GDS_Hope','$GDS_Bless','$GDS_Date')";
			
			$objDB->Execute($sql);
		
	
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script language="javascript">
alert('新增成功!');
location.href='layout.php?t=overview&RS_ID=<?php echo $RS_ID;?>';
</script>
<?php
		break;
		
		case "mdy":
		
			$RS_ID = $_POST["RS_ID"];				
			$GDS_ID = $_POST["GDS_ID"];
			$rs = $objDB->Recordset("SELECT * FROM gds WHERE RS_ID = '$RS_ID' AND GDS_ID = '$GDS_ID'");
			$row = $objDB->GetRows($rs);		
					
			$GDS_Score = $_POST["GDS_Score"];	
			$GDS_Satisfaction = $_POST["GDS_Satisfaction"];
            $GDS_Tired = $_POST["GDS_Tired"];
            $GDS_Inability = $_POST["GDS_Inability"];
            $GDS_Dare = $_POST["GDS_Dare"];
            $GDS_Value = $_POST["GDS_Value"];
            $GDS_Activity = $_POST["GDS_Activity"];
            $GDS_Emptiness = $_POST["GDS_Emptiness"];
            $GDS_Spirit = $_POST["GDS_Spirit"];
            $GDS_Unfortunately = $_POST["GDS_Unfortunately"];
            $GDS_Happy = $_POST["GDS_Happy"];
            $GDS_Remember = $_POST["GDS_Remember"];
            $GDS_Live = $_POST["GDS_Live"];
            $GDS_Energy = $_POST["GDS_Energy"];
            $GDS_Hope = $_POST["GDS_Hope"];
            $GDS_Bless = $_POST["GDS_Bless"];
			
			date_default_timezone_set('Asia/Taipei');
			$GDS_Date = date("Y-m-d H:i:s");

			$sql = "insert into gds (GDS_ID,RS_ID,GDS_Score,GDS_Satisfaction,GDS_Tired,GDS_Inability,GDS_Dare,GDS_Value,GDS_Activity,GDS_Emptiness,GDS_Spirit,GDS_Unfortunately,GDS_Happy,GDS_Remember,GDS_Live,GDS_Energy,GDS_Hope,GDS_Bless,GDS_Date) values ('$GDS_ID','$RS_ID','$GDS_Score','$GDS_Satisfaction','$GDS_Tired','$GDS_Inability','$GDS_Dare','$GDS_Value','$GDS_Activity','$GDS_Emptiness','$GDS_Spirit','$GDS_Unfortunately','$GDS_Happy','$GDS_Remember','$GDS_Live','$GDS_Energy','$GDS_Hope','$GDS_Bless','$GDS_Date')";
			
			$objDB->Execute($sql);
		
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />        
<script language="javascript">
alert('修改成功!');
location.href='layout.php?t=overview&RS_ID=<?php echo $RS_ID;?>';
</script>	
	<?php
		break;
	
		case "del":

				$GDS_ID = quotes(trim($_REQUEST["GDS_ID"]));
			
				$sql = "delete from gds where GDS_ID ='$GDS_ID'";
				$objDB->Execute($sql);
	?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script language="javascript">
	alert('資料刪除完畢!');
	location.href="admin.php";
	</script>
	<?php
		break;
			
	}	
	
?>
