<?php 
include("../public/mem_check.php");
include("../public/web_function.php");
	/*
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}
	*/
	if(is_numeric(quotes($_GET['RS_ID']))){
		 $RS_ID = quotes($_GET['RS_ID']);
	}else{
		 ?>
     <script language="javascript">		
		//location.href='../index.php';
	 </script>	
         <?php
	}	
	
	$sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
	$rs = $objDB->Recordset($sql);
	$row = $objDB->GetRows($rs);
    
    $rs_form = $objDB->Recordset("SELECT * FROM bsrs WHERE RS_ID = '$RS_ID' ORDER BY BS_Date DESC");
	$row_form = $objDB->GetRows($rs_form);
	
	$BS_ID = $row_form[0]['BS_ID'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.datepicker.min.js"></script>
<script>
var score0 = <?php echo $row_form[0]['BS_Sleep'];?>;
var score1 = <?php echo $row_form[0]['BS_NV']?>;
var score2 = <?php echo $row_form[0]['BS_AG']?>;
var score3 = <?php echo $row_form[0]['BS_GM']?>;
var score4 = <?php echo $row_form[0]['BS_CM']?>;
var score5 = <?php echo $row_form[0]['BS_SU']?>;
var total = 0;

var level ;

var caculate = function(){
	  
     total = score0 + score1 + score2 + score3 + score4 + score5;
	 
	 if(total>=15){
	 	level = "(重度情緒困擾)";
	 }else if(total >= 10 && total <=14){
	 	level = "(中度情緒困擾)";
	 }else if(total >=6 && total <= 9){
	 	level = "(輕度情緒困擾)";
	 }else{
	 	level = "(身心適應狀況良好)";
	 }
	 	$("#SCORE").text(total);
		$("#LEVEL").text(level); 
 }
$(document).ready(function(){
	$("input[name='BS_Sleep']").click(function(){
		var BS_Sleep = $("input[name='BS_Sleep']:checked").val();
		score0 = parseInt(BS_Sleep);
		caculate();		
	})
	$("input[name='BS_NV']").click(function(){
		var BS_NV = $("input[name='BS_NV']:checked").val();
		score1 = parseInt(BS_NV);		
		caculate();		
	})
	$("input[name='BS_AG']").click(function(){
		var BS_AG = $("input[name='BS_AG']:checked").val();
		score2 = parseInt(BS_AG);		
		caculate();		
	})	
	$("input[name='BS_GM']").click(function(){
		var BS_GM = $("input[name='BS_GM']:checked").val();
		score3 = parseInt(BS_GM);
		caculate();		
	})
	$("input[name='BS_CM']").click(function(){
		var BS_CM = $("input[name='BS_CM']:checked").val();
		score4 = parseInt(BS_CM);		
		caculate();		
	})
	$("input[name='BS_SU']").click(function(){
		var BS_SU = $("input[name='BS_SU']:checked").val();
		score5 = parseInt(BS_SU);		
		caculate();		
	})	
    $(".date-pick" ).datepicker({ 
		dateFormat: 'yy-mm-dd', 
		showOn: "button",
		buttonImage: "../js/calendar.png",
		buttonImageOnly: true
	});	
 	$("#mybtn").click(function(){	
			$("form#form1").submit();
				
	})	
})

</script>
<link type="text/css" href="../css/ui-darkness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<!-- header starting point -->
	<?php include("../include/header.php");?>
	<!-- header ending point -->    
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?>
        <!--menu ending point-->          
        </td>            
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top">
        <table width="830" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="30" class="content">專業照護 > 護理記錄 &gt; 修改</td>
              </tr>
              <tr>
                  <td height="10"></td>
              </tr>  
              <tr>
                <td height="10">
                <span class="form_title">
                     <input name="search" type="button" class="content" id="search" value="回上一頁" onclick="MM_goToURL('parent','layout.php?t=bsrs&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>
                </span></td>
              </tr>   
              <tr>
                  <td height="10"></td>
              </tr>                      
             <tr>
              	<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>   
             </tr>   
             <tr>
                <td height="30"></td>
              </tr>           
    <tr>
	<td style="font-size: 13pt"><strong>簡易自殺意念評估量表</strong>
	<?php echo "(".$row_form[0]['BS_Date'] .")";?>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	分數:<label id="SCORE"><?php echo $row_form[0]['BS_Total'];?></label><label id="LEVEL">
	<?php if($row_form[0]['BS_Total'] <= 5){
		echo "(身心適應狀況良好)";
	}else if($row_form[0]['BS_Total'] > 5 && $row_form[0]['BS_Total'] <=9){
		echo "(輕度情緒困擾)";
	}else if($row_form[0]['BS_Total'] >9 && $row_form[0]['BS_Total'] <=14){
		echo "(中度情緒困擾)";
	}else{
		echo "(重度情緒困擾)";
	}
	?></label></td>                
</tr> 
         <form name="form1" id="form1" method="post" action="bsrs_process.php" />
		<input type="hidden" name="action" id="action" value="mdy"/>              
        <input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>" />
		<input type="hidden" name="BS_ID" id="BS_ID" value="<?php echo $BS_ID;?>" />		
             <table>
                <tr>
                   <td height="10"></td> 
                </tr>
				<tr>
                  <td width="110" align="right"  class="content">日期：</td>
                  <td width="705">
					<input name="BS_Date"  id="BS_Date" type="text" class="txt date-pick" style="  width:80px;"  value="<?php  echo $row_form[0]['BS_Date']; ?>"  />
				  </td>
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="300" align="right" class="content">睡眠困難，譬如難以入睡、易醒或早醒：</td>
                  <td width="705">
					<input name="BS_Sleep" type="radio" class="form_fix" id="BS_Sleep1" value="0" <?php echo ckRadio('0',$row_form[0]['BS_Sleep']);?>/>完全沒有
					<input name="BS_Sleep" type="radio" class="form_fix" id="BS_Sleep2" value="1" <?php echo ckRadio('1',$row_form[0]['BS_Sleep']);?>/>輕微
					<input name="BS_Sleep" type="radio" class="form_fix" id="BS_Sleep3" value="2" <?php echo ckRadio('2',$row_form[0]['BS_Sleep']);?>/>中等程度
					<input name="BS_Sleep" type="radio" class="form_fix" id="BS_Sleep4" value="3" <?php echo ckRadio('3',$row_form[0]['BS_Sleep']);?>/>厲害
					<input name="BS_Sleep" type="radio" class="form_fix" id="BS_Sleep5" value="4" <?php echo ckRadio('4',$row_form[0]['BS_Sleep']);?>/>非常厲害
                  </td>  
                </tr>
				<tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">感覺緊張不安：</td>
                  <td width="705">
					<input name="BS_NV" type="radio" class="form_fix" id="BS_NV1" value="0" <?php echo ckRadio('0',$row_form[0]['BS_NV']);?>/>完全沒有
					<input name="BS_NV" type="radio" class="form_fix" id="BS_NV2" value="1" <?php echo ckRadio('1',$row_form[0]['BS_NV']);?>/>輕微
					<input name="BS_NV" type="radio" class="form_fix" id="BS_NV3" value="2" <?php echo ckRadio('2',$row_form[0]['BS_NV']);?>/>中等程度
					<input name="BS_NV" type="radio" class="form_fix" id="BS_NV4" value="3" <?php echo ckRadio('3',$row_form[0]['BS_NV']);?>/>厲害
					<input name="BS_NV" type="radio" class="form_fix" id="BS_NV5" value="4" <?php echo ckRadio('4',$row_form[0]['BS_NV']);?>/>非常厲害
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">覺得容易苦惱或動怒：</td>
                  <td width="705">
					<input name="BS_AG" type="radio" class="form_fix" id="BS_AG1" value="0" <?php echo ckRadio('0',$row_form[0]['BS_AG']);?>/>完全沒有
					<input name="BS_AG" type="radio" class="form_fix" id="BS_AG2" value="1" <?php echo ckRadio('1',$row_form[0]['BS_AG']);?>/>輕微
					<input name="BS_AG" type="radio" class="form_fix" id="BS_AG3" value="2" <?php echo ckRadio('2',$row_form[0]['BS_AG']);?>/>中等程度
					<input name="BS_AG" type="radio" class="form_fix" id="BS_AG4" value="3" <?php echo ckRadio('3',$row_form[0]['BS_AG']);?>/>厲害
					<input name="BS_AG" type="radio" class="form_fix" id="BS_AG5" value="4" <?php echo ckRadio('4',$row_form[0]['BS_AG']);?>/>非常厲害
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">感覺憂鬱、心情低落：</td>
                  <td width="705">
					<input name="BS_GM" type="radio" class="form_fix" id="BS_GM1" value="0" <?php echo ckRadio('0',$row_form[0]['BS_GM']);?>/>完全沒有
					<input name="BS_GM" type="radio" class="form_fix" id="BS_GM2" value="1" <?php echo ckRadio('1',$row_form[0]['BS_GM']);?>/>輕微
					<input name="BS_GM" type="radio" class="form_fix" id="BS_GM3" value="2" <?php echo ckRadio('2',$row_form[0]['BS_GM']);?>/>中等程度
					<input name="BS_GM" type="radio" class="form_fix" id="BS_GM4" value="3" <?php echo ckRadio('3',$row_form[0]['BS_GM']);?>/>厲害
					<input name="BS_GM" type="radio" class="form_fix" id="BS_GM5" value="4" <?php echo ckRadio('4',$row_form[0]['BS_GM']);?>/>非常厲害
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">覺得比不上別人：</td>
                  <td width="705">
					<input name="BS_CM" type="radio" class="form_fix" id="BS_CM1" value="0" <?php echo ckRadio('0',$row_form[0]['BS_CM']);?>/>完全沒有
					<input name="BS_CM" type="radio" class="form_fix" id="BS_CM2" value="1" <?php echo ckRadio('1',$row_form[0]['BS_CM']);?>/>輕微
					<input name="BS_CM" type="radio" class="form_fix" id="BS_CM3" value="2" <?php echo ckRadio('2',$row_form[0]['BS_CM']);?>/>中等程度
					<input name="BS_CM" type="radio" class="form_fix" id="BS_CM4" value="3" <?php echo ckRadio('3',$row_form[0]['BS_CM']);?>/>厲害
					<input name="BS_CM" type="radio" class="form_fix" id="BS_CM5" value="4" <?php echo ckRadio('4',$row_form[0]['BS_CM']);?>/>非常厲害
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">有自殺的想法：</td>
                  <td width="705">
					<input name="BS_SU" type="radio" class="form_fix" id="BS_SU1" value="0" <?php echo ckRadio('0',$row_form[0]['BS_SU']);?>/>完全沒有
					<input name="BS_SU" type="radio" class="form_fix" id="BS_SU2" value="1" <?php echo ckRadio('1',$row_form[0]['BS_SU']);?>/>輕微
					<input name="BS_SU" type="radio" class="form_fix" id="BS_SU3" value="2" <?php echo ckRadio('2',$row_form[0]['BS_SU']);?>/>中等程度
					<input name="BS_SU" type="radio" class="form_fix" id="BS_SU4" value="3" <?php echo ckRadio('3',$row_form[0]['BS_SU']);?>/>厲害
					<input name="BS_SU" type="radio" class="form_fix" id="BS_SU5" value="4" <?php echo ckRadio('4',$row_form[0]['BS_SU']);?>/>非常厲害
                  </td>  
                </tr>
				<tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="110" align="right" class="content">護理人員：</td>
                  <td width="705">
					<input name="BS_NS"  id="BS_NS" type="text" class="content" size="15" value="<?php echo $row_form[0]['BS_NS'];?>"/>
                  </td>  
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>
			           <input name="mybtn" type="submit" class="form_fix" id="mybtn"  value="確定送出"  /> 
 		               <input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>
		          </td>
                </tr>    
	</table>
	</form>              
	</td>
</tr>
</table>
          </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>
