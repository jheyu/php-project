<?php 

$rs_form = $objDB->Recordset("SELECT * FROM body WHERE RS_ID = '$RS_ID' ORDER BY body_Date DESC");
$row_form = $objDB->GetRows($rs_form);
	
$body_ID = $row_form[0]['body_ID'];
	
?>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script type="text/javascript">

$(document).ready(function(){
    $("#mybtn").click(function(){
	
		$("form#form1").submit();
		
	});
});

</script>

<tr>
	<td height="15"></td>
</tr>           
<tr>
	<td style="font-size: 13pt"><strong>身體評估紀錄</strong>
	<?php echo "(".$row_form[0]['body_Date'] .")";?>
	<!--
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	分數:<label id="SCORE"><?php echo $row_form[0]['ADL_Score'];?></label><label id="LEVEL">
	<?php if($row_form[0]['ADL_Score'] == 100){
		echo "(獨立)";
	}else if($row_form[0]['ADL_Score'] >=91 && $row_form[0]['ADL_Score'] <=99){
		echo "(輕度依賴)";
	}else if($row_form[0]['ADL_Score'] >=61 && $row_form[0]['ADL_Score'] <=90){
		echo "(中度依賴)";
	}else if($row_form[0]['ADL_Score'] >=21 && $row_form[0]['ADL_Score'] <=60){
		echo "(嚴重依賴)";
	}else{
		echo "(完全依賴)";
	}
	?></label>--></td>            
</tr> 
<tr>
	<td height="5"></td>
</tr>
<tr>
	<td ><img src="../images/blueline.jpg" /></td>
</tr> 
	<tr>
	<td height="5"></td>
</tr>             
<tr>         
	<td>   
	<form name="form1" id="form1" method="post" action="body_process.php"/>
	<input type="hidden" name="action" id="action" value="mdy"/>                           
	<input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>"  />
    <input type="hidden" name="body_ID" id="body_ID" value="<?php echo $body_ID;?>" />          
	<table>                
	<tr>
		<td height="10"></td>
	</tr>                
	<tr>
		<td width="110" align="right"  class="content">意識狀態：</td>
		<td width="705">
		<input name="body_awareness" type="radio" class="form_fix" id="body_awareness1" value="1" <?php echo ckRadio('1',$row_form[0]['body_awareness']);?> />清醒
		<input name="body_awareness" type="radio" class="form_fix" id="body_awareness2" value="2" <?php echo ckRadio('2',$row_form[0]['body_awareness']);?> />躁動
		<input name="body_awareness" type="radio" class="form_fix" id="body_awareness3" value="3" <?php echo ckRadio('3',$row_form[0]['body_awareness']);?> />混亂	
		<input name="body_awareness" type="radio" class="form_fix" id="body_awareness4" value="4" <?php echo ckRadio('4',$row_form[0]['body_awareness']);?> />木僵
		<input name="body_awareness" type="radio" class="form_fix" id="body_awareness5" value="5" <?php echo ckRadio('5',$row_form[0]['body_awareness']);?> />嗜睡
		<input name="body_awareness" type="radio" class="form_fix" id="body_awareness6" value="6" <?php echo ckRadio('6',$row_form[0]['body_awareness']);?> />昏迷
		<input name="body_awareness" type="radio" class="form_fix" id="body_awareness7" value="7" <?php echo ckRadio('7',$row_form[0]['body_awareness']);?> />無法評估	
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">認知功能：</td>
		<td width="705">
		<input name="body_cognitive" type="radio" class="form_fix" id="body_cognitive1" value="1" <?php echo ckRadio('1',$row_form[0]['body_cognitive']);?> />正常
		<input name="body_cognitive" type="radio" class="form_fix" id="body_cognitive2" value="2" <?php echo ckRadio('2',$row_form[0]['body_cognitive']);?> />失定向感	
		<input name="body_cognitive" type="radio" class="form_fix" id="body_cognitive3" value="3" <?php echo ckRadio('3',$row_form[0]['body_cognitive']);?> />失定向感(時)
		<input name="body_cognitive" type="radio" class="form_fix" id="body_cognitive4" value="4" <?php echo ckRadio('4',$row_form[0]['body_cognitive']);?> />失定向感(地)
		<input name="body_cognitive" type="radio" class="form_fix" id="body_cognitive5" value="5" <?php echo ckRadio('5',$row_form[0]['body_cognitive']);?> />失智
		<input name="body_cognitive" type="radio" class="form_fix" id="body_cognitive6" value="6" <?php echo ckRadio('6',$row_form[0]['body_cognitive']);?> />無法評估	
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">聽力：</td>
		<td width="705">
		<input name="body_hearing" type="radio" class="form_fix" id="body_hearing1" value="1" <?php echo ckRadio('1',$row_form[0]['body_hearing']);?> />正常
		<input name="body_hearing" type="radio" class="form_fix" id="body_hearing2" value="2" <?php echo ckRadio('2',$row_form[0]['body_hearing']);?> />重聽
		<input name="body_hearing" type="radio" class="form_fix" id="body_hearing3" value="3" <?php echo ckRadio('3',$row_form[0]['body_hearing']);?> />失聰
		<input name="body_hearing" type="radio" class="form_fix" id="body_hearing4" value="4" <?php echo ckRadio('4',$row_form[0]['body_hearing']);?> />無法判斷	
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">視力：</td>
		<td width="705">
		<input name="body_vision" type="radio" class="form_fix" id="body_vision1" value="1" <?php echo ckRadio('1',$row_form[0]['body_vision']);?> />正常
		<input name="body_vision" type="radio" class="form_fix" id="body_vision2" value="2" <?php echo ckRadio('2',$row_form[0]['body_vision']);?> />失明
		<input name="body_vision" type="radio" class="form_fix" id="body_vision3" value="3" <?php echo ckRadio('3',$row_form[0]['body_vision']);?> />模糊
		<input name="body_vision" type="radio" class="form_fix" id="body_vision4" value="4" <?php echo ckRadio('4',$row_form[0]['body_vision']);?> />無法判斷       
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">表達：</td>
		<td width="705">
		<input name="body_expression" type="radio" class="form_fix" id="body_expression1" value="1" <?php echo ckRadio('1',$row_form[0]['body_expression']);?> />正常
		<input name="body_expression" type="radio" class="form_fix" id="body_expression2" value="2" <?php echo ckRadio('2',$row_form[0]['body_expression']);?> />失語	
		<input name="body_expression" type="radio" class="form_fix" id="body_expression3" value="3" <?php echo ckRadio('3',$row_form[0]['body_expression']);?> />言語不清
		<input name="body_expression" type="radio" class="form_fix" id="body_expression4" value="4" <?php echo ckRadio('4',$row_form[0]['body_expression']);?> />肢體語言
		<input name="body_expression" type="radio" class="form_fix" id="body_expression5" value="5" <?php echo ckRadio('5',$row_form[0]['body_expression']);?> />筆談
		<input name="body_expression" type="radio" class="form_fix" id="body_expression6" value="6" <?php echo ckRadio('6',$row_form[0]['body_expression']);?> />無法判斷
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">牙齒：</td>
		<td width="705">
		<input name="body_tooth" type="radio" class="form_fix" id="body_tooth1" value="1" <?php echo ckRadio('1',$row_form[0]['body_tooth']);?> />正常
		<input name="body_tooth" type="radio" class="form_fix" id="body_tooth2" value="2" <?php echo ckRadio('2',$row_form[0]['body_tooth']);?> />缺牙	
		<input name="body_tooth" type="radio" class="form_fix" id="body_tooth3" value="3" <?php echo ckRadio('3',$row_form[0]['body_tooth']);?> />固定
		<input name="body_tooth" type="radio" class="form_fix" id="body_tooth4" value="4" <?php echo ckRadio('4',$row_form[0]['body_tooth']);?> />鬆動
		<input name="body_tooth" type="radio" class="form_fix" id="body_tooth5" value="5" <?php echo ckRadio('5',$row_form[0]['body_tooth']);?> />其他
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">清潔(牙齒)：</td>
		<td width="705">		
        <input name="body_clean" type="radio" class="form_fix" id="body_clean1" value="1" <?php echo ckRadio('1',$row_form[0]['body_clean']);?> />良好
		<input name="body_clean" type="radio" class="form_fix" id="body_clean2" value="2" <?php echo ckRadio('2',$row_form[0]['body_clean']);?> />普通	
		<input name="body_clean" type="radio" class="form_fix" id="body_clean3" value="3" <?php echo ckRadio('3',$row_form[0]['body_clean']);?> />差
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">黏膜、牙齦：</td>
		<td width="705">		
        <input name="body_gums" type="radio" class="form_fix" id="body_gums1" value="1" <?php echo ckRadio('1',$row_form[0]['body_gums']);?> />正常、完整
		<input name="body_gums" type="radio" class="form_fix" id="body_gums2" value="2" <?php echo ckRadio('2',$row_form[0]['body_gums']);?> />紅腫、發炎	
		<input name="body_gums" type="radio" class="form_fix" id="body_gums3" value="3" <?php echo ckRadio('3',$row_form[0]['body_gums']);?> />破損
		<input name="body_gums" type="radio" class="form_fix" id="body_gums4" value="4" <?php echo ckRadio('4',$row_form[0]['body_gums']);?> />其他
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">疼痛：</td>
		<td width="705">		
        <input name="body_pain" type="radio" class="form_fix" id="body_pain1" value="1" <?php echo ckRadio('1',$row_form[0]['body_pain']);?> />無疼痛
		<input name="body_pain" type="radio" class="form_fix" id="body_pain2" value="2" <?php echo ckRadio('2',$row_form[0]['body_pain']);?> />表達疼痛或不適	
		<input name="body_pain" type="radio" class="form_fix" id="body_pain3" value="3" <?php echo ckRadio('3',$row_form[0]['body_pain']);?> />其他(無法判斷)
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">肌力：</td>
		<td width="705">		
        <input name="body_muscle" type="radio" class="form_fix" id="body_muscle1" value="1" <?php echo ckRadio('1',$row_form[0]['body_muscle']);?> />左上
		<input name="body_muscle" type="radio" class="form_fix" id="body_muscle2" value="2" <?php echo ckRadio('2',$row_form[0]['body_muscle']);?> />右上
		<input name="body_muscle" type="radio" class="form_fix" id="body_muscle3" value="3" <?php echo ckRadio('3',$row_form[0]['body_muscle']);?> />左下
		<input name="body_muscle" type="radio" class="form_fix" id="body_muscle4" value="4" <?php echo ckRadio('4',$row_form[0]['body_muscle']);?> />右下
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
    <tr>
		<td width="110" align="right"  class="content">關節活動：</td>
		<td width="705">		
        <input name="body_joint" type="radio" class="form_fix" id="body_joint1" value="1" <?php echo ckRadio('1',$row_form[0]['body_joint']);?> />正常
		<input name="body_joint" type="radio" class="form_fix" id="body_joint2" value="2" <?php echo ckRadio('2',$row_form[0]['body_joint']);?> />攀縮
		<input name="body_joint" type="radio" class="form_fix" id="body_joint3" value="3" <?php echo ckRadio('3',$row_form[0]['body_joint']);?> />截肢
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">步態：</td>
		<td width="705">		
        <input name="body_gait" type="radio" class="form_fix" id="body_gait1" value="1" <?php echo ckRadio('1',$row_form[0]['body_gait']);?> />正常
		<input name="body_gait" type="radio" class="form_fix" id="body_gait2" value="2" <?php echo ckRadio('2',$row_form[0]['body_gait']);?> />緩慢
		<input name="body_gait" type="radio" class="form_fix" id="body_gait3" value="3" <?php echo ckRadio('3',$row_form[0]['body_gait']);?> />碎步
		<input name="body_gait" type="radio" class="form_fix" id="body_gait4" value="4" <?php echo ckRadio('4',$row_form[0]['body_gait']);?> />步履不穩
		<input name="body_gait" type="radio" class="form_fix" id="body_gait5" value="5" <?php echo ckRadio('5',$row_form[0]['body_gait']);?> />不自主運動
		<input name="body_gait" type="radio" class="form_fix" id="body_gait6" value="6" <?php echo ckRadio('6',$row_form[0]['body_gait']);?> />完全臥床
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">輔具：</td>
		<td width="705">		
        <input name="body_aids" type="radio" class="form_fix" id="body_aids1" value="1" <?php echo ckRadio('1',$row_form[0]['body_aids']);?> />四腳助行器
		<input name="body_aids" type="radio" class="form_fix" id="body_aids2" value="2" <?php echo ckRadio('2',$row_form[0]['body_aids']);?> />手杖
		<input name="body_aids" type="radio" class="form_fix" id="body_aids3" value="3" <?php echo ckRadio('3',$row_form[0]['body_aids']);?> />義肢
		<input name="body_aids" type="radio" class="form_fix" id="body_aids4" value="4" <?php echo ckRadio('4',$row_form[0]['body_aids']);?> />輪椅
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">睡眠型態：</td>
		<td width="705">		
        <input name="body_sleeping" type="radio" class="form_fix" id="body_muscle1" value="1" <?php echo ckRadio('1',$row_form[0]['body_sleeping']);?> />正常
		<input name="body_sleeping" type="radio" class="form_fix" id="body_muscle2" value="2" <?php echo ckRadio('2',$row_form[0]['body_sleeping']);?> />日夜顛倒
		<input name="body_sleeping" type="radio" class="form_fix" id="body_muscle3" value="3" <?php echo ckRadio('3',$row_form[0]['body_sleeping']);?> />睡眠紊亂
		<input name="body_sleeping" type="radio" class="form_fix" id="body_muscle4" value="4" <?php echo ckRadio('4',$row_form[0]['body_sleeping']);?> />造成家人困擾
		</td>
	</tr>  
	<tr>
		<td height="10"></td>
	</tr>    
	<tr>
		<td width="110" align="right"  class="content">身體活動：</td>
		<td width="705">		
        <input name="body_activity" type="radio" class="form_fix" id="body_muscle1" value="1" <?php echo ckRadio('1',$row_form[0]['body_activity']);?> />每日有固定離床時間
		<input name="body_activity" type="radio" class="form_fix" id="body_muscle2" value="2" <?php echo ckRadio('2',$row_form[0]['body_activity']);?> />久坐型態
		<input name="body_activity" type="radio" class="form_fix" id="body_muscle3" value="3" <?php echo ckRadio('3',$row_form[0]['body_activity']);?> />久臥型態
		</td>
	</tr>  
	<tr>
		<td height="10"></td>
	</tr>   
	<tr>
		<td width="110" align="right"  class="content">皮膚外觀：</td>
		<td width="705">		
        <input name="body_skin" type="radio" class="form_fix" id="body_muscle1" value="1" <?php echo ckRadio('1',$row_form[0]['body_skin']);?> />正常
		<input name="body_skin" type="radio" class="form_fix" id="body_muscle2" value="2" <?php echo ckRadio('2',$row_form[0]['body_skin']);?> />鬆弛
		<input name="body_skin" type="radio" class="form_fix" id="body_muscle3" value="3" <?php echo ckRadio('3',$row_form[0]['body_skin']);?> />乾燥
		<input name="body_skin" type="radio" class="form_fix" id="body_muscle4" value="4" <?php echo ckRadio('4',$row_form[0]['body_skin']);?> />搔癢
		<input name="body_skin" type="radio" class="form_fix" id="body_muscle5" value="5" <?php echo ckRadio('5',$row_form[0]['body_skin']);?> />水腫
		<input name="body_skin" type="radio" class="form_fix" id="body_muscle6" value="6" <?php echo ckRadio('6',$row_form[0]['body_skin']);?> />瘀血
		<input name="body_skin" type="radio" class="form_fix" id="body_muscle7" value="7" <?php echo ckRadio('7',$row_form[0]['body_skin']);?> />疹子
		<input name="body_skin" type="radio" class="form_fix" id="body_muscle8" value="8" <?php echo ckRadio('8',$row_form[0]['body_skin']);?> />腫塊
		<input name="body_skin" type="radio" class="form_fix" id="body_muscle9" value="9" <?php echo ckRadio('9',$row_form[0]['body_skin']);?> />水泡
		</td>
	</tr> 
	<tr>
		<td height="10"></td>
	</tr>    
	<tr>
		<td width="110" align="right"  class="content">褥瘡：</td>
		<td width="705">		
        <input name="body_decubitus" type="radio" class="form_fix" id="body_muscle1" value="1" <?php echo ckRadio('1',$row_form[0]['body_decubitus']);?> />無
		<input name="body_decubitus" type="radio" class="form_fix" id="body_muscle2" value="2" <?php echo ckRadio('2',$row_form[0]['body_decubitus']);?> />有
		</td>
	</tr>             
	<tr>
		<td align="right" class="content">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
		<input name="mybtn" type="button" class="form_fix" id="mybtn"  value="確定送出"  /> 
 		<input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>
		</td>
	</tr>                
	</table>
	</form>              
	</td>
</tr>
