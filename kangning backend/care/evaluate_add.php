<?php 
include("../public/mem_check.php");
include("../public/web_function.php");
	/*
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}
	*/
	if(is_numeric(quotes($_GET['RS_ID']))){
		 $RS_ID = quotes($_GET['RS_ID']);
	}else{
		 ?>
     <script language="javascript">		
		location.href='../index.php';
	 </script>	
         <?php
	}	
	$sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
	$rs = $objDB->Recordset($sql);
	$row = $objDB->GetRows($rs);	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.datepicker.min.js"></script>

<script>
$(document).ready(function(){

	$("#EV_ED1").hide();
	$("#EV_ED2").hide();
	$("#EV_HD1").hide();
	$("#EV_HD2").hide();
	$("#EV_TED1").hide();
	$("#EV_CD1").hide();
	$("#DIO").hide();
	$("#BUO").hide();
	$("#AidO").hide();
	$("#GAO").hide();
	$("#SPill").hide();
	
	$("input[name='EV_VI']").click(function() {
		 if($("#EV_VI4").prop("checked"))
		 {
			$("#EV_ED1").show();
		 }
		 else
		 {
			$("#EV_ED1").hide();
		 }
		 if($("#EV_VI5").prop("checked"))
		 {
			$("#EV_ED2").show();
		 }
		 else
		 {
			$("#EV_ED2").hide();
		 }
	});
	
	$("input[name='EV_HE']").click(function() {
		 if($("#EV_HE3").prop("checked"))
		 {
			$("#EV_HD1").show();
		 }
		 else
		 {
			$("#EV_HD1").hide();
		 }
		 if($("#EV_HE4").prop("checked"))
		 {
			$("#EV_HD2").show();
		 }
		 else
		 {
			$("#EV_HD2").hide();
		 }
	});
	$("input[name='EV_Teeth']").click(function() {
		 if($("#EV_Teeth3").prop("checked"))
		 {
			$("#EV_TED1").show();
		 }
		 else
		 {
			$("#EV_TED1").hide();
		 }
	});
	$("input[name='EV_CH']").click(function() {
		 if($("#EV_CH5").prop("checked"))
		 {
			$("#EV_CD1").show();
		 }
		 else
		 {
			$("#EV_CD1").hide();
		 }
	});
	$("input[name='EV_DI']").click(function() {
		 if($("#EV_DI3").prop("checked"))
		 {
			$("#DIO").show();
		 }
		 else
		 {
			$("#DIO").hide();
		 }
	});
	$("input[name='EV_BU']").click(function() {
		 if($("#EV_BU6").prop("checked"))
		 {
			$("#BUO").show();
		 }
		 else
		 {
			$("#BUO").hide();
		 }
	});
	$("input[name='EV_Aid']").click(function() {
		 if($("#EV_Aid5").prop("checked"))
		 {
			$("#AidO").show();
		 }
		 else
		 {
			$("#AidO").hide();
		 }
	});
	$("input[name='EV_GA']").click(function() {
		 if($("#EV_GA3").prop("checked"))
		 {
			$("#GAO").show();
		 }
		 else
		 {
			$("#GAO").hide();
		 }
	});
	$("input[name='EV_SPill']").click(function() {
		 if($("#EV_SPill2").prop("checked"))
		 {
			$("#SPill").show();
		 }
		 else
		 {
			$("#SPill").hide();
		 }
	});
	
	$(".date-pick" ).datepicker({ 
		dateFormat: 'yy-mm-dd',
		showOn: "button",
		buttonImage: "../js/calendar.png",
		buttonImageOnly: true,
		
	});	
 	$("#mybtn").click(function(){	
			$("form#form1").submit();
		}		
	)	
})

</script>
<link type="text/css" href="../css/ui-darkness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<!-- header starting point -->
	<?php include("../include/header.php");?>
	<!-- header ending point -->    
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?>
        <!--menu ending point-->          
        </td>            
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top">
        <table width="830" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="30" class="content">專業照護 > 評估量表 &gt; 新增</td>
              </tr>
              <tr>
                  <td height="10"></td>
              </tr>  
              <tr>
                <td height="10">
                <span class="form_title">
                     <input name="search" type="button" class="content" id="search" value="回上一頁" onclick="MM_goToURL('parent','layout.php?t=evaluate&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>
                </span></td>
              </tr>   
              <tr>
                  <td height="10"></td>
              </tr>                      
             <tr>
              	<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>   
             </tr>   
             <tr>
                <td height="30"></td>
              </tr>           
              
              <tr>
              		<td style="font-size: 13pt"><strong>健康評估表</strong>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  </td>            
              </tr> 
              <tr>
                <td height="5"></td>
              </tr>
              <tr>
              	  <td ><img src="../images/blueline.jpg" /></td>
              </tr> 
               <tr>
                <td height="5"></td>
              </tr>   
            <tr>         
              <td>    
              <!--管理員管理startinging-->              
              <form name="form1" id="form1" method="post" action="evaluate_process.php" />
			   <input type="hidden" name="action" id="action" value="new"/>              
              <input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>" />             
             <table>
                <tr>
                   <td height="10"></td> 
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">日期：</td>
                  <td width="705">
					<input name="EV_Date"  id="EV_Date" type="text" class="txt date-pick" style="width:80px;"  value="<?php  echo date("Y-m-d"); ?>"  />
                  </td>
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<!--tr>
                  <td width="200" align="right" class="content">GCS：</td>
                  <td width="705">
					E<select>
					<?php for($i=4;$i>=1;$i--){?>
					<option id="Eye" name="Eye" value="<?php echo $i?>"><?php echo $i;?></option><?php }?>
					</select> 
					M
					<select>
					<?php for($i=6;$i>=1;$i--){?>
					<option id="Verbal" name="Verbal" value="<?php echo $i?>"><?php echo $i;?></option><?php }?>
					</select> 
					V
					<select>
					<?php for($i=5;$i>=1;$i--){?>
					<option id="Motor" name="Motor" value="<?php echo $i?>"><?php echo $i;?></option><?php }?>
					</select> 
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr-->
				<tr>
                  <td width="110" align="right" class="content">意識：</td>
                  <td width="705">
					<input name="EV_AW" type="radio" class="form_fix" id="EV_AW1" value="清醒" />清醒
					<input name="EV_AW" type="radio" class="form_fix" id="EV_AW2" value="混淆"/>混淆
					<input name="EV_AW" type="radio" class="form_fix" id="EV_AW3" value="嗜睡"/>嗜睡
					<input name="EV_AW" type="radio" class="form_fix" id="EV_AW4" value="呆滯"/>呆滯
					<input name="EV_AW" type="radio" class="form_fix" id="EV_AW5" value="昏迷"/>昏迷
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">呼吸：</td>
                  <td width="705">
					<input name="EV_BR" type="radio" class="form_fix" id="EV_BR1" value="正常" />正常
					<input name="EV_BR" type="radio" class="form_fix" id="EV_BR2" value="端坐"/>端坐
					<input name="EV_BR" type="radio" class="form_fix" id="EV_BR3" value="張口"/>張口
					<input name="EV_BR" type="radio" class="form_fix" id="EV_BR4" value="氧氣"/>氧氣
					<input name="EV_BR" type="radio" class="form_fix" id="EV_BR5" value="氣切"/>氣切
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">自咳能力：</td>
                  <td width="705">
					<input name="EV_CU" type="radio" class="form_fix" id="EV_CU1" value="自咳" />自咳
					<input name="EV_CU" type="radio" class="form_fix" id="EV_CU2" value="抽痰"/>抽痰
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">痰液性狀：</td>
                  <td width="705">
					<input name="EV_SP" type="radio" class="form_fix" id="EV_SP1" value="黏" />黏
					<input name="EV_SP" type="radio" class="form_fix" id="EV_SP2" value="稠"/>稠
					<input name="EV_SP" type="radio" class="form_fix" id="EV_SP3" value="稀"/>稀
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">痰液顏色：</td>
                  <td width="705">
					<input name="EV_SPC" type="radio" class="form_fix" id="EV_SPC1" value="白" />白
					<input name="EV_SPC" type="radio" class="form_fix" id="EV_SPC2" value="黃綠"/>黃綠
					<input name="EV_SPC" type="radio" class="form_fix" id="EV_SPC3" value="血絲"/>血絲
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">腸胃蠕動：</td>
                  <td width="705">
					<input name="EV_GM" type="radio" class="form_fix" id="EV_GM1" value="正常" />正常
					<input name="EV_GM" type="radio" class="form_fix" id="EV_GM2" value="快"/>快
					<input name="EV_GM" type="radio" class="form_fix" id="EV_GM3" value="慢"/>慢
					<input name="EV_GM" type="radio" class="form_fix" id="EV_GM4" value="無"/>無
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">腸胃外觀：</td>
                  <td width="705">
					<input name="EV_GA" type="radio" class="form_fix" id="EV_GA1" value="正常" />正常
					<input name="EV_GA" type="radio" class="form_fix" id="EV_GA2" value="脹"/>脹
					<input name="EV_GA" type="radio" class="form_fix" id="EV_GA3" value="其他"/>其他
					<font id="GAO">
					<input name="EV_GAO" type="text" class="content" size="10" value="">
					</font>
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">進食：</td>
                  <td width="705">
					<input name="EV_SE" type="radio" class="form_fix" id="EV_SE1" value="進食" />進食
					<input name="EV_SE" type="radio" class="form_fix" id="EV_SE2" value="鼻胃管"/>鼻胃管
					<input name="EV_SE" type="radio" class="form_fix" id="EV_SE3" value="胃造廔"/>胃造廔
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">消化：</td>
                  <td width="705">
					<input name="EV_DI" type="radio" class="form_fix" id="EV_DI1" value="正常" />正常
					<input name="EV_DI" type="radio" class="form_fix" id="EV_DI2" value="差"/>差
					<input name="EV_DI" type="radio" class="form_fix" id="EV_DI3" value="其他"/>其他
					<font id="DIO">
					<input name="EV_DIO" type="text" class="content" size="10" value="">
					</font>
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">食欲：</td>
                  <td width="705">
					<input name="EV_AP" type="radio" class="form_fix" id="EV_AP1" value="佳" />佳
					<input name="EV_AP" type="radio" class="form_fix" id="EV_AP2" value="普通"/>普通
					<input name="EV_AP" type="radio" class="form_fix" id="EV_AP3" value="差"/>差
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">視力：</td>
                  <td width="705">
					<input name="EV_VI" type="radio" class="form_fix" id="EV_VI1" value="正常" />正常
					<input name="EV_VI" type="radio" class="form_fix" id="EV_VI2" value="老花"/>老花
					<input name="EV_VI" type="radio" class="form_fix" id="EV_VI3" value="失明"/>失明
					<input name="EV_VI" type="radio" class="form_fix" id="EV_VI4" value="白內障"/>白內障
					<font id="EV_ED1">
					(<input name="EV_VID" type="radio" class="form_fix" id="EV_VID1" value="左"/>左
					<input name="EV_VID" type="radio" class="form_fix" id="EV_VID2" value="右"/>右
					<input name="EV_VID" type="radio" class="form_fix" id="EV_VID3" value="雙眼"/>雙眼)
					</font>
					<input name="EV_VI" type="radio" class="form_fix" id="EV_VI5" value="青光眼"/>青光眼
					<font id="EV_ED2">
					(<input name="EV_VID" type="radio" class="form_fix" id="EV_VID4" value="左"/>左
					 <input name="EV_VID" type="radio" class="form_fix" id="EV_VID5" value="右"/>右
					 <input name="EV_VID" type="radio" class="form_fix" id="EV_VID6" value="雙眼"/>雙眼)
					</font>
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">聽力：</td>
                  <td width="705">
					<input name="EV_HE" type="radio" class="form_fix" id="EV_HE1" value="全聾" />全聾
					<input name="EV_HE" type="radio" class="form_fix" id="EV_HE2" value="正常"/>正常
					<input name="EV_HE" type="radio" class="form_fix" id="EV_HE3" value="重聽"/>重聽
					<font id="EV_HD1">
					(<input name="EV_HED" type="radio" class="form_fix" id="EV_HED1" value="左"/>左
					<input name="EV_HED" type="radio" class="form_fix" id="EV_HED2" value="右"/>右
					<input name="EV_HED" type="radio" class="form_fix" id="EV_HED3" value="雙耳"/>雙耳)
					</font>
					<input name="EV_HE" type="radio" class="form_fix" id="EV_HE4" value="助聽器"/>助聽器
					<font id="EV_HD2">
					(<input name="EV_HED" type="radio" class="form_fix" id="EV_HED4" value="左"/>左
					<input name="EV_HED" type="radio" class="form_fix" id="EV_HED5" value="右"/>右
					<input name="EV_HED" type="radio" class="form_fix" id="EV_HED6" value="雙耳"/>雙耳)
					</font>
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">牙齒：</td>
                  <td width="705">
					<input name="EV_Teeth" type="radio" class="form_fix" id="EV_Teeth1" value="正常" />正常
					<input name="EV_Teeth" type="radio" class="form_fix" id="EV_Teeth2" value="固定假牙"/>固定假牙
					<input name="EV_Teeth" type="radio" class="form_fix" id="EV_Teeth3" value="活動假牙"/>活動假牙
					<font id="EV_TED1">
					(<input name="EV_TD" type="radio" class="form_fix" id="EV_TD1" value="上"/>上
					<input name="EV_TD" type="radio" class="form_fix" id="EV_TD2" value="下"/>下
					<input name="EV_TD" type="radio" class="form_fix" id="EV_TD3" value="皆有"/>皆有)
					</font>
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">排便：</td>
                  <td width="705">
					<input name="EV_BU" type="radio" class="form_fix" id="EV_BU1" value="正常" />正常
					<input name="EV_BU" type="radio" class="form_fix" id="EV_BU2" value="失禁"/>失禁
					<input name="EV_BU" type="radio" class="form_fix" id="EV_BU3" value="便秘"/>便秘
					<input name="EV_BU" type="radio" class="form_fix" id="EV_BU4" value="腹瀉"/>腹瀉
					<input name="EV_BU" type="radio" class="form_fix" id="EV_BU5" value="人工肛門"/>人工肛門
					<input name="EV_BU" type="radio" class="form_fix" id="EV_BU6" value="其他"/>其他
					<font id="BUO">
					<input name="EV_BUO" type="text" class="content" size="10" value="">
					</font>
                  </td>  
                </tr>
				<tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">排便輔助：</td>
                  <td width="705">
					<input name="EV_Aid" type="radio" class="form_fix" id="EV_Aid6" value="無" />無
					<input name="EV_Aid" type="radio" class="form_fix" id="EV_Aid1" value="軟便劑" />軟便劑
					<input name="EV_Aid" type="radio" class="form_fix" id="EV_Aid2" value="塞劑"/>塞劑
					<input name="EV_Aid" type="radio" class="form_fix" id="EV_Aid3" value="灌腸"/>灌腸
					<input name="EV_Aid" type="radio" class="form_fix" id="EV_Aid4" value="手挖"/>手挖
					<input name="EV_Aid" type="radio" class="form_fix" id="EV_Aid5" value="其他"/>其他
					<font id="AidO">
					<input name="EV_AidO" type="text" class="content" size="10" value="">
					</font>
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">排尿：</td>
                  <td width="705">
					<input name="EV_UR" type="radio" class="form_fix" id="EV_UR1" value="正常" />正常
					<input name="EV_UR" type="radio" class="form_fix" id="EV_UR2" value="失禁"/>失禁
					<input name="EV_UR" type="radio" class="form_fix" id="EV_UR3" value="頻尿"/>頻尿
					<input name="EV_UR" type="radio" class="form_fix" id="EV_UR4" value="尿管"/>尿管
					<input name="EV_UR" type="radio" class="form_fix" id="EV_UR5" value="造口"/>造口
					<input name="EV_UR" type="radio" class="form_fix" id="EV_UR6" value="尿布"/>尿布
					<input name="EV_UR" type="radio" class="form_fix" id="EV_UR7" value="尿套"/>尿套
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">排尿性狀：</td>
                  <td width="705">
					<input name="EV_CH" type="radio" class="form_fix" id="EV_CH1" value="清澈" />清澈
					<input name="EV_CH" type="radio" class="form_fix" id="EV_CH2" value="混濁"/>混濁
					<input name="EV_CH" type="radio" class="form_fix" id="EV_CH3" value="紫尿"/>紫尿
					<input name="EV_CH" type="radio" class="form_fix" id="EV_CH4" value="血尿"/>血尿
					<input name="EV_CH" type="radio" class="form_fix" id="EV_CH5" value="沉澱物"/>沉澱物
					<font id="EV_CD1">
					(<input name="EV_CHD" type="radio" class="form_fix" id="EV_CHD1" value="少"/>少
					<input name="EV_CHD" type="radio" class="form_fix" id="EV_CHD2" value="中"/>中
					<input name="EV_CHD" type="radio" class="form_fix" id="EV_CHD3" value="多"/>多)
					</font>
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">皮膚外觀：</td>
                  <td width="705">
					<input name="EV_Skin" type="radio" class="form_fix" id="EV_Skin1" value="正常" />正常
					<input name="EV_Skin" type="radio" class="form_fix" id="EV_Skin2" value="乾燥"/>乾燥
					<input name="EV_Skin" type="radio" class="form_fix" id="EV_Skin3" value="蒼白"/>蒼白
					<input name="EV_Skin" type="radio" class="form_fix" id="EV_Skin4" value="黃疸"/>黃疸
					<input name="EV_Skin" type="radio" class="form_fix" id="EV_Skin5" value="紅疹"/>紅疹
					<!--input name="EV_Skin" type="radio" class="form_fix" id="EV_Skin6" value="壓瘡/傷口"/>壓瘡/傷口；部位：       ；長    ×寬    ×高     公分，水腫□無□有，部位：         級數：<-->
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">睡眠：</td>
                  <td width="705">
					<input name="EV_Sleep" type="radio" class="form_fix" id="EV_Sleep1" value="正常" />正常
					<input name="EV_Sleep" type="radio" class="form_fix" id="EV_Sleep2" value="混亂"/>混亂
					<input name="EV_Sleep" type="radio" class="form_fix" id="EV_Sleep3" value="日夜顛倒"/>日夜顛倒
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">睡眠：</td>
                  <td width="705">
					<select name="EV_SleepHr" id="EV_SleepHr">
					<option value = "" >請選擇</option>
					<?php for($i=1;$i<=12;$i++){?>
					<option value="<?php echo $i; ?>"><?php echo $i;?></option>
					<?php }?>
					</select>
					小時/天
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">使用安眠藥：</td>
                  <td width="705">
					<input name="EV_SPill" type="radio" class="form_fix" id="EV_SPill1" value="無" />無
					<input name="EV_SPill" type="radio" class="form_fix" id="EV_SPill2" value="有" />有
					<font id="SPill">
					藥名:<input name="EV_PN" type="text" class="content" size="10" value="">
					</font>
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">肌肉外觀：</td>
                  <td width="705">
					<input name="EV_MA" type="radio" class="form_fix" id="EV_MA1" value="正常" />正常
					<input name="EV_MA" type="radio" class="form_fix" id="EV_MA2" value="萎縮"/>萎縮
					<input name="EV_MA" type="radio" class="form_fix" id="EV_MA3" value="鬆弛"/>鬆弛
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">關節型態：</td>
                  <td width="705">
					<input name="EV_Joint" type="radio" class="form_fix" id="EV_Joint1" value="正常" />正常
					<input name="EV_Joint" type="radio" class="form_fix" id="EV_Joint2" value="僵硬" />僵硬
					<input name="EV_Joint" type="radio" class="form_fix" id="EV_Joint3" value="彎縮" />彎縮
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">日常活動：</td>
                  <td width="705">
					<input type="checkbox" name="EV_DA[]" id="EV_DA1" value="臥床" >臥床    
					<input type="checkbox" name="EV_DA[]" id="EV_DA2" value="散步" >散步
					<input type="checkbox" name="EV_DA[]" id="EV_DA3" value="運動" >運動
					<input type="checkbox" name="EV_DA[]" id="EV_DA4" value="看電視" >看電視
					<input type="checkbox" name="EV_DA[]" id="EV_DA5" value="閱讀" >閱讀
					<input type="checkbox" name="EV_DA[]" id="EV_DA6" value="手工藝" >手工藝
					<input type="checkbox" name="EV_DA[]" id="EV_DA7" value="其他" >其他
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="110" align="right" class="content">護理人員：</td>
                  <td width="705">
					<input name="EV_NS"  id="EV_NS" type="text" class="content" size="15" value=""/>
                  </td>  
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>
			           <input name="mybtn" type="submit" class="form_fix" id="mybtn"  value="確定送出"  /> 
 		               <input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>
		          </td>
                </tr>    
             </table>
              </form>
              <!--管理員管理 ending-->              
              </td>
            </tr>
            
          </table>
          </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>
