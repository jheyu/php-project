<?php 	
include("../public/mem_check.php");
include("../public/web_function.php");
	/*
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}
	*/
	if(is_numeric(quotes($_GET['RS_ID']))){
		 $RS_ID = quotes($_GET['RS_ID']);
	}else{
?>
    <script language="javascript">		
		//location.href='../index.php';
	</script>	
<?php
}	
    $sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
	$rs_form = $objDB->Recordset($sql);
	$row_form = $objDB->GetRows($rs_form);

	$rs = $objDB->Recordset("SELECT * FROM pain WHERE RS_ID = '$RS_ID' ORDER BY pain_Date DESC");
	$row = $objDB->GetRows($rs);	
    
    $pain_ID = $row[0]['pain_ID'];
	$pain_reaction = explode(",",$row[0]['pain_reaction']); 
	//echo $pain_reaction[0] . " " . $pain_reaction[1] . " " . $pain_reaction[2] . " " . $pain_reaction[3] . " " . $pain_reaction[4] . "<br>";
    //echo count($pain_reaction) . "<br>";
	$pain_part = explode(",",$row[0]['pain_part']);
	$pain_natrue = explode(",",$row[0]['pain_natrue']);
	$pain_serious = explode(",",$row[0]['pain_serious']);
	$pain_follow = explode(",",$row[0]['pain_follow']);
	$pain_affect = explode(",",$row[0]['pain_affect']);
	$pain_ease = explode(",",$row[0]['pain_ease']); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.datepicker.min.js"></script>

<script>
$(document).ready(function(){
    
    $(".date-pick" ).datepicker({ 
		dateFormat: 'yy-mm-dd', 
		showOn: "button",
		buttonImage: "../js/calendar.png",
		buttonImageOnly: true
	});

    $("#mybtn").click(function(){
		$("form#form1").submit();		
	});

})
</script>
<link type="text/css" href="../css/ui-darkness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<!-- header starting point -->
	<?php include("../include/header.php");?>
	<!-- header ending point -->    
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?>
        <!--menu ending point-->          
        </td>            
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top">
        <table width="830" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="30" class="content">專業照護 > 護理記錄 &gt; 修改</td>
              </tr>
              <tr>
                  <td height="10"></td>
              </tr>  
              <tr>
                <td height="10">
                <span class="form_title">
                     <input name="search" type="button" class="content" id="search" value="回上一頁" onclick="MM_goToURL('parent','layout.php?t=pain&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>
                </span></td>
              </tr>   
              <tr>
                  <td height="10"></td>
              </tr>                      
             <tr>
              	<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row_form[0]['RS_Name'];?></td>   
             </tr>   
             <tr>
                <td height="30"></td>
              </tr>             
<tr>
	<td style="font-size: 13pt"><strong>疼痛評估紀錄</strong>
	    <?php echo "(".$row[0]['pain_Date'] .")";?>
	</td>
</tr> 
<tr>
	<td height="5"></td>
</tr>
<tr>
	<td ><img src="../images/blueline.jpg" /></td>
</tr> 
<tr>
	<td height="5"></td>
</tr>   
<tr>         
	<td>   
	<form name="form1" id="form1" method="post" action="pain_process.php"/>
	<input type="hidden" name="action" id="action" value="mdy"/>                           
	<input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>"  />
    <input type="hidden" name="pain_ID" id="pain_ID" value="<?php echo $pain_ID;?>" />          
	<table>                
	<tr>
		<td height="10"></td>
	</tr>   
	<img src="pain.jpg"/>         
	<tr>
		<td width="110" align="right"  class="content">一、意識狀態：</td>
		<td width="705">
		<input name="pain_awareness" type="radio" class="form_fix" id="pain_awareness1" value="清醒"   <?php echo ckRadio('清醒',$row[0]['pain_awareness']);?> />清醒
		<input name="pain_awareness" type="radio" class="form_fix" id="pain_awareness2" value="嗜睡"   <?php echo ckRadio('嗜睡',$row[0]['pain_awareness']);?> />嗜睡	
		<input name="pain_awareness" type="radio" class="form_fix" id="pain_awareness3" value="混亂"   <?php echo ckRadio('混亂',$row[0]['pain_awareness']);?> />混亂
		<input name="pain_awareness" type="radio" class="form_fix" id="pain_awareness4" value="木僵"   <?php echo ckRadio('木僵',$row[0]['pain_awareness']);?> />木僵
		<input name="pain_awareness" type="radio" class="form_fix" id="pain_awareness5" value="半昏迷" <?php echo ckRadio('半昏迷',$row[0]['pain_awareness']);?> />半昏迷
		<input name="pain_awareness" type="radio" class="form_fix" id="pain_awareness6" value="昏迷"   <?php echo ckRadio('昏迷',$row[0]['pain_awareness']);?> />昏迷	
		</td>
	</tr>   
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
        <td width="110" align="right"  class="content">二、疼痛反應：</td>
		<td width="705">
        <input type="checkbox" name="pain_reaction[]" id="pain_reaction1" value="逃避按壓" <?php echo ckbox('逃避按壓',$pain_reaction);?> />逃避按壓
        <input type="checkbox" name="pain_reaction[]" id="pain_reaction2" value="呻吟"     <?php echo ckbox('呻吟',$pain_reaction);?> />呻吟
        <input type="checkbox" name="pain_reaction[]" id="pain_reaction3" value="愁眉苦臉" <?php echo ckbox('愁眉苦臉',$pain_reaction);?> />愁眉苦臉
        <input type="checkbox" name="pain_reaction[]" id="pain_reaction4" value="屈身"     <?php echo ckbox('屈身',$pain_reaction);?> />屈身
        <input type="checkbox" name="pain_reaction[]" id="pain_reaction5" value="不敢移動" <?php echo ckbox('不敢移動',$pain_reaction);?> />不敢移動
        <input type="checkbox" name="pain_reaction[]" id="pain_reaction6" value="其他"     <?php echo ckbox('其他',$pain_reaction);?> />其他
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
    <tr>
        <td width="110" align="right"  class="content">三、疼痛部位：</td>
		<td width="705">
        <input type="checkbox" name="pain_part[]" id="pain_part1" value="頭部" <?php echo ckbox('頭部',$pain_part);?> />頭部
        <input type="checkbox" name="pain_part[]" id="pain_part2" value="脖子" <?php echo ckbox('脖子',$pain_part);?> />脖子
        <input type="checkbox" name="pain_part[]" id="pain_part3" value="肩膀" <?php echo ckbox('肩膀',$pain_part);?> />肩膀
        <input type="checkbox" name="pain_part[]" id="pain_part4" value="胸部" <?php echo ckbox('胸部',$pain_part);?> />胸部
        <input type="checkbox" name="pain_part[]" id="pain_part5" value="腰部" <?php echo ckbox('腰部',$pain_part);?> />腰部
        <input type="checkbox" name="pain_part[]" id="pain_part6" value="手臂" <?php echo ckbox('手臂',$pain_part);?> />手臂
        <input type="checkbox" name="pain_part[]" id="pain_part7" value="手部" <?php echo ckbox('手部',$pain_part);?> />手部
        <input type="checkbox" name="pain_part[]" id="pain_part8" value="屁股" <?php echo ckbox('屁股',$pain_part);?> />屁股
        <input type="checkbox" name="pain_part[]" id="pain_part9" value="大腿" <?php echo ckbox('大腿',$pain_part);?> />大腿
        <input type="checkbox" name="pain_part[]" id="pain_part10" value="小腿" <?php echo ckbox('小腿',$pain_part);?> />小腿
        <input type="checkbox" name="pain_part[]" id="pain_part11" value="腳部" <?php echo ckbox('腳部',$pain_part);?> />腳部
        <input type="checkbox" name="pain_part[]" id="pain_part12" value="其他" <?php echo ckbox('其他',$pain_part);?> />其他
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
    <tr>
        <td width="110" align="right"  class="content">四、疼痛性質：</td>
		<td width="705">
        <input type="checkbox" name="pain_natrue[]" id="pain_natrue1" value="刺痛" <?php echo ckbox('刺痛',$pain_natrue);?> />刺痛
        <input type="checkbox" name="pain_natrue[]" id="pain_natrue2" value="刀割痛" <?php echo ckbox('刀割痛',$pain_natrue);?> />刀割痛
        <input type="checkbox" name="pain_natrue[]" id="pain_natrue3" value="鈍痛" <?php echo ckbox('鈍痛',$pain_natrue);?> />鈍痛
        <input type="checkbox" name="pain_natrue[]" id="pain_natrue4" value="悶痛" <?php echo ckbox('悶痛',$pain_natrue);?> />悶痛
        <input type="checkbox" name="pain_natrue[]" id="pain_natrue5" value="抽痛" <?php echo ckbox('抽痛',$pain_natrue);?> />抽痛
        <input type="checkbox" name="pain_natrue[]" id="pain_natrue6" value="壓痛" <?php echo ckbox('壓痛',$pain_natrue);?> />壓痛
        <input type="checkbox" name="pain_natrue[]" id="pain_natrue7" value="燒灼痛" <?php echo ckbox('燒灼痛',$pain_natrue);?> />燒灼痛
        <input type="checkbox" name="pain_natrue[]" id="pain_natrue8" value="感覺異常痛" <?php echo ckbox('感覺異常痛',$pain_natrue);?> />感覺異常痛
        <input type="checkbox" name="pain_natrue[]" id="pain_natrue9" value="戳痛" <?php echo ckbox('戳痛',$pain_natrue);?> />戳痛
        <input type="checkbox" name="pain_natrue[]" id="pain_natrue10" value="其他" <?php echo ckbox('其他',$pain_natrue);?> />其他
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
    <tr>
        <tr>
            <td width="110" align="right"  class="content">五、疼痛強度：</td>
		    <td width="705">
		</tr>

		<tr>
		    <td width="110" align="right"  class="content">(1)最痛時：</td>
		    <td width="705">
		    <input type="radio" name="pain_most" id="pain_most1" value="1" <?php echo ckRadio('1',$row[0]['pain_most']);?> />1
            <input type="radio" name="pain_most" id="pain_most2" value="2" <?php echo ckRadio('2',$row[0]['pain_most']);?> />2
            <input type="radio" name="pain_most" id="pain_most3" value="3" <?php echo ckRadio('3',$row[0]['pain_most']);?> />3
            <input type="radio" name="pain_most" id="pain_most4" value="4" <?php echo ckRadio('4',$row[0]['pain_most']);?> />4
            <input type="radio" name="pain_most" id="pain_most5" value="5" <?php echo ckRadio('5',$row[0]['pain_most']);?> />5
            <input type="radio" name="pain_most" id="pain_most6" value="6" <?php echo ckRadio('6',$row[0]['pain_most']);?> />6
            <input type="radio" name="pain_most" id="pain_most7" value="7" <?php echo ckRadio('7',$row[0]['pain_most']);?> />7
            <input type="radio" name="pain_most" id="pain_most8" value="8" <?php echo ckRadio('8',$row[0]['pain_most']);?> />8
            <input type="radio" name="pain_most" id="pain_most9" value="9" <?php echo ckRadio('9',$row[0]['pain_most']);?> />9
            <input type="radio" name="pain_most" id="pain_most10" value="10" <?php echo ckRadio('10',$row[0]['pain_most']);?> />10
		    &nbsp;分</td>
		</tr>

		<tr>
		     <td width="110" align="right"  class="content">(2)最輕時：</td>
		    <td width="705">
		    <input type="radio" name="pain_light" id="pain_light1" value="1" <?php echo ckRadio('1',$row[0]['pain_light']);?> />1
            <input type="radio" name="pain_light" id="pain_light2" value="2" <?php echo ckRadio('2',$row[0]['pain_light']);?> />2
            <input type="radio" name="pain_light" id="pain_light3" value="3" <?php echo ckRadio('3',$row[0]['pain_light']);?> />3
            <input type="radio" name="pain_light" id="pain_light4" value="4" <?php echo ckRadio('4',$row[0]['pain_light']);?> />4
            <input type="radio" name="pain_light" id="pain_light5" value="5" <?php echo ckRadio('5',$row[0]['pain_light']);?> />5
            <input type="radio" name="pain_light" id="pain_light6" value="6" <?php echo ckRadio('6',$row[0]['pain_light']);?> />6
            <input type="radio" name="pain_light" id="pain_light7" value="7" <?php echo ckRadio('7',$row[0]['pain_light']);?> />7
            <input type="radio" name="pain_light" id="pain_light8" value="8" <?php echo ckRadio('8',$row[0]['pain_light']);?> />8
            <input type="radio" name="pain_light" id="pain_light9" value="9" <?php echo ckRadio('9',$row[0]['pain_light']);?> />9
            <input type="radio" name="pain_light" id="pain_light10" value="10" <?php echo ckRadio('10',$row[0]['pain_light']);?> />10
		    &nbsp;分</td>
		</tr>

		<tr>
		    <td width="110" align="right"  class="content">(3)忍受度：</td>
		    <td width="705">
		    <input type="radio" name="pain_endure" id="pain_endure1" value="1" <?php echo ckRadio('1',$row[0]['pain_endure']);?> />1
            <input type="radio" name="pain_endure" id="pain_endure2" value="2" <?php echo ckRadio('2',$row[0]['pain_endure']);?> />2
            <input type="radio" name="pain_endure" id="pain_endure3" value="3" <?php echo ckRadio('3',$row[0]['pain_endure']);?> />3
            <input type="radio" name="pain_endure" id="pain_endure4" value="4" <?php echo ckRadio('4',$row[0]['pain_endure']);?> />4
            <input type="radio" name="pain_endure" id="pain_endure5" value="5" <?php echo ckRadio('5',$row[0]['pain_endure']);?> />5
            <input type="radio" name="pain_endure" id="pain_endure6" value="6" <?php echo ckRadio('6',$row[0]['pain_endure']);?> />6
            <input type="radio" name="pain_endure" id="pain_endure7" value="7" <?php echo ckRadio('7',$row[0]['pain_endure']);?> />7
            <input type="radio" name="pain_endure" id="pain_endure8" value="8" <?php echo ckRadio('8',$row[0]['pain_endure']);?> />8
            <input type="radio" name="pain_endure" id="pain_endure9" value="9" <?php echo ckRadio('9',$row[0]['pain_endure']);?> />9
            <input type="radio" name="pain_endure" id="pain_endure10" value="10" <?php echo ckRadio('10',$row[0]['pain_endure']);?> />10
		    &nbsp;分</td>
		</tr>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
        <td width="190" align="right"  class="content">六、疼痛開始時間：</td>
        <td width="630" class="form_title">&nbsp;<input name="pain_start" type="text" id="pain_start"  class="date-pick"  style="width:80px;"  readonly="readonly" value="<?php echo $row[0]['pain_start'];?>"/>
        
        最痛時刻：
        <select name="pain_highest" id="pain_highest">
                  	<option value="--">--</option>
						<option value="00" <?php echo ckselect("00",$row[0]['pain_highest'])?> >00</option>
						<option value="01" <?php echo ckselect("01",$row[0]['pain_highest'])?> >01</option>
						<option value="02" <?php echo ckselect("02",$row[0]['pain_highest'])?> >02</option>
						<option value="03" <?php echo ckselect("03",$row[0]['pain_highest'])?> >03</option>
						<option value="04" <?php echo ckselect("04",$row[0]['pain_highest'])?> >04</option>
						<option value="05" <?php echo ckselect("05",$row[0]['pain_highest'])?> >05</option>
						<option value="06" <?php echo ckselect("06",$row[0]['pain_highest'])?> >06</option>
						<option value="07" <?php echo ckselect("07",$row[0]['pain_highest'])?> >07</option>
						<option value="08" <?php echo ckselect("08",$row[0]['pain_highest'])?> >08</option>
						<option value="09" <?php echo ckselect("09",$row[0]['pain_highest'])?> >09</option>
						<option value="10" <?php echo ckselect("10",$row[0]['pain_highest'])?> >10</option>
						<option value="11" <?php echo ckselect("11",$row[0]['pain_highest'])?> >11</option>
						<option value="12" <?php echo ckselect("12",$row[0]['pain_highest'])?> >12</option>
						<option value="13" <?php echo ckselect("13",$row[0]['pain_highest'])?> >13</option>
						<option value="14" <?php echo ckselect("14",$row[0]['pain_highest'])?> >14</option>
						<option value="15" <?php echo ckselect("15",$row[0]['pain_highest'])?> >15</option>
						<option value="16" <?php echo ckselect("16",$row[0]['pain_highest'])?> >16</option>
						<option value="17" <?php echo ckselect("17",$row[0]['pain_highest'])?> >17</option>
						<option value="18" <?php echo ckselect("18",$row[0]['pain_highest'])?> >18</option>
						<option value="19" <?php echo ckselect("19",$row[0]['pain_highest'])?> >19</option>
						<option value="20" <?php echo ckselect("20",$row[0]['pain_highest'])?> >20</option>
						<option value="21" <?php echo ckselect("21",$row[0]['pain_highest'])?> >21</option>
						<option value="22" <?php echo ckselect("22",$row[0]['pain_highest'])?> >22</option>
						<option value="23" <?php echo ckselect("23",$row[0]['pain_highest'])?> >23</option>
        </select>
        </td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
        <td width="110" align="right"  class="content">七、疼痛頻率：</td>
		<td>一天<input name="pain_frequency" type="text" class="form_fix" id="pain_frequency" style="width:30px;" value="<?php echo $row[0]['pain_frequency'];?>" />次</td>

	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
        <td width="110" align="right"  class="content">八、疼痛持續時間：</td>
		<td width="705">
        
        <select name="pain_continueTime" id="pain_continueTime">
                  	<option value="--">--</option>
						<option value="00" <?php echo ckselect("00",$row[0]['pain_continueTime'])?> >00</option>
						<option value="01" <?php echo ckselect("01",$row[0]['pain_continueTime'])?> >01</option>
						<option value="02" <?php echo ckselect("02",$row[0]['pain_continueTime'])?> >02</option>
						<option value="03" <?php echo ckselect("03",$row[0]['pain_continueTime'])?> >03</option>
						<option value="04" <?php echo ckselect("04",$row[0]['pain_continueTime'])?> >04</option>
						<option value="05" <?php echo ckselect("05",$row[0]['pain_continueTime'])?> >05</option>
						<option value="06" <?php echo ckselect("06",$row[0]['pain_continueTime'])?> >06</option>
						<option value="07" <?php echo ckselect("07",$row[0]['pain_continueTime'])?> >07</option>
						<option value="08" <?php echo ckselect("08",$row[0]['pain_continueTime'])?> >08</option>
						<option value="09" <?php echo ckselect("09",$row[0]['pain_continueTime'])?> >09</option>
						<option value="10" <?php echo ckselect("10",$row[0]['pain_continueTime'])?> >10</option>
						<option value="11" <?php echo ckselect("11",$row[0]['pain_continueTime'])?> >11</option>
						<option value="12" <?php echo ckselect("12",$row[0]['pain_continueTime'])?> >12</option>
						<option value="13" <?php echo ckselect("13",$row[0]['pain_continueTime'])?> >13</option>
						<option value="14" <?php echo ckselect("14",$row[0]['pain_continueTime'])?> >14</option>
						<option value="15" <?php echo ckselect("15",$row[0]['pain_continueTime'])?> >15</option>
						<option value="16" <?php echo ckselect("16",$row[0]['pain_continueTime'])?> >16</option>
						<option value="17" <?php echo ckselect("17",$row[0]['pain_continueTime'])?> >17</option>
						<option value="18" <?php echo ckselect("18",$row[0]['pain_continueTime'])?> >18</option>
						<option value="19" <?php echo ckselect("19",$row[0]['pain_continueTime'])?> >19</option>
						<option value="20" <?php echo ckselect("20",$row[0]['pain_continueTime'])?> >20</option>
						<option value="21" <?php echo ckselect("21",$row[0]['pain_continueTime'])?> >21</option>
						<option value="22" <?php echo ckselect("22",$row[0]['pain_continueTime'])?> >22</option>
						<option value="23" <?php echo ckselect("23",$row[0]['pain_continueTime'])?> >23</option>
        </select>時:
                  <select name="pain_continueMinute" id="pain_continueMinute">
                  	<option value="--">--</option>
						<option value="00" <?php echo ckselect("00",$row[0]['pain_continueMinute'])?> >00</option>
						<option value="05" <?php echo ckselect("05",$row[0]['pain_continueMinute'])?> >05</option>
						<option value="10" <?php echo ckselect("10",$row[0]['pain_continueMinute'])?> >10</option>
						<option value="15" <?php echo ckselect("15",$row[0]['pain_continueMinute'])?> >15</option>
						<option value="20" <?php echo ckselect("20",$row[0]['pain_continueMinute'])?> >20</option>
						<option value="25" <?php echo ckselect("25",$row[0]['pain_continueMinute'])?> >25</option>
						<option value="30" <?php echo ckselect("30",$row[0]['pain_continueMinute'])?> >30</option>
						<option value="35" <?php echo ckselect("35",$row[0]['pain_continueMinute'])?> >35</option>
						<option value="40" <?php echo ckselect("40",$row[0]['pain_continueMinute'])?> >40</option>
						<option value="45" <?php echo ckselect("45",$row[0]['pain_continueMinute'])?> >45</option>
						<option value="50" <?php echo ckselect("50",$row[0]['pain_continueMinute'])?> >50</option>
						<option value="55" <?php echo ckselect("55",$row[0]['pain_continueMinute'])?> >55</option>
        </select>分
        </td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
    <tr>
        <td width="110" align="right"  class="content">九、加重疼痛因素：</td>
		<td width="705">
        <input type="checkbox" name="pain_serious[]" id="pain_serious1" value="按摩" <?php echo ckbox('按摩',$pain_serious);?> />按摩
        <input type="checkbox" name="pain_serious[]" id="pain_serious2" value="觸碰" <?php echo ckbox('觸碰',$pain_serious);?> />觸碰
        <input type="checkbox" name="pain_serious[]" id="pain_serious3" value="移動" <?php echo ckbox('移動',$pain_serious);?> />移動
        <input type="checkbox" name="pain_serious[]" id="pain_serious4" value="咳嗽" <?php echo ckbox('咳嗽',$pain_serious);?> />咳嗽
        <input type="checkbox" name="pain_serious[]" id="pain_serious5" value="進食" <?php echo ckbox('進食',$pain_serious);?> />進食
        <input type="checkbox" name="pain_serious[]" id="pain_serious6" value="其他" <?php echo ckbox('其他',$pain_serious);?> />其他
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
    <tr>
        <td width="110" align="right"  class="content">十、因疼痛而伴隨發生之症狀：</td>
		<td width="705">
        <input type="checkbox" name="pain_follow[]" id="pain_follow1" value="噁心" <?php echo ckbox('噁心',$pain_follow);?> />噁心
        <input type="checkbox" name="pain_follow[]" id="pain_follow2" value="暈眩" <?php echo ckbox('暈眩',$pain_follow);?> />暈眩
        <input type="checkbox" name="pain_follow[]" id="pain_follow3" value="血壓高" <?php echo ckbox('血壓高',$pain_follow);?> />血壓高
        <input type="checkbox" name="pain_follow[]" id="pain_follow4" value="呼吸速率改變" <?php echo ckbox('呼吸速率改變',$pain_follow);?> />呼吸速率改變
        <input type="checkbox" name="pain_follow[]" id="pain_follow5" value="脈搏速率改變" <?php echo ckbox('脈搏速率改變',$pain_follow);?> />脈搏速率改變
        <input type="checkbox" name="pain_follow[]" id="pain_follow6" value="肌肉張力增加" <?php echo ckbox('肌肉張力增加',$pain_follow);?> />肌肉張力增加
        <input type="checkbox" name="pain_follow[]" id="pain_follow7" value="出汗" <?php echo ckbox('出汗',$pain_follow);?> />出汗
        <input type="checkbox" name="pain_follow[]" id="pain_follow8" value="其他" <?php echo ckbox('其他',$pain_follow);?> />其他
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
    <tr>
        <td width="110" align="right"  class="content">十一、因疼痛造成之影響：</td>
		<td width="705">
        <tr>
		    <td width="110" align="right"  class="content">(1)睡眠：</td>
		    <td width="705">

            <input type="checkbox" name="pain_affect[]" id="pain_affect1" value="易醒" <?php echo ckbox('易醒',$pain_affect);?> />易醒
            <input type="checkbox" name="pain_affect[]" id="pain_affect2" value="不易入睡" <?php echo ckbox('不易入睡',$pain_affect);?> />不易入睡
            <input type="checkbox" name="pain_affect[]" id="pain_affect3" value="失眠" <?php echo ckbox('失眠',$pain_affect);?> />失眠
            <input type="checkbox" name="pain_affect[]" id="pain_affect4" value="無影響" <?php echo ckbox('無影響',$pain_affect);?> />無影響
            </td>
        </tr>

        <tr>
		    <td width="110" align="right"  class="content">(2)一般活動：</td>
		    <td width="705">

            <input type="checkbox" name="pain_affect[]" id="pain_affect1" value="僅可從事輕度活動" <?php echo ckbox('僅可從事輕度活動',$pain_affect);?> />僅可從事輕度活動
            <input type="checkbox" name="pain_affect[]" id="pain_affect2" value="受限臥床" <?php echo ckbox('受限臥床',$pain_affect);?> />受限臥床
            <input type="checkbox" name="pain_affect[]" id="pain_affect3" value="無法活動" <?php echo ckbox('無法活動',$pain_affect);?> />無法活動
            <input type="checkbox" name="pain_affect[]" id="pain_affect4" value="躁動" <?php echo ckbox('躁動',$pain_affect);?> />躁動
            <input type="checkbox" name="pain_affect[]" id="pain_affect5" value="無影響" <?php echo ckbox('無影響',$pain_affect);?> />無影響
            </td>
        </tr>

        <tr>
		    <td width="110" align="right"  class="content">(3)食慾：</td>
		    <td width="705">

            <input type="checkbox" name="pain_affect[]" id="pain_affect1" value="食慾差" <?php echo ckbox('食慾差',$pain_affect);?> />食慾差
            <input type="checkbox" name="pain_affect[]" id="pain_affect2" value="無法進食" <?php echo ckbox('無法進食',$pain_affect);?> />無法進食
            <input type="checkbox" name="pain_affect[]" id="pain_affect3" value="無影響" <?php echo ckbox('無影響',$pain_affect);?> />無影響
            </td>
        </tr>

        <tr>
		    <td width="110" align="right"  class="content">(4)注意力：</td>
		    <td width="705">

            <input type="checkbox" name="pain_affect[]" id="pain_affect1" value="集中於自己身上" <?php echo ckbox('集中於自己身上',$pain_affect);?> />集中於自己身上
            <input type="checkbox" name="pain_affect[]" id="pain_affect2" value="無法集中" <?php echo ckbox('無法集中',$pain_affect);?> />無法集中
            <input type="checkbox" name="pain_affect[]" id="pain_affect3" value="無影響" <?php echo ckbox('無影響',$pain_affect);?> />無影響
            </td>
        </tr>

        <tr>
		    <td width="110" align="right"  class="content">(5)情緒：</td>
		    <td width="705">

            <input type="checkbox" name="pain_affect[]" id="pain_affect1" value="焦慮" <?php echo ckbox('焦慮',$pain_affect);?> />焦慮
            <input type="checkbox" name="pain_affect[]" id="pain_affect2" value="哭泣" <?php echo ckbox('哭泣',$pain_affect);?> />哭泣
            <input type="checkbox" name="pain_affect[]" id="pain_affect3" value="懼怕" <?php echo ckbox('懼怕',$pain_affect);?> />懼怕
            <input type="checkbox" name="pain_affect[]" id="pain_affect4" value="憤怒" <?php echo ckbox('憤怒',$pain_affect);?> />憤怒
            <input type="checkbox" name="pain_affect[]" id="pain_affect5" value="淡漠" <?php echo ckbox('淡漠',$pain_affect);?> />淡漠
            <input type="checkbox" name="pain_affect[]" id="pain_affect6" value="憂鬱" <?php echo ckbox('憂鬱',$pain_affect);?> />憂鬱
             <input type="checkbox" name="pain_affect[]" id="pain_affect7" value="自殺意圖" <?php echo ckbox('自殺意圖',$pain_affect);?> />自殺意圖
            <input type="checkbox" name="pain_affect[]" id="pain_affect8" value="無影響" <?php echo ckbox('無影響',$pain_affect);?> />無影響
            </td>
        </tr>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">十二、緩解疼痛的方法：</td>
		<td width="705">
		<input type="checkbox" name="pain_ease[]" class="form_fix" id="pain_ease1" value="按摩" <?php echo ckbox('按摩',$pain_ease);?> />按摩
		<input type="checkbox" name="pain_ease[]" class="form_fix" id="pain_ease2" value="熱敷" <?php echo ckbox('熱敷',$pain_ease);?> />熱敷	
		<input type="checkbox" name="pain_ease[]" class="form_fix" id="pain_ease3" value="冷敷" <?php echo ckbox('冷敷',$pain_ease);?> />冷敷
		<input type="checkbox" name="pain_ease[]" class="form_fix" id="pain_ease4" value="不動" <?php echo ckbox('不動',$pain_ease);?> />不動
		<input type="checkbox" name="pain_ease[]" class="form_fix" id="pain_ease5" value="不碰觸" <?php echo ckbox('不碰觸',$pain_ease);?> />不碰觸
		<input type="checkbox" name="pain_ease[]" class="form_fix" id="pain_ease6" value="使用藥物" <?php echo ckbox('使用藥物',$pain_ease);?> />使用藥物
		<input type="checkbox" name="pain_ease[]" class="form_fix" id="pain_ease7" value="其他" <?php echo ckbox('其他',$pain_ease);?> />其他
		</td>
	</tr>   
	<tr>
		<td height="10"></td>
	</tr>                   
	<tr>
		<td align="right" class="content">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
			<input name="mybtn" type="button" class="form_fix" id="mybtn"  value="確定送出"  /> 
 		 <input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>
		</td>
	</tr>                
	</table>
	</form>              
	</td>
</tr>
</table>
          </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>
