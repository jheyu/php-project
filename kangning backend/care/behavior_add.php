<?php 
include("../public/mem_check.php");
include("../public/web_function.php");
	/*
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}
	*/
	if(is_numeric(quotes($_GET['RS_ID']))){
		 $RS_ID = quotes($_GET['RS_ID']);
	}else{
		 ?>
     <script language="javascript">		
		location.href='../index.php';
	 </script>	
         <?php
	}	

	$sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
	$rs = $objDB->Recordset($sql);
	$row = $objDB->GetRows($rs);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.datepicker.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){
    
    $(".date-pick" ).datepicker({ 
		dateFormat: 'yy-mm-dd', 
		showOn: "button",
		buttonImage: "../js/calendar.png",
		buttonImageOnly: true
	});	

    $("#mybtn").click(function(){
	
		$("form#form1").submit();
		
	});
})

</script>
<link type="text/css" href="../css/ui-darkness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<!-- header starting point -->
	<?php include("../include/header.php");?>
	<!-- header ending point -->    
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?>
        <!--menu ending point-->          
        </td>            
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top">
        <table width="830" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="30" class="content">專業照護 > 評估與記錄 &gt; 新增</td>
              </tr>
              <tr>
                  <td height="10"></td>
              </tr>  
              <tr>
                <td height="10">
                <span class="form_title">
                     <input name="search" type="button" class="content" id="search" value="回上一頁" onclick="MM_goToURL('parent','layout.php?t=beh&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>
                </span></td>
              </tr>   
              <tr>
                  <td height="10"></td>
              </tr>                   
             <tr>
              	<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>   
             </tr>   
             <tr>
                <td height="30"></td>
              </tr> 
          
<tr>
	<td style="font-size: 13pt"><strong>行為評估表</strong>
	</td>
</tr> 
<tr>
	<td height="5"></td>
</tr>
<tr>
	<td ><img src="../images/blueline.jpg" /></td>
</tr> 
	<tr>
	<td height="5"></td>
</tr>   
<tr>         
	<td>   
	<form name="form1" id="form1" method="post" action="behavior_process.php"/>
	<input type="hidden" name="action" id="action" value="new"/>                           
	<input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>"  />
    <input type="hidden" name="BEH_ID" id="BEH_ID" value="<?php echo $BEH_ID;?>" />          
	<table>                
	<tr>
		<td height="10"></td>
	</tr>   
	<img src="behavior.jpg"/>    
	<tr>
        <td width="110" align="left"  class="content">危害他人行為：</td>
		<td width="720">
		<table>
		<tr><td width="100" align="left" class="content">身體攻擊:</td>
        <td><input name="BEH_Attack1" type="radio" class="form_fix" id="BEH_Attack4" value="4"  />4
		    <input name="BEH_Attack1" type="radio" class="form_fix" id="BEH_Attack3" value="3"  />3
		    <input name="BEH_Attack1" type="radio" class="form_fix" id="BEH_Attack2" value="2"  />2	
		    <input name="BEH_Attack1" type="radio" class="form_fix" id="BEH_Attack1" value="1"  />1	
		    <input name="BEH_Attack1" type="radio" class="form_fix" id="BEH_Attack0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Attack2" type="radio" class="form_fix" id="BEH_Attack4" value="4"  />4
		    <input name="BEH_Attack2" type="radio" class="form_fix" id="BEH_Attack3" value="3"  />3
		    <input name="BEH_Attack2" type="radio" class="form_fix" id="BEH_Attack2" value="2"  />2	
		    <input name="BEH_Attack2" type="radio" class="form_fix" id="BEH_Attack1" value="1"  />1	
		    <input name="BEH_Attack2" type="radio" class="form_fix" id="BEH_Attack0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Attack3" type="radio" class="form_fix" id="BEH_Attack4" value="4"  />4
		    <input name="BEH_Attack3" type="radio" class="form_fix" id="BEH_Attack3" value="3"  />3
		    <input name="BEH_Attack3" type="radio" class="form_fix" id="BEH_Attack2" value="2"  />2	
		    <input name="BEH_Attack3" type="radio" class="form_fix" id="BEH_Attack1" value="1"  />1	
		    <input name="BEH_Attack3" type="radio" class="form_fix" id="BEH_Attack0" value="0"  />0
	    </td>
	    </tr>
	    <tr></tr>
	    <tr><td width="100" align="left" class="content">間接危害:</td>
        <td><input name="BEH_Indirect1" type="radio" class="form_fix" id="BEH_Indirect4" value="4"  />4
		    <input name="BEH_Indirect1" type="radio" class="form_fix" id="BEH_Indirect3" value="3"  />3
		    <input name="BEH_Indirect1" type="radio" class="form_fix" id="BEH_Indirect2" value="2"  />2	
		    <input name="BEH_Indirect1" type="radio" class="form_fix" id="BEH_Indirect1" value="1"  />1	
		    <input name="BEH_Indirect1" type="radio" class="form_fix" id="BEH_Indirect0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Indirect2" type="radio" class="form_fix" id="BEH_Indirect4" value="4"  />4
		    <input name="BEH_Indirect2" type="radio" class="form_fix" id="BEH_Indirect3" value="3"  />3
		    <input name="BEH_Indirect2" type="radio" class="form_fix" id="BEH_Indirect2" value="2"  />2	
		    <input name="BEH_Indirect2" type="radio" class="form_fix" id="BEH_Indirect1" value="1"  />1	
		    <input name="BEH_Indirect2" type="radio" class="form_fix" id="BEH_Indirect0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Indirect3" type="radio" class="form_fix" id="BEH_Indirect4" value="4"  />4
		    <input name="BEH_Indirect3" type="radio" class="form_fix" id="BEH_Indirect3" value="3"  />3
		    <input name="BEH_Indirect3" type="radio" class="form_fix" id="BEH_Indirect2" value="2"  />2	
		    <input name="BEH_Indirect3" type="radio" class="form_fix" id="BEH_Indirect1" value="1"  />1	
		    <input name="BEH_Indirect3" type="radio" class="form_fix" id="BEH_Indirect0" value="0"  />0
	    </td>
        </tr>
		</table>
        </td>
    </tr>
	<tr>
		<td height="20"></td>
	</tr>
	<tr>
        <td width="110" align="left"  class="content">危害自我行為：</td>
		<td width="720">
		<table>
		<tr><td width="100" align="left" class="content">身體自我傷害:</td>
        <td><input name="BEH_Self1" type="radio" class="form_fix" id="BEH_Self4" value="4"  />4
		    <input name="BEH_Self1" type="radio" class="form_fix" id="BEH_Self3" value="3"  />3
		    <input name="BEH_Self1" type="radio" class="form_fix" id="BEH_Self2" value="2"  />2	
		    <input name="BEH_Self1" type="radio" class="form_fix" id="BEH_Self1" value="1"  />1	
		    <input name="BEH_Self1" type="radio" class="form_fix" id="BEH_Self0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Self2" type="radio" class="form_fix" id="BEH_Self4" value="4"  />4
		    <input name="BEH_Self2" type="radio" class="form_fix" id="BEH_Self3" value="3"  />3
		    <input name="BEH_Self2" type="radio" class="form_fix" id="BEH_Self2" value="2"  />2	
		    <input name="BEH_Self2" type="radio" class="form_fix" id="BEH_Self1" value="1"  />1	
		    <input name="BEH_Self2" type="radio" class="form_fix" id="BEH_Self0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Self3" type="radio" class="form_fix" id="BEH_Self4" value="4"  />4
		    <input name="BEH_Self3" type="radio" class="form_fix" id="BEH_Self3" value="3"  />3
		    <input name="BEH_Self3" type="radio" class="form_fix" id="BEH_Self2" value="2"  />2	
		    <input name="BEH_Self3" type="radio" class="form_fix" id="BEH_Self1" value="1"  />1	
		    <input name="BEH_Self3" type="radio" class="form_fix" id="BEH_Self0" value="0"  />0
	    </td>
	    </tr>
	    <tr></tr>
	    <tr><td width="100" align="left" class="content">危險性行動:</td>
        <td><input name="BEH_Dangrous1" type="radio" class="form_fix" id="BEH_Dangrous4" value="4"  />4
		    <input name="BEH_Dangrous1" type="radio" class="form_fix" id="BEH_Dangrous3" value="3"  />3
		    <input name="BEH_Dangrous1" type="radio" class="form_fix" id="BEH_Dangrous2" value="2"  />2	
		    <input name="BEH_Dangrous1" type="radio" class="form_fix" id="BEH_Dangrous1" value="1"  />1	
		    <input name="BEH_Dangrous1" type="radio" class="form_fix" id="BEH_Dangrous0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Dangrous2" type="radio" class="form_fix" id="BEH_Dangrous4" value="4"  />4
		    <input name="BEH_Dangrous2" type="radio" class="form_fix" id="BEH_Dangrous3" value="3"  />3
		    <input name="BEH_Dangrous2" type="radio" class="form_fix" id="BEH_Dangrous2" value="2"  />2	
		    <input name="BEH_Dangrous2" type="radio" class="form_fix" id="BEH_Dangrous1" value="1"  />1	
		    <input name="BEH_Dangrous2" type="radio" class="form_fix" id="BEH_Dangrous0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Dangrous3" type="radio" class="form_fix" id="BEH_Dangrous4" value="4"  />4
		    <input name="BEH_Dangrous3" type="radio" class="form_fix" id="BEH_Dangrous3" value="3"  />3
		    <input name="BEH_Dangrous3" type="radio" class="form_fix" id="BEH_Dangrous2" value="2"  />2	
		    <input name="BEH_Dangrous3" type="radio" class="form_fix" id="BEH_Dangrous1" value="1"  />1	
		    <input name="BEH_Dangrous3" type="radio" class="form_fix" id="BEH_Dangrous0" value="0"  />0
	    </td>
        </tr>
        <tr></tr>
	    <tr><td width="100" align="left" class="content">拒絕生理上的照護:</td>
        <td><input name="BEH_Care1" type="radio" class="form_fix" id="BEH_Care4" value="4"  />4
		    <input name="BEH_Care1" type="radio" class="form_fix" id="BEH_Care3" value="3"  />3
		    <input name="BEH_Care1" type="radio" class="form_fix" id="BEH_Care2" value="2"  />2	
		    <input name="BEH_Care1" type="radio" class="form_fix" id="BEH_Care1" value="1"  />1	
		    <input name="BEH_Care1" type="radio" class="form_fix" id="BEH_Care0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Care2" type="radio" class="form_fix" id="BEH_Care4" value="4"  />4
		    <input name="BEH_Care2" type="radio" class="form_fix" id="BEH_Care3" value="3"  />3
		    <input name="BEH_Care2" type="radio" class="form_fix" id="BEH_Care2" value="2"  />2	
		    <input name="BEH_Care2" type="radio" class="form_fix" id="BEH_Care1" value="1"  />1	
		    <input name="BEH_Care2" type="radio" class="form_fix" id="BEH_Care0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Care3" type="radio" class="form_fix" id="BEH_Care4" value="4"  />4
		    <input name="BEH_Care3" type="radio" class="form_fix" id="BEH_Care3" value="3"  />3
		    <input name="BEH_Care3" type="radio" class="form_fix" id="BEH_Care2" value="2"  />2	
		    <input name="BEH_Care3" type="radio" class="form_fix" id="BEH_Care1" value="1"  />1	
		    <input name="BEH_Care3" type="radio" class="form_fix" id="BEH_Care0" value="0"  />0
	    </td>
        </tr>
        <tr></tr>
	    <tr><td width="100" align="left" class="content">其他可能的危害自我行為:</td>
        <td><input name="BEH_Harm1" type="radio" class="form_fix" id="BEH_Harm4" value="4"  />4
		    <input name="BEH_Harm1" type="radio" class="form_fix" id="BEH_Harm3" value="3"  />3
		    <input name="BEH_Harm1" type="radio" class="form_fix" id="BEH_Harm2" value="2"  />2	
		    <input name="BEH_Harm1" type="radio" class="form_fix" id="BEH_Harm1" value="1"  />1	
		    <input name="BEH_Harm1" type="radio" class="form_fix" id="BEH_Harm0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Harm2" type="radio" class="form_fix" id="BEH_Harm4" value="4"  />4
		    <input name="BEH_Harm2" type="radio" class="form_fix" id="BEH_Harm3" value="3"  />3
		    <input name="BEH_Harm2" type="radio" class="form_fix" id="BEH_Harm2" value="2"  />2	
		    <input name="BEH_Harm2" type="radio" class="form_fix" id="BEH_Harm1" value="1"  />1	
		    <input name="BEH_Harm2" type="radio" class="form_fix" id="BEH_Harm0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Harm3" type="radio" class="form_fix" id="BEH_Harm4" value="4"  />4
		    <input name="BEH_Harm3" type="radio" class="form_fix" id="BEH_Harm3" value="3"  />3
		    <input name="BEH_Harm3" type="radio" class="form_fix" id="BEH_Harm2" value="2"  />2	
		    <input name="BEH_Harm3" type="radio" class="form_fix" id="BEH_Harm1" value="1"  />1	
		    <input name="BEH_Harm3" type="radio" class="form_fix" id="BEH_Harm0" value="0"  />0
	    </td>
        </tr>
		</table>
        </td>
    </tr>
	<tr>
		<td height="20"></td>
	</tr>
	<tr>
        <td width="110" align="left"  class="content">困擾他人行為：</td>
		<td width="720">
		<table>
		<tr><td width="100" align="left" class="content">言語困擾:</td>
        <td><input name="BEH_Speech1" type="radio" class="form_fix" id="BEH_Speech4" value="4"  />4
		    <input name="BEH_Speech1" type="radio" class="form_fix" id="BEH_Speech3" value="3"  />3
		    <input name="BEH_Speech1" type="radio" class="form_fix" id="BEH_Speech2" value="2"  />2	
		    <input name="BEH_Speech1" type="radio" class="form_fix" id="BEH_Speech1" value="1"  />1	
		    <input name="BEH_Speech1" type="radio" class="form_fix" id="BEH_Speech0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Speech2" type="radio" class="form_fix" id="BEH_Speech4" value="4"  />4
		    <input name="BEH_Speech2" type="radio" class="form_fix" id="BEH_Speech3" value="3"  />3
		    <input name="BEH_Speech2" type="radio" class="form_fix" id="BEH_Speech2" value="2"  />2	
		    <input name="BEH_Speech2" type="radio" class="form_fix" id="BEH_Speech1" value="1"  />1	
		    <input name="BEH_Speech2" type="radio" class="form_fix" id="BEH_Speech0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Speech3" type="radio" class="form_fix" id="BEH_Speech4" value="4"  />4
		    <input name="BEH_Speech3" type="radio" class="form_fix" id="BEH_Speech3" value="3"  />3
		    <input name="BEH_Speech3" type="radio" class="form_fix" id="BEH_Speech2" value="2"  />2	
		    <input name="BEH_Speech3" type="radio" class="form_fix" id="BEH_Speech1" value="1"  />1	
		    <input name="BEH_Speech3" type="radio" class="form_fix" id="BEH_Speech0" value="0"  />0
	    </td>
	    </tr>
	    <tr></tr>
	    <tr><td width="100" align="left" class="content">不適宜的走動:</td>
        <td><input name="BEH_Move1" type="radio" class="form_fix" id="BEH_Move4" value="4"  />4
		    <input name="BEH_Move1" type="radio" class="form_fix" id="BEH_Move3" value="3"  />3
		    <input name="BEH_Move1" type="radio" class="form_fix" id="BEH_Move2" value="2"  />2	
		    <input name="BEH_Move1" type="radio" class="form_fix" id="BEH_Move1" value="1"  />1	
		    <input name="BEH_Move1" type="radio" class="form_fix" id="BEH_Move0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Move2" type="radio" class="form_fix" id="BEH_Move4" value="4"  />4
		    <input name="BEH_Move2" type="radio" class="form_fix" id="BEH_Move3" value="3"  />3
		    <input name="BEH_Move2" type="radio" class="form_fix" id="BEH_Move2" value="2"  />2	
		    <input name="BEH_Move2" type="radio" class="form_fix" id="BEH_Move1" value="1"  />1	
		    <input name="BEH_Move2" type="radio" class="form_fix" id="BEH_Move0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Move3" type="radio" class="form_fix" id="BEH_Move4" value="4"  />4
		    <input name="BEH_Move3" type="radio" class="form_fix" id="BEH_Move3" value="3"  />3
		    <input name="BEH_Move3" type="radio" class="form_fix" id="BEH_Move2" value="2"  />2	
		    <input name="BEH_Move3" type="radio" class="form_fix" id="BEH_Move1" value="1"  />1	
		    <input name="BEH_Move3" type="radio" class="form_fix" id="BEH_Move0" value="0"  />0
	    </td>
        </tr>
        <tr></tr>
	    <tr><td width="100" align="left" class="content">破壞行為:</td>
        <td><input name="BEH_Damage1" type="radio" class="form_fix" id="BEH_Damage4" value="4"  />4
		    <input name="BEH_Damage1" type="radio" class="form_fix" id="BEH_Damage3" value="3"  />3
		    <input name="BEH_Damage1" type="radio" class="form_fix" id="BEH_Damage2" value="2"  />2	
		    <input name="BEH_Damage1" type="radio" class="form_fix" id="BEH_Damage1" value="1"  />1	
		    <input name="BEH_Damage1" type="radio" class="form_fix" id="BEH_Damage0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Damage2" type="radio" class="form_fix" id="BEH_Damage4" value="4"  />4
		    <input name="BEH_Damage2" type="radio" class="form_fix" id="BEH_Damage3" value="3"  />3
		    <input name="BEH_Damage2" type="radio" class="form_fix" id="BEH_Damage2" value="2"  />2	
		    <input name="BEH_Damage2" type="radio" class="form_fix" id="BEH_Damage1" value="1"  />1	
		    <input name="BEH_Damage2" type="radio" class="form_fix" id="BEH_Damage0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Damage3" type="radio" class="form_fix" id="BEH_Damage4" value="4"  />4
		    <input name="BEH_Damage3" type="radio" class="form_fix" id="BEH_Damage3" value="3"  />3
		    <input name="BEH_Damage3" type="radio" class="form_fix" id="BEH_Damage2" value="2"  />2	
		    <input name="BEH_Damage3" type="radio" class="form_fix" id="BEH_Damage1" value="1"  />1	
		    <input name="BEH_Damage3" type="radio" class="form_fix" id="BEH_Damage0" value="0"  />0
	    </td>
        </tr>
        <tr></tr>
	    <tr><td width="100" align="left" class="content">取走他人財物:</td>
        <td><input name="BEH_Property1" type="radio" class="form_fix" id="BEH_Property4" value="4"  />4
		    <input name="BEH_Property1" type="radio" class="form_fix" id="BEH_Property3" value="3"  />3
		    <input name="BEH_Property1" type="radio" class="form_fix" id="BEH_Property2" value="2"  />2	
		    <input name="BEH_Property1" type="radio" class="form_fix" id="BEH_Property1" value="1"  />1	
		    <input name="BEH_Property1" type="radio" class="form_fix" id="BEH_Property0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Property2" type="radio" class="form_fix" id="BEH_Property4" value="4"  />4
		    <input name="BEH_Property2" type="radio" class="form_fix" id="BEH_Property3" value="3"  />3
		    <input name="BEH_Property2" type="radio" class="form_fix" id="BEH_Property2" value="2"  />2	
		    <input name="BEH_Property2" type="radio" class="form_fix" id="BEH_Property1" value="1"  />1	
		    <input name="BEH_Property2" type="radio" class="form_fix" id="BEH_Property0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Property3" type="radio" class="form_fix" id="BEH_Property4" value="4"  />4
		    <input name="BEH_Property3" type="radio" class="form_fix" id="BEH_Property3" value="3"  />3
		    <input name="BEH_Property3" type="radio" class="form_fix" id="BEH_Property2" value="2"  />2	
		    <input name="BEH_Property3" type="radio" class="form_fix" id="BEH_Property1" value="1"  />1	
		    <input name="BEH_Property3" type="radio" class="form_fix" id="BEH_Property0" value="0"  />0
	    </td>
        </tr>
        <tr></tr>
	    <tr><td width="100" align="left" class="content">不合宜的便溺行為:</td>
        <td><input name="BEH_Urinating1" type="radio" class="form_fix" id="BEH_Urinating4" value="4"  />4
		    <input name="BEH_Urinating1" type="radio" class="form_fix" id="BEH_Urinating3" value="3"  />3
		    <input name="BEH_Urinating1" type="radio" class="form_fix" id="BEH_Urinating2" value="2"  />2	
		    <input name="BEH_Urinating1" type="radio" class="form_fix" id="BEH_Urinating1" value="1"  />1	
		    <input name="BEH_Urinating1" type="radio" class="form_fix" id="BEH_Urinating0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Urinating2" type="radio" class="form_fix" id="BEH_Urinating4" value="4"  />4
		    <input name="BEH_Urinating2" type="radio" class="form_fix" id="BEH_Urinating3" value="3"  />3
		    <input name="BEH_Urinating2" type="radio" class="form_fix" id="BEH_Urinating2" value="2"  />2	
		    <input name="BEH_Urinating2" type="radio" class="form_fix" id="BEH_Urinating1" value="1"  />1	
		    <input name="BEH_Urinating2" type="radio" class="form_fix" id="BEH_Urinating0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Urinating3" type="radio" class="form_fix" id="BEH_Urinating4" value="4"  />4
		    <input name="BEH_Urinating3" type="radio" class="form_fix" id="BEH_Urinating3" value="3"  />3
		    <input name="BEH_Urinating3" type="radio" class="form_fix" id="BEH_Urinating2" value="2"  />2	
		    <input name="BEH_Urinating3" type="radio" class="form_fix" id="BEH_Urinating1" value="1"  />1	
		    <input name="BEH_Urinating3" type="radio" class="form_fix" id="BEH_Urinating0" value="0"  />0
	    </td>
        </tr>
        <tr></tr>
	    <tr><td width="100" align="left" class="content">性困擾:</td>
        <td><input name="BEH_Sexual1" type="radio" class="form_fix" id="BEH_Sexual4" value="4"  />4
		    <input name="BEH_Sexual1" type="radio" class="form_fix" id="BEH_Sexual3" value="3"  />3
		    <input name="BEH_Sexual1" type="radio" class="form_fix" id="BEH_Sexual2" value="2"  />2	
		    <input name="BEH_Sexual1" type="radio" class="form_fix" id="BEH_Sexual1" value="1"  />1	
		    <input name="BEH_Sexual1" type="radio" class="form_fix" id="BEH_Sexual0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Sexual2" type="radio" class="form_fix" id="BEH_Sexual4" value="4"  />4
		    <input name="BEH_Sexual2" type="radio" class="form_fix" id="BEH_Sexual3" value="3"  />3
		    <input name="BEH_Sexual2" type="radio" class="form_fix" id="BEH_Sexual2" value="2"  />2	
		    <input name="BEH_Sexual2" type="radio" class="form_fix" id="BEH_Sexual1" value="1"  />1	
		    <input name="BEH_Sexual2" type="radio" class="form_fix" id="BEH_Sexual0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Sexual3" type="radio" class="form_fix" id="BEH_Sexual4" value="4"  />4
		    <input name="BEH_Sexual3" type="radio" class="form_fix" id="BEH_Sexual3" value="3"  />3
		    <input name="BEH_Sexual3" type="radio" class="form_fix" id="BEH_Sexual2" value="2"  />2	
		    <input name="BEH_Sexual3" type="radio" class="form_fix" id="BEH_Sexual1" value="1"  />1	
		    <input name="BEH_Sexual3" type="radio" class="form_fix" id="BEH_Sexual0" value="0"  />0
	    </td>
        </tr>
        <tr></tr>
	    <tr><td width="100" align="left" class="content">異常情緒反應:</td>
        <td><input name="BEH_Mood1" type="radio" class="form_fix" id="BEH_Mood4" value="4"  />4
		    <input name="BEH_Mood1" type="radio" class="form_fix" id="BEH_Mood3" value="3"  />3
		    <input name="BEH_Mood1" type="radio" class="form_fix" id="BEH_Mood2" value="2"  />2	
		    <input name="BEH_Mood1" type="radio" class="form_fix" id="BEH_Mood1" value="1"  />1	
		    <input name="BEH_Mood1" type="radio" class="form_fix" id="BEH_Mood0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Mood2" type="radio" class="form_fix" id="BEH_Mood4" value="4"  />4
		    <input name="BEH_Mood2" type="radio" class="form_fix" id="BEH_Mood3" value="3"  />3
		    <input name="BEH_Mood2" type="radio" class="form_fix" id="BEH_Mood2" value="2"  />2	
		    <input name="BEH_Mood2" type="radio" class="form_fix" id="BEH_Mood1" value="1"  />1	
		    <input name="BEH_Mood2" type="radio" class="form_fix" id="BEH_Mood0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Mood3" type="radio" class="form_fix" id="BEH_Mood4" value="4"  />4
		    <input name="BEH_Mood3" type="radio" class="form_fix" id="BEH_Mood3" value="3"  />3
		    <input name="BEH_Mood3" type="radio" class="form_fix" id="BEH_Mood2" value="2"  />2	
		    <input name="BEH_Mood3" type="radio" class="form_fix" id="BEH_Mood1" value="1"  />1	
		    <input name="BEH_Mood3" type="radio" class="form_fix" id="BEH_Mood0" value="0"  />0
	    </td>
        </tr>
        <tr></tr>
	    <tr><td width="100" align="left" class="content">其他困擾他人行為:</td>
        <td><input name="BEH_Puzzle1" type="radio" class="form_fix" id="BEH_Puzzle4" value="4"  />4
		    <input name="BEH_Puzzle1" type="radio" class="form_fix" id="BEH_Puzzle3" value="3"  />3
		    <input name="BEH_Puzzle1" type="radio" class="form_fix" id="BEH_Puzzle2" value="2"  />2	
		    <input name="BEH_Puzzle1" type="radio" class="form_fix" id="BEH_Puzzle1" value="1"  />1	
		    <input name="BEH_Puzzle1" type="radio" class="form_fix" id="BEH_Puzzle0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Puzzle2" type="radio" class="form_fix" id="BEH_Puzzle4" value="4"  />4
		    <input name="BEH_Puzzle2" type="radio" class="form_fix" id="BEH_Puzzle3" value="3"  />3
		    <input name="BEH_Puzzle2" type="radio" class="form_fix" id="BEH_Puzzle2" value="2"  />2	
		    <input name="BEH_Puzzle2" type="radio" class="form_fix" id="BEH_Puzzle1" value="1"  />1	
		    <input name="BEH_Puzzle2" type="radio" class="form_fix" id="BEH_Puzzle0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Puzzle3" type="radio" class="form_fix" id="BEH_Puzzle4" value="4"  />4
		    <input name="BEH_Puzzle3" type="radio" class="form_fix" id="BEH_Puzzle3" value="3"  />3
		    <input name="BEH_Puzzle3" type="radio" class="form_fix" id="BEH_Puzzle2" value="2"  />2	
		    <input name="BEH_Puzzle3" type="radio" class="form_fix" id="BEH_Puzzle1" value="1"  />1	
		    <input name="BEH_Puzzle3" type="radio" class="form_fix" id="BEH_Puzzle0" value="0"  />0
	    </td>
        </tr>
		</table>
        </td>
    </tr>
	<tr>
		<td height="20"></td>
	</tr>
	<tr>
        <td width="110" align="left"  class="content">不危害亦不困擾他人但需受關照的行為：</td>
		<td width="720">
		<table>
		<tr><td width="100" align="left" class="content">藏匿行為:</td>
        <td><input name="BEH_Hide1" type="radio" class="form_fix" id="BEH_Hide4" value="4"  />4
		    <input name="BEH_Hide1" type="radio" class="form_fix" id="BEH_Hide3" value="3"  />3
		    <input name="BEH_Hide1" type="radio" class="form_fix" id="BEH_Hide2" value="2"  />2	
		    <input name="BEH_Hide1" type="radio" class="form_fix" id="BEH_Hide1" value="1"  />1	
		    <input name="BEH_Hide1" type="radio" class="form_fix" id="BEH_Hide0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Hide2" type="radio" class="form_fix" id="BEH_Hide4" value="4"  />4
		    <input name="BEH_Hide2" type="radio" class="form_fix" id="BEH_Hide3" value="3"  />3
		    <input name="BEH_Hide2" type="radio" class="form_fix" id="BEH_Hide2" value="2"  />2	
		    <input name="BEH_Hide2" type="radio" class="form_fix" id="BEH_Hide1" value="1"  />1	
		    <input name="BEH_Hide2" type="radio" class="form_fix" id="BEH_Hide0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Hide3" type="radio" class="form_fix" id="BEH_Hide4" value="4"  />4
		    <input name="BEH_Hide3" type="radio" class="form_fix" id="BEH_Hide3" value="3"  />3
		    <input name="BEH_Hide3" type="radio" class="form_fix" id="BEH_Hide2" value="2"  />2	
		    <input name="BEH_Hide3" type="radio" class="form_fix" id="BEH_Hide1" value="1"  />1	
		    <input name="BEH_Hide3" type="radio" class="form_fix" id="BEH_Hide0" value="0"  />0
	    </td>
	    </tr>
	    <tr></tr>
	    <tr><td width="100" align="left" class="content">貯藏行為:</td>
        <td><input name="BEH_Storage1" type="radio" class="form_fix" id="BEH_Storage4" value="4"  />4
		    <input name="BEH_Storage1" type="radio" class="form_fix" id="BEH_Storage3" value="3"  />3
		    <input name="BEH_Storage1" type="radio" class="form_fix" id="BEH_Storage2" value="2"  />2	
		    <input name="BEH_Storage1" type="radio" class="form_fix" id="BEH_Storage1" value="1"  />1	
		    <input name="BEH_Storage1" type="radio" class="form_fix" id="BEH_Storage0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Storage2" type="radio" class="form_fix" id="BEH_Storage4" value="4"  />4
		    <input name="BEH_Storage2" type="radio" class="form_fix" id="BEH_Storage3" value="3"  />3
		    <input name="BEH_Storage2" type="radio" class="form_fix" id="BEH_Storage2" value="2"  />2	
		    <input name="BEH_Storage2" type="radio" class="form_fix" id="BEH_Storage1" value="1"  />1	
		    <input name="BEH_Storage2" type="radio" class="form_fix" id="BEH_Storage0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Storage3" type="radio" class="form_fix" id="BEH_Storage4" value="4"  />4
		    <input name="BEH_Storage3" type="radio" class="form_fix" id="BEH_Storage3" value="3"  />3
		    <input name="BEH_Storage3" type="radio" class="form_fix" id="BEH_Storage2" value="2"  />2	
		    <input name="BEH_Storage3" type="radio" class="form_fix" id="BEH_Storage1" value="1"  />1	
		    <input name="BEH_Storage3" type="radio" class="form_fix" id="BEH_Storage0" value="0"  />0
	    </td>
        </tr>
        <tr></tr>
	    <tr><td width="100" align="left" class="content">不合宜的行為:</td>
        <td><input name="BEH_Inappropriate1" type="radio" class="form_fix" id="BEH_Inappropriate4" value="4"  />4
		    <input name="BEH_Inappropriate1" type="radio" class="form_fix" id="BEH_Inappropriate3" value="3"  />3
		    <input name="BEH_Inappropriate1" type="radio" class="form_fix" id="BEH_Inappropriate2" value="2"  />2	
		    <input name="BEH_Inappropriate1" type="radio" class="form_fix" id="BEH_Inappropriate1" value="1"  />1	
		    <input name="BEH_Inappropriate1" type="radio" class="form_fix" id="BEH_Inappropriate0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Inappropriate2" type="radio" class="form_fix" id="BEH_Inappropriate4" value="4"  />4
		    <input name="BEH_Inappropriate2" type="radio" class="form_fix" id="BEH_Inappropriate3" value="3"  />3
		    <input name="BEH_Inappropriate2" type="radio" class="form_fix" id="BEH_Inappropriate2" value="2"  />2	
		    <input name="BEH_Inappropriate2" type="radio" class="form_fix" id="BEH_Inappropriate1" value="1"  />1	
		    <input name="BEH_Inappropriate2" type="radio" class="form_fix" id="BEH_Inappropriate0" value="0"  />0
	    </td>
	    <td width="10"></td>
		<td align="left"  class="content">
		    <input name="BEH_Inappropriate3" type="radio" class="form_fix" id="BEH_Inappropriate4" value="4"  />4
		    <input name="BEH_Inappropriate3" type="radio" class="form_fix" id="BEH_Inappropriate3" value="3"  />3
		    <input name="BEH_Inappropriate3" type="radio" class="form_fix" id="BEH_Inappropriate2" value="2"  />2	
		    <input name="BEH_Inappropriate3" type="radio" class="form_fix" id="BEH_Inappropriate1" value="1"  />1	
		    <input name="BEH_Inappropriate3" type="radio" class="form_fix" id="BEH_Inappropriate0" value="0"  />0
	    </td>
        </tr>
		</table>
        </td>
    </tr>
	<tr>
		<td height="20"></td>
	</tr>              
	<tr>
		<td align="right" class="content">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
			<input name="mybtn" type="button" class="form_fix" id="mybtn"  value="確定送出"  /> 
 		 <input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>
		</td>
	</tr>                
	</table>
	</form>              
	</td>
</tr>
            
          </table>
          </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>
