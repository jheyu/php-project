<?php 
include("../public/mem_check.php");
include("../public/web_function.php");

  if(is_numeric(quotes($_GET['RS_ID']))){
     $RS_ID = quotes($_GET['RS_ID']);
  }else{
     ?>
     <script language="javascript">   
    //location.href='../index.php';
   </script>  
<?php
  } 

  $sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
  $rs = $objDB->Recordset($sql);
  $row = $objDB->GetRows($rs);

	$rs_form = $objDB->Recordset("SELECT * FROM adapt WHERE RS_ID = '$RS_ID'");
	$row_form = $objDB->GetRows($rs_form);
	
	$AD_ID = $row_form[0]['AD_ID'];
	$AD_Act = explode(",",$row_form[0]['AD_Act']);
	$AD_FA = explode(",",$row_form[0]['AD_FA']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.datepicker.min.js"></script>
<script>
$(document).ready(function(){
    $(".date-pick" ).datepicker({ 
		dateFormat: 'yy-mm-dd', 
		showOn: "button",
		buttonImage: "../js/calendar.png",
		buttonImageOnly: true
	});	
 	$("#mybtn").click(function(){	
			$("form#form1").submit();
		})	
})

</script>
<link type="text/css" href="../css/ui-darkness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
  <!-- header starting point -->
  <?php include("../include/header.php");?>
  <!-- header ending point -->    
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?>
        <!--menu ending point-->          
        </td>            
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top">
        <table width="830" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="30" class="content">專業照護 > 護理記錄 &gt; 修改</td>
              </tr>
              <tr>
                  <td height="10"></td>
              </tr>  
              <tr>
                <td height="10">
                <span class="form_title">
                     <input name="search" type="button" class="content" id="search" value="回上一頁" onclick="MM_goToURL('parent','layout.php?t=adapt&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>
                </span></td>
              </tr>   
              <tr>
                  <td height="10"></td>
              </tr>                      
             <tr>
                <td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>   
             </tr>   
             <tr>
                <td height="30"></td>
              </tr>              
  <tr>
	  <td style="font-size: 13pt"><strong>生活適應評估表</strong>
    </label></td>            
  </tr> 
      <form name="form1" id="form1" method="post" action="adapt_process.php" />
		    <input type="hidden" name="action" id="action" value="mdy"/>              
        <input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>" />
		    <input type="hidden" name="AD_ID" id="AD_ID" value="<?php echo $AD_ID;?>" />		
             <table>
                <tr>
                   <td height="10"></td> 
                </tr>
				<tr>
                  <td width="110" align="right"  class="content">日期：</td>
                  <td width="705">
					<input name="AD_Date"  id="AD_Date" type="text" class="txt date-pick" style="width:80px;"  value="<?php  echo $row_form[0]['AD_Date']; ?>"  />
                  </td>
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="200" align="right" class="content">最近一週晚上入睡情形：</td>
                  <td width="705">
					<input name="AD_Sleep" type="radio" class="form_fix" id="AD_Sleep1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_Sleep']);?>/>服用藥物才能睡
					<input name="AD_Sleep" type="radio" class="form_fix" id="AD_Sleep2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_Sleep']);?>/>一陣子才入睡
					<input name="AD_Sleep" type="radio" class="form_fix" id="AD_Sleep3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_Sleep']);?>/>躺下就睡著
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">晚上睡眠的品質：</td>
                  <td width="705">
					<input name="AD_SQ" type="radio" class="form_fix" id="AD_SQ1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_SQ']);?>/>睡得不好
					<input name="AD_SQ" type="radio" class="form_fix" id="AD_SQ2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_SQ']);?>/>尚可
					<input name="AD_SQ" type="radio" class="form_fix" id="AD_SQ3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_SQ']);?>/>睡得很好
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">對餐點及飯菜的感覺：</td>
                  <td width="705">
					<input name="AD_Eat" type="radio" class="form_fix" id="AD_Eat1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_Eat']);?>/>不好吃
					<input name="AD_Eat" type="radio" class="form_fix" id="AD_Eat2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_Eat']);?>/>尚可
					<input name="AD_Eat" type="radio" class="form_fix" id="AD_Eat3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_Eat']);?>/>很好吃
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">吃東西的胃口：</td>
                  <td width="705">
					<input name="AD_AP" type="radio" class="form_fix" id="AD_AP1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_AP']);?>/>沒胃口，吃不下
					<input name="AD_AP" type="radio" class="form_fix" id="AD_AP2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_AP']);?>/>多少吃一點
					<input name="AD_AP" type="radio" class="form_fix" id="AD_AP3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_AP']);?>/>胃口不錯，吃得下
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">最近一個月，有無看診或住院：</td>
                  <td width="705">
					<input name="AD_HI" type="radio" class="form_fix" id="AD_HI1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_HI']);?>/>有住院
					<input name="AD_HI" type="radio" class="form_fix" id="AD_HI2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_HI']);?>/>有看門診
					<input name="AD_HI" type="radio" class="form_fix" id="AD_HI3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_HI']);?>/>都沒有
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">運動的習慣與入住前比較：</td>
                  <td width="705">
					<input name="AD_SP" type="radio" class="form_fix" id="AD_SP1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_SP']);?>/>減少許多
					<input name="AD_SP" type="radio" class="form_fix" id="AD_SP2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_SP']);?>/>本身沒有運動習慣
					<input name="AD_SP" type="radio" class="form_fix" id="AD_SP3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_SP']);?>/>增加且規律
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">衛生習慣與入住前比較：</td>
                  <td width="705">
					<input name="AD_HH" type="radio" class="form_fix" id="AD_HH1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_HH']);?>/>相同
					<input name="AD_HH" type="radio" class="form_fix" id="AD_HH2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_HH']);?>/>配合團體生活而有所改變
					<input name="AD_HH" type="radio" class="form_fix" id="AD_HH3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_HH']);?>/>衛生習慣較佳
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">整體的健康情況：</td>
                  <td width="705">
					<input name="AD_All" type="radio" class="form_fix" id="AD_All1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_All']);?>/>普通
					<input name="AD_All" type="radio" class="form_fix" id="AD_All2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_All']);?>/>好
					<input name="AD_All" type="radio" class="form_fix" id="AD_All3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_All']);?>/>很好
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">入住後覺得心情開朗：</td>
                  <td width="705">
					<input name="AD_CH" type="radio" class="form_fix" id="AD_CH1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_CH']);?>/>不覺得
					<input name="AD_CH" type="radio" class="form_fix" id="AD_CH2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_CH']);?>/>平靜
					<input name="AD_CH" type="radio" class="form_fix" id="AD_CH3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_CH']);?>/>會
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">入住後覺得有人關心：</td>
                  <td width="705">
					<input name="AD_CR" type="radio" class="form_fix" id="AD_CR1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_CR']);?>/>不覺得
					<input name="AD_CR" type="radio" class="form_fix" id="AD_CR2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_CR']);?>/>偶爾
					<input name="AD_CR" type="radio" class="form_fix" id="AD_CR3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_CR']);?>/>會
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">入住後覺得受人尊重：</td>
                  <td width="705">
					<input name="AD_RS" type="radio" class="form_fix" id="AD_RS1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_RS']);?>/>不覺得
					<input name="AD_RS" type="radio" class="form_fix" id="AD_RS2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_RS']);?>/>偶爾
					<input name="AD_RS" type="radio" class="form_fix" id="AD_RS3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_RS']);?>/>會
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">入住後覺得日子過的很充實：</td>
                  <td width="705">
					<input name="AD_EH" type="radio" class="form_fix" id="AD_EH1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_EH']);?>/>不覺得
					<input name="AD_EH" type="radio" class="form_fix" id="AD_EH2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_EH']);?>/>偶爾
					<input name="AD_EH" type="radio" class="form_fix" id="AD_EH3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_EH']);?>/>會
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">入住後覺得有人隨時訴說煩惱：</td>
                  <td width="705">
					<input name="AD_TR" type="radio" class="form_fix" id="AD_TR1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_TR']);?>/>不覺得
					<input name="AD_TR" type="radio" class="form_fix" id="AD_TR2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_TR']);?>/>偶爾
					<input name="AD_TR" type="radio" class="form_fix" id="AD_TR3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_TR']);?>/>會
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">入住後覺得生活很無奈：</td>
                  <td width="705">
					<input name="AD_HL" type="radio" class="form_fix" id="AD_HL1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_HL']);?>/>不覺得
					<input name="AD_HL" type="radio" class="form_fix" id="AD_HL2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_HL']);?>/>偶爾
					<input name="AD_HL" type="radio" class="form_fix" id="AD_HL3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_HL']);?>/>會
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">入住後覺得是別人的負擔：</td>
                  <td width="705">
					<input name="AD_BU" type="radio" class="form_fix" id="AD_BU1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_BU']);?>/>不覺得
					<input name="AD_BU" type="radio" class="form_fix" id="AD_BU2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_BU']);?>/>偶爾
					<input name="AD_BU" type="radio" class="form_fix" id="AD_BU3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_BU']);?>/>會
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">入住後覺得生活氣氛很輕鬆：</td>
                  <td width="705">
					<input name="AD_RX" type="radio" class="form_fix" id="AD_RX1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_RX']);?>/>不覺得
					<input name="AD_RX" type="radio" class="form_fix" id="AD_RX2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_RX']);?>/>偶爾
					<input name="AD_RX" type="radio" class="form_fix" id="AD_RX3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_RX']);?>/>會
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">入住後覺得生活很有尊嚴：</td>
                  <td width="705">
					<input name="AD_DG" type="radio" class="form_fix" id="AD_DG1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_DG']);?>/>不覺得
					<input name="AD_DG" type="radio" class="form_fix" id="AD_DG2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_DG']);?>/>偶爾
					<input name="AD_DG" type="radio" class="form_fix" id="AD_DG3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_DG']);?>/>會
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">會與其他認識的住民打招呼：</td>
                  <td width="705">
					<input name="AD_GE" type="radio" class="form_fix" id="AD_GE1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_GE']);?>/>不覺得
					<input name="AD_GE" type="radio" class="form_fix" id="AD_GE2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_GE']);?>/>偶爾
					<input name="AD_GE" type="radio" class="form_fix" id="AD_GE3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_GE']);?>/>會
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">會跟其他住民交談：</td>
                  <td width="705">
					<input name="AD_Talk" type="radio" class="form_fix" id="AD_Talk1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_Talk']);?>/>不會
					<input name="AD_Talk" type="radio" class="form_fix" id="AD_Talk2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_Talk']);?>/>偶爾
					<input name="AD_Talk" type="radio" class="form_fix" id="AD_Talk3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_Talk']);?>/>會
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">在機構是否有聊得來的朋友：</td>
                  <td width="705">
					<input name="AD_FD" type="radio" class="form_fix" id="AD_FD1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_FD']);?>/>沒有
					<input name="AD_FD" type="radio" class="form_fix" id="AD_FD2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_FD']);?>/>有
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">是否會參加機構舉辦的活動：</td>
                  <td width="705">
					<input name="AD_AC" type="radio" class="form_fix" id="AD_AC1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_AC']);?>/>不會
					<input name="AD_AC" type="radio" class="form_fix" id="AD_AC2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_AC']);?>/>偶爾
					<input name="AD_AC" type="radio" class="form_fix" id="AD_AC3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_AC']);?>/>會
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">會參加哪些活動（複選）：</td>
                  <td width="705">
					<input type="checkbox" name="AD_Act[]" id="AD_Act1" value="看電視" <?php echo ckbox('看電視',$AD_Act);?>>看電視
					<input type="checkbox" name="AD_Act[]" id="AD_Act2" value="聊天" <?php echo ckbox('聊天',$AD_Act);?>>聊天
					<input type="checkbox" name="AD_Act[]" id="AD_Act3" value="聽音樂" <?php echo ckbox('聽音樂',$AD_Act);?>>聽音樂
					<input type="checkbox" name="AD_Act[]" id="AD_Act4" value="唱歌" <?php echo ckbox('唱歌',$AD_Act);?>>唱歌
					<input type="checkbox" name="AD_Act[]" id="AD_Act5" value="閱讀" <?php echo ckbox('閱讀',$AD_Act);?>>閱讀
					<input type="checkbox" name="AD_Act[]" id="AD_Act6" value="下棋或打牌" <?php echo ckbox('下棋或打牌',$AD_Act);?>>下棋或打牌
					<input type="checkbox" name="AD_Act[]" id="AD_Act7" value="宗教活動" <?php echo ckbox('宗教活動',$AD_Act);?>>宗教活動
					<input type="checkbox" name="AD_Act[]" id="AD_Act8" value="慶生會" <?php echo ckbox('慶生會',$AD_Act);?>>慶生會
					<input type="checkbox" name="AD_Act[]" id="AD_Act9" value="志工關懷" <?php echo ckbox('志工關懷',$AD_Act);?>>志工關懷
					<input type="checkbox" name="AD_Act[]" id="AD_Act10" value="其他" <?php echo ckbox('其他',$AD_Act);?>>其他
					<input type="checkbox" name="AD_Act[]" id="AD_Act11" value="從不" <?php echo ckbox('從不',$AD_Act);?>>從不
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">與機構外的朋友是否有連絡：</td>
                  <td width="705">
					<input name="AD_CO" type="radio" class="form_fix" id="AD_CO1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_CO']);?>/>沒有
					<input name="AD_CO" type="radio" class="form_fix" id="AD_CO2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_CO']);?>/>偶爾
					<input name="AD_CO" type="radio" class="form_fix" id="AD_CO3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_CO']);?>/>經常
					<input name="AD_CO" type="radio" class="form_fix" id="AD_CO4" value="4" <?php echo ckRadio('4',$row_form[0]['AD_CO']);?>/>從不
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">會與家人通電話：</td>
                  <td width="705">
					<input name="AD_Phone" type="radio" class="form_fix" id="AD_Phone1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_Phone']);?>/>每天
					<input name="AD_Phone" type="radio" class="form_fix" id="AD_Phone2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_Phone']);?>/>常常
					<input name="AD_Phone" type="radio" class="form_fix" id="AD_Phone3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_Phone']);?>/>偶爾
					<input name="AD_Phone" type="radio" class="form_fix" id="AD_Phone4" value="4" <?php echo ckRadio('4',$row_form[0]['AD_Phone']);?>/>很少
					<input name="AD_Phone" type="radio" class="form_fix" id="AD_Phone5" value="5" <?php echo ckRadio('5',$row_form[0]['AD_Phone']);?>/>從不
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">親友探視時都做些什麼（複選）：</td>
                  <td width="705">
					<input type="checkbox" name="AD_FA[]" id="AD_FA1" value="陪伴" <?php echo ckbox('陪伴',$AD_FA);?>>陪伴
					<input type="checkbox" name="AD_FA[]" id="AD_FA2" value="外出活動" <?php echo ckbox('外出活動',$AD_FA);?>>外出活動
					<input type="checkbox" name="AD_FA[]" id="AD_FA3" value="告知家務事" <?php echo ckbox('告知家務事',$AD_FA);?>>告知家務事
					<input type="checkbox" name="AD_FA[]" id="AD_FA4" value="帶食物或衣服" <?php echo ckbox('帶食物或衣服',$AD_FA);?>>帶食物或衣服
					<input type="checkbox" name="AD_FA[]" id="AD_FA5" value="其他" <?php echo ckbox('其他',$AD_FA);?>>其他
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">是否認識這裡的工作人員：</td>
                  <td width="705">
					<input name="AD_ST" type="radio" class="form_fix" id="AD_ST1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_ST']);?>/>認識
					<input name="AD_ST" type="radio" class="form_fix" id="AD_ST2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_ST']);?>/>不認識
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">會與這裡的工作人員聊天：</td>
                  <td width="705">
					<input name="AD_Chat" type="radio" class="form_fix" id="AD_Chat1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_Chat']);?>/>不會
					<input name="AD_Chat" type="radio" class="form_fix" id="AD_Chat2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_Chat']);?>/>偶爾
					<input name="AD_Chat" type="radio" class="form_fix" id="AD_Chat3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_Chat']);?>/>會
                  </td>  
                </tr>
				<tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">最大活動範圍：</td>
                  <td width="705">
					<input name="AD_RG" type="radio" class="form_fix" id="AD_RG1" value="1" <?php echo ckRadio('1',$row_form[0]['AD_RG']);?>/>住室
					<input name="AD_RG" type="radio" class="form_fix" id="AD_RG2" value="2" <?php echo ckRadio('2',$row_form[0]['AD_RG']);?>/>公共空間
					<input name="AD_RG" type="radio" class="form_fix" id="AD_RG3" value="3" <?php echo ckRadio('3',$row_form[0]['AD_RG']);?>/>戶外
                  </td>  
                </tr>
				<tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="110" align="right" class="content">護理人員：</td>
                  <td width="705">
					<input name="AD_NS"  id="AD_NS" type="text" class="content" size="15" value="<?php echo $row_form[0]['AD_NS'];?>"/>
                  </td>  
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>
			           <input name="mybtn" type="submit" class="form_fix" id="mybtn"  value="確定送出"  /> 
 		               <input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>
		          </td>
                </tr>    
	</table>
	</form>              
	</td>
</tr>

</table>
          </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>