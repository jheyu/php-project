<?php
include_once '../public/web_function.php';
include_once '../public/mem_check.php';
	
	/*
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}
	*/	
	
	$action = $_REQUEST["action"];
	
	switch ($action) {

		case "new":	

			$pain_ID = $objDB->GetMaxID('pain_ID','pain',3);
			$RS_ID = $_POST["RS_ID"];	
		    
			$pain_awareness = $_POST["pain_awareness"];

            $reaction = $_POST["pain_reaction"];
            $pain_reaction = implode(",", $reaction);

            $part = $_POST["pain_part"];
            $pain_part = implode(",", $part);

            $natrue = $_POST["pain_natrue"];
            $pain_natrue = implode(",", $natrue);

            $pain_most = $_POST["pain_most"];
            $pain_light = $_POST["pain_light"];
            $pain_endure = $_POST["pain_endure"];
            $pain_start = $_POST["pain_start"];
            $pain_highest = $_POST["pain_highest"];
            $pain_frequency = $_POST["pain_frequency"];
            $pain_continueTime = $_POST["pain_continueTime"];
            $pain_continueMinute = $_POST["pain_continueMinute"];

            $serious = $_POST["pain_serious"];
            $pain_serious = implode(",", $serious);

            $follow = $_POST["pain_follow"];
            $pain_follow = implode(",", $follow);

            $affect = $_POST["pain_affect"];
            $pain_affect = implode(",", $affect);

            $ease = $_POST["pain_ease"];
            $pain_ease = implode(",", $ease);

			$pain_Date = date("Y-m-d H:i:s");

			$sql = "insert into pain (RS_ID,pain_ID,pain_awareness,pain_reaction,pain_part,pain_natrue,pain_most,pain_light,pain_endure,pain_start,pain_highest,pain_frequency,pain_continueTime,pain_continueMinute,pain_serious,pain_follow,pain_affect,pain_ease,pain_Date) values ('$RS_ID','$pain_ID','$pain_awareness','$pain_reaction','$pain_part','$pain_natrue','$pain_most','$pain_light','$pain_endure','$pain_start','$pain_highest','$pain_frequency','$pain_continueTime','$pain_continueMinute','$pain_serious','$pain_follow','$pain_affect','$pain_ease','$pain_Date')";
			
			$objDB->Execute($sql);

?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script language="javascript">
alert('新增成功!');
location.href='layout.php?t=overview&RS_ID=<?php echo $RS_ID;?>';
</script>
<?php
		break;
		
		case "mdy":
		    
		    $rs = $objDB->Recordset("SELECT * FROM pain WHERE RS_ID = '$RS_ID' AND pain_ID = '$pain_ID'");
			$row = $objDB->GetRows($rs);

			$pain_ID = $_POST["pain_ID"];
			$RS_ID = $_POST["RS_ID"];	
		    
			$pain_awareness = $_POST["pain_awareness"];

            $reaction = $_POST["pain_reaction"];
            $pain_reaction = implode(",", $reaction);

            $part = $_POST["pain_part"];
            $pain_part = implode(",", $part);

            $natrue = $_POST["pain_natrue"];
            $pain_natrue = implode(",", $natrue);

            $most = $_POST["pain_most"];
            $pain_most = implode(",", $most);

            $pain_most = ConvertToArray($_POST["pain_most1"],$_POST["pain_most2"],$_POST["pain_most2"]);

            $pain_light = $_POST["pain_light"];
            $pain_endure = $_POST["pain_endure"];
            $pain_start = $_POST["pain_start"];
            $pain_highest = $_POST["pain_highest"];
            $pain_frequency = $_POST["pain_frequency"];
            $pain_continueTime = $_POST["pain_continueTime"];
            $pain_continueMinute = $_POST["pain_continueMinute"];
            
            $serious = $_POST["pain_serious"];
            $pain_serious = implode(",", $serious);
  
            $follow = $_POST["pain_follow"];
            $pain_follow = implode(",", $follow);

            $affect = $_POST["pain_affect"];
            $pain_affect = implode(",", $affect);

            $ease = $_POST["pain_ease"];
            $pain_ease = implode(",", $ease);

			$pain_Date = date("Y-m-d H:i:s");
			
			if($row[0]['pain_awareness'] == $pain_awareness && $row[0]['pain_reaction'] == $pain_reaction && $row[0]['pain_part'] == $pain_part && $row[0]['pain_natrue'] == $pain_natrue && $row[0]['pain_most'] == $pain_most && $row[0]['pain_light'] == $pain_light && $row[0]['pain_endure'] == $pain_endure && $row[0]['pain_highest'] == $pain_highest && $row[0]['pain_frequency'] == $pain_frequency && $row[0]['pain_continueTime'] == $pain_continueTime && $row[0]['pain_continueMinute'] == $pain_continueMinute && $row[0]['pain_serious'] == $pain_serious && $row[0]['pain_follow'] == $pain_follow && $row[0]['pain_affect'] == $pain_affect && $row[0]['pain_ease'] == $pain_ease)
			{
				$sql = "UPDATE pain SET pain_awareness = '$pain_awareness', pain_reaction = '$pain_reaction', pain_part = '$pain_part', pain_natrue = '$pain_natrue', pain_most = '$pain_most', pain_light = '$pain_light', pain_endure = '$pain_endure', pain_highest = '$pain_highest', pain_frequency = '$pain_frequency', pain_continueTime = '$pain_continueTime', pain_continueMinute = '$pain_continueMinute', pain_serious = '$pain_serious', pain_follow = '$pain_follow', pain_affect = '$pain_affect', pain_ease = '$pain_ease', pain_Date = '$pain_Date'  WHERE  pain_ID = '$pain_ID'";
			}else{
				$pain_ID = $objDB->GetMaxID('pain_ID','pain',3);

				$sql = "insert into pain (RS_ID,pain_ID,pain_awareness,pain_reaction,pain_part,pain_natrue,pain_most,pain_light,pain_endure,pain_start,pain_highest,pain_frequency,pain_continueTime,pain_continueMinute,pain_serious,pain_follow,pain_affect,pain_ease,pain_Date) values ('$RS_ID','$pain_ID','$pain_awareness','$pain_reaction','$pain_part','$pain_natrue','$pain_most','$pain_light','$pain_endure','$pain_start','$pain_highest','$pain_frequency','$pain_continueTime','$pain_continueMinute','$pain_serious','$pain_follow','$pain_affect','$pain_ease','$pain_Date')";
				//echo "else<br>";
			}
			$objDB->Execute($sql);
		
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />        
<script language="javascript">
alert('修改成功!');
location.href='layout.php?t=overview&RS_ID=<?php echo $RS_ID;?>';
</script>	
<?php
		break;
	
		case "del":
				$pain_ID = quotes(trim($_REQUEST["pain_ID"]));
			
				$sql = "delete from pain where pain_ID ='$pain_ID'";
				$objDB->Execute($sql);
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script language="javascript">
alert('資料刪除完畢!');
location.href="admin.php";
</script>
<?php
	break;
			
	}		
?>