<?php 
	
	$rs_form = $objDB->Recordset("SELECT * FROM gcs WHERE RS_ID = '$RS_ID' ORDER BY GCS_Date DESC");
	$row_form = $objDB->GetRows($rs_form);
	
	$GCS_ID = $row_form[0]['GCS_ID'];
	
?>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script>
var score0 = <?php echo $row_form[0]['GCS_Eye']?>;
var score1 = <?php echo $row_form[0]['GCS_Verbal']?>;
var score2 = <?php echo $row_form[0]['GCS_Motor']?>;
var total = 0;

var level ;

var caculate = function(){
	  
     total = score0 + score1 + score2 ;
	 
	 if(total==15){
	 	level = "(意識清楚)";
	 }else if(total >= 13 && total <=14){
	 	level = "(輕度昏迷)";
	 }else if(total >=9 && total <= 12){
	 	level = "(中度昏迷)";
	 }else{
	 	level = "(重度昏迷)";
	 }
	 	$("#SCORE").text(total);
		$("#LEVEL").text(level); 
 }
 $(document).ready(function(){
	  
	$("input[name='GCS_Eye']").click(function(){
		var GCS_Eye = $("input[name='GCS_Eye']:checked").val();
		score0 = parseInt(GCS_Eye);
		caculate();		
	})
	$("input[name='GCS_Verbal']").click(function(){
		var GCS_Verbal = $("input[name='GCS_Verbal']:checked").val();
		score1 = parseInt(GCS_Verbal);		
		caculate();		
	})
	$("input[name='GCS_Motor']").click(function(){
		var GCS_Motor = $("input[name='GCS_Motor']:checked").val();
		score2 = parseInt(GCS_Motor);		
		caculate();		
	})	
	
	 
 	$("#mybtn").click(function(){
		$("#GCS_Score").val(total);
	
		$("form#form1").submit();
		
	})
 	
 })

</script>

<tr>
	<td height="15"></td>
</tr>           
<tr>
	<td style="font-size: 13pt"><strong>GCS昏迷指數</strong>
	<?php echo "(".$row_form[0]['GCS_Date'] .")";?>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	分數:<label id="SCORE"><?php echo $row_form[0]['GCS_Score'];?></label><label id="LEVEL">
	<?php if($row_form[0]['GCS_Score'] == 15){
		echo "(意識清楚)";
	}else if($row_form[0]['GCS_Score'] >=13 && $row_form[0]['GCS_Score'] <=14){
		echo "(輕度昏迷)";
	}else if($row_form[0]['GCS_Score'] >=9 && $row_form[0]['GCS_Score'] <=12){
		echo "(中度昏迷)";
	}else{
		echo "(重度昏迷)";
	}
	?></label></td>            
</tr> 
<tr>
	<td height="5"></td>
</tr>
<tr>
	<td ><img src="../images/blueline.jpg" /></td>
</tr> 
	<tr>
	<td height="5"></td>
</tr>             
<tr>         
	<td>   
	<form name="form1" id="form1" method="post" action="gcs_process.php"/>
	<input type="hidden" name="action" id="action" value="mdy"/>                           
	<input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>"  />
    <input type="hidden" name="GCS_ID" id="GCS_ID" value="<?php echo $GCS_ID;?>" />
	<input type="hidden" name="GCS_Score" id="GCS_Score" />              
	<table>                
	<tr>
		<td height="10"></td>
	</tr>                
	<tr>
		<td width="80" align="right"  class="content">睜眼反應：</td>
		<td width="720">
		<input name="GCS_Eye" type="radio" class="form_fix" id="GCS_Eye4" value="4" <?php echo ckRadio('4',$row_form[0]['GCS_Eye']);?> />自動睜眼(spontaneous)<br />
		<input name="GCS_Eye" type="radio" class="form_fix" id="GCS_Eye3" value="3" <?php echo ckRadio('3',$row_form[0]['GCS_Eye']);?> />呼喚會睜眼(to speech)<br />
		<input name="GCS_Eye" type="radio" class="form_fix" id="GCS_Eye2" value="2" <?php echo ckRadio('2',$row_form[0]['GCS_Eye']);?> />有刺激或痛楚會睜眼(to pain)<br />
		<input name="GCS_Eye" type="radio" class="form_fix" id="GCS_Eye1" value="1" <?php echo ckRadio('1',$row_form[0]['GCS_Eye']);?> />對刺激無反應(none)
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="80" align="right"  class="content">說話反應：</td>
		<td width="720">
		<input name="GCS_Verbal" type="radio" class="form_fix" id="GCS_Verbal5" value="5" <?php echo ckRadio('5',$row_form[0]['GCS_Verbal']);?> />說話有條理(oriented)<br />
		<input name="GCS_Verbal" type="radio" class="form_fix" id="GCS_Verbal4" value="4" <?php echo ckRadio('4',$row_form[0]['GCS_Verbal']);?> />可應答，但有答非所問的情況(confused)<br />
		<input name="GCS_Verbal" type="radio" class="form_fix" id="GCS_Verbal3" value="3" <?php echo ckRadio('3',$row_form[0]['GCS_Verbal']);?> />可說出單字(inappropriate words)<br />
		<input name="GCS_Verbal" type="radio" class="form_fix" id="GCS_Verbal2" value="2" <?php echo ckRadio('2',$row_form[0]['GCS_Verbal']);?> />可發出呻吟(unintelligible sounds)<br />
		<input name="GCS_Verbal" type="radio" class="form_fix" id="GCS_Verbal1" value="1" <?php echo ckRadio('1',$row_form[0]['GCS_Verbal']);?> />無任何反應(none)
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="80" align="right"  class="content">運動反應：</td>
		<td width="720">
		<input name="GCS_Motor" type="radio" class="form_fix" id="GCS_Motor6" value="6" <?php echo ckRadio('6',$row_form[0]['GCS_Motor']);?> />可依指令動作(obey commands)<br />
		<input name="GCS_Motor" type="radio" class="form_fix" id="GCS_Motor5" value="5" <?php echo ckRadio('5',$row_form[0]['GCS_Motor']);?> />施以刺激時，可定位出疼痛位置(localize)<br />
		<input name="GCS_Motor" type="radio" class="form_fix" id="GCS_Motor4" value="4" <?php echo ckRadio('4',$row_form[0]['GCS_Motor']);?> />對疼痛刺激有反應，肢體會回縮(withdrawal)<br />
		<input name="GCS_Motor" type="radio" class="form_fix" id="GCS_Motor4" value="3" <?php echo ckRadio('3',$row_form[0]['GCS_Motor']);?> />對疼痛刺激有反應，肢體會彎曲(decorticate flexion)<br />
		<input name="GCS_Motor" type="radio" class="form_fix" id="GCS_Motor4" value="2" <?php echo ckRadio('2',$row_form[0]['GCS_Motor']);?> />對疼痛刺激有反應，肢體會伸直(decerebrate extension)<br />
		<input name="GCS_Motor" type="radio" class="form_fix" id="GCS_Motor4" value="1" <?php echo ckRadio('1',$row_form[0]['GCS_Motor']);?> />無任何反應(no response)
		</td>
	</tr>
                     
	<tr>
		<td align="right" class="content">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
			<input name="mybtn" type="button" class="form_fix" id="mybtn"  value="確定送出"  /> 
 		 <input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>
		</td>
	</tr>                
	</table>
	</form>              
	</td>
</tr>
