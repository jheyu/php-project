<?php 
include("../public/mem_check.php");
include("../public/web_function.php");
	/*
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}
	*/
	if(is_numeric(quotes($_GET['RS_ID']))){
		 $RS_ID = quotes($_GET['RS_ID']);
	}else{
		 ?>
     <script language="javascript">		
		location.href='../index.php';
	 </script>	
         <?php
	}	
	$sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
	$rs = $objDB->Recordset($sql);
	$row = $objDB->GetRows($rs);	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.datepicker.min.js"></script>

<script>
$(document).ready(function(){
    $(".date-pick" ).datepicker({ 
		dateFormat: 'yy-mm-dd', 
		showOn: "button",
		buttonImage: "../js/calendar.png",
		buttonImageOnly: true
	});	
 	$("#mybtn").click(function(){	
			$("form#form1").submit();
		}		
	)	
})

</script>

<link type="text/css" href="../css/ui-darkness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />	
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<!-- header starting point -->
	<?php include("../include/header.php");?>
	<!-- header ending point -->    
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?>
        <!--menu ending point-->          
        </td>            
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top">
        <table width="830" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="30" class="content">專業照護 > 評估量表 &gt; 新增</td>
              </tr>
              <tr>
                  <td height="10"></td>
              </tr>  
              <tr>
                <td height="10">
                <span class="form_title">
                     <input  type="button" class="content" value="回上一頁" onclick="MM_goToURL('parent','layout.php?t=bsrs&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>
                </span></td>
              </tr>   
              <tr>
                  <td height="10"></td>
              </tr>                      
             <tr>
              	<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>   
             </tr>   
             <tr>
                <td height="30"></td>
              </tr>           
              
              <tr>
              		<td style="font-size: 13pt"><strong>護理之家簡易自殺意念評估量表</strong>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  </td>            
              </tr> 
              <tr>
                <td height="5"></td>
              </tr>
              <tr>
              	  <td ><img src="../images/blueline.jpg" /></td>
              </tr> 
               <tr>
                <td height="5"></td>
              </tr>   
            <tr>         
              <td>    
              <!--管理員管理startinging-->              
              <form name="form1" id="form1" method="post" action="bsrs_process.php" />
			   <input type="hidden" name="action" id="action" value="new"/>              
              <input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>" />             
             <table>
                <tr>
                   <td height="10"></td> 
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">日期：</td>
                  <td width="705">
					<input name="BS_Date"  id="BS_Date" type="text" class="txt date-pick" style="  width:80px;"  value="<?php  echo date("Y-m-d"); ?>"  />
                  </td>
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="300" align="right" class="content">睡眠困難，譬如難以入睡、易醒或早醒：</td>
                  <td width="705">
					<input name="BS_Sleep" type="radio" class="form_fix" id="BS_Sleep1" value="0" />完全沒有
					<input name="BS_Sleep" type="radio" class="form_fix" id="BS_Sleep2" value="1"/>輕微
					<input name="BS_Sleep" type="radio" class="form_fix" id="BS_Sleep3" value="2"/>中等程度
					<input name="BS_Sleep" type="radio" class="form_fix" id="BS_Sleep4" value="3"/>厲害
					<input name="BS_Sleep" type="radio" class="form_fix" id="BS_Sleep5" value="4"/>非常厲害
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">感覺緊張不安：</td>
                  <td width="705">
					<input name="BS_NV" type="radio" class="form_fix" id="BS_NV1" value="0" />完全沒有
					<input name="BS_NV" type="radio" class="form_fix" id="BS_NV2" value="1"/>輕微
					<input name="BS_NV" type="radio" class="form_fix" id="BS_NV3" value="2"/>中等程度
					<input name="BS_NV" type="radio" class="form_fix" id="BS_NV4" value="3"/>厲害
					<input name="BS_NV" type="radio" class="form_fix" id="BS_NV5" value="4"/>非常厲害
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">覺得容易苦惱或動怒：</td>
                  <td width="705">
					<input name="BS_AG" type="radio" class="form_fix" id="BS_AG1" value="0" />完全沒有
					<input name="BS_AG" type="radio" class="form_fix" id="BS_AG2" value="1"/>輕微
					<input name="BS_AG" type="radio" class="form_fix" id="BS_AG3" value="2"/>中等程度
					<input name="BS_AG" type="radio" class="form_fix" id="BS_AG4" value="3"/>厲害
					<input name="BS_AG" type="radio" class="form_fix" id="BS_AG5" value="4"/>非常厲害
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">感覺憂鬱、心情低落：</td>
                  <td width="705">
					<input name="BS_GM" type="radio" class="form_fix" id="BS_GM1" value="0" />完全沒有
					<input name="BS_GM" type="radio" class="form_fix" id="BS_GM2" value="1"/>輕微
					<input name="BS_GM" type="radio" class="form_fix" id="BS_GM3" value="2"/>中等程度
					<input name="BS_GM" type="radio" class="form_fix" id="BS_GM4" value="3"/>厲害
					<input name="BS_GM" type="radio" class="form_fix" id="BS_GM5" value="4"/>非常厲害
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">覺得比不上別人：</td>
                  <td width="705">
					<input name="BS_CM" type="radio" class="form_fix" id="BS_CM1" value="0" />完全沒有
					<input name="BS_CM" type="radio" class="form_fix" id="BS_CM2" value="1"/>輕微
					<input name="BS_CM" type="radio" class="form_fix" id="BS_CM3" value="2"/>中等程度
					<input name="BS_CM" type="radio" class="form_fix" id="BS_CM4" value="3"/>厲害
					<input name="BS_CM" type="radio" class="form_fix" id="BS_CM5" value="4"/>非常厲害
                  </td>  
                </tr>
				 <tr>
                  <td height="10"></td>
                </tr>
				<tr>
                  <td width="110" align="right" class="content">有自殺的想法：</td>
                  <td width="705">
					<input name="BS_SU" type="radio" class="form_fix" id="BS_SU1" value="0" />完全沒有
					<input name="BS_SU" type="radio" class="form_fix" id="BS_SU2" value="1"/>輕微
					<input name="BS_SU" type="radio" class="form_fix" id="BS_SU3" value="2"/>中等程度
					<input name="BS_SU" type="radio" class="form_fix" id="BS_SU4" value="3"/>厲害
					<input name="BS_SU" type="radio" class="form_fix" id="BS_SU5" value="4"/>非常厲害
                  </td>  
                </tr>
				<tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="110" align="right" class="content">護理人員：</td>
                  <td width="705">
					<input name="BS_NS"  id="BS_NS" type="text" class="content" size="15" value=""/>
                  </td>  
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>
			           <input name="mybtn" type="submit" class="form_fix" id="mybtn"  value="確定送出"  /> 
 		               <input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>
		          </td>
                </tr>    
             </table>
              </form>
              <!--管理員管理 ending-->              
              </td>
            </tr>
            <tr>
			
			<td >
			分數說明：完全沒有：0分；輕微：1分；中等程度：2分；厲害：3分；非常厲害：4分<br/>
			1至5題之總分：<br/>
			0~5分：身心適應狀況良好。<br/>
			6-9分：輕度情緒困擾，建議找家人或朋友談談，抒發情緒。<br/>
			10~14分：中度情緒困擾，建議尋求心理諮商或接受專業諮詢。<br/>
			15分以上：重度情緒困擾，需高關懷，建議尋求專業輔導或精神治療。<br/>	
			<strong>※附加題：有自殺的想法：<br/>
			此題為單項評分，2分以上（中等程度）時，即建議尋求專業輔導或精神科治療。</strong>

			</td>
			</tr>
          </table>
          </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>
