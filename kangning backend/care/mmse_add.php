<?php 
include("../public/mem_check.php");
include("../public/web_function.php");
	/*
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}
  */
	if(is_numeric(quotes($_GET['RS_ID']))){
		 $RS_ID = quotes($_GET['RS_ID']);
	}else{
		 ?>
     <script language="javascript">		
		location.href='../index.php';
	 </script>	
         <?php
	}	
	
	$sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
	$rs = $objDB->Recordset($sql);
	$row = $objDB->GetRows($rs);	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>

<script language="Javascript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script> 

var score0 = 0;
var score1 = 0;
var score2 = 0;
var score3 = 0;
var score4 = 0;
var score5 = 0;
var score6 = 0;
var score7 = 0;
var score8 = 0;
var score9 = 0;
var score10 = 0;

var total = 0;
var level;

var caculate = function(){

total = score0 + score1 + score2 + score3 + 
        score4 + score5 + score6 + score7 +
        score8 + score9 + score10;
   
   if(total < 15){
    level = "(重度心智功能障礙)";
   }else if(total > 15 && total <= 25){
    level = "(中度心智功能障礙)";
   }else{
    level = "(心智功能完整)";
   }
    $("#SCORE").text(total);
    $("#LEVEL").text(level); 
}
$(document).ready(function(){    
    $("input[name='MS_Time[]']").click(function(){
      /*
      var MS_Time = $("input[name='MS_Time[]']:checked").val();
      score0 = parseInt(MS_Time);
      */
      //alert($("input[type=checkbox]:checked").length);
      score0 = $("input[name='MS_Time[]']:checked").length;
      caculate();   
    });

    $("input[name='MS_Where[]']").click(function(){

      score1 = $("input[name='MS_Where[]']:checked").length;
      caculate();  

    });

    $("input[name='MS_Repeat[]']").click(function(){

      score2 = $("input[name='MS_Repeat[]']:checked").length;
      caculate();  
       
    });

    $("input[name='MS_Calculate[]']").click(function(){

      score3 = $("input[name='MS_Calculate[]']:checked").length;
      caculate();  
       
    });

    $("input[name='MS_Remember[]']").click(function(){

      score4 = $("input[name='MS_Remember[]']:checked").length;
      caculate();  
       
    });

    $("input[name='MS_What[]']").click(function(){

      score5 = $("input[name='MS_What[]']:checked").length;
      caculate();  
       
    });

    $("input[name='MS_Study[]']").click(function(){

      score6 = $("input[name='MS_Study[]']:checked").length;
      caculate();  
       
    });

    $("input[name='MS_Action[]']").click(function(){

      score7 = $("input[name='MS_Action[]']:checked").length;
      caculate();  
       
    });

    $("input[name='MS_Read[]']").click(function(){

      score8 = $("input[name='MS_Read[]']:checked").length;
      caculate();  
       
    });

    $("input[name='MS_Write[]']").click(function(){

      score9 = $("input[name='MS_Write[]']:checked").length;
      caculate();  
       
    });

    $("input[name='MS_Graph[]']").click(function(){

      score10 = $("input[name='MS_Graph[]']:checked").length;
      caculate();  
       
    });
    
    $("#mybtn").click(function(){
       $("#MS_Score").val(total);
  
       $("form#form1").submit();
    });

});
</script>
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<!-- header starting point -->
	<?php include("../include/header.php");?>
	<!-- header ending point -->    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1000" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="160" valign="top" background="../images/bkline.jpg">
          	<!--menu starting point-->
            <?php include("../include/menu.php");?>
            <!--menu ending point-->          
          </td>
            
          <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
          <td width="930" valign="top">
          <table width="830" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="30" class="content">專業照護 > 評估與記錄 &gt; 新增</td>
              </tr>
              <tr>
                  <td height="10"></td>
              </tr>   
              <tr>
                <td height="10">
                <span class="form_title">
                     <input name="search" type="button" class="content" id="search" value="回上一頁" onclick="MM_goToURL('parent','layout.php?t=mmse&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>
                </span></td>
              </tr>   
              <tr>
                  <td height="10"></td>
              </tr>                     
             <tr>
              	<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>   
             </tr>   
             <tr>
                <td height="30"></td>
              </tr>           
              
              <tr>
              		<td style="font-size: 13pt"><strong>簡易智能量表評估</strong>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  分數:<label id="SCORE"> </label><label id="LEVEL"></label>
                  </td>            
              </tr> 
              <tr>
                <td height="5"></td>
              </tr>
              <tr>
              	  <td ><img src="../images/blueline.jpg" /></td>
              </tr> 
               <tr>
                <td height="5"></td>
              </tr>   
            <tr>         
              <td>    
              <!--管理員管理startinging-->              
              <form name="form1" id="form1" method="post" action="mmse_process.php" />
			        <input type="hidden" name="action" id="action" value="new"/>
              <input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>" />
              <input type="hidden" name="MS_Score" id="MS_Score" />
              <!--
              <table width="825" border="0">
              	  <tr>
                  <td width="110" align="right" class="content"><input name="FA_Disable" type="checkbox" class="form_fix" id="FA_Disable" value="Y"/>不適用&nbsp;&nbsp;&nbsp;原因</td>
                  <td width="705"><input name="FA_Reason" type="text" class="form_fix" id="FA_Reason" style="width:150px"/></td>    
                </tr>
                </table>
              -->  
                <table>
                <tr>
                  <td height="10"></td>
                </tr>
                
               <tr>
       <td width="110" align="right"  class="content">項目一：時間</td>
       <td width="705">
       <input name="MS_Time[]" type="checkbox" class="form_fix" id="MS_Time1" value="1" <?php echo ckbox('1',$MS_Time);?>/>現在是民國哪一年?
       <input name="MS_Time[]" type="checkbox" class="form_fix" id="MS_Time2" value="2" <?php echo ckbox('2',$MS_Time);?>/>現在是什麼季節?
       <input name="MS_Time[]" type="checkbox" class="form_fix" id="MS_Time3" value="3" <?php echo ckbox('3',$MS_Time);?>/>今天幾號?
       <input name="MS_Time[]" type="checkbox" class="form_fix" id="MS_Time4" value="4" <?php echo ckbox('4',$MS_Time);?>/>星期幾?
       <input name="MS_Time[]" type="checkbox" class="form_fix" id="MS_Time5" value="5" <?php echo ckbox('5',$MS_Time);?>/>現在幾月? 
       </td>
    </tr>
    <tr>
      <td height="10"></td>
    </tr>
    <tr>
       <td width="110" align="right"  class="content">項目二：地方</td>
       <td width="705">
       <input name="MS_Where[]" type="checkbox" class="form_fix" id="MS_Where1" value="1" <?php echo ckbox('1',$MS_Where);?>/>現在在什麼縣市?
       <input name="MS_Where[]" type="checkbox" class="form_fix" id="MS_Where2" value="2" <?php echo ckbox('2',$MS_Where);?>/>什麼路?
       <input name="MS_Where[]" type="checkbox" class="form_fix" id="MS_Where3" value="3" <?php echo ckbox('3',$MS_Where);?>/>什麼醫院?
       <input name="MS_Where[]" type="checkbox" class="form_fix" id="MS_Where4" value="4" <?php echo ckbox('4',$MS_Where);?>/>幾樓?
       <input name="MS_Where[]" type="checkbox" class="form_fix" id="MS_Where5" value="5" <?php echo ckbox('5',$MS_Where);?>/>什麼科?
       </td>
    </tr>
    <tr>
      <td height="10"></td>
    </tr>
    <tr>
       <td width="110" align="right"  class="content">項目三：訊息登錄</td>
       <td width="705">
       <input name="MS_Repeat[]" type="checkbox" class="form_fix" id="MS_Repeat1" value="1" <?php echo ckbox('1',$MS_Repeat);?>/>紅色
       <input name="MS_Repeat[]" type="checkbox" class="form_fix" id="MS_Repeat2" value="2" <?php echo ckbox('2',$MS_Repeat);?>/>眼鏡
       <input name="MS_Repeat[]" type="checkbox" class="form_fix" id="MS_Repeat3" value="3" <?php echo ckbox('3',$MS_Repeat);?>/>誠實
       </td>
    </tr>
    <tr>
      <td height="10"></td>
    </tr>
    <tr>
       <td width="110" align="right"  class="content">項目四：系列減七</td>
       <td width="705">
       <input name="MS_Calculate[]" type="checkbox" class="form_fix" id="MS_Calculate1" value="1" <?php echo ckbox('1',$MS_Calculate);?>/>93
       <input name="MS_Calculate[]" type="checkbox" class="form_fix" id="MS_Calculate2" value="2" <?php echo ckbox('2',$MS_Calculate);?>/>86
       <input name="MS_Calculate[]" type="checkbox" class="form_fix" id="MS_Calculate3" value="3" <?php echo ckbox('3',$MS_Calculate);?>/>79
       <input name="MS_Calculate[]" type="checkbox" class="form_fix" id="MS_Calculate4" value="4" <?php echo ckbox('4',$MS_Calculate);?>/>72
       <input name="MS_Calculate[]" type="checkbox" class="form_fix" id="MS_Calculate5" value="5" <?php echo ckbox('5',$MS_Calculate);?>/>65
       </td>
    </tr>
    <tr>
      <td height="10"></td>
    </tr>
    <tr>
       <td width="110" align="right"  class="content">項目五：記憶<三分鐘後></td>
       <td width="705">
       <input name="MS_Remember[]" type="checkbox" class="form_fix" id="MS_Remember1" value="1" <?php echo ckbox('1',$MS_Remember);?>/>紅色
       <input name="MS_Remember[]" type="checkbox" class="form_fix" id="MS_Remember2" value="2" <?php echo ckbox('2',$MS_Remember);?>/>眼鏡
       <input name="MS_Remember[]" type="checkbox" class="form_fix" id="MS_Remember3" value="3" <?php echo ckbox('3',$MS_Remember);?>/>誠實
       </td>
    </tr>
    <tr>
      <td height="10"></td>
    </tr>
    <tr>
       <td width="110" align="right"  class="content">項目六：命名</td>
       <td width="705">
       <input name="MS_What[]" type="checkbox" class="form_fix" id="MS_What1" value="1" <?php echo ckbox('1',$MS_What);?>/>手錶
       <input name="MS_What[]" type="checkbox" class="form_fix" id="MS_What2" value="2" <?php echo ckbox('2',$MS_What);?>/>鉛筆
       </td>
    </tr>
    <tr>
      <td height="10"></td>
    </tr>
    <tr>
       <td width="110" align="right"  class="content">項目七：覆誦</td>
       <td width="705">
       <input name="MS_Study[]" type="checkbox" class="form_fix" id="MS_Study1" value="1" <?php echo ckbox('1',$MS_Study);?>/>有來無趣真趣味(台語) 或 有無往來不自在(國語)
       </td>
    </tr>
    <tr>
      <td height="10"></td>
    </tr>
    <tr>
       <td width="110" align="right"  class="content">項目八：口語理解</td>
       <td width="705">
       <input name="MS_Action[]" type="checkbox" class="form_fix" id="MS_Action1" value="1" <?php echo ckbox('1',$MS_Action);?>/>用你的非慣用手拿這張紙
       <input name="MS_Action[]" type="checkbox" class="form_fix" id="MS_Action2" value="2" <?php echo ckbox('2',$MS_Action);?>/>把紙折成對半
       <input name="MS_Action[]" type="checkbox" class="form_fix" id="MS_Action3" value="3" <?php echo ckbox('3',$MS_Action);?>/>再把紙折成對半
       </td>
    </tr>
    <tr>
      <td height="10"></td>
    </tr>
    <tr>
       <td width="110" align="right"  class="content">項目九：閱讀理解</td>
       <td width="705">
       <input name="MS_Read[]" type="checkbox" class="form_fix" id="MS_Read1" value="1" <?php echo ckbox('1',$MS_Read);?>/>閉上眼睛
       </td>
    </tr>
    <tr>
      <td height="10"></td>
    </tr>
    <tr>
       <td width="110" align="right"  class="content">項目十：造句</td>
       <td width="705">
       <input name="MS_Write[]" type="checkbox" class="form_fix" id="MS_Write1" value="1" <?php echo ckbox('1',$MS_Write);?>/>完整的句子
       </td>
    </tr>
    <tr>
      <td height="10"></td>
    </tr>
    <tr>
       <td width="110" align="right"  class="content">項目十一：建構力:</td>
       <td width="705">
       <input name="MS_Graph[]" type="checkbox" class="form_fix" id="MS_Graph1" value="1" <?php echo ckbox('1',$MS_Graph);?>/>請畫一個一樣的圖
       </td>
       <tr>
       <td><img src="Graph.jpg"/></td></tr>
       </tr>

                <tr>
                  <td height="10"></td>
                </tr>
                     
                <tr>
                  <td align="right" class="content">&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>
                      <input name="mybtn" type="button" class="form_fix" id="mybtn"  value="確定送出" />&nbsp; 
                      <input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>&nbsp;
                   </td>
                </tr>                
              </table>
              </form>
              <!--管理員管理 ending-->              
              </td>
            </tr>
            
          </table>
          </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>
