<?php 
include("../public/mem_check.php");
include("../public/web_function.php");
	/*
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}*/
	if(is_numeric(quotes($_GET['RS_ID']))){
		 $RS_ID = quotes($_GET['RS_ID']);
	}else{
		 ?>
     <script language="javascript">		
		location.href='../index.php';
	 </script>	
         <?php
	}	
	
	$sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
	$rs = $objDB->Recordset($sql);
	$row = $objDB->GetRows($rs);	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script type="text/javascript">

$(document).ready(function(){
    $("#mybtn").click(function(){
	
		$("form#form1").submit();
		
	});
});

</script>

<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<!-- header starting point -->
	<?php include("../include/header.php");?>
	<!-- header ending point -->    
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?>
        <!--menu ending point-->          
        </td>            
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top">
        <table width="830" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="30" class="content">專業照護 > 評估與記錄 &gt; 新增</td>
              </tr>
              <tr>
                  <td height="10"></td>
              </tr>      
              <tr>
                <td height="10">
                <span class="form_title">
                     <input name="search" type="button" class="content" id="search" value="回上一頁" onclick="MM_goToURL('parent','layout.php?t=overview&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>
                </span></td>
              </tr>   
              <tr>
                  <td height="10"></td>
              </tr>                    
             <tr>
              	<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>   
             </tr>   
             <tr>
                <td height="30"></td>
              </tr>           
              
              <tr>
              		<td style="font-size: 13pt"><strong>身體評估紀錄</strong>
                <!--  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  分數:<label id="SCORE"> </label><label id="LEVEL"> </label>
                -->  </td>            
              </tr> 
              
              <tr>
                <td height="5"></td>
              </tr>
              <tr>
              	  <td ><img src="../images/blueline.jpg" /></td>
              </tr> 
               <tr>
                <td height="5"></td>
              </tr>   
            <tr>         
              <td>    
              <!--管理員管理startinging-->              
              <form name="form1" id="form1" method="post" action="body_process.php"/>
	<input type="hidden" name="action" id="action" value="new"/>                          
	<input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>"  />
    
	<table>                
	<tr>
		<td height="10"></td>
	</tr>                
	<tr>
		<td width="110" align="right"  class="content">意識狀態：</td>
		<td width="705">
		<input name="body_awareness" type="radio" class="form_fix" id="body_awareness1" value="1"  />清醒
		<input name="body_awareness" type="radio" class="form_fix" id="body_awareness2" value="2"  />躁動
		<input name="body_awareness" type="radio" class="form_fix" id="body_awareness3" value="3"  />混亂	
		<input name="body_awareness" type="radio" class="form_fix" id="body_awareness4" value="4"  />木僵
		<input name="body_awareness" type="radio" class="form_fix" id="body_awareness5" value="5"  />嗜睡
		<input name="body_awareness" type="radio" class="form_fix" id="body_awareness6" value="6"  />昏迷
		<input name="body_awareness" type="radio" class="form_fix" id="body_awareness7" value="7"  />無法評估	
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">認知功能：</td>
		<td width="705">
		<input name="body_cognitive" type="radio" class="form_fix" id="body_cognitive1" value="1"  />正常
		<input name="body_cognitive" type="radio" class="form_fix" id="body_cognitive2" value="2"  />失定向感	
		<input name="body_cognitive" type="radio" class="form_fix" id="body_cognitive3" value="3"  />失定向感(時)
		<input name="body_cognitive" type="radio" class="form_fix" id="body_cognitive4" value="4"  />失定向感(地)
		<input name="body_cognitive" type="radio" class="form_fix" id="body_cognitive5" value="5"  />失智
		<input name="body_cognitive" type="radio" class="form_fix" id="body_cognitive6" value="6"  />無法評估	
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">聽力：</td>
		<td width="705">
		<input name="body_hearing" type="radio" class="form_fix" id="body_hearing1" value="1"  />正常
		<input name="body_hearing" type="radio" class="form_fix" id="body_hearing2" value="2"  />重聽
		<input name="body_hearing" type="radio" class="form_fix" id="body_hearing3" value="3"  />失聰
		<input name="body_hearing" type="radio" class="form_fix" id="body_hearing4" value="4"  />無法判斷	
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">視力：</td>
		<td width="705">
		<input name="body_vision" type="radio" class="form_fix" id="body_vision1" value="1"  />正常
		<input name="body_vision" type="radio" class="form_fix" id="body_vision2" value="2"  />失明
		<input name="body_vision" type="radio" class="form_fix" id="body_vision3" value="3"  />模糊
		<input name="body_vision" type="radio" class="form_fix" id="body_vision4" value="4"  />無法判斷       
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">表達：</td>
		<td width="705">
		<input name="body_expression" type="radio" class="form_fix" id="body_expression1" value="1"  />正常
		<input name="body_expression" type="radio" class="form_fix" id="body_expression2" value="2"  />失語	
		<input name="body_expression" type="radio" class="form_fix" id="body_expression3" value="3"  />言語不清
		<input name="body_expression" type="radio" class="form_fix" id="body_expression4" value="4"  />肢體語言
		<input name="body_expression" type="radio" class="form_fix" id="body_expression5" value="5"  />筆談
		<input name="body_expression" type="radio" class="form_fix" id="body_expression6" value="6"  />無法判斷
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">牙齒：</td>
		<td width="705">
		<input name="body_tooth" type="radio" class="form_fix" id="body_tooth1" value="1"  />正常
		<input name="body_tooth" type="radio" class="form_fix" id="body_tooth2" value="2"  />缺牙	
		<input name="body_tooth" type="radio" class="form_fix" id="body_tooth3" value="3"  />固定
		<input name="body_tooth" type="radio" class="form_fix" id="body_tooth4" value="4"  />鬆動
		<input name="body_tooth" type="radio" class="form_fix" id="body_tooth5" value="5"  />其他
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">清潔(牙齒)：</td>
		<td width="705">		
        <input name="body_clean" type="radio" class="form_fix" id="body_clean1" value="1"  />良好
		<input name="body_clean" type="radio" class="form_fix" id="body_clean2" value="2"  />普通	
		<input name="body_clean" type="radio" class="form_fix" id="body_clean3" value="3"  />差
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">黏膜、牙齦：</td>
		<td width="705">		
        <input name="body_gums" type="radio" class="form_fix" id="body_gums1" value="1"  />正常、完整
		<input name="body_gums" type="radio" class="form_fix" id="body_gums2" value="2"  />紅腫、發炎	
		<input name="body_gums" type="radio" class="form_fix" id="body_gums3" value="3"  />破損
		<input name="body_gums" type="radio" class="form_fix" id="body_gums4" value="4"  />其他
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">疼痛：</td>
		<td width="705">		
        <input name="body_pain" type="radio" class="form_fix" id="body_pain1" value="1"  />無疼痛
		<input name="body_pain" type="radio" class="form_fix" id="body_pain2" value="2"  />表達疼痛或不適	
		<input name="body_pain" type="radio" class="form_fix" id="body_pain3" value="3"  />其他(無法判斷)
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">肌力：</td>
		<td width="705">		
        <input name="body_muscle" type="radio" class="form_fix" id="body_muscle1" value="1"  />左上
		<input name="body_muscle" type="radio" class="form_fix" id="body_muscle2" value="2"  />右上
		<input name="body_muscle" type="radio" class="form_fix" id="body_muscle3" value="3"  />左下
		<input name="body_muscle" type="radio" class="form_fix" id="body_muscle4" value="4"  />右下
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
    <tr>
		<td width="110" align="right"  class="content">關節活動：</td>
		<td width="705">		
        <input name="body_joint" type="radio" class="form_fix" id="body_joint1" value="1"  />正常
		<input name="body_joint" type="radio" class="form_fix" id="body_joint2" value="2"  />攀縮
		<input name="body_joint" type="radio" class="form_fix" id="body_joint3" value="3"  />截肢
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">步態：</td>
		<td width="705">		
        <input name="body_gait" type="radio" class="form_fix" id="body_gait1" value="1"  />正常
		<input name="body_gait" type="radio" class="form_fix" id="body_gait2" value="2"  />緩慢
		<input name="body_gait" type="radio" class="form_fix" id="body_gait3" value="3"  />碎步
		<input name="body_gait" type="radio" class="form_fix" id="body_gait4" value="4"  />步履不穩
		<input name="body_gait" type="radio" class="form_fix" id="body_gait5" value="5"  />不自主運動
		<input name="body_gait" type="radio" class="form_fix" id="body_gait6" value="6"  />完全臥床
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">輔具：</td>
		<td width="705">		
        <input name="body_aids" type="radio" class="form_fix" id="body_aids1" value="1"  />四腳助行器
		<input name="body_aids" type="radio" class="form_fix" id="body_aids2" value="2"  />手杖
		<input name="body_aids" type="radio" class="form_fix" id="body_aids3" value="3"  />義肢
		<input name="body_aids" type="radio" class="form_fix" id="body_aids4" value="4"  />輪椅
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">睡眠型態：</td>
		<td width="705">		
        <input name="body_sleeping" type="radio" class="form_fix" id="body_muscle1" value="1"  />正常
		<input name="body_sleeping" type="radio" class="form_fix" id="body_muscle2" value="2"  />日夜顛倒
		<input name="body_sleeping" type="radio" class="form_fix" id="body_muscle3" value="3"  />睡眠紊亂
		<input name="body_sleeping" type="radio" class="form_fix" id="body_muscle4" value="4"  />造成家人困擾
		</td>
	</tr>  
	<tr>
		<td height="10"></td>
	</tr>    
	<tr>
		<td width="110" align="right"  class="content">身體活動：</td>
		<td width="705">		
        <input name="body_activity" type="radio" class="form_fix" id="body_muscle1" value="1" />每日有固定離床時間
		<input name="body_activity" type="radio" class="form_fix" id="body_muscle2" value="2"  />久坐型態
		<input name="body_activity" type="radio" class="form_fix" id="body_muscle3" value="3"  />久臥型態
		</td>
	</tr>  
	<tr>
		<td height="10"></td>
	</tr>   
	<tr>
		<td width="110" align="right"  class="content">皮膚外觀：</td>
		<td width="705">		
        <input name="body_skin" type="radio" class="form_fix" id="body_muscle1" value="1"  />正常
		<input name="body_skin" type="radio" class="form_fix" id="body_muscle2" value="2"  />鬆弛
		<input name="body_skin" type="radio" class="form_fix" id="body_muscle3" value="3"  />乾燥
		<input name="body_skin" type="radio" class="form_fix" id="body_muscle4" value="4"  />搔癢
		<input name="body_skin" type="radio" class="form_fix" id="body_muscle5" value="5"  />水腫
		<input name="body_skin" type="radio" class="form_fix" id="body_muscle6" value="6"  />瘀血
		<input name="body_skin" type="radio" class="form_fix" id="body_muscle7" value="7"  />疹子
		<input name="body_skin" type="radio" class="form_fix" id="body_muscle8" value="8"  />腫塊
		<input name="body_skin" type="radio" class="form_fix" id="body_muscle9" value="9"  />水泡
		</td>
	</tr> 
	<tr>
		<td height="10"></td>
	</tr>    
	<tr>
		<td width="110" align="right"  class="content">褥瘡：</td>
		<td width="705">		
        <input name="body_decubitus" type="radio" class="form_fix" id="body_muscle1" value="1" />無
		<input name="body_decubitus" type="radio" class="form_fix" id="body_muscle2" value="2" />有
		</td>
	</tr>             
	<tr>
		<td align="right" class="content">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
			<input name="mybtn" type="button" class="form_fix" id="mybtn"  value="確定送出"  /> 
 		 <!--<input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>-->
		</td>
	</tr>                
	</table>
	</form>              
              <!--管理員管理 ending-->              
              </td>
            </tr>
            
          </table>
          </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>
