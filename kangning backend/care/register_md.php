<?php 
include_once("../public/mem_check.php");
include_once("../public/web_function.php");

  if(!$_SESSION['KNH_LOGIN_ID'])
  {
    header("location:../index.php");
    exit;
  } 

  if(is_numeric(quotes($_GET['RS_ID']))){
    $RS_ID = quotes($_GET['RS_ID']);
  }else{
?>
  <script language="javascript">   
    location.href='../index.php';
  </script>

<?php
  }

  $rs = $objDB->Recordset("select * from resident where RS_ID ='$RS_ID'");
  $row = $objDB->GetRows($rs);

  $RS_Reason = explode(",",$row[0]['RS_Reason']);
  $RS_PHistory = explode(",",$row[0]['RS_PHistory']);
  $RS_Allergy = explode(",",$row[0]['RS_Allergy']);
  $RS_HCondition = explode(",",$row[0]['RS_HCondition']);
  $RS_SCondition = explode(",",$row[0]['RS_SCondition']);
  $RS_MCondition = explode(",",$row[0]['RS_MCondition']);
  $RS_Auxiliary = explode(",",$row[0]['RS_Auxiliary']);
  $RS_Care = explode(",",$row[0]['RS_Care']);
  $RS_Problems = explode(",",$row[0]['RS_Problems']);
  $RS_Pipe = explode(",",$row[0]['RS_Pipe']);
  $RS_Diagnosis = explode(",", $row[0]['RS_Diagnosis']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.datepicker.min.js"></script>
<script>

 $(document).ready(function(){
  
  $(".date-pick" ).datepicker({ 
      dateFormat: 'yy-mm-dd', 
      showOn: "button",
      buttonImage: "../js/calendar.png",
      buttonImageOnly: true,
      changeMonth: true,
      changeYear: true,
      yearRange:'-120:+120'
  }); 

  $("#mybtn").click(function(){
     
      if($("#AM_Pass").val()!=''){
        if($("#AM_Pass").val()=='') {
          alert('請填入新密碼');
          $("#AM_Pass").focus();
          return false;
        }else if($("#AM_Pass").val()!=$("#AM_RePass").val()) {
          alert("確認密碼不相符");
          $("#AM_RePass").focus();
          return false;
        }
     }
     
     if($("#AM_Name").val()=='') {
          alert("請填入名稱");
          $("#AM_Name").focus();
          return false;
       }else{
            $("form#form1").submit();
    }
  });

  $("input[name='RS_LCondition']").click(function() {
      $("#RS_LCondition_Other").hide();
  });

  $("input[name='RS_Source']").click(function() {
      $("#RS_Source_Other").hide();
  });
  
  $("input[name='RS_Expression']").click(function() {
      $("#Expression").hide();
  });

  $("input[name='RS_Identity']").click(function() {
      $("#RS_UIdentity").hide();
      $("#RS_MIdentity").hide();
  });

  
  $("#RS_LCondition_Other").hide();
  $("#RS_Source_Other").hide();
  $("#RS_Reason_Other").hide();
  $("#RS_PHistory_Other").hide();
  $("#Allergy").hide();
  $("#Expression").hide();
  $("#RS_UIdentity").hide();
  $("#RS_MIdentity").hide();
  $("#auxiliary1").hide();
  $("#auxiliary2").hide();
  $("#auxiliary3").hide();
  $("#auxiliary4").hide();
  $("#auxiliary5").hide();
  $("#mcondition").hide();
  

  if($("#RS_LCondition5").is(":checked"))
  {
      $("#RS_LCondition_Other").show();
  }
  $("#RS_LCondition5").click(function(){
    if($("#RS_LCondition5").prop("checked"))
    {
      $("#RS_LCondition_Other").show();
    }
    else
    {
      $("#RS_LCondition_Other").hide();
    }
  });

  
  if($("#RS_Source5").is(":checked"))
  {
      $("#RS_Source_Other").show();
  }
  $("#RS_Source5").click(function(){
    if($("#RS_Source5").prop("checked"))
    {
      $("#RS_Source_Other").show();
    }
    else
    {
      $("#RS_Source_Other").hide();
    }
  });

  
  if($("#RS_Reason7").is(":checked"))
  {
      $("#RS_Reason_Other").show();
  }
  $("#RS_Reason7").click(function(){
    if($("#RS_Reason7").prop("checked"))
    {
      $("#RS_Reason_Other").show();
    }
    else
    {
      $("#RS_Reason_Other").hide();
    }
  });

  
  if($("#RS_PHistory8").is(":checked"))
  {
      $("#RS_PHistory_Other").show();
  }
  $("#RS_PHistory8").click(function(){
    if($("#RS_PHistory8").prop("checked"))
    {
      $("#RS_PHistory_Other").show();
    }
    else
    {
      $("#RS_PHistory_Other").hide();
    }
  });

  
  if($("#RS_Allergy2").is(":checked"))
  {
      $("#Allergy").show();
  }
  $("#RS_Allergy2").click(function(){
    if($("#RS_Allergy2").prop("checked"))
    {
      $("#Allergy").show();
    }
    else
    {
      $("#Allergy").hide();
    }
  });
  $("#RS_Allergy1").click(function(){
    $("#Allergy").hide();
  });

  
  if($("#RS_Expression6").is(":checked"))
  {
      $("#Expression").show();
  }
  $("#RS_Expression6").click(function(){
    if($("#RS_Expression6").prop("checked"))
    {
      $("#Expression").show();
    }
    else
    {
      $("#Expression").hide();
    }
  });

  

  if($("#RS_Identity5").is(":checked"))
  {
      $("#RS_UIdentity").show();
  }
  $("#RS_Identity5").click(function(){
    if($("#RS_Identity5").prop("checked"))
    {
      $("#RS_UIdentity").show();
    }
    else
    {
      $("#RS_UIdentity").hide();
    }
  });

  if($("#RS_Identity6").is(":checked"))
  {
      $("#RS_MIdentity").show();
  }
  $("#RS_Identity6").click(function(){
    if($("#RS_Identity6").prop("checked"))
    {
      $("#RS_MIdentity").show();
    }
    else
    {
      $("#RS_MIdentity").hide();
    }
  });
  


  if($("#RS_MCondition2").is(":checked"))
  {
      $("#mcondition").show();
  }
  $("#RS_MCondition2").click(function(){
    if($("#RS_MCondition2").prop("checked"))
    {
      $("#mcondition").show();
    }
    else
    {
      $("#mcondition").hide();
    }
  });
  $("#RS_MCondition1").click(function(){
      $("#mcondition").hide();
  });

  
  if($("#RS_Auxiliary1").is(":checked"))
  {
      $("#auxiliary1").show();
  }
  $("#RS_Auxiliary1").click(function(){
    if($("#RS_Auxiliary1").prop("checked"))
    {
      $("#auxiliary1").show();
    }
    else
    {
      $("#auxiliary1").hide();
    }
  });

  if($("#RS_Auxiliary4").is(":checked"))
  {
      $("#auxiliary2").show();
  }
  $("#RS_Auxiliary4").click(function(){
    if($("#RS_Auxiliary4").prop("checked"))
    {
      $("#auxiliary2").show();
    }
    else
    {
      $("#auxiliary2").hide();
    }
  });


  if($("#RS_Auxiliary8").is(":checked"))
  {
      $("#auxiliary3").show();
  }
  $("#RS_Auxiliary8").click(function(){
    if($("#RS_Auxiliary8").prop("checked"))
    {
      $("#auxiliary3").show();
    }
    else
    {
      $("#auxiliary3").hide();
    }
  });


  if($("#RS_Auxiliary11").is(":checked"))
  {
      $("#auxiliary4").show();
  }
  $("#RS_Auxiliary11").click(function(){
    if($("#RS_Auxiliary11").prop("checked"))
    {
      $("#auxiliary4").show();
    }
    else
    {
      $("#auxiliary4").hide();
    }
  });

  if($("#RS_Auxiliary18").is(":checked"))
  {
      $("#auxiliary5").show();
  }
  $("#RS_Auxiliary18").click(function(){
    if($("#RS_Auxiliary18").prop("checked"))
    {
      $("#auxiliary5").show();
    }
    else
    {
      $("#auxiliary5").hide();
    }
  });
  
})

  var countMin = 1;  
  var countMax = 10; 
  var target = <?php echo count($RS_Diagnosis);?>;
  var count = target; 

  function addField() {  
      if(count > countMax)  
          alert("最多"+countMax+"個欄位");  
      else      
          $("#fieldSpace").append("<div>" + (++count) + '<input type="text" name="RS_Diagnosis[]" id="RS_Diagnosis'+ count +'"></div>');      
  } 
  function delField() { 
      if (count >= countMin) {  
          //document.getElementById("fieldSpace").removeChild(document.getElementById("fieldSpace").lastChild); 
          $("#fieldSpace div").last().remove();
          count--; 
      }else { 
          alert("無新增欄位可以刪除"); 
      }   
  } 

  
  var targetAno = <?php echo big(count($RS_HCondition),count($RS_SCondition)); ?>;
  var countAno = targetAno;

  function addCField() {
      if(countAno > countMax)
          alert("最多"+countMax+"個欄位");
      else
          $("#fieldCondition").append('<div>'+(++countAno)+'<input type="text" name="RS_HCondition[]" id="RS_HCondition'+ countAno +'">醫院'
                                                          +'<input type="text" name="RS_SCondition[]" id="RS_SCondition'+ countAno +'">科</div>');
  }
  function delCField() {
      if(countAno >= countMin) {
          $("#fieldCondition div").last().remove();
          countAno--;
      }else {
          alert("無新增欄位可以刪除");
      }
  }

</script>
<link type="text/css" href="../css/ui-darkness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
    <!-- header starting point -->
    <?php include("../include/header.php");?>
    <!-- header ending point -->    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1000" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="160" valign="top" background="../images/bkline.jpg">
            <!--menu starting point-->
            <?php include("../include/menu.php");?>
            <!--menu ending point-->          
          </td>
            
          <td width="10" valign="top"><img src="../images/spacer.gif" width="1" height="1" /></td>
          <td width="830" valign="top">
          <table width="830" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="30" class="content">專業照護 > 待床管理 &gt; 修改</td>
            </tr>
            <tr>            
              <td height="15"></td>
            </tr>
            <tr>
              <td height="10">                
                <input name="search" type="button" class="content" id="search" value="回上一頁" onClick="history.back(-1)"/>
              </td>
            </tr>
            <tr>
              <td height="10"><h1>基本資料</h1></td>   
            </tr>
            <tr>         
              <td>                          
              <!--管理員管理startinging-->              
              <form name="form1" id="form1" method="post" action="register_process.php">
              <input type="hidden" name="action" id="action" value="mdy">
              <input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>">
              <table>
                <tr>
                  <td align="right" class="content"><span style="color:#ff0000;">*</span>姓名：</td>
                  <td><input name="RS_Name" type="text" class="form_fix" id="RS_Name" style="width:80px" value="<?php echo $row[0]['RS_Name'];?>"/></td>    
                </tr>
                <tr>
                  <td height="5"></td>
                </tr>
                <tr>
                  <td width="90" align="right"  class="content"><span style="color:#ff0000;"></span>身分證字號：</td>
                  <td width="705">
                  <input name="RS_IdentityCard" type="text" class="form_fix" id="RS_IdentityCard" value="<?php echo $row[0]['RS_IdentityCard'];?>"/>&nbsp;&nbsp;<img src="../js/ajax-loader.gif"  id="loading" style="margin-left:4px;display:none;" ><span id="checkid"></span> 
                  </td>
                </tr>
                <tr>
                  <td height="5"></td>
                </tr>
                <tr>
                  <td width="90" align="right"  class="content">性別：</td>
                  <td width="705">
                  <input name="RS_Sex" type="radio" class="form_fix" id="RS_Sex" value="1" <?php echo ckRadio('1',$row[0]['RS_Sex']);?> />男
                  <input name="RS_Sex" type="radio" class="form_fix" id="RS_Sex" value="2" <?php echo ckRadio('2',$row[0]['RS_Sex']);?> />女
                  </td>
                </tr>
                  <tr>
                  <td height="5"></td>
                </tr>
                <tr>
                  <td align="right" class="content">出生日期：</td>
                  <td>
                  <!--<input name="RS_Birthday" type="text" id="RS_Birthday"  class="date-pick"  style="width:80px;"  readonly="readonly" />-->
                  <input name="RS_Birthday" type="text" id="RS_Birthday"  class="date-pick"  placeholder="西元年/月/日" style="width:80px" value="<?php echo $row[0]['RS_Birthday'];?>"/>
                  </td>
                </tr>
                <tr>
                  <td height="5"></td>
                </tr>
                <tr>
                  <td align="right" class="content">狀態(1:審核 2:入住 3.入院保留床 4.退住)：</td>
                  <td>
                  <select name="RS_Status" id="RS_Status">
                  <option value="0">請選擇</option>
                  <?php for($i=1;$i<=4;$i++){ ?>
                       <option value="<?php echo $i;?>" <?php if($row[0]["RS_Status"] == $i) echo "selected"; ?>><?php echo $i; ?></option>
                  <?php }?>
                  </select>
                  </td>
                  </td>
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">住民病史：</td>
                  <td>
                      <?php   
                          $RS_History = $row[0]['RS_History'];   
              
                          include_once "../CKEdit/ckeditor/ckeditor.php";
  
                          $CKEditor1 = new CKEditor();
                          //$CKEditor->config['width'] = 500;
                          $CKEditor1->config['toolbar'] = 'MyToolbar';           
                          $CKEditor1->basePath = '../CKEdit/ckeditor/';
                          $CKEditor1->editor("RS_History", $RS_History);         
                      ?>
                  </td>
                </tr> 
                <tr>
                  <td height="5"></td>
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">診斷：</td>
                  <td width="705">
                    <span id="fieldSpace">
                    <?php
                      for($i=0; $i<count($RS_Diagnosis); $i++)
                      {
                          echo '<div>'.($i+1).'<input type="text" name="RS_Diagnosis[]" id="RS_Diagnosis'.($i+1).'" value="'.$RS_Diagnosis[$i].'"></div>';
                      }
                    ?>
                    </span>  
                    <a href="javascript:" onclick="addField()">新增欄位</a> 
                    <a href="javascript:" onclick="delField()">刪除欄位</a> 
                  </td>
                </tr>
                <tr>
                    <td height="5"></td>
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">教育程度：</td>
                  <td width="705">
                    <input name="RS_Education" type="radio" class="form_fix" id="RS_Education" value="1" <?php echo ckRadio('1',$row[0]['RS_Education']);?>/>不識字
                    <input name="RS_Education" type="radio" class="form_fix" id="RS_Education" value="2" <?php echo ckRadio('2',$row[0]['RS_Education']);?>/>識字未就學
                    <input name="RS_Education" type="radio" class="form_fix" id="RS_Education" value="3" <?php echo ckRadio('3',$row[0]['RS_Education']);?>/>國小
                    <input name="RS_Education" type="radio" class="form_fix" id="RS_Education" value="4" <?php echo ckRadio('4',$row[0]['RS_Education']);?>/>國中
                    <input name="RS_Education" type="radio" class="form_fix" id="RS_Education" value="5" <?php echo ckRadio('5',$row[0]['RS_Education']);?>/>高中職
                    <input name="RS_Education" type="radio" class="form_fix" id="RS_Education" value="6" <?php echo ckRadio('6',$row[0]['RS_Education']);?>/>大專以上
                  </td>
                </tr>
                <tr>
                  <td height="5"></td>
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">居住狀況：</td>
                  <td width="705">
                    <input name="RS_LCondition" type="radio" class="form_fix" id="RS_LCondition1" value="1" <?php echo ckRadio('1',$row[0]['RS_LCondition']);?>/>獨居
                    <input name="RS_LCondition" type="radio" class="form_fix" id="RS_LCondition2" value="2" <?php echo ckRadio('2',$row[0]['RS_LCondition']);?>/>與配偶居
                    <input name="RS_LCondition" type="radio" class="form_fix" id="RS_LCondition3" value="3" <?php echo ckRadio('3',$row[0]['RS_LCondition']);?>/>與其他家人
                    <input name="RS_LCondition" type="radio" class="form_fix" id="RS_LCondition4" value="4" <?php echo ckRadio('4',$row[0]['RS_LCondition']);?>/>與親友居
                    <input name="RS_LCondition" type="radio" class="form_fix" id="RS_LCondition5" value="5" <?php echo ckRadio('5',$row[0]['RS_LCondition']);?>/>其他
                    <input name="RS_LCondition_Other" id="RS_LCondition_Other" style="width:100px;" value="<?php echo $row[0]['RS_LCondition_Other'];?>"/>
                  </td>
                </tr>
                <tr>
                  <td height="5"></td>
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">來源：</td>
                  <td width="705">
                    <input name="RS_Source" type="radio" class="form_fix" id="RS_Source1" value="1" <?php echo ckRadio('1',$row[0]['RS_Source']);?>/>醫院
                    <input name="RS_Source" type="radio" class="form_fix" id="RS_Source2" value="2" <?php echo ckRadio('2',$row[0]['RS_Source']);?>/>門診
                    <input name="RS_Source" type="radio" class="form_fix" id="RS_Source3" value="3" <?php echo ckRadio('3',$row[0]['RS_Source']);?>/>養護機構
                    <input name="RS_Source" type="radio" class="form_fix" id="RS_Source4" value="4" <?php echo ckRadio('4',$row[0]['RS_Source']);?>/>社區
                    <input name="RS_Source" type="radio" class="form_fix" id="RS_Source5" value="5" <?php echo ckRadio('5',$row[0]['RS_Source']);?>/>其他
                    <input name="RS_Source_Other" id="RS_Source_Other" style="width:100px;" value="<?php echo $row[0]['RS_Source_Other'];?>"/>
                  </td>
                </tr>
                <tr>
                  <td height="5"></td>
                </tr>
                <tr>
                  <td width="150" align="right"  class="content">入住原因：</td>
                  <td width="705">
                    <input name="RS_Reason[]" type="checkbox" class="form_fix" id="RS_Reason1" value="1" <?php echo ckbox('1',$RS_Reason);?>/>家中缺乏人手 
                    <input name="RS_Reason[]" type="checkbox" class="form_fix" id="RS_Reason2" value="2" <?php echo ckbox('2',$RS_Reason);?>/>家屬缺乏照顧能力 
                    <input name="RS_Reason[]" type="checkbox" class="form_fix" id="RS_Reason3" value="3" <?php echo ckbox('3',$RS_Reason);?>/>擔心出院病情變化，送醫不便 
                    <input name="RS_Reason[]" type="checkbox" class="form_fix" id="RS_Reason4" value="4" <?php echo ckbox('4',$RS_Reason);?>/>家屬偏遠，送醫不便 
                    <input name="RS_Reason[]" type="checkbox" class="form_fix" id="RS_Reason5" value="5" <?php echo ckbox('5',$RS_Reason);?>/>居家環境不良，無法妥善安置病人
                    <input name="RS_Reason[]" type="checkbox" class="form_fix" id="RS_Reason6" value="6" <?php echo ckbox('6',$RS_Reason);?>/>居家變遷，尚未安排妥當
                    <input name="RS_Reason[]" type="checkbox" class="form_fix" id="RS_Reason7" value="7" <?php echo ckbox('7',$RS_Reason);?>/>其它
                    <input name="RS_Reason_Other" id="RS_Reason_Other" style="width:100px;" value="<?php echo $row[0]['RS_Reason_Other'];?>"/>
                  </td>
                </tr>
                <tr>
                  <td height="5"></td>
                </tr>
                <tr>
                  <td width="150" align="right"  class="content">過去病史：</td>
                  <td width="705">
                  <table>
                    <tr>
                      <td>
                        <input name="RS_PHistory[]" type="checkbox" class="form_fix" id="RS_PHistory1" value="1" <?php echo ckbox('1',$RS_PHistory);?>/>糖尿病 
                        <input name="RS_PHistory[]" type="checkbox" class="form_fix" id="RS_PHistory2" value="2" <?php echo ckbox('2',$RS_PHistory);?>/>高血壓 
                        <input name="RS_PHistory[]" type="checkbox" class="form_fix" id="RS_PHistory3" value="3" <?php echo ckbox('3',$RS_PHistory);?>/>心臟病 
                        <input name="RS_PHistory[]" type="checkbox" class="form_fix" id="RS_PHistory4" value="4" <?php echo ckbox('4',$RS_PHistory);?>/>氣喘 
                        <input name="RS_PHistory[]" type="checkbox" class="form_fix" id="RS_PHistory5" value="5" <?php echo ckbox('5',$RS_PHistory);?>/>腎臟病
                        <input name="RS_PHistory[]" type="checkbox" class="form_fix" id="RS_PHistory6" value="6" <?php echo ckbox('6',$RS_PHistory);?>/>中風
                        <input name="RS_PHistory[]" type="checkbox" class="form_fix" id="RS_PHistory7" value="7" <?php echo ckbox('7',$RS_PHistory);?>/>癌症
                        <input name="RS_PHistory[]" type="checkbox" class="form_fix" id="RS_PHistory8" value="8" <?php echo ckbox('8',$RS_PHistory);?>/>其他
                        <input name="RS_PHistory_Other" id="RS_PHistory_Other" style="width:100px;" value="<?php echo $row[0]['RS_PHistory_Other'];?>"/>
                      </td>
                    </tr>
                  </table>
                <tr>
                  <td height="5"></td>
                </tr>
                <tr>
                  <td width="150" align="right"  class="content">過敏史：</td>
                  <td width="705">
                  <table>
                    <tr>
                      <td>
                        <input name="RS_Allergy[]" type="radio" class="form_fix" id="RS_Allergy1" value="1" <?php echo ckbox('1',$RS_Allergy);?>/>無
                        <input name="RS_Allergy[]" type="radio" class="form_fix" id="RS_Allergy2" value="2" <?php echo ckbox('2',$RS_Allergy);?>/>有
                      </td>
                      <td id="Allergy">
                        <input name="RS_Allergy[]" type="checkbox" class="form_fix" id="RS_Allergy3" value="3" <?php echo ckbox('3',$RS_Allergy);?>/>藥物
                        <input name="RS_MAllergy" id="RS_MAllergy" style="width:100px;" value="<?php echo $row[0]['RS_MAllergy'];?>"/>
                        <input name="RS_Allergy[]" type="checkbox" class="form_fix" id="RS_Allergy4" value="4" <?php echo ckbox('4',$RS_Allergy);?>/>食物
                        <input name="RS_FAllergy" id="RS_FAllergy" style="width:100px;" value="<?php echo $row[0]['RS_FAllergy'];?>"/>
                      </td>
                    </tr>
                  </table>
                </tr>
                <tr>
                   <td height="5"></td>
                </tr>
                <tr>
                  <td width="150" align="right"  class="content">表達：</td>
                  <td width="705">
                  <table>
                  <tr>
                    <td>
                      <input name="RS_Expression" type="radio" class="form_fix" id="RS_Expression1" value="1" <?php echo ckRadio('1',$row[0]['RS_Expression']);?>/>正常 
                      <input name="RS_Expression" type="radio" class="form_fix" id="RS_Expression2" value="2" <?php echo ckRadio('2',$row[0]['RS_Expression']);?>/>語言困難 
                      <input name="RS_Expression" type="radio" class="form_fix" id="RS_Expression3" value="3" <?php echo ckRadio('3',$row[0]['RS_Expression']);?>/>答非所問 
                      <input name="RS_Expression" type="radio" class="form_fix" id="RS_Expression4" value="4" <?php echo ckRadio('4',$row[0]['RS_Expression']);?>/>沉默 
                      <input name="RS_Expression" type="radio" class="form_fix" id="RS_Expression5" value="5" <?php echo ckRadio('5',$row[0]['RS_Expression']);?>/>無法言語
                      <input name="RS_Expression" type="radio" class="form_fix" id="RS_Expression6" value="6" <?php echo ckRadio('6',$row[0]['RS_Expression']);?>/>GCS
                    <td id="Expression">
                        E<input name="RS_EExpression" id="RS_Expression_E" size="3" value="<?php echo $row[0]['RS_EExpression']; ?>" />
                        V<input name="RS_VExpression" id="RS_Expression_v" size="3" value="<?php echo $row[0]['RS_VExpression']; ?>" /> 
                        M<input name="RS_MExpression" id="RS_Expression_M" size="3" value="<?php echo $row[0]['RS_MExpression']; ?>" /> 
                    </td>
                  </tr>
                  </table>
                </tr>
                <tr>
                   <td height="5"></td>
                </tr>
                <tr>
                  <td width="150" align="right"  class="content">就醫狀況：</td>
                  <td width="705">
                      <span id="fieldCondition">
                      <?php
                        for($i=0; $i<big(count($RS_HCondition),count($RS_SCondition)); $i++)
                        {
                            echo '<div>'.($i+1).'<input type="text" name="RS_HCondition[]" id="RS_HCondition'.($i+1).'" value="'.$RS_HCondition[$i].'">醫院'.
                                                '<input type="text"  name="RS_SCondition[]" id="RS_SCondition'.($i+1).'" value="'.$RS_SCondition[$i].'">科'.'</div>';
                        }
                      ?>
                      </span>
                      <a href="javascript:" onclick="addCField()">新增欄位</a> 
                      <a href="javascript:" onclick="delCField()">刪除欄位</a>
                  </td>
                </tr>
                <tr>
                   <td height="5"></td>
                </tr>
                <tr>
                  <td width="90" align="right"  class="content">身分別：</td>
                  <td width="705">
                  <table>
                    <tr>
                      <td>
                        <input name="RS_Identity" type="radio" class="form_fix" id="RS_Identity1" value="1" <?php echo ckRadio('1',$row[0]['RS_Identity']);?>/>自費 
                        <input name="RS_Identity" type="radio" class="form_fix" id="RS_Identity2" value="2" <?php echo ckRadio('2',$row[0]['RS_Identity']);?>/>中低收 
                        <input name="RS_Identity" type="radio" class="form_fix" id="RS_Identity3" value="3" <?php echo ckRadio('3',$row[0]['RS_Identity']);?>/>榮民 
                        <input name="RS_Identity" type="radio" class="form_fix" id="RS_Identity4" value="4" <?php echo ckRadio('4',$row[0]['RS_Identity']);?>/>榮眷 
                        <input name="RS_Identity" type="radio" class="form_fix" id="RS_Identity5" value="5" <?php echo ckRadio('5',$row[0]['RS_Identity']);?>/>轉介單位
                        <input name="RS_UIdentity" id="RS_UIdentity" style="width:100px;" value="<?php echo $row[0]['RS_UIdentity'];?>"/>
                        <input name="RS_Identity" type="radio" class="form_fix" id="RS_Identity6" value="6" <?php echo ckRadio('6',$row[0]['RS_Identity']);?>/>身障手冊
                        <input name="RS_MIdentity" id="RS_MIdentity" style="width:100px;" value="<?php echo $row[0]['RS_MIdentity'];?>"/>
                      </td>
                    </tr>
                  </table>
                  </td>
                </tr>
                <tr>
                   <td height="5"></td>
                </tr>
                <tr>
                  <td width="150" align="right"  class="content">用藥情形：</td>
                  <td width="705">
                  <table>
                    <tr>
                      <td>
                        <input name="RS_MCondition[]" type="radio" class="form_fix" id="RS_MCondition1" value="1" <?php echo ckbox('1',$RS_MCondition);?>/>無 
                        <input name="RS_MCondition[]" type="radio" class="form_fix" id="RS_MCondition2" value="2" <?php echo ckbox('2',$RS_MCondition);?>/>有
                      </td>
                      <td id="mcondition">
                        <input name="RS_MCondition[]" type="checkbox" class="form_fix" id="RS_MCondition3" value="3" <?php echo ckbox('3',$RS_MCondition);?>/>本院
                        <input name="RS_MCondition[]" type="checkbox" class="form_fix" id="RS_MCondition4" value="4" <?php echo ckbox('4',$RS_MCondition);?>/>他院
                      </td>
                    </tr>
                  </table>
                </tr>
                <tr>
                  <td height="5"></td>
                </tr>
                <tr>
                  <td width="150" align="right"  class="content">輔助物：</td>
                  <td width="705">
                  <table>
                    <tr>
                      <td><input name="RS_Auxiliary[]" type="checkbox" class="form_fix" id="RS_Auxiliary1" value="1" <?php echo ckbox('1',$RS_Auxiliary);?>/>活動假牙</td>
                      <td id="auxiliary1">
                          <input name="RS_Auxiliary[]" type="checkbox" class="form_fix" id="RS_Auxiliary2" value="2" <?php echo ckbox('2',$RS_Auxiliary);?>/>上
                          <input name="RS_Auxiliary[]" type="checkbox" class="form_fix" id="RS_Auxiliary3" value="3" <?php echo ckbox('3',$RS_Auxiliary);?>/>下
                      </td>
                    </tr>
                    <tr>
                      <td><input name="RS_Auxiliary[]" type="checkbox" class="form_fix" id="RS_Auxiliary4" value="4" <?php echo ckbox('4',$RS_Auxiliary);?>/>固定假牙</td>
                      <td id="auxiliary2">
                          <input name="RS_Auxiliary[]" type="checkbox" class="form_fix" id="RS_Auxiliary5" value="5" <?php echo ckbox('5',$RS_Auxiliary);?>/>上
                          <input name="RS_Auxiliary[]" type="checkbox" class="form_fix" id="RS_Auxiliary6" value="6" <?php echo ckbox('6',$RS_Auxiliary);?>/>下
                      </td>
                    </tr>
                    <tr>
                      <td><input name="RS_Auxiliary[]" type="checkbox" class="form_fix" id="RS_Auxiliary7" value="7" <?php echo ckbox('7',$RS_Auxiliary);?>/>眼鏡</td>
                    </tr>
                    <tr>
                      <td><input name="RS_Auxiliary[]" type="checkbox" class="form_fix" id="RS_Auxiliary8" value="8" <?php echo ckbox('8',$RS_Auxiliary);?>/>助聽器</td>
                      <td id="auxiliary3">
                          <input name="RS_Auxiliary[]" type="checkbox" class="form_fix" id="RS_Auxiliary9" value="9" <?php echo ckbox('9',$RS_Auxiliary);?>/>左耳
                          <input name="RS_Auxiliary[]" type="checkbox" class="form_fix" id="RS_Auxiliary10" value="10" <?php echo ckbox('10',$RS_Auxiliary);?>/>右耳
                      </td> 
                    </tr>
                    <tr> 
                      <td><input name="RS_Auxiliary[]" type="checkbox" class="form_fix" id="RS_Auxiliary11" value="11" <?php echo ckbox('11',$RS_Auxiliary);?>/>義眼</td>
                      <td id="auxiliary4">    
                          <input name="RS_Auxiliary[]" type="checkbox" class="form_fix" id="RS_Auxiliary12" value="12" <?php echo ckbox('12',$RS_Auxiliary);?>/>左眼
                          <input name="RS_Auxiliary[]" type="checkbox" class="form_fix" id="RS_Auxiliary13" value="13" <?php echo ckbox('13',$RS_Auxiliary);?>/>右眼
                      </td>
                    </tr>
                    <tr><td><input name="RS_Auxiliary[]" type="checkbox" class="form_fix" id="RS_Auxiliary14" value="14" <?php echo ckbox('14',$RS_Auxiliary);?>/>義肢
                    <tr><td><input name="RS_Auxiliary[]" type="checkbox" class="form_fix" id="RS_Auxiliary15" value="15" <?php echo ckbox('15',$RS_Auxiliary);?>/>助行器
                    <tr><td><input name="RS_Auxiliary[]" type="checkbox" class="form_fix" id="RS_Auxiliary16" value="16" <?php echo ckbox('16',$RS_Auxiliary);?>/>輪椅
                    <tr><td><input name="RS_Auxiliary[]" type="checkbox" class="form_fix" id="RS_Auxiliary17" value="17" <?php echo ckbox('17',$RS_Auxiliary);?>/>氣墊床
                    <tr><td><input name="RS_Auxiliary[]" type="checkbox" class="form_fix" id="RS_Auxiliary18" value="18" <?php echo ckbox('18',$RS_Auxiliary);?>/>其他
                    <td id="auxiliary5"><input name="RS_Auxiliary_Other" id="RS_Auxiliary_Other" style="width:100px;" value="<?php echo $row[0]['RS_Auxiliary_Other'];?>"/></td>
                    </tr>
                  </table>
                 </td>
                </tr>
                <tr>
                    <td height="5"></td>
                </tr>
                <tr>
                    <td width="110" align="right"  class="content">特殊護理裝置：</td>
                    <td width="705">
                    <table>
                    <tr>
                        <td><input name="RS_Pipe[]" type="checkbox" class="form_fix" id="RS_Pipe1" value="1" <?php echo ckbox('1', $RS_Pipe);?>/>氣切</td>
                        <td><input name="RS_TracheostomyFR" type="text" id="RS_TracheostomyFR" style="width:50px;" value="<?php echo $row[0]['RS_TracheostomyFR'];?>">Fr，
                        型式／管路ID／OD<input name="RS_TracheostomyType" type="text" id="RS_TracheostomyType" style="width:50px;" value="<?php echo $row[0]['RS_Tracheostomy_Type'];?>">
                        ，預更換日<input name="RS_Tracheostomy_Date" type="text" id="RS_Tracheostomy_Date" class="date-pick" style="width:80px;" value="<?php echo $row[0]['RS_Tracheostomy_Date'];?>">
                        </td>
                    </tr>
                    <tr>
                        <td><input name="RS_Pipe[]" type="checkbox" class="form_fix" id="RS_Pipe2" value="2" <?php echo ckbox('2', $RS_Pipe);?>/>鼻胃管</td>
                    　　<td><input name="RS_NGtubeFR" type="text" id="RS_NGtubeFR" style="width:50px;" value="<?php echo $row[0]['RS_NGtubeFR'];?>">Fr，
                        ，預更換日<input name="RS_NGtube_Date" type="text" id="RS_NGtube_Date" class="date-pick" style="width:80px;" value="<?php echo $row[0]['RS_NGtube_Date'];?>">
                        </td>
                    </tr>
                    <tr>
                        <td><input name="RS_Pipe[]" type="checkbox" class="form_fix" id="RS_Pipe3" value="3" <?php echo ckbox('3', $RS_Pipe);?>/>導尿管</td>
                    　　<td><input name="RS_CatheterFR" type="text" id="RS_CatheterFR" style="width:50px;" value="<?php echo $row[0]['RS_CatheterFR'];?>">Fr，
                        ，預更換日<input name="RS_Catheter_Date" type="text" id="RS_Catheter_Date" class="date-pick" style="width:80px;" value="<?php echo $row[0]['RS_Catheter_Date'];?>">
                        </td>
                    </tr>
                    <tr>
                        <td><input name="RS_Pipe[]" type="checkbox" class="form_fix" id="RS_Pipe4" value="4" <?php echo ckbox('4', $RS_Pipe);?>/>膀胱瘻口</td>
                    　　<td><input name="RS_BladderFR" type="text" id="RS_BladderFR" style="width:50px;" value="<?php echo $row[0]['RS_BladderFR'];?>">Fr，
                        ，預更換日<input name="RS_Bladder_Date" type="text" id="RS_Bladder_Date" class="date-pick" style="width:80px;" value="<?php echo $row[0]['RS_Bladder_Date'];?>">
                        </td>
                    </tr>
                    <tr>
                        <td><input name="RS_Pipe[]" type="checkbox" class="form_fix" id="RS_Pipe5" value="5" <?php echo ckbox('5', $RS_Pipe);?>/>胃瘻口</td>
                    　　<td><input name="RS_StomachFR" type="text" id="RS_StomachFR" style="width:50px;" value="<?php echo $row[0]['RS_StomachFR'];?>">Fr，
                        ，預更換日<input name="RS_Stomach_Date" type="text" id="RS_Stomach_Date" class="date-pick" style="width:80px;" value="<?php echo $row[0]['RS_Stomach_Date'];?>">
                        </td>
                    </tr>
                    <tr>
                        <td><input name="RS_Pipe[]" type="checkbox" class="form_fix" id="RS_Pipe6" value="6" <?php echo ckbox('6', $RS_Pipe);?>/>傷口護理</td>
                    　　<td><input name="RS_Wound" type="text" id="RS_Wound" style="width:80px;" value="<?php echo $row[0]['RS_Wound'];?>"></td>
                    </tr>
                    <tr>
                        <td><input name="RS_Pipe[]" type="checkbox" class="form_fix" id="RS_Pipe7" value="7" <?php echo ckbox('7', $RS_Pipe);?>/>其他</td>
                    　　<td><input name="RS_Pipe_Other" type="text" id="RS_Pipe_Other" style="width:200px;" value="<?php echo $row[0]['RS_Pipe_Other'];?>"></td>
                    </tr>
                    </table>
                    </td>
                </tr>
                <tr>
                  <td height="5"></td>
                </tr>          
                <tr>
                    <td width="110" align="right"  class="content">家屬期待：</td>
                    <td>
                        <?php   
                          $RS_Expect = $row[0]['RS_Expect'];   
              
                          include_once "../CKEdit/ckeditor/ckeditor.php";
  
                          $CKEditor2 = new CKEditor();
                          //$CKEditor->config['width'] = 500;
                          $CKEditor2->config['toolbar'] = 'MyToolbar';           
                          $CKEditor2->basePath = '../CKEdit/ckeditor/';
                          $CKEditor2->editor("RS_Expect", $RS_Expect);         
                        ?>
                    </td>
                </tr>
                <tr>
                    <td height="5"></td>
                </tr>
                <tr>
                    <td width="110" align="right"  class="content">家庭評估：</td>
                    <td width="705">
                    <table>
                      <tr>
                        <td>有<input name="RS_Broster" id="RS_Broster" style="width:60px;" value="<?php echo $row[0]['RS_Broster'];?>">個兄弟姊妹，</td>
                        <td><input name="RS_Sonter" id="RS_Sonter" style="width:60px;" value="<?php echo $row[0]['RS_Sonter'];?>">個兒子女兒，</td>
                        <td><input name="RS_Grand" id="RS_Grand" style="width:60px;" value="<?php echo $row[0]['RS_Grand'];?>">個孫子孫女</td>
                      </tr>
                      <tr>
                        <td>緊急聯絡人:<input name="RS_Contact" id="RS_Contact" style="width:100px;" value="<?php echo $row[0]['RS_Contact'];?>"/></td>                     
                        <td>關係:<input name="RS_Relationship" id="RS_Relationship" style="width:100px;" value="<?php echo $row[0]['RS_Relationship'];?>"/></td> 
                        <td>手機號碼:<input name="RS_Mobile" id="RS_Mobile" style="width:100px;" value="<?php echo $row[0]['RS_Mobile'];?>"/></td>
                      </tr>
                    </table>
                    </td>
                </tr>
                <tr>
                    <td height="5"></td>
                </tr>
                <tr>
                  <td width="150" align="right"  class="content">照護重點：</td>
                  <td width="705">
                  <table>
                    <tr>
                      <td>
                        <input name="RS_Care[]" type="checkbox" class="form_fix" id="RS_Care1" value="1" <?php echo ckbox('1',$RS_Care);?>/>入住流程/篩檢/門診/緊急就醫處置流程說明 
                        <input name="RS_Care[]" type="checkbox" class="form_fix" id="RS_Care2" value="2" <?php echo ckbox('2',$RS_Care);?>/>安全維護 
                        <input name="RS_Care[]" type="checkbox" class="form_fix" id="RS_Care3" value="3" <?php echo ckbox('3',$RS_Care);?>/>自聘照服員健康篩檢及管理流程 
                        <input name="RS_Care[]" type="checkbox" class="form_fix" id="RS_Care4" value="4" <?php echo ckbox('4',$RS_Care);?>/>營養照護 
                        <input name="RS_Care[]" type="checkbox" class="form_fix" id="RS_Care5" value="5" <?php echo ckbox('5',$RS_Care);?>/>管路照護及合併症預防
                        <input name="RS_Care[]" type="checkbox" class="form_fix" id="RS_Care6" value="6" <?php echo ckbox('6',$RS_Care);?>/>心理調適與就醫
                        <input name="RS_Care[]" type="checkbox" class="form_fix" id="RS_Care7" value="7" <?php echo ckbox('7',$RS_Care);?>/>肢體/日常生活功能復健
                        <input name="RS_Care[]" type="checkbox" class="form_fix" id="RS_Care8" value="8" <?php echo ckbox('8',$RS_Care);?>/>傷口照護
                      </td>
                    </tr>
                    <tr> 
                      <td>
                        <?php
                          $RS_PCare = $row[0]['RS_PCare'];   
              
                          include_once "../CKEdit/ckeditor/ckeditor.php";
  
                          $CKEditor3 = new CKEditor();
                          //$CKEditor->config['width'] = 500;
                          $CKEditor3->config['toolbar'] = 'MyToolbar';           
                          $CKEditor3->basePath = '../CKEdit/ckeditor/';
                          $CKEditor3->editor("RS_PCare", $RS_PCare); 
                        ?>
                      </td>
                    </tr>
                  </table>
                  </td>
                </tr>
                <tr>
                    <td height="5"></td>
                </tr>
                <tr>
                  <td width="150" align="right"  class="content">潛在問題：</td>
                  <td width="705">
                    <input name="RS_Problems[]" type="checkbox" class="form_fix" id="RS_Problems1" value="1" <?php echo ckbox('1',$RS_Problems);?>/>跌倒 
                    <input name="RS_Problems[]" type="checkbox" class="form_fix" id="RS_Problems2" value="2" <?php echo ckbox('2',$RS_Problems);?>/>感染 
                    <input name="RS_Problems[]" type="checkbox" class="form_fix" id="RS_Problems3" value="3" <?php echo ckbox('3',$RS_Problems);?>/>調適困難 
                    <input name="RS_Problems[]" type="checkbox" class="form_fix" id="RS_Problems4" value="4" <?php echo ckbox('4',$RS_Problems);?>/>藥物毒性 
                    <input name="RS_Problems[]" type="checkbox" class="form_fix" id="RS_Problems5" value="5" <?php echo ckbox('5',$RS_Problems);?>/>走失
                    <input name="RS_Problems[]" type="checkbox" class="form_fix" id="RS_Problems6" value="6" <?php echo ckbox('6',$RS_Problems);?>/>再住院
                    <input name="RS_Problems[]" type="checkbox" class="form_fix" id="RS_Problems7" value="7" <?php echo ckbox('7',$RS_Problems);?>/>憂鬱
                    <input name="RS_Problems[]" type="checkbox" class="form_fix" id="RS_Problems8" value="8" <?php echo ckbox('8',$RS_Problems);?>/>瘻管不良
                  </td>
                </tr>
                <tr>
                    <td height="5"></td>
                </tr>
                <tr>
                  <td width="150" align="right"  class="content">會談結果：</td>
                  <td width="705">
                    <input name="RS_Result" type="radio" class="form_fix" id="RS_Result1" value="1" <?php echo ckRadio('1',$row[0]['RS_Result']);?>/>適合入住 
                    <input name="RS_Result" type="radio" class="form_fix" id="RS_Result2" value="2" <?php echo ckRadio('2',$row[0]['RS_Result']);?>/>宜再評估 
                    <input name="RS_Result" type="radio" class="form_fix" id="RS_Result3" value="3" <?php echo ckRadio('3',$row[0]['RS_Result']);?>/>不適合入住 
                  </td>
                </tr>
                <tr>
                  <td align="right" class="content">&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>
                      <input name="mybtn" type="button" class="form_fix" id="mybtn"  value="確定送出"/>
                  </td>
                </tr>                
              </table>
              </form>
              <!--管理員管理 ending--></td>
            </tr>   
          </table></td>
        </tr> 
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>

<!--
<select name="RS_Status" id="RS_Status">
                  <option id="RS_Status0" name="RS_Status" value="" >請選擇</option>
                  <option  id="RS_Status1" name="RS_Status" value="1" <?php if($row[0]["RS_Status"] == 1) echo "selected"; ?>>1</option>
                  <option  id="RS_Status2" name="RS_Status" value="2" <?php if($row[0]["RS_Status"] == 2) echo "selected"; ?>>2</option>
                  </select>
                  -->