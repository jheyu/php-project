<?php
    include_once '../public/web_function.php';
	include_once '../public/mem_check.php';
	
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}	
	
	$action = $_REQUEST["action"];
	switch ($action) {
		case "new":	
		
		$RS_ID = $objDB->GetMaxID('RS_ID','resident',3);
		$RS_Name = quotes($_POST["RS_Name"]);		
		$RS_Birthday = quotes($_POST["RS_Birthday"]);
        $RS_Status = quotes($_POST["RS_Status"]);
		$RS_IdentityCard = quotes($_POST["RS_IdentityCard"]);
		$RS_Sex = quotes($_POST["RS_Sex"]);		
		$RS_History = quotes($_POST["RS_History"]);
		$RS_Education = quotes($_POST["RS_Education"]);
        $RS_LCondition = quotes($_POST["RS_LCondition"]);
        $RS_LCondition_Other = quotes($_POST["RS_LCondition_Other"]);
        $RS_Source = quotes($_POST["RS_Source"]);
        $RS_Source_Other = quotes($_POST["RS_Source_Other"]);
        
        $diagnosis = quotes($_POST["RS_Diagnosis"]);
        $RS_Diagnosis = implode(",", $diagnosis);

        $reason = quotes($_POST["RS_Reason"]);
        $RS_Reason = implode(",", $reason);
        
        $RS_Reason_Other = quotes($_POST["RS_Reason_Other"]);

        $phistory = quotes($_POST["RS_PHistory"]);
        $RS_PHistory = implode(",", $phistory);
        
        $RS_PHistory_Other = quotes($_POST["RS_PHistory_Other"]);
        
        $allergy = quotes($_POST["RS_Allergy"]);
        $RS_Allergy = implode(",", $allergy);
 
        $RS_MAllergy = quotes($_POST["RS_MAllergy"]);
        $RS_FAllergy = quotes($_POST["RS_FAllergy"]);

        $RS_Expression = quotes($_POST["RS_Expression"]);
        $RS_EExpression = quotes($_POST["RS_EExpression"]);
        $RS_VExpression = quotes($_POST["RS_VExpression"]);
        $RS_MExpression = quotes($_POST["RS_MExpression"]);

        $hcondition = quotes($_POST["RS_HCondition"]);
        $RS_HCondition = implode(",", $hcondition);
        
        $scondition = quotes($_POST["RS_SCondition"]);
        $RS_SCondition = implode(",", $scondition);

        $RS_Identity = quotes($_POST["RS_Identity"]);
        $RS_UIdentity = quotes($_POST["RS_UIdentity"]);
        $RS_MIdentity = quotes($_POST["RS_MIdentity"]);
        
        $mcondition = quotes($_POST["RS_MCondition"]);
        $RS_MCondition = implode(",", $mcondition);
        
        $auxiliary = quotes($_POST["RS_Auxiliary"]);
        $RS_Auxiliary = implode(",", $auxiliary);
        
        $pipe = quotes($_POST["RS_Pipe"]);
        $RS_Pipe = implode(",", $pipe);
        
        $RS_TracheostomyFR = quotes($_POST["RS_TracheostomyFR"]);
        $RS_TracheostomyType = quotes($_POST["RS_TracheostomyType"]);
        $RS_Tracheostomy_Date = quotes($_POST["RS_Tracheostomy_Date"]);
        $RS_NGtubeFR = quotes($_POST["RS_NGtubeFR"]);
        $RS_NGtube_Date = quotes($_POST["RS_NGtube_Date"]);
        $RS_CatheterFR = quotes($_POST["RS_CatheterFR"]);
        $RS_Catheter_Date = quotes($_POST["RS_Catheter_Date"]);
        $RS_BladderFR = quotes($_POST["RS_BladderFR"]);
        $RS_Bladder_Date = quotes($_POST["RS_Bladder_Date"]);
        $RS_StomachFR = quotes($_POST["RS_StomachFR"]);
        $RS_Stomach_Date = quotes($_POST["RS_Stomach_Date"]);
        $RS_Wound = quotes($_POST["RS_Wound"]);
        $RS_Pipe_Other = quotes($_POST["RS_Pipe_Other"]);
        $RS_Auxiliary_Other = quotes($_POST["RS_Auxiliary_Other"]);
        $RS_Expect = quotes($_POST["RS_Expect"]);
        $RS_Broster = quotes($_POST["RS_Broster"]);
        $RS_Sonter = quotes($_POST["RS_Sonter"]);
        $RS_Grand = quotes($_POST["RS_Grand"]);
        $RS_Contact = quotes($_POST["RS_Contact"]);
        $RS_Relationship = quotes($_POST["RS_Relationship"]);
        $RS_Tel = quotes($_POST["RS_Tel"]);
        $RS_Mobile = quotes($_POST["RS_Mobile"]);
        
        $care = quotes($_POST["RS_Care"]);
        $RS_Care = implode(",", $care);

        $RS_PCare = quotes($_POST["RS_PCare"]);

        $problems = quotes($_POST["RS_Problems"]);
        $RS_Problems = implode(",", $problems);

        $RS_Result = quotes($_POST["RS_Result"]);

		$RS_Date = date("Y-m-d H:i:s");	
		
		$sql = "insert into resident (RS_ID,RS_Name,RS_Status,RS_IdentityCard,RS_Birthday,RS_Sex,RS_History,RS_Education,
		   RS_LCondition,RS_LCondition_Other,RS_Source,RS_Source_Other,RS_Reason,RS_Reason_Other,RS_PHistory,RS_PHistory_Other,
		   RS_Allergy,RS_MAllergy,RS_FAllergy,RS_Expression,RS_EExpression,RS_VExpression,RS_MExpression,RS_HCondition,RS_SCondition
           RS_Identity,RS_UIdentity,RS_MIdentity,RS_MCondition,RS_Auxiliary,RS_Auxiliary_Other,RS_Pipe,RS_TracheostomyFR,
           RS_TracheostomyType,RS_Tracheostomy_Date,RS_NGtubeFR,RS_NGtube_Date,RS_CatheterFR,RS_Catheter_Date,RS_BladderFR,
           RS_Bladder_Date,RS_StomachFR,RS_Stomach_Date,RS_Wound,RS_Pipe_Other,RS_Expect,RS_Broster,RS_Sonter,RS_Grand,
		   RS_Contact,RS_Relationship,RS_Tel,RS_Mobile,RS_Care,RS_PCare,RS_Problems,RS_Result,RS_Diagnosis,RS_Date) 
           values ('$RS_ID','$RS_Name','$RS_Status',
		   '$RS_IdentityCard','$RS_Birthday','$RS_Sex','$RS_History','$RS_Education','$RS_LCondition','$RS_LCondition_Other',
		   '$RS_Source','$RS_Source_Other','$RS_Reason','$RS_Reason_Other','$RS_PHistory','$RS_PHistory_Other',
		   '$RS_Allergy','$RS_MAllergy','$RS_FAllergy','$RS_Expression','$RS_EExpression','$RS_VExpression','$RS_MExpression',
           '$RS_HCondition','$RS_SCondition','$RS_Identity','$RS_UIdentity','$RS_MIdentity','$RS_MCondition','$RS_Auxiliary','$RS_Auxiliary_Other',
           '$RS_Pipe','$RS_TracheostomyFR','$RS_TracheostomyType','$RS_Tracheostomy_Date','$RS_NGtubeFR','$RS_NGtube_Date',
           '$RS_CatheterFR','$RS_Catheter_Date','$RS_BladderFR','$RS_Bladder_Date','$RS_StomachFR','$RS_Stomach_Date',
           '$RS_Wound','$RS_Pipe_Other','$RS_Expect','$RS_Broster','$RS_Sonter','$RS_Grand','$RS_Contact','$RS_Relationship',
           '$RS_Tel','$RS_Mobile','$RS_Care','$RS_PCare','$RS_Problems','$RS_Result','$RS_Diagnosis','$RS_Date')";
		$objDB->Execute($sql);
		
	
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script language="javascript">
alert('新增成功!');
<?php 
  if($RS_Status == 1){
?>
location.href='../welcome/main.php?RS_Status=1';
<?php 
  }
  else {
?>
  location.href='../welcome/main.php?RS_Status=2';
<?php
  }
?>
</script>
<?php
		break;
		
		case "mdy":	

		$RS_ID = $_POST["RS_ID"];		
		$RS_Name = quotes($_POST["RS_Name"]);		
		$RS_Birthday = quotes($_POST["RS_Birthday"]);
		$RS_IdentityCard = quotes($_POST["RS_IdentityCard"]);
        $RS_Status = quotes($_POST["RS_Status"]);
		$RS_Sex = quotes($_POST["RS_Sex"]);		
		$RS_History = quotes($_POST["RS_History"]);
		$RS_Education = quotes($_POST["RS_Education"]);
        $RS_LCondition = quotes($_POST["RS_LCondition"]);
        $RS_LCondition_Other = quotes($_POST["RS_LCondition_Other"]);
        $RS_Source = quotes($_POST["RS_Source"]);
        $RS_Source_Other = quotes($_POST["RS_Source_Other"]);
        
        $diagnosis = quotes($_POST["RS_Diagnosis"]);
        $RS_Diagnosis = implode(",", $diagnosis);

        $reason = quotes($_POST["RS_Reason"]);
        $RS_Reason = implode(",", $reason);
        
        $RS_Reason_Other = quotes($_POST["RS_Reason_Other"]);

        $phistory = quotes($_POST["RS_PHistory"]);
        $RS_PHistory = implode(",", $phistory);
        
        $RS_PHistory_Other = quotes($_POST["RS_PHistory_Other"]);
        
        $allergy = quotes($_POST["RS_Allergy"]);
        echo $allergy[0].' '.$allergy[1].'<br>';
        $RS_Allergy = implode(",", $allergy);
        echo $RS_Allergy."<br>";

        $RS_MAllergy = quotes($_POST["RS_MAllergy"]);
        $RS_FAllergy = quotes($_POST["RS_FAllergy"]);

        $RS_Expression = quotes($_POST["RS_Expression"]);
        $RS_EExpression = quotes($_POST["RS_EExpression"]);
        $RS_VExpression = quotes($_POST["RS_VExpression"]);
        $RS_MExpression = quotes($_POST["RS_MExpression"]);

        $hcondition = quotes($_POST["RS_HCondition"]);
        $RS_HCondition = implode(",", $hcondition);

        $scondition = quotes($_POST["RS_SCondition"]);
        $RS_SCondition = implode(",", $scondition);

        $RS_Identity = quotes($_POST["RS_Identity"]);
        $RS_UIdentity = quotes($_POST["RS_UIdentity"]);
        $RS_MIdentity = quotes($_POST["RS_MIdentity"]);
        
        $mcondition = quotes($_POST["RS_MCondition"]);
        $RS_MCondition = implode(",", $mcondition);

        //echo $RS_MCondition[0].' '.$RS_MCondition[1].' ';
        $auxiliary = quotes($_POST["RS_Auxiliary"]);
        $RS_Auxiliary = implode(",", $auxiliary);
        
        $pipe = quotes($_POST["RS_Pipe"]);
        $RS_Pipe = implode(",", $pipe);
        
        $RS_TracheostomyFR = quotes($_POST["RS_TracheostomyFR"]);
        $RS_TracheostomyType = quotes($_POST["RS_TracheostomyType"]);
        $RS_Tracheostomy_Date = quotes($_POST["RS_Tracheostomy_Date"]);
        $RS_NGtubeFR = quotes($_POST["RS_NGtubeFR"]);
        $RS_NGtube_Date = quotes($_POST["RS_NGtube_Date"]);
        $RS_CatheterFR = quotes($_POST["RS_CatheterFR"]);
        $RS_Catheter_Date = quotes($_POST["RS_Catheter_Date"]);
        $RS_BladderFR = quotes($_POST["RS_BladderFR"]);
        $RS_Bladder_Date = quotes($_POST["RS_Bladder_Date"]);
        $RS_StomachFR = quotes($_POST["RS_StomachFR"]);
        $RS_Stomach_Date = quotes($_POST["RS_Stomach_Date"]);
        $RS_Wound = quotes($_POST["RS_Wound"]);
        $RS_Pipe_Other = quotes($_POST["RS_Pipe_Other"]);
        $RS_Auxiliary_Other = quotes($_POST["RS_Auxiliary_Other"]);
        $RS_Expect = quotes($_POST["RS_Expect"]);
        $RS_Broster = quotes($_POST["RS_Broster"]);
        $RS_Sonter = quotes($_POST["RS_Sonter"]);
        $RS_Grand = quotes($_POST["RS_Grand"]);
        $RS_Contact = quotes($_POST["RS_Contact"]);
        $RS_Relationship = quotes($_POST["RS_Relationship"]);
        $RS_Tel = quotes($_POST["RS_Tel"]);
        $RS_Mobile = quotes($_POST["RS_Mobile"]);
        
        $care = quotes($_POST["RS_Care"]);
        $RS_Care = implode(",", $care);

        $RS_PCare = quotes($_POST["RS_PCare"]);

        $problems = quotes($_POST["RS_Problems"]);
        $RS_Problems = implode(",", $problems);

        $RS_Result = quotes($_POST["RS_Result"]);

		$RS_Date = date("Y-m-d H:i:s");	
		
		$sql = "update resident set RS_Name='$RS_Name',RS_Status='$RS_Status',RS_IdentityCard='$RS_IdentityCard',RS_Birthday='$RS_Birthday',
		   RS_Sex='$RS_Sex',RS_History='$RS_History',RS_Education='$RS_Education',RS_LCondition='$RS_LCondition',
		   RS_LCondition_Other='$RS_LCondition_Other',RS_Source='$RS_Source',RS_Source_Other='$RS_Source_Other',
		   RS_Reason='$RS_Reason',RS_Reason_Other='$RS_Reason_Other',RS_PHistory='$RS_PHistory',RS_PHistory_Other='$RS_PHistory_Other',
		   RS_Allergy='$RS_Allergy',RS_MAllergy='$RS_MAllergy',RS_FAllergy='$RS_FAllergy',RS_Expression='$RS_Expression',
		   RS_EExpression='$RS_EExpression',RS_VExpression='$RS_VExpression',RS_MExpression='$RS_MExpression',
		   RS_HCondition='$RS_HCondition',RS_SCondition='$RS_SCondition',RS_Identity='$RS_Identity',RS_UIdentity='$RS_UIdentity',RS_MIdentity='$RS_MIdentity',
		   RS_MCondition='$RS_MCondition',RS_Auxiliary='$RS_Auxiliary',RS_Auxiliary_Other='$RS_Auxiliary_Other',RS_Pipe='$RS_Pipe',
           RS_TracheostomyFR='$RS_TracheostomyFR',RS_TracheostomyType='$RS_TracheostomyType',RS_Tracheostomy_Date='$RS_Tracheostomy_Date',
           RS_NGtubeFR='$RS_NGtubeFR',RS_NGtube_Date='$RS_NGtube_Date',RS_CatheterFR='$RS_CatheterFR',RS_Catheter_Date='$RS_Catheter_Date',
           RS_BladderFR='$RS_BladderFR',RS_Bladder_Date='$RS_Bladder_Date',RS_StomachFR='$RS_StomachFR',RS_Stomach_Date='$RS_Stomach_Date',
           RS_Wound='$RS_Wound',RS_Pipe_Other='$RS_Pipe_Other',RS_Expect='$RS_Expect',RS_Broster='$RS_Broster',RS_Sonter='$RS_Sonter',
           RS_Grand='$RS_Grand',RS_Contact='$RS_Contact',RS_Relationship='$RS_Relationship',RS_Tel='$RS_Tel',RS_Mobile='$RS_Mobile',
           RS_Care='$RS_Care',RS_PCare='$RS_PCare',RS_Problems='$RS_Problems',RS_Result='$RS_Result',RS_Diagnosis='$RS_Diagnosis',RS_Date='$RS_Date' where RS_ID='$RS_ID'"; 
		
		$objDB->Execute($sql);
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />        
<script language="javascript">
alert('修改成功!');
<?php 
  if($RS_Status == 1){
?>
  location.href='../welcome/main.php?RS_Status=1&RS_ID=<?php echo $RS_ID;?>';
<?php 
  }
  else {
?>
  location.href='../welcome/main.php?RS_Status=2&RS_ID=<?php echo $RS_ID;?>';
<?php
  }
?>
</script>	
	<?php
		break;
	
		case "del":
				$RS_ID = quotes(trim($_REQUEST["RS_ID"]));
			
				$sql = "delete from resident where RS_ID ='$RS_ID'";
				$objDB->Execute($sql);
	?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script language="javascript">
alert('資料刪除完畢!');
location.href='register.php';
</script>
<?php
	break;
			
}	
	
?>
