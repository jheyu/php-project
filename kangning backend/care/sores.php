<?php 	
	$rs = $objDB->Recordset("SELECT * FROM sores WHERE RS_ID = '$RS_ID' ORDER BY SR_Date DESC");
	$row = $objDB->GetRows($rs);	

	$SR_ID = $row[0]['SR_ID'];
?>

<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.datepicker.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){
    
    $(".date-pick" ).datepicker({ 
		dateFormat: 'yy-mm-dd', 
		showOn: "button",
		buttonImage: "../js/calendar.png",
		buttonImageOnly: true
	});

    $("#mybtn").click(function(){
		$("form#form1").submit();		
	});

})

</script>
<link type="text/css" href="../css/ui-darkness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<link href="../css/backend.css" rel="stylesheet" type="text/css" />

<tr>
	<td height="15"></td>
</tr>           
<tr>
	<td style="font-size: 13pt"><strong>壓瘡評估紀錄</strong>
	    <?php echo "(".$row[0]['SR_Date'] .")";?>
	</td>
</tr> 
<tr>
	<td height="5"></td>
</tr>
<tr>
	<td ><img src="../images/blueline.jpg" /></td>
</tr> 
<tr>
	<td height="5"></td>
</tr>   
<tr>         
	<td>   
	<form name="form1" id="form1" method="post" action="sores_process.php"/>
	<input type="hidden" name="action" id="action" value="mdy"/>                           
	<input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>"  />
    <input type="hidden" name="SR_ID" id="SR_ID" value="<?php echo $SR_ID;?>" />          
	<table>                
	<tr>
		<td height="10"></td>
	</tr>  
	<tr>
        <td width="110" align="right"  class="content">部位：</td>
		<td><input name="SR_Position" type="text" class="form_fix" id="SR_Position" style="width:120px;" value="<?php echo $row[0]['SR_Position'];?>" /></td>
	</tr> 
	<tr>
		<td height="10"></td>
	</tr>        
	<tr>
		<td width="110" align="right"  class="content">型態：</td>
		<td width="705">
		<input name="SR_Type" type="radio" class="form_fix" id="SR_Type1" value="1" <?php echo ckRadio('1',$row[0]['SR_Type']);?> />一般
		<input name="SR_Type" type="radio" class="form_fix" id="SR_Type2" value="2" <?php echo ckRadio('2',$row[0]['SR_Type']);?> />壓瘡	
		<input name="SR_Type" type="radio" class="form_fix" id="SR_Type3" value="3" <?php echo ckRadio('3',$row[0]['SR_Type']);?> />術後	
		</td>
	</tr>   
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
        <td width="110" align="right"  class="content">範圍：</td>
        <td width="705">
		長<input name="SR_Length" type="text" class="form_fix" id="SR_Length" style="width:30px;" value="<?php echo $row[0]['SR_Length'];?>" />cm &nbsp;  
		寬<input name="SR_Width" type="text" class="form_fix" id="SR_Width" style="width:30px;" value="<?php echo $row[0]['SR_Width'];?>" />cm &nbsp; 
		深<input name="SR_Depth" type="text" class="form_fix" id="SR_Depth" style="width:30px;" value="<?php echo $row[0]['SR_Depth'];?>" />cm &nbsp; 
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
        <td width="110" align="right"  class="content">層級：</td>
		<td width="705">
		<input name="SR_Level" type="radio" class="form_fix" id="SR_Level1" value="1" <?php echo ckRadio('1',$row[0]['SR_Level']);?> />Ⅰ
		<input name="SR_Level" type="radio" class="form_fix" id="SR_Level2" value="2" <?php echo ckRadio('2',$row[0]['SR_Level']);?> />Ⅱ	
		<input name="SR_Level" type="radio" class="form_fix" id="SR_Level3" value="3" <?php echo ckRadio('3',$row[0]['SR_Level']);?> />Ⅲ
		<input name="SR_Level" type="radio" class="form_fix" id="SR_Level4" value="4" <?php echo ckRadio('4',$row[0]['SR_Level']);?> />Ⅳ	
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
        <td width="110" align="right"  class="content">顏色：</td>
		<td width="705">
		<input name="SR_Color" type="radio" class="form_fix" id="SR_Color1" value="1" <?php echo ckRadio('1',$row[0]['SR_Color']);?> />紅
		<input name="SR_Color" type="radio" class="form_fix" id="SR_Color2" value="2" <?php echo ckRadio('2',$row[0]['SR_Color']);?> />黃	
		<input name="SR_Color" type="radio" class="form_fix" id="SR_Color3" value="3" <?php echo ckRadio('3',$row[0]['SR_Color']);?> />黑
		<input name="SR_Color" type="radio" class="form_fix" id="SR_Color4" value="4" <?php echo ckRadio('4',$row[0]['SR_Color']);?> />其他	
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
        <td width="110" align="right"  class="content">分泌物：</td>
		<td width="700">
		<table>
		<tr><td width="30" align="left" class="content">量:</td>
        <td><input name="SR_SAmount" type="radio" class="form_fix" id="SR_SAmount1" value="1"  <?php echo ckRadio('1',$row[0]['SR_SAmount']);?>/>少(5cc/天，CD 1次/天)
		    <input name="SR_SAmount" type="radio" class="form_fix" id="SR_SAmount2" value="2"  <?php echo ckRadio('2',$row[0]['SR_SAmount']);?>/>中(5-10cc/天，CD 2-3次/天)
		    <input name="SR_SAmount" type="radio" class="form_fix" id="SR_SAmount3" value="3"  <?php echo ckRadio('3',$row[0]['SR_SAmount']);?>/>多(>10cc/天, CD >3次/天)	
	    </td>
	    </tr>
	    <tr></tr>
	    <tr><td width="30" align="left" class="content">顏色:</td>
        <td><input name="SR_SColor" type="radio" class="form_fix" id="SR_SColor1" value="1"  <?php echo ckRadio('1',$row[0]['SR_SColor']);?>/>清水性(透明)
		    <input name="SR_SColor" type="radio" class="form_fix" id="SR_SColor2" value="2"  <?php echo ckRadio('2',$row[0]['SR_SColor']);?>/>膿液性(黃，綠，黃綠混合或褐色粘稠)
		    <input name="SR_SColor" type="radio" class="form_fix" id="SR_SColor3" value="3"  <?php echo ckRadio('3',$row[0]['SR_SColor']);?>/>漿液性(潛紅血液狀)	
		    <input name="SR_SColor" type="radio" class="form_fix" id="SR_SColor4" value="4"  <?php echo ckRadio('4',$row[0]['SR_SColor']);?>/>血水性(透明淺紅)	
		    <input name="SR_SColor" type="radio" class="form_fix" id="SR_SColor5" value="5"  <?php echo ckRadio('5',$row[0]['SR_SColor']);?>/>膿血性(棕褐，黃綠或紅色等混雜的黏稠血性狀態，有異臭)
	    </td>
        </tr>
		</table>
        </td>
    </tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
        <td width="110" align="right"  class="content">換藥：</td>
		<td width="700">
		<table>
		<tr><td width="30" align="left" class="content">方式:</td>
        <td><input name="SR_DMode" type="radio" class="form_fix" id="SR_DMode1" value="1"  <?php echo ckRadio('1',$row[0]['SR_DMode']);?>/>sofra-tule/凡士林紗
		    <input name="SR_DMode" type="radio" class="form_fix" id="SR_DMode2" value="2"  <?php echo ckRadio('2',$row[0]['SR_DMode']);?>/>NS紗布濕敷
		    <input name="SR_DMode" type="radio" class="form_fix" id="SR_DMode3" value="3"  <?php echo ckRadio('3',$row[0]['SR_DMode']);?>/>1:20(優碘:NS)紗布濕敷	
		    <input name="SR_DMode" type="radio" class="form_fix" id="SR_DMode4" value="4"  <?php echo ckRadio('4',$row[0]['SR_DMode']);?>/>薄膜敷料
		    <input name="SR_DMode" type="radio" class="form_fix" id="SR_DMode5" value="5"  <?php echo ckRadio('5',$row[0]['SR_DMode']);?>/>水性膠體敷料
		    <input name="SR_DMode" type="radio" class="form_fix" id="SR_DMode6" value="6"  <?php echo ckRadio('6',$row[0]['SR_DMode']);?>/>海藻膠敷料
		    <input name="SR_DMode" type="radio" class="form_fix" id="SR_DMode7" value="7"  <?php echo ckRadio('7',$row[0]['SR_DMode']);?>/>其他
	    </td>
	    </tr>
	    <tr></tr>
	    <tr><td width="30" align="left" class="content">頻率:</td>
        <td>一天<input name="SR_DFrequency" type="text" class="form_fix" id="SR_DFrequency" style="width:30px;" value="<?php echo $row[0]['SR_DFrequency'];?>" />次
	    </td>
        </tr>
		</table>
        </td>
    </tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
        <td width="110" align="right"  class="content">簽名：</td>
		<td><input name="SR_Signature" type="text" class="form_fix" id="SR_Signature" style="width:120px;" value="<?php echo $row[0]['SR_Signature'];?>" />
		</td>
	</tr>   
	<tr>
		<td height="10"></td>
	</tr>                  
	<tr>
		<td align="right" class="content">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
			<input name="mybtn" type="button" class="form_fix" id="mybtn"  value="確定送出"  /> 
 		 <input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>
		</td>
	</tr>                
	</table>
	</form>              
	</td>
</tr>