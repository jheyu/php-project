<?php 
include("../public/mem_check.php");
include("../public/web_function.php");
	/*
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}*/
	if(is_numeric(quotes($_GET['RS_ID']))){
		 $RS_ID = quotes($_GET['RS_ID']);
	}else{
		 ?>
     <script language="javascript">		
		location.href='../index.php';
	 </script>	
         <?php
	}	
	
	$sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
	$rs = $objDB->Recordset($sql);
	$row = $objDB->GetRows($rs);	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script>
var score0 = 0;
var score1 = 0;
var score2 = 0;
var score3 = 0;
var score4 = 0;

var total = 0;
var level;

var caculate = function(){
	  
	total = score0 + score1 + score2 + score3 + score4;

    if(total >= 21){
	 	level = "(重度)";
	}else if(total >= 15 && total <= 20){
	 	level = "(中度)";
	}else{
	 	level = "(輕度)";
	}

	$("#SCORE").text(total);
	$("#LEVEL").text(level);
}

$(document).ready(function(){
	
	$("input[name='TAI_Spirit']").click(function(){
		var TAI_Spirit = $("input[name='TAI_Spirit']:checked").val();
		score0 = parseInt(TAI_Spirit);		
		caculate();		
	})

	$("input[name='TAI_Activity']").click(function(){
		var TAI_Activity = $("input[name='TAI_Activity']:checked").val();
		score1 = parseInt(TAI_Activity);		
		caculate();		
	})

	$("input[name='TAI_Eat']").click(function(){
		var TAI_Eat = $("input[name='TAI_Eat']:checked").val();
		score2 = parseInt(TAI_Eat);		
		caculate();		
	})

	$("input[name='TAI_Drain']").click(function(){
		var TAI_Drain = $("input[name='TAI_Drain']:checked").val();
		score3 = parseInt(TAI_Drain);		
		caculate();		
	})	

	$("input[name='TAI_Medical']").click(function(){
		var TAI_Medical = $("input[name='TAI_Medical']:checked").val();
		score4 = parseInt(TAI_Medical);		
		caculate();		
	})	
	 
 	$("#mybtn").click(function(){
		$("#TAI_Score").val(total);
		$("form#form1").submit();	
	})
})
</script>
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<!-- header starting point -->
	<?php include("../include/header.php");?>
	<!-- header ending point -->    
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?>
        <!--menu ending point-->          
        </td>            
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top">
        <table width="830" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="30" class="content">專業照護 > 評估與記錄 &gt; 新增</td>
              </tr>
              <tr>
                  <td height="10"></td>
              </tr> 
              <tr>
                <td height="10">
                <span class="form_title">
                     <input name="search" type="button" class="content" id="search" value="回上一頁" onclick="MM_goToURL('parent','layout.php?t=overview&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>
                </span></td>
              </tr>   
              <tr>
                  <td height="10"></td>
              </tr>                         
             <tr>
              	<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>   
             </tr>   
             <tr>
                <td height="30"></td>
              </tr>           
              
              <tr>
              		<td style="font-size: 13pt"><strong>照護分級量表(TAI)</strong>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  分數:<label id="SCORE"> </label><label id="LEVEL"> </label>
                  </td>            
              </tr> 
              <tr>
                <td height="5"></td>
              </tr>
              <tr>
              	  <td ><img src="../images/blueline.jpg" /></td>
              </tr> 
               <tr>
                <td height="5"></td>
              </tr>   
            <tr>         
              <td>    
              <!--管理員管理startinging-->              
              <form name="form1" id="form1" method="post" action="tai_process.php" />
			   <input type="hidden" name="action" id="action" value="new"/>              
              <input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>" />
              <input type="hidden" name="TAI_Score" id="TAI_Score" />               
             <table>
                <tr>
                  <td height="10"></td>
                </tr>                
                <tr>
                 <td width="80" align="right"  class="content">精神：</td>
                 <td width="735">
                 <input name="TAI_Spirit" type="radio" class="form_fix" id="TAI_Spirit5" value="5"/>5
                 <input name="TAI_Spirit" type="radio" class="form_fix" id="TAI_Spirit4" value="4"/>4
                 <input name="TAI_Spirit" type="radio" class="form_fix" id="TAI_Spirit3" value="3"/>3 
                 <input name="TAI_Spirit" type="radio" class="form_fix" id="TAI_Spirit2" value="2"/>2
                 <input name="TAI_Spirit" type="radio" class="form_fix" id="TAI_Spirit1" value="1"/>1                
                 </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                 <td width="80" align="right"  class="content">活動：</td>
                 <td width="735">
                 <input name="TAI_Activity" type="radio" class="form_fix" id="TAI_Activity5" value="5"/>5
                 <input name="TAI_Activity" type="radio" class="form_fix" id="TAI_Activity4" value="4"/>4
                 <input name="TAI_Activity" type="radio" class="form_fix" id="TAI_Activity3" value="3"/>3 
                 <input name="TAI_Activity" type="radio" class="form_fix" id="TAI_Activity2" value="2"/>2
                 <input name="TAI_Activity" type="radio" class="form_fix" id="TAI_Activity1" value="1"/>1                 
                 </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                 <td width="80" align="right"  class="content">飲食：</td>
                 <td width="735">
                 <input name="TAI_Eat" type="radio" class="form_fix" id="TAI_Eat5" value="5"/>5
                 <input name="TAI_Eat" type="radio" class="form_fix" id="TAI_Eat4" value="4"/>4
                 <input name="TAI_Eat" type="radio" class="form_fix" id="TAI_Eat3" value="3"/>3 
                 <input name="TAI_Eat" type="radio" class="form_fix" id="TAI_Eat2" value="2"/>2
                 <input name="TAI_Eat" type="radio" class="form_fix" id="TAI_Eat1" value="1"/>1                 
                 </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                 <td width="80" align="right"  class="content">排泄：</td>
                 <td width="735">
                 <input name="TAI_Drain" type="radio" class="form_fix" id="TAI_Drain5" value="5"/>5
                 <input name="TAI_Drain" type="radio" class="form_fix" id="TAI_Drain4" value="4"/>4
                 <input name="TAI_Drain" type="radio" class="form_fix" id="TAI_Drain3" value="3"/>3 
                 <input name="TAI_Drain" type="radio" class="form_fix" id="TAI_Drain2" value="2"/>2
                 <input name="TAI_Drain" type="radio" class="form_fix" id="TAI_Drain1" value="1"/>1                
                 </td>
                </tr>                  
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                 <td width="80" align="right"  class="content">醫療：</td>
                 <td width="735">
                 <input name="TAI_Medical" type="radio" class="form_fix" id="TAI_Medical5" value="5"/>5
                 <input name="TAI_Medical" type="radio" class="form_fix" id="TAI_Medical4" value="4"/>4
                 <input name="TAI_Medical" type="radio" class="form_fix" id="TAI_Medical3" value="3"/>3 
                 <input name="TAI_Medical" type="radio" class="form_fix" id="TAI_Medical2" value="2"/>2
                 <input name="TAI_Medical" type="radio" class="form_fix" id="TAI_Medical1" value="1"/>1                 
                 </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>  
                     
                <tr>
                  <td align="right" class="content">&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>
                      <input name="mybtn" type="button" class="form_fix" id="mybtn"  value="確定送出" />&nbsp; 
                      <!--<input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>-->
                   </td>
                </tr>                
              </table>
              </form>
              <!--管理員管理 ending-->              
              </td>
            </tr>
            
          </table>
          </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>