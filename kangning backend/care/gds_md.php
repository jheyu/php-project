<?php 	
include("../public/mem_check.php");
include("../public/web_function.php");

if(is_numeric(quotes($_GET['RS_ID']))){
		 $RS_ID = quotes($_GET['RS_ID']);
}else{
?>
     <script language="javascript">		
		//location.href='../index.php';
	 </script>	
<?php
}	
    
    $sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
	$rs = $objDB->Recordset($sql);
	$row = $objDB->GetRows($rs);

	$rs_form = $objDB->Recordset("SELECT * FROM gds WHERE RS_ID = '$RS_ID' ORDER BY GDS_Date DESC");
	$row_form = $objDB->GetRows($rs_form);
	
	$GDS_ID = $row_form[0]['GDS_ID'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.datepicker.min.js"></script>
<script>
var score0 = <?php echo $row_form[0]['GDS_Satisfaction']?>;
var score1 = <?php echo $row_form[0]['GDS_Tired']?>;
var score2 = <?php echo $row_form[0]['GDS_Inability']?>;
var score3 = <?php echo $row_form[0]['GDS_Dare']?>;
var score4 = <?php echo $row_form[0]['GDS_Value']?>;
var score5 = <?php echo $row_form[0]['GDS_Activity']?>;
var score6 = <?php echo $row_form[0]['GDS_Emptiness']?>;
var score7 = <?php echo $row_form[0]['GDS_Spirit']?>;
var score8 = <?php echo $row_form[0]['GDS_Unfortunately']?>;
var score9 = <?php echo $row_form[0]['GDS_Happy']?>;
var score10 = <?php echo $row_form[0]['GDS_Remember']?>;
var score11 = <?php echo $row_form[0]['GDS_Live']?>;
var score12 = <?php echo $row_form[0]['GDS_Energy']?>;
var score13 = <?php echo $row_form[0]['GDS_Hope']?>;
var score14 = <?php echo $row_form[0]['GDS_Bless']?>;

var total = 0;

var level ;

var caculate = function(){
	  
    total = score0 + score1 + score2 +
	 		score3 + score4 + score5 +
			score6 + score7 + score8 + 
			score9 + score10 + score11 + 
			score12 + score13 + score14;
	 
	if(total>=10){
	 	level = "(憂鬱症)";
	}else if(total >= 5 && total <=9){
	 	level = "(可能憂鬱)";
	}else{
	 	level = "";
	}

	$("#SCORE").text(total);
	$("#LEVEL").text(level); 
 }
 $(document).ready(function(){
	  
	$("input[name='GDS_Satisfaction']").click(function(){
		var GDS_Satisfaction = $("input[name='GDS_Satisfaction']:checked").val();
		score0 = parseInt(GDS_Satisfaction);
		caculate();		
	})
	$("input[name='GDS_Tired']").click(function(){
		var GDS_Tired = $("input[name='GDS_Tired']:checked").val();
		score1 = parseInt(GDS_Tired);		
		caculate();		
	})
	$("input[name='GDS_Inability']").click(function(){
		var GDS_Inability = $("input[name='GDS_Inability']:checked").val();
		score2 = parseInt(GDS_Inability);		
		caculate();		
	})
	$("input[name='GDS_Dare']").click(function(){
		var GDS_Dare = $("input[name='GDS_Dare']:checked").val();
		score3 = parseInt(GDS_Dare);		
		caculate();		
	})
	$("input[name='GDS_Value']").click(function(){
		var GDS_Value = $("input[name='GDS_Value']:checked").val();
		score4 = parseInt(GDS_Value);		
		caculate();		
	})
	$("input[name='GDS_Activity']").click(function(){
		var GDS_Activity = $("input[name='GDS_Activity']:checked").val();
		score5 = parseInt(GDS_Activity);		
		caculate();		
	})
	$("input[name='GDS_Emptiness']").click(function(){
		var GDS_Emptiness = $("input[name='GDS_Emptiness']:checked").val();
		score6 = parseInt(GDS_Emptiness);		
		caculate();		
	})
	$("input[name='GDS_Spirit']").click(function(){
		var GDS_Spirit = $("input[name='GDS_Spirit']:checked").val();
		score7 = parseInt(GDS_Spirit);		
		caculate();		
	})
	$("input[name='GDS_Unfortunately']").click(function(){
		var GDS_Unfortunately = $("input[name='GDS_Unfortunately']:checked").val();
		score8 = parseInt(GDS_Unfortunately);		
		caculate();		
	})	
	$("input[name='GDS_Happy']").click(function(){
		var GDS_Happy = $("input[name='GDS_Happy']:checked").val();
		score9 = parseInt(GDS_Happy);		
		caculate();		
	})		
	$("input[name='GDS_Remember']").click(function(){
		var GDS_Remember = $("input[name='GDS_Remember']:checked").val();
		score10 = parseInt(GDS_Remember);		
		caculate();		
	})	
	$("input[name='GDS_Live']").click(function(){
		var GDS_Live = $("input[name='GDS_Live']:checked").val();
		score11 = parseInt(GDS_Live);		
		caculate();		
	})	
	$("input[name='GDS_Energy']").click(function(){
		var GDS_Energy = $("input[name='GDS_Energy']:checked").val();
		score12 = parseInt(GDS_Energy);		
		caculate();		
	})	
	$("input[name='GDS_Hope']").click(function(){
		var GDS_Hope = $("input[name='GDS_Hope']:checked").val();
		score13 = parseInt(GDS_Hope);		
		caculate();		
	})	
	$("input[name='GDS_Bless']").click(function(){
		var GDS_Bless = $("input[name='GDS_Bless']:checked").val();
		score14 = parseInt(GDS_Bless);		
		caculate();		
	})	

	 
 	$("#mybtn").click(function(){
		$("#GDS_Score").val(total);	
		$("form#form1").submit();
	})
 })

</script>
<link type="text/css" href="../css/ui-darkness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<!-- header starting point -->
	<?php include("../include/header.php");?>
	<!-- header ending point -->    
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?>
        <!--menu ending point-->          
        </td>            
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top">
        <table width="830" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="30" class="content">專業照護 > 護理記錄 &gt; 修改</td>
              </tr>
              <tr>
                  <td height="10"></td>
              </tr>  
              <tr>
                <td height="10">
                <span class="form_title">
                     <input name="search" type="button" class="content" id="search" value="回上一頁" onclick="MM_goToURL('parent','layout.php?t=gds&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>
                </span></td>
              </tr> 
              <tr>
                  <td height="15"></td>
              </tr>                      
             <tr>
              	<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>   
             </tr>   
             <tr>
	            <td height="30"></td>
            </tr>           
           <tr>
	<td style="font-size: 13pt"><strong>老人憂鬱量表(GDS)</strong>
	<?php echo "(".$row_form[0]['GDS_Date'] .")";?>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	分數:<label id="SCORE"><?php echo $row_form[0]['GDS_Score'];?></label><label id="LEVEL">
	<?php 
	if($row_form[0]['GDS_Score'] >= 10){
		echo "(憂鬱症)";
	}else if($row_form[0]['GDS_Score'] >= 5 && $row_form[0]['GDS_Score'] <= 9){
		echo "(可能憂鬱)";
	}else{
		echo "";
	}
	?></label></td>                 
</tr> 
<tr>
	<td height="5"></td>
</tr>
<tr>
	<td ><img src="../images/blueline.jpg" /></td>
</tr> 
	<tr>
	<td height="5"></td>
</tr>             
<tr>         
	<td>   
	<form name="form1" id="form1" method="post" action="gds_process.php"/>
	<input type="hidden" name="action" id="action" value="mdy"/>                           
	<input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>"/>
    <input type="hidden" name="GDS_ID" id="GDS_ID" value="<?php echo $GDS_ID;?>"/>
	<input type="hidden" name="GDS_Score" id="GDS_Score" />              
	<table>                
	<tr>
		<td height="10"></td>
	</tr>                
	<tr>
		<td width="300" align="right"  class="content">基本上，您對您的生活滿意嗎？</td>
		<td width="705">
		<input name="GDS_Satisfaction" type="radio" class="form_fix" id="GDS_Satisfaction00" value="0" <?php echo ckRadio('0',$row_form[0]['GDS_Satisfaction']);?> />是
		<input name="GDS_Satisfaction" type="radio" class="form_fix" id="GDS_Satisfaction01" value="1" <?php echo ckRadio('1',$row_form[0]['GDS_Satisfaction']);?> />否
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="300" align="right"  class="content">您是否常常感到厭煩？</td>
		<td width="705">
		<input name="GDS_Tired" type="radio" class="form_fix" id="GDS_Tired01" value="1" <?php echo ckRadio('1',$row_form[0]['GDS_Tired']);?> />是
		<input name="GDS_Tired" type="radio" class="form_fix" id="GDS_Tired00" value="0" <?php echo ckRadio('0',$row_form[0]['GDS_Tired']);?> />否
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="300" align="right"  class="content">您是否常常感到無論做什麼，都沒有用？</td>
		<td width="705">
		<input name="GDS_Inability" type="radio" class="form_fix" id="GDS_Inability01" value="1" <?php echo ckRadio('1',$row_form[0]['GDS_Inability']);?> />是
		<input name="GDS_Inability" type="radio" class="form_fix" id="GDS_Inability00" value="0" <?php echo ckRadio('0',$row_form[0]['GDS_Inability']);?> />否
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="300" align="right"  class="content">您是否比較喜歡待在家裡而較不喜歡外出及不喜歡做新的事？</td>
		<td width="705">
		<input name="GDS_Dare" type="radio" class="form_fix" id="GDS_Dare01" value="1" <?php echo ckRadio('1',$row_form[0]['GDS_Dare']);?> />是
		<input name="GDS_Dare" type="radio" class="form_fix" id="GDS_Dare00" value="0" <?php echo ckRadio('0',$row_form[0]['GDS_Dare']);?> />否
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="300" align="right"  class="content">您是否感覺您現在活得很沒有價值？</td>
		<td width="705">
		<input name="GDS_Value" type="radio" class="form_fix" id="GDS_Value01" value="1" <?php echo ckRadio('1',$row_form[0]['GDS_Value']);?> />是
		<input name="GDS_Value" type="radio" class="form_fix" id="GDS_Value00" value="0" <?php echo ckRadio('0',$row_form[0]['GDS_Value']);?> />否
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="300" align="right"  class="content">您是否減少很多的活動和嗜好？</td>
		<td width="705">
		<input name="GDS_Activity" type="radio" class="form_fix" id="GDS_Activity01" value="1" <?php echo ckRadio('1',$row_form[0]['GDS_Activity']);?> />是
		<input name="GDS_Activity" type="radio" class="form_fix" id="GDS_Activity00" value="0" <?php echo ckRadio('0',$row_form[0]['GDS_Activity']);?> />否
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="300" align="right"  class="content">您是否覺得您的生活很空虛？</td>
		<td width="705">
		<input name="GDS_Emptiness" type="radio" class="form_fix" id="GDS_Emptiness01" value="1" <?php echo ckRadio('1',$row_form[0]['GDS_Emptiness']);?> />是
		<input name="GDS_Emptiness" type="radio" class="form_fix" id="GDS_Emptiness00" value="0" <?php echo ckRadio('0',$row_form[0]['GDS_Emptiness']);?> />否
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="300" align="right"  class="content">您是否大部份時間精神都很好？</td>
		<td width="705">
		<input name="GDS_Spirit" type="radio" class="form_fix" id="GDS_Spirit00" value="0" <?php echo ckRadio('0',$row_form[0]['GDS_Spirit']);?> />是
		<input name="GDS_Spirit" type="radio" class="form_fix" id="GDS_Spirit01" value="1" <?php echo ckRadio('1',$row_form[0]['GDS_Spirit']);?> />否
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="300" align="right"  class="content">您是否害怕將有不幸的事情發生在您身上嗎？</td>
		<td width="705">
		<input name="GDS_Unfortunately" type="radio" class="form_fix" id="GDS_Unfortunately01" value="1" <?php echo ckRadio('1',$row_form[0]['GDS_Unfortunately']);?> />是
		<input name="GDS_Unfortunately" type="radio" class="form_fix" id="GDS_Unfortunately00" value="0" <?php echo ckRadio('0',$row_form[0]['GDS_Unfortunately']);?> />否
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="300" align="right"  class="content">您是否大部份的時間都感到快樂？</td>
		<td width="705">
		<input name="GDS_Happy" type="radio" class="form_fix" id="GDS_Happy00" value="0" <?php echo ckRadio('0',$row_form[0]['GDS_Happy']);?> />是
		<input name="GDS_Happy" type="radio" class="form_fix" id="GDS_Happy01" value="1" <?php echo ckRadio('1',$row_form[0]['GDS_Happy']);?> />否
		</td>
	</tr>   
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="300" align="right"  class="content">您是否覺得您比大多數人有較多記憶的問題？</td>
		<td width="705">
		<input name="GDS_Remember" type="radio" class="form_fix" id="GDS_Remember01" value="1" <?php echo ckRadio('1',$row_form[0]['GDS_Remember']);?> />是
		<input name="GDS_Remember" type="radio" class="form_fix" id="GDS_Remember00" value="0" <?php echo ckRadio('0',$row_form[0]['GDS_Remember']);?> />否
		</td>
	</tr>    
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="300" align="right"  class="content">您是否覺得現在還能活著是很好的事？</td>
		<td width="705">
		<input name="GDS_Live" type="radio" class="form_fix" id="GDS_Live00" value="0" <?php echo ckRadio('0',$row_form[0]['GDS_Live']);?> />是
		<input name="GDS_Live" type="radio" class="form_fix" id="GDS_Live01" value="1" <?php echo ckRadio('1',$row_form[0]['GDS_Live']);?> />否
		</td>
	</tr>  
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="300" align="right"  class="content">您是否覺得精力很充沛？</td>
		<td width="705">
		<input name="GDS_Energy" type="radio" class="form_fix" id="GDS_Energy00" value="0" <?php echo ckRadio('0',$row_form[0]['GDS_Energy']);?> />是
		<input name="GDS_Energy" type="radio" class="form_fix" id="GDS_Energy01" value="1" <?php echo ckRadio('1',$row_form[0]['GDS_Energy']);?> />否
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="300" align="right"  class="content">您是否覺得您現在的情況是沒有希望的？</td>
		<td width="705">
		<input name="GDS_Hope" type="radio" class="form_fix" id="GDS_Hope01" value="1" <?php echo ckRadio('1',$row_form[0]['GDS_Hope']);?> />是
		<input name="GDS_Hope" type="radio" class="form_fix" id="GDS_Hope00" value="0" <?php echo ckRadio('0',$row_form[0]['GDS_Hope']);?> />否
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="300" align="right"  class="content">您是否覺得大部份的人都比您幸福？</td>
		<td width="705">
		<input name="GDS_Bless" type="radio" class="form_fix" id="GDS_Bless01" value="1" <?php echo ckRadio('1',$row_form[0]['GDS_Bless']);?> />是
		<input name="GDS_Bless" type="radio" class="form_fix" id="GDS_Bless00" value="0" <?php echo ckRadio('0',$row_form[0]['GDS_Bless']);?> />否
		</td>
	</tr> 
	<tr>
		<td height="10"></td>
	</tr>             
	<tr>
		<td width="350" align="right" class="content"><h3>※以上建議照會精神科門診並定期追蹤</h3>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
			<input name="mybtn" type="button" class="form_fix" id="mybtn"  value="確定送出"  /> 
 		 <input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>
		</td>
	</tr>                
	</table>
	</form>              
	</td>
</tr>
 </table>
          </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>
