<?php 	
	$rs_form = $objDB->Recordset("SELECT * FROM tai WHERE RS_ID = '$RS_ID' ORDER BY TAI_Date DESC");
	$row_form = $objDB->GetRows($rs_form);
	
	$ADL_ID = $row_form[0]['TAI_ID'];
?>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script>
var score0 = <?php echo $row_form[0]['TAI_Spirit']?>;
var score1 = <?php echo $row_form[0]['TAI_Activity']?>;
var score2 = <?php echo $row_form[0]['TAI_Eat']?>;
var score3 = <?php echo $row_form[0]['TAI_Drain']?>;
var score4 = <?php echo $row_form[0]['TAI_Medical']?>;

var total = 0;
var level ;

var caculate = function(){
	
	total = score0 + score1 + score2 + score3 + score4;

	if(total >= 21){
	 	level = "(重度)";
	}else if(total >= 15 && total <= 20){
	 	level = "(中度)";
	}else{
	 	level = "(輕度)";
	}
	 	$("#SCORE").text(total);
		$("#LEVEL").text(level); 
}
$(document).ready(function(){
	
	$("input[name='TAI_Spirit']").click(function(){
		var TAI_Spirit = $("input[name='TAI_Spirit']:checked").val();
		score0 = parseInt(TAI_Spirit);		
		caculate();		
	});

	$("input[name='TAI_Activity']").click(function(){
		var TAI_Activity = $("input[name='TAI_Activity']:checked").val();
		score1 = parseInt(TAI_Activity);		
		caculate();		
	});

	$("input[name='TAI_Eat']").click(function(){
		var TAI_Eat = $("input[name='TAI_Eat']:checked").val();
		score2 = parseInt(TAI_Eat);		
		caculate();		
	});

	$("input[name='TAI_Drain']").click(function(){
		var TAI_Drain = $("input[name='TAI_Drain']:checked").val();
		score3 = parseInt(TAI_Drain);		
		caculate();		
	});		
	
	$("input[name='TAI_Medical']").click(function(){
		var TAI_Medical = $("input[name='TAI_Medical']:checked").val();
		score4 = parseInt(TAI_Medical);		
		caculate();		
	});	
	 
 	$("#mybtn").click(function(){
		$("#TAI_Score").val(total);
	
		$("form#form1").submit();
		
	});
})
</script>

<tr>
	<td height="15"></td>
</tr>
<tr>
	<td style="font-size: 13pt"><strong>照護分級量表(TAI)</strong>
	<?php echo "(".$row_form[0]['TAI_Date'] .")";?>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	分數:<label id="SCORE"><?php echo $row_form[0]['TAI_Score'];?></label><label id="LEVEL">
	<?php 
	if($row_form[0]['TAI_Score'] >=21){
		echo "(重度)";
	}else if($row_form[0]['TAI_Score'] >=15 && $row_form[0]['TAI_Score'] <=20){
		echo "(中度)";
	}else{
		echo "(輕度)";
	}
	?></label></td>            
</tr> 
<tr>
	<td height="5"></td>
</tr>
<tr>
	<td ><img src="../images/blueline.jpg" /></td>
</tr> 
	<tr>
	<td height="5"></td>
</tr>             
<tr>         
	<td>   
	<form name="form1" id="form1" method="post" action="tai_process.php"/>
	<input type="hidden" name="action" id="action" value="mdy"/>                           
	<input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>"  />
    <input type="hidden" name="TAI_ID" id="TAI_ID" value="<?php echo $TAI_ID;?>" />
	<input type="hidden" name="TAI_Score" id="TAI_Score" />              
	<table>                
	<tr>
		<td height="10"></td>
	</tr>                
	<tr>
		<td width="110" align="right"  class="content">精神：</td>
		<td width="705">
		<input name="TAI_Spirit" type="radio" class="form_fix" id="TAI_Spirit5" value="5" <?php echo ckRadio('5',$row_form[0]['TAI_Spirit']);?> />5
		<input name="TAI_Spirit" type="radio" class="form_fix" id="TAI_Spirit4" value="4" <?php echo ckRadio('4',$row_form[0]['TAI_Spirit']);?> />4
		<input name="TAI_Spirit" type="radio" class="form_fix" id="TAI_Spirit3" value="3" <?php echo ckRadio('3',$row_form[0]['TAI_Spirit']);?> />3	
		<input name="TAI_Spirit" type="radio" class="form_fix" id="TAI_Spirit2" value="2" <?php echo ckRadio('2',$row_form[0]['TAI_Spirit']);?> />2	
		<input name="TAI_Spirit" type="radio" class="form_fix" id="TAI_Spirit1" value="1" <?php echo ckRadio('1',$row_form[0]['TAI_Spirit']);?> />1		
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">活動：</td>
		<td width="705">
		<input name="TAI_Activity" type="radio" class="form_fix" id="TAI_Activity5" value="5" <?php echo ckRadio('5',$row_form[0]['TAI_Activity']);?> />5
		<input name="TAI_Activity" type="radio" class="form_fix" id="TAI_Activity4" value="4" <?php echo ckRadio('4',$row_form[0]['TAI_Activity']);?> />4
		<input name="TAI_Activity" type="radio" class="form_fix" id="TAI_Activity3" value="3" <?php echo ckRadio('3',$row_form[0]['TAI_Activity']);?> />3	
		<input name="TAI_Activity" type="radio" class="form_fix" id="TAI_Activity2" value="2" <?php echo ckRadio('2',$row_form[0]['TAI_Activity']);?> />2	
		<input name="TAI_Activity" type="radio" class="form_fix" id="TAI_Activity1" value="1" <?php echo ckRadio('1',$row_form[0]['TAI_Activity']);?> />1		
		</td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">飲食：</td>
		<td width="705">
		<input name="TAI_Eat" type="radio" class="form_fix" id="TAI_Eat5" value="5" <?php echo ckRadio('5',$row_form[0]['TAI_Eat']);?> />5
		<input name="TAI_Eat" type="radio" class="form_fix" id="TAI_Eat4" value="4" <?php echo ckRadio('4',$row_form[0]['TAI_Eat']);?> />4
		<input name="TAI_Eat" type="radio" class="form_fix" id="TAI_Eat3" value="3" <?php echo ckRadio('3',$row_form[0]['TAI_Eat']);?> />3	
		<input name="TAI_Eat" type="radio" class="form_fix" id="TAI_Eat2" value="2" <?php echo ckRadio('2',$row_form[0]['TAI_Eat']);?> />2	
		<input name="TAI_Eat" type="radio" class="form_fix" id="TAI_Eat1" value="1" <?php echo ckRadio('1',$row_form[0]['TAI_Eat']);?> />1		
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">排泄：</td>
		<td width="705">
		<input name="TAI_Drain" type="radio" class="form_fix" id="TAI_Drain5" value="5" <?php echo ckRadio('5',$row_form[0]['TAI_Drain']);?> />5
		<input name="TAI_Drain" type="radio" class="form_fix" id="TAI_Drain4" value="4" <?php echo ckRadio('4',$row_form[0]['TAI_Drain']);?> />4
		<input name="TAI_Drain" type="radio" class="form_fix" id="TAI_Drain3" value="3" <?php echo ckRadio('3',$row_form[0]['TAI_Drain']);?> />3	
		<input name="TAI_Drain" type="radio" class="form_fix" id="TAI_Drain2" value="2" <?php echo ckRadio('2',$row_form[0]['TAI_Drain']);?> />2	
		<input name="TAI_Drain" type="radio" class="form_fix" id="TAI_Drain1" value="1" <?php echo ckRadio('1',$row_form[0]['TAI_Drain']);?> />1		
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td width="110" align="right"  class="content">醫療：</td>
		<td width="705">
		<input name="TAI_Medical" type="radio" class="form_fix" id="TAI_Medical5" value="5" <?php echo ckRadio('5',$row_form[0]['TAI_Medical']);?> />5
		<input name="TAI_Medical" type="radio" class="form_fix" id="TAI_Medical4" value="4" <?php echo ckRadio('4',$row_form[0]['TAI_Medical']);?> />4
		<input name="TAI_Medical" type="radio" class="form_fix" id="TAI_Medical3" value="3" <?php echo ckRadio('3',$row_form[0]['TAI_Medical']);?> />3	
		<input name="TAI_Medical" type="radio" class="form_fix" id="TAI_Medical2" value="2" <?php echo ckRadio('2',$row_form[0]['TAI_Medical']);?> />2	
		<input name="TAI_Medical" type="radio" class="form_fix" id="TAI_Medical1" value="1" <?php echo ckRadio('1',$row_form[0]['TAI_Medical']);?> />1		
		</td>
	</tr>
    <tr>
		<td height="10"></td>
	</tr>                   
	<tr>
		<td align="right" class="content">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
			<input name="mybtn" type="button" class="form_fix" id="mybtn"  value="確定送出"  /> 
 		 <input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>
		</td>
	</tr>                
	</table>
	</form>              
	</td>
</tr>
