<?php 
include("../public/mem_check.php");
include("../public/web_function.php");
	
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}
	if(is_numeric(quotes($_GET['RS_ID']))){
		 $RS_ID = quotes($_GET['RS_ID']);
	}else{
		 ?>
     <script language="javascript">		
		location.href='../index.php';
	 </script>	
         <?php
	}	
	
	$sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
	$rs = $objDB->Recordset($sql);
	$row = $objDB->GetRows($rs);	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>

<script>
var score0 = 0;
var score1 = 0;
var score2 = 0;
var score3 = 0;
var score4 = 0;
var score5 = 0;
var score6 = 0;
var score7 = 0;
var score8 = 0;
var score9 = 0;
/*
var score10 = 0;
var score11 = 0;
*/
var total = 0;

var level ;

var caculate = function(){
	  
     total = score0 + score1 + score2 +
	 		 score3 + score4 + score5 +
			 score6 + score7 + score8 + score9;
	 
	 if(total==100){
	 	level = "(獨立)";
	 }else if(total >= 91 && total <=99){
	 	level = "(輕度依賴)";
	 }else if(total >=61 && total <= 90){
	 	level = "(中度依賴)";
	 }else if(total >=21 && total <= 60){
	 	level = "(嚴重依賴)";
	 }else{
	 	level = "(完全依賴)";
	 }
	 	$("#SCORE").text(total);
		$("#LEVEL").text(level); 
 }
 $(document).ready(function(){
	  
	 
	$("input[name='ADL_Eat']").click(function(){
		var ADL_Eat = $("input[name='ADL_Eat']:checked").val();
		score0 = parseInt(ADL_Eat);
		caculate();		
	})
	$("input[name='ADL_Move']").click(function(){
		var ADL_Move = $("input[name='ADL_Move']:checked").val();
		score1 = parseInt(ADL_Move);		
		caculate();		
	})
	$("input[name='ADL_Makeup']").click(function(){
		var ADL_Makeup = $("input[name='ADL_Makeup']:checked").val();
		score2 = parseInt(ADL_Makeup);		
		caculate();		
	})
	$("input[name='ADL_Toilet']").click(function(){
		var ADL_Toilet = $("input[name='ADL_Toilet']:checked").val();
		score3 = parseInt(ADL_Toilet);		
		caculate();		
	})
	$("input[name='ADL_Shower']").click(function(){
		var ADL_Shower = $("input[name='ADL_Shower']:checked").val();
		score4 = parseInt(ADL_Shower);		
		caculate();		
	})
	$("input[name='ADL_Pace']").click(function(){
		var ADL_Pace = $("input[name='ADL_Pace']:checked").val();
		score5 = parseInt(ADL_Pace);		
		caculate();		
	})
	$("input[name='ADL_Stair']").click(function(){
		var ADL_Stair = $("input[name='ADL_Stair']:checked").val();
		score6 = parseInt(ADL_Stair);		
		caculate();		
	})
	$("input[name='ADL_Clothes']").click(function(){
		var ADL_Clothes = $("input[name='ADL_Clothes']:checked").val();
		score7 = parseInt(ADL_Clothes);		
		caculate();		
	})
	$("input[name='ADL_Defecate']").click(function(){
		var ADL_Defecate = $("input[name='ADL_Defecate']:checked").val();
		score8 = parseInt(ADL_Defecate);		
		caculate();		
	})	
	$("input[name='ADL_Urinate']").click(function(){
		var ADL_Urinate = $("input[name='ADL_Urinate']:checked").val();
		score9 = parseInt(ADL_Urinate);		
		caculate();		
	})		 
	 
 	$("#mybtn").click(function(){
		$("#ADL_Score").val(total);
	
		$("form#form1").submit();
		
	})
 	
 })

</script>
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<!-- header starting point -->
	<?php include("../include/header.php");?>
	<!-- header ending point -->    
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?>
        <!--menu ending point-->          
        </td>            
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top">
        <table width="830" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="30" class="content">專業照護 > 評估與記錄 &gt; 新增</td>
              </tr>
              <tr>
                  <td height="10"></td>
              </tr>    
              <tr>
                <td height="10">
                <span class="form_title">
                     <input name="search" type="button" class="content" id="search" value="回上一頁" onclick="MM_goToURL('parent','layout.php?t=adl&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>
                </span></td>
              </tr>   
              <tr>
                  <td height="10"></td>
              </tr>                      
             <tr>
              	<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>   
             </tr>   
             <tr>
                <td height="30"></td>
              </tr>           
              
              <tr>
              		<td style="font-size: 13pt"><strong>巴氏量表(ADL)</strong>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  分數:<label id="SCORE"> </label><label id="LEVEL"> </label>
                  </td>            
              </tr> 
              <tr>
                <td height="5"></td>
              </tr>
              <tr>
              	  <td ><img src="../images/blueline.jpg" /></td>
              </tr> 
               <tr>
                <td height="5"></td>
              </tr>   
            <tr>         
              <td>    
              <!--管理員管理startinging-->              
              <form name="form1" id="form1" method="post" action="adl_process.php" />
			   <input type="hidden" name="action" id="action" value="new"/>              
              <input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>" />
              <input type="hidden" name="ADL_Score" id="ADL_Score" />               
             <table>
                <tr>
                  <td height="10"></td>
                </tr>                
                <tr>
                  <td width="80" align="right"  class="content">進食：</td>
                  <td width="735">
                 <input name="ADL_Eat" type="radio" class="form_fix" id="ADL_Eat10" value="10" />自己在合理的時間內(約十秒鐘吃一口)可用筷子取食眼前的食物。若需進食輔具時，應會自行穿脫。<br />
                 <input name="ADL_Eat" type="radio" class="form_fix" id="ADL_Eat5" value="5"/>需別人幫忙穿脫輔具或只會用湯匙進食。<br />
                 <input name="ADL_Eat" type="radio" class="form_fix" id="ADL_Eat0" value="0"/>無法自行取食或耗費時間過長。                 
                  </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="80" align="right"  class="content">移動：</td>
                  <td width="735">
                 <input name="ADL_Move" type="radio" class="form_fix" id="ADL_Move15" value="15"/>可獨立完成，包括輸椅的煞車及移開腳踏板。<br />
                 <input name="ADL_Move" type="radio" class="form_fix" id="ADL_Move10" value="10"/>需要稍微的協助(例如:予以輕扶以保持平衡)或需要口頭指導。<br />
                 <input name="ADL_Move" type="radio" class="form_fix" id="ADL_Move5" value="5"/>可自行從床上坐起來，但移位時仍需別人幫忙。<br />
                 <input name="ADL_Move" type="radio" class="form_fix" id="ADL_Move0" value="0"/>需別人幫忙方可坐起來或移位。               
                  </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="80" align="right"  class="content">修飾：</td>
                  <td width="735">
                 <input name="ADL_Makeup" type="radio" class="form_fix" id="ADL_Makeup5" value="5"/>可獨立完成洗臉、洗手、刷牙及梳頭髮。<br />
                 <input name="ADL_Makeup" type="radio" class="form_fix" id="ADL_Makeup0" value="0"/>需要別人幫忙。                 
                  </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="80" align="right"  class="content">如廁：</td>
                  <td width="735">
                 <input name="ADL_Toilet" type="radio" class="form_fix" id="ADL_Toilet10" value="10"/>可自行進出廁所，不會弄髒衣物，並能穿好衣服。使用便盆者，可自行清理便盆。<br />
                 <input name="ADL_Toilet" type="radio" class="form_fix" id="ADL_Toilet5" value="5"/>需幫忙保持姿勢的平衡，整理衣物或使用衛生紙。<br />
                 <input name="ADL_Toilet" type="radio" class="form_fix" id="ADL_Toilet0" value="0"/>需他人幫忙。
                  </td>
                </tr>                     
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="80" align="right"  class="content">沐浴：</td>
                  <td width="735">
                 <input name="ADL_Shower" type="radio" class="form_fix" id="ADL_Shower5" value="5"/>可獨立完成(不論是盆浴或沐浴)。<br />
                 <input name="ADL_Shower" type="radio" class="form_fix" id="ADL_Shower0" value="0"/>需要別人幫忙。
                  </td>
                </tr> 
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="80" align="right"  class="content">步行：</td>
                  <td width="735">
                 <input name="ADL_Pace" type="radio" class="form_fix" id="ADL_Pace15" value="15"/>使用或不使用輔具皆可獨立行走50公尺以上。<br />
                 <input name="ADL_Pace" type="radio" class="form_fix" id="ADL_Pace10" value="10"/>需要稍微的扶持或口頭指導方可行走50公尺以上。<br />
                 <input name="ADL_Pace" type="radio" class="form_fix" id="ADL_Pace5" value="5"/>雖無法行走，但可獨立操縱輪椅(包括轉彎、進門、及接近桌子、床沿)並可推行輪椅50公尺以上。<br />
                 <input name="ADL_Pace" type="radio" class="form_fix" id="ADL_Pace0" value="0"/>需別人幫忙推輪椅。
                  </td>
                </tr> 
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="80" align="right"  class="content">上下樓梯：</td>
                  <td width="735">
                 <input name="ADL_Stair" type="radio" class="form_fix" id="ADL_Stair10" value="10"/>可自行上下樓梯(允許抓扶手、用拐杖)。<br />
                 <input name="ADL_Stair" type="radio" class="form_fix" id="ADL_Stair5" value="5"/>需要稍微幫忙或口頭指導。<br />
                 <input name="ADL_Stair" type="radio" class="form_fix" id="ADL_Stair0" value="0"/>無法上下樓梯。
                  </td>
                </tr> 
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="80" align="right"  class="content">穿脫衣服：</td>
                  <td width="735">
                 <input name="ADL_Clothes" type="radio" class="form_fix" id="ADL_Clothes10" value="10"/>可自行穿脫衣服、鞋子及輔具。<br />
                 <input name="ADL_Clothes" type="radio" class="form_fix" id="ADL_Clothes5" value="5"/>在別人幫忙下，可自行完成一半以上的動作。<br />
                 <input name="ADL_Clothes" type="radio" class="form_fix" id="ADL_Clothes0" value="0"/>需別人幫忙。
                  </td>
                </tr> 
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="80" align="right"  class="content">大便控制：</td>
                  <td width="735">
                 <input name="ADL_Defecate" type="radio" class="form_fix" id="ADL_Defecate10" value="10"/>不會失禁，並可自行使用塞劑。<br />
                 <input name="ADL_Defecate" type="radio" class="form_fix" id="ADL_Defecate5" value="5"/>偶爾會失禁(每週不超過一次)或使用塞劑時需人幫助。<br />
                 <input name="ADL_Defecate" type="radio" class="form_fix" id="ADL_Defecate0" value="0"/>需別人處理。
                  </td>
                </tr> 
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="80" align="right"  class="content">小便控制：</td>
                  <td width="735">
                 <input name="ADL_Urinate" type="radio" class="form_fix" id="ADL_Urinate10" value="10"/>日夜皆不會尿失禁，或可自行使用並清理尿套<br />
                 <input name="ADL_Urinate" type="radio" class="form_fix" id="ADL_Urinate5" value="5"/>偶爾會尿失禁(每週不超過一次)或尿急(無法等待便盆或無法即時趕到廁所)或需別人幫忙處理尿套。<br />
                 <input name="ADL_Urinate" type="radio" class="form_fix" id="ADL_Urinate0" value="0"/>需別人處理。
                  </td>
                </tr> 
                     
                <tr>
                  <td align="right" class="content">&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>
                      <input name="mybtn" type="button" class="form_fix" id="mybtn"  value="確定送出" />&nbsp; 
                      <!--<input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>-->
                   </td>
                </tr>                
              </table>
              </form>
              <!--管理員管理 ending-->              
              </td>
            </tr>
            
          </table>
          </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>
