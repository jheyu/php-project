<?php 
include_once("../public/mem_check.php");
include_once("../public/web_function.php");

	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}	

	
	if(is_numeric(quotes($_GET['RS_ID']))){
		 $RS_ID = quotes($_GET['RS_ID']);
	}else{
		 ?>
     <script language="javascript">		
		location.href='../index.php';
	 </script>	
         <?php
	}

	$rs = $objDB->Recordset("select * from resident where RS_ID ='$RS_ID'");
	$row = $objDB->GetRows($rs);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>個案管理</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script>

 $(document).ready(function(){
 
 	$("#mybtn").click(function(){
		 
		  if($("#AM_Pass").val()!=''){
			  if($("#AM_Pass").val()=='') {
					alert('請填入新密碼');
					$("#AM_Pass").focus();
					return false;
			  }else if($("#AM_Pass").val()!=$("#AM_RePass").val()) {
					alert("確認密碼不相符");
					$("#AM_RePass").focus();
					return false;
			  }
		 }
		 
		 if($("#AM_Name").val()=='') {
					alert("請填入名稱");
					$("#AM_Name").focus();
					return false;
	     }else{
			  		$("form#form1").submit();
		}
	})
 	
 })

</script>
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<!-- header starting point -->
	<?php include("../include/header.php");?>
	<!-- header ending point -->    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1000" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="160" valign="top" background="../images/bkline.jpg">
          	<!--menu starting point-->
            <?php include("../include/menu.php");?>
            <!--menu ending point-->          </td>
            
          <td width="10" valign="top"><img src="../images/spacer.gif" width="1" height="1" /></td>
          <td width="830" valign="top"><table width="830" border="0" cellpadding="1" cellspacing="1">
            <tr>
              <td class="content">個案管理 > 待床管理 &gt; 修改</td>
              </tr>
            <tr>            
              <td height="10"></td>
             </tr>
            <tr>
              <td height="10">
                <span class="form_title">
                <input name="search" type="button" class="content" id="search" value="回上一頁" onClick="history.back(-1)"/>
                </span></td>
             </tr>
             <tr>         
              <td>                          
              <!--管理員管理startinging-->              
              <form name="form1" id="form1" method="post" action="register_process.php">
			   <input type="hidden" name="action" id="action" value="mdy">
              <input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>">
              <table width="495" border="0">
                <tr>
                  <td align="right" class="content"><span style="color:#ff0000;">*</span>姓名：</td>
                  <td><input name="RS_Name" type="text" class="form_fix" id="RS_Name" style="width:80px" value="<?php echo $row[0]['RS_Name'];?>"/></td>    
                </tr>
                <tr>
                  <td height="5"></td>
                </tr>
                <tr>
                  <td width="90" align="right"  class="content"><span style="color:#ff0000;"></span>身分證字號：</td>
                  <td width="480">
                  <input name="RS_IdentityCard" type="text" class="form_fix" id="RS_IdentityCard" value="<?php echo $row[0]['RS_IdentityCard'];?>"/>&nbsp;&nbsp;<img src="../js/ajax-loader.gif"  id="loading" style="margin-left:4px;display:none;" ><span id="checkid"></span> 
                  </td>
                </tr>
                <tr>
                  <td height="5"></td>
                </tr>
                <tr>
                  <td width="90" align="right"  class="content">性別：</td>
                  <td width="480">
                  <input name="RS_Sex" type="radio" class="form_fix" id="RS_Sex" value="1" <?php echo ckRadio('1',$row[0]['RS_Sex']);?> />男
                  <input name="RS_Sex" type="radio" class="form_fix" id="RS_Sex" value="2" <?php echo ckRadio('2',$row[0]['RS_Sex']);?> />女
                  </td>
                </tr>
                  <tr>
                  <td height="5"></td>
                </tr>
                <tr>
                  <td align="right" class="content">出生：</td>
                  <td>
                  <!--<input name="RS_Birthday" type="text" id="RS_Birthday"  class="date-pick"  style="width:80px;"  readonly="readonly" />-->
                  <input name="RS_Birthday" type="text" id="RS_Birthday"  placeholder="西元年/月/日" style="width:80px" value="<?php echo $row[0]['RS_Birthday'];?>"/>
                  </td>
                </tr>
                <tr>
                  <td height="5"></td>
                </tr>
                <tr>
                   <td width="110" align="right"  class="content">住民病史：</td>
                   <td><input name="RS_History" type="text" class="form_fix" id="RS_History" style="width:120px;" value="<?php echo $row[0]['RS_History'];?>" /></td>
                </tr> 
                <tr>
                   <td height="5"></td>
                </tr>
                <tr>
                  <td width="90" align="right"  class="content">狀態：</td>
                  <td width="480">
                  <input name="RS_Status" type="radio" class="form_fix" id="RS_Status" value="1" <?php echo ckRadio('1',$row[0]['RS_Status']);?> />未審核
                  <input name="RS_Status" type="radio" class="form_fix" id="RS_Status" value="2" <?php echo ckRadio('2',$row[0]['RS_Status']);?> />審核通過
                  </td>
                </tr>
                
                <tr>
                  <td align="right" class="content">&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>
                      <input name="mybtn" type="button" class="form_fix" id="mybtn"  value="確定送出"/>
                  </td>
                </tr>                
              </table>
              </form>
              <!--管理員管理 ending-->              </td>
            </tr>
            
            
          </table>         </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>
