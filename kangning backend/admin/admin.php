<?php 
include_once("../public/mem_check.php");
include_once("../public/web_function.php");

if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}	
/*
//下面是計算全部留言用的
$ALL_COUNT = $objDB->Recordset("SELECT * FROM admin where AM_Type='1' order by Create_date desc");
$ALL_WORDS = $objDB->RecordCount($ALL_COUNT);

//分頁選單用的
$ALL_PAGE=ceil($ALL_WORDS/$READ_MEMNUM); //計算所需頁數

//判斷頁數為數字
if(is_numeric(quotes($_GET['page_num']))){
	 $page_num = quotes($_GET['page_num']);
}else{
     $page_num = 0;
}
*/

	$rs = $objDB->Recordset("SELECT * FROM admin order by Create_date desc");	
	$row = $objDB->GetRows($rs);

					 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title><?php echo $html_title;?>系統設定</title>
<script src="../js/common.js" language="javascript"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.js"></script>
<script type="text/JavaScript">

function changed(theselect) {
	//var am_type = theselect.selectedIndex + 1 ;
	var am_type = $('#type_slt').val();
	window.location.href="admin.php?am_type="+am_type;
}
/*
$(function() {   
			
			$("#mybtn1").click(function(){
				
				 var mycount = 0;
				$("input[class='checkitem']").each(function() { 
					if($(this).attr("checked")!=undefined){
						mycount = mycount+1;
					}
				});
		 
				  if(mycount>0){
					$("form#frmGroupudp").submit();
				  }else{
				   alert("請勾選要刪除的管理者");
				  }
			 }) 
			 
			 
			 
		 $("#checkAll").click(function() {
	
		   if($("#checkAll").attr("checked"))
			{
				$("input[class='checkitem']").each(function() { 
				  $(this).attr("checked", true);
			  });
			}
			else
			{
			  	$("input[class='checkitem']").each(function() { 
				  $(this).attr("checked", false);
			  });           
		   }
		   
		 });
		 
		     
		
});   
*/
</script>

<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>

<body onload="MM_preloadImages('../images/logout_r.gif','../images/logout_r.gif')">
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td><!-- header starting point -->
        <?php include("../include/header.php");?>
        <!-- header ending point -->
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="160" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?></td>
        <!--menu ending point-->
        <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="930" valign="top"><table width="830" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="30" class="content">系統設定 &gt; 機構人員</td>
          </tr>
          <tr>
            <td height="10" class="content_red_b"><img src="../images/spacer.gif" width="1" height="1" /></td>
          </tr>
          <tr>
            <td valign="top">
               <table width="825" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td>
                        <table width="830" height="25" border="0" cellpadding="0" cellspacing="0">
                        
                          <tr>
                            <td width="218" height="30">
                            <input name="button3" type="button" class="content" id="button3" onclick="MM_goToURL('parent','add_admin.php');return document.MM_returnValue" value="新增帳號" />
                            </td>
                          </tr>                                                   
                          <tr>
            					<td height="10" class="content"></td>
       					  </tr>
                      </table></td>
                  </tr>
                  <tr>
                    <td>
                      <table width="820" height="55" border="0" cellpadding="1" cellspacing="1" bordercolor="#777777" bgcolor="#555555">
                        <tr>                        	 
                          <td width="139" align="center" bgcolor="#898989" class="back_w"><a href="#">名稱</a></td>
                          <td width="153" align="center" bgcolor="#898989" class="back_w"><a href="#">帳號</a></td>
                          <td bgcolor="#898989" align="center" class="back_w"><a href="#">建立時間</a></td>
                          <td width="144" align="center" bgcolor="#898989" class="back_w"><a href="#">上次登入時間</a></td>
                          <td bgcolor="#898989" align="center" class="back_w"><a href="#">功能操作</a></td>
                        </tr>
                       
                        <?php for($i=0;$i<$objDB->RecordCount($rs);$i++){?>
                        <?php if($i%2==0){$bgcolor="#EBEBEB";}else{$bgcolor="#FFFFFF";}?>
                        <tr>                        	 
                          <td align="center" bgcolor="<?php echo $bgcolor;?>" class="content"><?php echo $row[$i]["AM_Name"]; ?> </td>
                          <td align="center" bgcolor="<?php echo $bgcolor;?>" class="content"><?php echo $row[$i]["AM_Account"]; ?> </td>
                          <td width="161" bgcolor="<?php echo $bgcolor;?>" class="content" style="width:px;word-wrap:break-word;overflow:auto" align="center"><?php echo $row[$i]["Create_date"]; ?></td>
                          <td align="center" bgcolor="<?php echo $bgcolor;?>" class="content"><span class="content" style="width:px;word-wrap:break-word;overflow:auto"><?php echo $row[$i]["Last_login"]; ?></span></td>
                          <td width="124" align="center" bgcolor="<?php echo $bgcolor;?>">
                        <input name="button2" type="button" class="login" id="button2" onclick="MM_goToURL('parent','md_admin.php?AM_ID=<?php echo $row[$i]["AM_ID"]; ?>');return document.MM_returnValue" value="修改" />
                        <input name="button4" type="button" class="login" id="button4" value="刪除" onclick="if(confirm('確定要刪除此筆資料嗎?'))MM_goToURL('parent','admin_process.php?action=del&AM_ID=<?php echo $row[$i]["AM_ID"]; ?>');return document.MM_returnValue"  
						  />
                         	 </td>
                        </tr>
                        <?php }?>
                        <?php if($objDB->RecordCount($rs)==0){?>
                        <div align="center" style="margin-top:5px">無資料！</div>
                        <?php }?>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td><table width="830" border="0" cellspacing="0" cellpadding="0">
                    </table></td>
                  </tr>
                </table>              </td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td class="copyright">
      <!--footer starting point-->
      <?php include("../include/footer.php");?>
      <!--footer starting point-->
    </td>
  </tr>
</table>
</body>
</html>
