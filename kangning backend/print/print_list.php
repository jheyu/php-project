<?php 
include("../public/mem_check.php");
include("../public/web_function.php");
	/*
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}
	*/
	if(is_numeric(quotes($_GET['RS_ID']))){
		 $RS_ID = quotes($_GET['RS_ID']);
	}else{
		 ?>
     <script language="javascript">		
		//location.href='../index.php';
	 </script>	
<?php
	}	
	
	$sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
	$rs = $objDB->Recordset($sql);
	$row = $objDB->GetRows($rs);

	$type = $_GET['t'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="../js/ui/minified/jquery.ui.datepicker.min.js"></script>

<script>

</script>
<link type="text/css" href="../css/ui-darkness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<!-- header starting point -->
	<?php include("../include/header.php");?>
	<!-- header ending point -->    
    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1100" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="170" valign="top" background="../images/bkline.jpg">
        <!--menu starting point-->
        <?php include("../include/menu.php");?>
        <!--menu ending point-->          
        </td>            
        <td width="30" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
        <td width="725" valign="top">
        <table width="830" border="0" cellpadding="0" cellspacing="0" border-color="#e0e0e0" >
            <tr>
              <td height="30" class="content">專業照護 > 護理記錄 &gt; 修改</td>
              </tr>
              <tr>
                  <td height="10"></td>
              </tr>                       
             <tr>
                <td height="30"></td>
              </tr> 
              <tr align="center">
	 <h2 align="left"><strong>列印列表</strong></h2>
	</tr>
<tr>
    <td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>   
</tr>
<tr>
    <td height="10" class="content"></td>
</tr>	
<tr>
	<td style="font-size: 16pt"><strong>
	<?php 
		switch($type)
		{
			case "fall":
				echo "跌倒評估";
			break;
			case "skin":
				echo "皮膚評估";
			break;
			case "bsrs":
				echo "自殺評估";
			break;
			case "pain":
				echo "疼痛評估";
			break;
			case "evaluate":
				echo "健康評估";
			break;
			case "adl":
				echo "巴氏量表";
			break;
			case "mmse":
				echo "簡易智能量表";
			break;
			case "behavior":
				echo "問題行為型態評估";
			break;
			case "gds":
				echo "老人憂鬱量表";
			break;
			case "adapt":
				echo "生活適應評估";
			break;
			case "vital":
				echo "生命徵象評估";
			break;
			case "ins":
				echo "胰島素注射紀錄";
			break;
			case "bs":
				echo "血糖紀錄";
			break;
			case "io":
				echo "攝入及排出";
			break;
		}
	?>
	</strong></td>
</tr>
<tr>
	<td ><img src="../images/blueline.jpg" /></td>
</tr> 
	<tr>
	<td height="5"></td>
</tr>             
<tr>         
	<td>            
	<table>     
	<tr><td><a href="print_list.php?t=fall&RS_ID=<?php echo $RS_ID;?>" class="overed">跌倒評估</a></td></tr>
	<tr><td><a href="print_list.php?t=skin&RS_ID=<?php echo $RS_ID;?>" class="overed">皮膚評估</a></td></tr>
	<tr><td><a href="print_list.php?t=bsrs&RS_ID=<?php echo $RS_ID;?>" class="overed">自殺評估</a></td></tr>
	<tr><td><a href="print_list.php?t=pain&RS_ID=<?php echo $RS_ID;?>" class="overed">疼痛評估</a></td></tr>
	<tr><td><a href="print_list.php?t=evaluate&RS_ID=<?php echo $RS_ID;?>" class="overed">健康評估</a></td></tr>
	<tr><td><a href="print_list.php?t=adl&RS_ID=<?php echo $RS_ID;?>" class="overed">巴氏量表</a></td></tr>
	<tr><td><a href="print_list.php?t=mmse&RS_ID=<?php echo $RS_ID;?>" class="overed">簡易智能量表</a></td></tr>
    <tr><td><a href="print_list.php?t=behavior&RS_ID=<?php echo $RS_ID;?>" class="overed">問題行為型態評估</a></td></tr>
    <tr><td><a href="print_list.php?t=gds&RS_ID=<?php echo $RS_ID;?>" class="overed">老人憂鬱量表</a></td></tr>
    <tr><td><a href="print_list.php?t=adapt&RS_ID=<?php echo $RS_ID;?>" class="overed">生活適應評估</a></td></tr>
    <tr><td><a href="print_list.php?t=vital&RS_ID=<?php echo $RS_ID;?>" class="overed">生命徵象評估</a></td></tr>
    <tr><td><a href="print_list.php?t=ins&RS_ID=<?php echo $RS_ID;?>" class="overed">胰島素注射紀錄</a></td></tr>
    <tr><td><a href="print_list.php?t=bs&RS_ID=<?php echo $RS_ID;?>" class="overed">血糖紀錄</a></td></tr>
    <tr><td><a href="print_list.php?t=io&RS_ID=<?php echo $RS_ID;?>" class="overed">攝入及排出</a></td></tr>                       
    <tr>
    	<td height="10"></td>
    </tr>
	<tr>
		<td width="227" height="20" bgcolor="#3da7d3" class="enter_wb" align="center">時間</td>
        <td width="222" height="20" bgcolor="#3da7d3" class="enter_wb" align="center">編輯</td>
    </tr>
    <?php

		if($type == 'evaluate')
		{

			$rs_form = $objDB->Recordset("SELECT * FROM evaluate WHERE RS_ID = '$RS_ID' ORDER BY EV_Date DESC");
	        $row_form = $objDB->GetRows($rs_form);
	            
		    for($i=0;$i<$objDB->RecordCount($rs_form);$i++)
			{
				if($i%2==0)
					$bgcolor="#e5f9ff";
				else
					$bgcolor="#FFFFFF";
				
	?>
	<tr>
		<td height="41" align="center" bgcolor="<?php echo $bgcolor;?>"  class="content" style="color:<?php echo $color;?>"><?php echo $row_form[$i]["EV_Date"];?></td>
		<td width="123" align="center" bgcolor="<?php echo $bgcolor;?>"><input name="button" type="button" class="content" id="button4" onclick="MM_openBrWindow('priview.php?t=evaluate&EV_ID=<?php echo $row_form[$i]["EV_ID"];?>&RS_ID=<?php echo $row_form[$i]["RS_ID"]; ?>','exporExcel','')" value="預覽"/></td>
    </tr>		
	<?php   } 
        
        }

        else if($type == 'adl')
		{
			$rs_form = $objDB->Recordset("SELECT * FROM adl WHERE RS_ID = '$RS_ID' ORDER BY ADL_Date DESC");
	        $row_form = $objDB->GetRows($rs_form);
	           
		    for($i=0;$i<$objDB->RecordCount($rs_form);$i++)
			{
				if($i%2==0)
					$bgcolor="#EBEBEB";
				else
					$bgcolor="#FFFFFF";
				
	?>
	<tr>
		<td height="41" align="center" bgcolor="<?php echo $bgcolor;?>"  class="content" style="color:<?php echo $color;?>"><?php echo $row_form[$i]['ADL_Date'];?></td>
		<td width="123" align="center" bgcolor="<?php echo $bgcolor;?>"><input name="button" type="button" class="content" id="button4" onclick="MM_openBrWindow('priview.php?t=adl&ADL_ID=<?php echo $row_form[$i]["ADL_ID"];?>&RS_ID=<?php echo $row_form[$i]["RS_ID"]; ?>','exporExcel','')" value="預覽"/></td>
    </tr>		
	<?php   } 
        
        }

        else if($type == 'bsrs')
		{
			$rs_form = $objDB->Recordset("SELECT * FROM bsrs WHERE RS_ID = '$RS_ID' ORDER BY BS_Date DESC");
	        $row_form = $objDB->GetRows($rs_form);
	           
		    for($i=0;$i<$objDB->RecordCount($rs_form);$i++)
			{
				if($i%2==0)
					$bgcolor="#EBEBEB";
				else
					$bgcolor="#FFFFFF";
				
	?>
	<tr>
		<td height="41" align="center" bgcolor="<?php echo $bgcolor;?>"  class="content" style="color:<?php echo $color;?>"><?php echo $row_form[$i]['BS_Date'];?></td>
		<td width="123" align="center" bgcolor="<?php echo $bgcolor;?>"><input name="button" type="button" class="content" id="button4" onclick="MM_openBrWindow('priview_bsrs.php?t=bsrs&BS_ID=<?php echo $row_form[$i]["BS_ID"];?>&RS_ID=<?php echo $row_form[$i]["RS_ID"]; ?>','exporExcel','')" value="預覽"/></td>
    </tr>		
	<?php   } 
        
        }
		else if($type == 'adapt')
		{
			$rs_form = $objDB->Recordset("SELECT * FROM adapt WHERE RS_ID = '$RS_ID' ORDER BY AD_Date DESC");
	        $row_form = $objDB->GetRows($rs_form);
	           
		    for($i=0;$i<$objDB->RecordCount($rs_form);$i++)
			{
				if($i%2==0)
					$bgcolor="#EBEBEB";
				else
					$bgcolor="#FFFFFF";
		?>
	<tr>
		<td height="41" align="center" bgcolor="<?php echo $bgcolor;?>"  class="content" style="color:<?php echo $color;?>"><?php echo $row_form[$i]['AD_Date'];?></td>
		<td width="123" align="center" bgcolor="<?php echo $bgcolor;?>"><input name="button" type="button" class="content" id="button4" onclick="MM_openBrWindow('adapt.php?AD_ID=<?php echo $row_form[$i]["AD_ID"];?>&RS_ID=<?php echo $row_form[$i]["RS_ID"]; ?>','exporExcel','')" value="預覽"/></td>
    </tr>			
	<?php   } 
        
        }
		else if($type == 'behavior')
		{
			$rs_form = $objDB->Recordset("SELECT * FROM behavior WHERE RS_ID = '$RS_ID' ORDER BY BEH_Date DESC");
	        $row_form = $objDB->GetRows($rs_form);
	           
		    for($i=0;$i<$objDB->RecordCount($rs_form);$i++)
			{
				if($i%2==0)
					$bgcolor="#EBEBEB";
				else
					$bgcolor="#FFFFFF";
		?>
	<tr>
		<td height="41" align="center" bgcolor="<?php echo $bgcolor;?>"  class="content" style="color:<?php echo $color;?>"><?php echo $row_form[$i]['BEH_Date'];?></td>
		<td width="123" align="center" bgcolor="<?php echo $bgcolor;?>"><input name="button" type="button" class="content" id="button4" onclick="MM_openBrWindow('priview_behavior.php?BEH_ID=<?php echo $row_form[$i]["BEH_ID"];?>&RS_ID=<?php echo $row_form[$i]["RS_ID"]; ?>','exporExcel','')" value="預覽"/></td>
    </tr>	
    <?php
            }
        }
    ?>
    <tr>
    	<td height="10"></td>
    </tr>

	<tr>
		<td align="right" class="content">&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
		<!-- <input name="mybtn" type="button" class="form_fix" id="mybtn"  value="確定送出"/> --> 
 		<!-- <input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/> -->
		</td>
	</tr>                
	</table>
	</form>              
	</td>
</tr>
</table>
          </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>
