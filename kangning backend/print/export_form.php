<?php
//ini_set("memory_limit","2048M");

require_once('../public/PHPExcel.php');
require_once('../public/PHPExcel/Writer/Excel5.php');
require_once("../public/mem_check.php");
require_once("../public/web_function.php");
require_once("../public/PHPExcel/IOFactory.php");

$type = $_REQUEST["t"];
$RS_ID = $_REQUEST["RS_ID"];
$test = '123';

if($type == 'adl')
{
    $ADL_ID = $_REQUEST["ADL_ID"];

	$rs = $objDB->Recordset("SELECT * FROM adl where ADL_ID = '$ADL_ID' AND RS_ID = '$RS_ID'");
    $row = $objDB->GetRows($rs);

    $objPHPExcel = new PHPExcel();
    $objReader = PHPExcel_IOFactory::createReader('Excel2007');

    $objPHPExcel = $objReader->load("template.xlsx");
    $objPHPExcel->setActiveSheetIndex(1);

	$objPHPExcel->getActiveSheet()->setCellValue('D2', $row[0]['ADL_Date']);
    $objPHPExcel->getActiveSheet()->setCellValue('D3', $row[0]['ADL_Eat']);
    $objPHPExcel->getActiveSheet()->setCellValue('D26', $row[0]['ADL_Move']);
    $objPHPExcel->getActiveSheet()->setCellValue('D7', $row[0]['ADL_Shower']);
    $objPHPExcel->getActiveSheet()->setCellValue('D9', $row[0]['ADL_Makeup']);
    $objPHPExcel->getActiveSheet()->setCellValue('D21', $row[0]['ADL_Toilet']);
    $objPHPExcel->getActiveSheet()->setCellValue('D31', $row[0]['ADL_Pace']);
    $objPHPExcel->getActiveSheet()->setCellValue('D36', $row[0]['ADL_Stair']);
    $objPHPExcel->getActiveSheet()->setCellValue('D11', $row[0]['ADL_Clothes']);       
    $objPHPExcel->getActiveSheet()->setCellValue('D14', $row[0]['ADL_Defecate']);
    $objPHPExcel->getActiveSheet()->setCellValue('D17', $row[0]['ADL_Urinate']);
    $objPHPExcel->getActiveSheet()->setCellValue('D39', $row[0]['ADL_Score']);

	//設定字體大小
	$styleArray = array( 'font' => array( 'size' => 8, 'underline' => false),);
	$objPHPExcel->getActiveSheet()->getStyle('E25')->applyFromArray($styleArray);			  
    
    $test = $ADL_ID;
}

if($type == 'evaluate')
{
    $EV_ID = $_REQUEST["EV_ID"];

	$rs = $objDB->Recordset("SELECT * FROM evaluate where EV_ID = '$EV_ID' AND RS_ID = '$RS_ID'");
    $row = $objDB->GetRows($rs);

    $objPHPExcel = new PHPExcel();
    $objReader = PHPExcel_IOFactory::createReader('Excel2007');

    $objPHPExcel = $objReader->load("template.xlsx");
    $objPHPExcel->setActiveSheetIndex(0);
    
    $objPHPExcel->getActiveSheet()->setCellValue('D2', $row[0]['EV_Date']);
	$objPHPExcel->getActiveSheet()->setCellValue('D3', 'GCS:E  M  V  ￭'.$row[0]['EV_AW']);
	$objPHPExcel->getActiveSheet()->setCellValue('D6', '￭'.$row[0]['EV_BR']);
	$objPHPExcel->getActiveSheet()->setCellValue('D7', '￭'.$row[0]['EV_CU']);
	$objPHPExcel->getActiveSheet()->setCellValue('D8', '￭'.$row[0]['EV_SP']);
    $objPHPExcel->getActiveSheet()->setCellValue('D9', '￭'.$row[0]['EV_SPC']);
    $objPHPExcel->getActiveSheet()->setCellValue('D11', '￭'.$row[0]['EV_GM']);
    $objPHPExcel->getActiveSheet()->setCellValue('D12', '￭'.$row[0]['EV_GA']);
    $objPHPExcel->getActiveSheet()->setCellValue('D13', '￭'.$row[0]['EV_SE']);
    $objPHPExcel->getActiveSheet()->setCellValue('D14', '￭'.$row[0]['EV_DI']);
    $objPHPExcel->getActiveSheet()->setCellValue('D15', '￭'.$row[0]['EV_AP']);
    $objPHPExcel->getActiveSheet()->setCellValue('D16', '￭'.$row[0]['EV_VI']);
    $objPHPExcel->getActiveSheet()->setCellValue('D20', '￭'.$row[0]['EV_HE']);
    $objPHPExcel->getActiveSheet()->setCellValue('D24', '￭'.$row[0]['EV_Teeth']);
    $objPHPExcel->getActiveSheet()->setCellValue('D26', '￭'.$row[0]['EV_BU']);
    $objPHPExcel->getActiveSheet()->setCellValue('D29', '￭'.$row[0]['EV_Aid']);
	$objPHPExcel->getActiveSheet()->setCellValue('D31', '￭'.$row[0]['EV_UR']);
	$objPHPExcel->getActiveSheet()->setCellValue('D32', '￭'.$row[0]['EV_CH']);
	$objPHPExcel->getActiveSheet()->setCellValue('D34', '￭'.$row[0]['EV_Skin']);
    $objPHPExcel->getActiveSheet()->setCellValue('D42', '￭'.$row[0]['EV_Sleep']);
    $objPHPExcel->getActiveSheet()->setCellValue('C49', '￭'.$row[0]['EV_MA']);
    $objPHPExcel->getActiveSheet()->setCellValue('C52', '￭'.$row[0]['EV_Joint']);
    $objPHPExcel->getActiveSheet()->setCellValue('C54', $row[0]['EV_NS']);

	//設定字體大小
	$styleArray = array( 'font' => array( 'size' => 8, 'underline' => false),);
	$objPHPExcel->getActiveSheet()->getStyle('E25')->applyFromArray($styleArray);	

    $test = $EV_ID;
}

// ＊＊＊＊＊重要 不加上 可能導致BOM 匯出格式錯誤＊＊＊＊＊ //
ob_clean();

header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
//header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment; filename=\"$test.xlsx\"");
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
ob_clean();
// We'll be outputting an excel file
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
// It will be called file.xls

// Write file to the browser
$objWriter->save('php://output');
exit();


?>