
<script type="text/javascript">


function doChange1(){
  
  //alert("function: doChange1");
  var hasRS_ID='<?php echo is_numeric(quotes($_GET['RS_ID'])); ?>';
  if(hasRS_ID!='1')
  {
      alert("請先選擇住民!!!");
  }
  else
  {
      $("#hide1").toggle();
  }
  
};

function doChange2(){
  
  //alert("function: doChange2");

  var hasRS_ID='<?php echo is_numeric(quotes($_GET['RS_ID'])); ?>';
  if(hasRS_ID!='1')
  {
      alert("請先選擇住民!!!");
  }
  else
  {
      $("#hide2").toggle();
  };
};

function doChange3(){

  var hasRS_ID='<?php echo is_numeric(quotes($_GET['RS_ID'])); ?>';
  if(hasRS_ID!='1')
  {
      alert("請先選擇住民!!!");
  }
  else
  {
      $("#hide3").toggle();
  }
};

</script>
<div id="menulist" class="arrowlistmenu">

  <div class="headerbar" id="menu-member">入住/退住護理紀錄</div>
  <ul>
      <li><a href="#" onclick="MM_goToURL('parent','../welcome/main.php?RS_Status=2&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue">已入住住民</a></li>
      <li><a href="#" onclick="MM_goToURL('parent','../welcome/main.php?RS_Status=1&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue">待入住住民</a></li>
      <li><a href="JavaScript:doChange2();">護理紀錄</a></li>
      <li><ul id="hide2" style="display:none;">
          <li><a href="#" style="color:#A31D1B;" onclick="MM_goToURL('parent','../care/register_md.php?RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue">&nbsp;&nbsp;入住評估</a></li>
          <li><a href="#" style="color:#A31D1B;" onclick="MM_goToURL('parent','../care/register_md.php?RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue">&nbsp;&nbsp;退住評估</a></li>
      </ul></li>
  </ul>
  <div class="headerbar" id="menu-member">臨床護理紀錄</div>
  <ul>
      <li><a href="JavaScript:doChange1();">高危險因子</a></li>
      <li><ul id="hide1" style="display:none;">
          <li><a href="#" style="color:#A31D1B;" onclick="MM_goToURL('parent','../care/layout.php?t=fall&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>&nbsp;&nbsp;跌倒評估</a></li>
          <li><a href="#" style="color:#A31D1B;" onclick="MM_goToURL('parent','../care/layout.php?t=skin&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>&nbsp;&nbsp;皮膚評估</a></li>
          <li><a href="#" style="color:#A31D1B;" onclick="MM_goToURL('parent','../care/layout.php?t=bsrs&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>&nbsp;&nbsp;自殺評估</a></li>
          <li><a href="#" style="color:#A31D1B;" onclick="MM_goToURL('parent','../care/layout.php?t=pain&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue"/>&nbsp;&nbsp;疼痛評估</a></li>
      </ul></li>
      <li><a href="#" onclick="MM_goToURL('parent','../care/layout.php?t=evaluate&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue">健康評估</a></li>
      <li><a href="#" onclick="MM_goToURL('parent','../care/layout.php?t=adl&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue">巴氏量表</a></li>
      <li><a href="#" onclick="MM_goToURL('parent','../care/layout.php?t=mmse&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue">簡易智能量表</a></li>
      <li><a href="#" onclick="MM_goToURL('parent','../care/layout.php?t=beh&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue">行為能力評估</a></li>
      <li><a href="#" onclick="MM_goToURL('parent','../care/layout.php?t=gds&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue">老人憂鬱量表</a></li>
      <li><a href="#" onclick="MM_goToURL('parent','../care/layout.php?t=adapt&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue">生活適應評估</a></li>
      <li><a href="#" onclick="MM_goToURL('parent','../diabetes/people_list.php?t=vital');return document.MM_returnValue">生命徵象評估</a></li>
      <li><a href="#" onclick="MM_goToURL('parent','../diabetes/people_list.php?t=ins');return document.MM_returnValue">胰島素注射紀錄</a></li>
      <li><a href="#" onclick="MM_goToURL('parent','../diabetes/people_list.php?t=bs');return document.MM_returnValue">血糖紀錄</a></li>
      <li><a href="#" onclick="MM_goToURL('parent','../record/layout.php?t=io&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue">攝入及排出</a></li>
      <li><a href="JavaScript:doChange3()">一般護理紀錄</a></li>
      <li><ul id="hide3" style="display:none;">
          <li><a href="#" style="color:#A31D1B;" onclick="MM_goToURL('parent','../common/record_list.php?RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue">&nbsp;&nbsp;護理紀錄</a></li>
          <li><a href="#" style="color:#A31D1B;" oncick="MM_goToURL('parent','../common/nd_list.php?RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue">&nbsp;&nbsp;護理診斷</a></li>
          <li><a href="#" style="color:#A31D1B;" onclick="MM_goToURL('parent','../common/plan_list.php?RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue">&nbsp;&nbsp;護理計畫</a></li>
      </ul></li>
  </ul>
	<div class="headerbar" id="menu-member">其他</div>	
  <ul>
      <li><a href="#" onclick="MM_goToURL('parent','../other/tube_list.php?RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue">管路更換</a></li>
      <li><a href="#" onclick="MM_goToURL('parent','../record/layout.php?t=exchange&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue">護理交班</a></li>
      <li><a href="#" onclick="MM_goToURL('parent','../record/layout.php?t=med&RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue">藥物</a></li> 
      <li><a href="#" onclick="MM_goToURL('parent','../print/print_list.php?RS_ID=<?php echo $RS_ID;?>');return document.MM_returnValue">列印</a></li>
      <li><a href="#">暫存</a></li>
  </ul>
  <div class="headerbar" id="menu_member">系統設定</div>
	<ul>    	
    	<li><a href="../admin/admin.php">機構人員</a></li>     
	</ul> 
  
</div>    