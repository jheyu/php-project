<?php 
include("../public/mem_check.php");
include("../public/web_function.php");
	
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}
	if(is_numeric(quotes($_GET['RS_ID']))){
		 $RS_ID = quotes($_GET['RS_ID']);
	}else{
		 ?>
     <script language="javascript">		
		location.href='../index.php';
	 </script>	
         <?php
	}	
	
	$sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
	$rs = $objDB->Recordset($sql);
	$row = $objDB->GetRows($rs);	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>

<script>
var score0 = 0;
var score1 = 0;
var score2 = 0;
/*
var score3 = 0;
var score4 = 0;
var score5 = 0;
var score6 = 0;
var score7 = 0;
var score8 = 0;
var score9 = 0;
var score10 = 0;
var score11 = 0;
*/
var total = 0;

var level ;

var caculate = function(){
	  
     total = score0 + score1 + score2 ;
	 
	 if(total==15){
	 	level = "(意識清楚)";
	 }else if(total >= 13 && total <=14){
	 	level = "(輕度昏迷)";
	 }else if(total >=9 && total <= 12){
	 	level = "(中度昏迷)";
	 }else{
	 	level = "(重度昏迷)";
	 }
	 	$("#SCORE").text(total);
		$("#LEVEL").text(level); 
 }
 $(document).ready(function(){
	  
	 
	$("input[name='GCS_Eye']").click(function(){
		var GCS_Eye = $("input[name='GCS_Eye']:checked").val();
		score0 = parseInt(GCS_Eye);
		caculate();		
	})
	$("input[name='GCS_Verbal']").click(function(){
		var GCS_Verbal = $("input[name='GCS_Verbal']:checked").val();
		score1 = parseInt(GCS_Verbal);		
		caculate();		
	})
	$("input[name='GCS_Motor']").click(function(){
		var GCS_Motor = $("input[name='GCS_Motor']:checked").val();
		score2 = parseInt(GCS_Motor);		
		caculate();		
	})	
	 
	 
 	$("#mybtn").click(function(){
		$("#GCS_Score").val(total);
	
		$("form#form1").submit();
		
	})
 	
 })

</script>
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<!-- header starting point -->
	<?php include("../include/header.php");?>
	<!-- header ending point -->    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1000" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="160" valign="top" background="../images/bkline.jpg">
          	<!--menu starting point-->
            <?php include("../include/menu.php");?>
            <!--menu ending point-->          </td>
            
          <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
          <td width="930" valign="top">
          <table width="830" border="0" cellpadding="1" cellspacing="1">
            <tr>
              <td height="20" class="content">專業照護 > 評估與記錄 &gt; 新增</td>
              </tr>
              <tr>
                  <td height="10"></td>
              </tr>                       
             <tr>
              	<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>   
             </tr>   
             <tr>
                <td height="15"></td>
              </tr>           
              
              <tr>
              		<td style="font-size: 13pt"><strong>GCS昏迷指數</strong>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  分數:<label id="SCORE"> </label><label id="LEVEL"> </label>
                  </td>            
              </tr> 
              <tr>
                <td height="5"></td>
              </tr>
              <tr>
              	  <td ><img src="../images/blueline.jpg" /></td>
              </tr> 
               <tr>
                <td height="5"></td>
              </tr>   
            <tr>         
              <td>    
              <!--管理員管理startinging-->              
              <form name="form1" id="form1" method="post" action="gcs_process.php" />
			   <input type="hidden" name="action" id="action" value="new"/>              
              <input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>" />
              <input type="hidden" name="GCS_Score" id="GCS_Score" />               
             <table>
                <tr>
                  <td height="10"></td>
                </tr>
                
                <tr>
                  <td width="110" align="right"  class="content">睜眼反應：</td>
                  <td width="705">
                 <input name="GCS_Eye" type="radio" class="form_fix" id="GCS_Eye4" value="4" />自動睜眼(spontaneous)<br />
                 <input name="GCS_Eye" type="radio" class="form_fix" id="GCS_Eye3" value="3"/>呼喚會睜眼(to speech)<br />
                 <input name="GCS_Eye" type="radio" class="form_fix" id="GCS_Eye2" value="2"/>有刺激或痛楚會睜眼(to pain)<br />
                 <input name="GCS_Eye" type="radio" class="form_fix" id="GCS_Eye1" value="1"/>對刺激無反應(none)
                  </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">說話反應：</td>
                  <td width="705">
                 <input name="GCS_Verbal" type="radio" class="form_fix" id="GCS_Verbal5" value="5"/>說話有條理(oriented)<br />
                 <input name="GCS_Verbal" type="radio" class="form_fix" id="GCS_Verbal4" value="4"/>可應答，但有答非所問的情況(confused)<br />
                 <input name="GCS_Verbal" type="radio" class="form_fix" id="GCS_Verbal3" value="3"/>可說出單字(inappropriate words)<br />
                 <input name="GCS_Verbal" type="radio" class="form_fix" id="GCS_Verbal2" value="2"/>可發出呻吟(unintelligible sounds)<br />
                 <input name="GCS_Verbal" type="radio" class="form_fix" id="GCS_Verbal1" value="1"/>無任何反應(none)
                  </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">運動反應：</td>
                  <td width="705">
                 <input name="GCS_Motor" type="radio" class="form_fix" id="GCS_Motor6" value="6"/>可依指令動作(obey commands)<br />
                 <input name="GCS_Motor" type="radio" class="form_fix" id="GCS_Motor5" value="5"/>施以刺激時，可定位出疼痛位置(localize)<br />
                 <input name="GCS_Motor" type="radio" class="form_fix" id="GCS_Motor4" value="4"/>對疼痛刺激有反應，肢體會回縮(withdrawal)<br />
                 <input name="GCS_Motor" type="radio" class="form_fix" id="GCS_Motor4" value="3"/>對疼痛刺激有反應，肢體會彎曲(decorticate flexion)<br />
                 <input name="GCS_Motor" type="radio" class="form_fix" id="GCS_Motor4" value="2"/>對疼痛刺激有反應，肢體會伸直(decerebrate extension)<br />
                 <input name="GCS_Motor" type="radio" class="form_fix" id="GCS_Motor4" value="1"/>無任何反應(no response)
                  </td>
                </tr>                    
                     
                <tr>
                  <td align="right" class="content">&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>
                      <input name="mybtn" type="button" class="form_fix" id="mybtn"  value="確定送出" />&nbsp; 
                      <!--<input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>-->
                   </td>
                </tr>                
              </table>
              </form>
              <!--管理員管理 ending-->              
              </td>
            </tr>
            
          </table>
          </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>
