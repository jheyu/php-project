<?php 
include("../public/mem_check.php");
include("../public/web_function.php");
	
	if(!$_SESSION['KNH_LOGIN_ID'])
	{
		header("location:../index.php");
		exit;
	}
	if(is_numeric(quotes($_GET['RS_ID']))){
		 $RS_ID = quotes($_GET['RS_ID']);
	}else{
		 ?>
     <script language="javascript">		
		location.href='../index.php';
	 </script>	
         <?php
	}	
	
	$sql = "SELECT * FROM resident where RS_ID ='$RS_ID'";
	$rs = $objDB->Recordset($sql);
	$row = $objDB->GetRows($rs);	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $html_title;?>專業照護</title>
<script language="JavaScript" src="../js/common.js"></script>
<script language="javascript" src="../js/jquery.js" ></script>

<script>
var score0 = 0;
var score1 = 0;
var score2 = 0;
var score3 = 0;
var score4 = 0;
var score5 = 0;
var score6 = 0;
var score7 = 0;
var score8 = 0;
var score9 = 0;
var score10 = 0;
var score11 = 0;
var total = 0;

var level ;

var caculate = function(){
	  
     total = score0 + score1 + score2 + score3 + 
	 		  score4 + score5 + score6 + score7 +
			  score8 + score9 + score10 + score11;
	 
	 if(total<=20){
	 	level = "(輕度危險)";
	 }else if(total > 20 && total <=28){
	 	level = "(中度危險)";
	 }else{
	 	level = "(重度危險)";
	 }
	 	$("#SCORE").text(total);
		$("#LEVEL").text(level); 
 }
 $(document).ready(function(){
	  
	 
	$("input[name='FA_Age']").click(function(){
		var FA_Age = $("input[name='FA_Age']:checked").val();
		score0 = parseInt(FA_Age);
		caculate();		
	})
	$("input[name='FA_Pee']").click(function(){
		var FA_Pee = $("input[name='FA_Pee']:checked").val();
		score1 = parseInt(FA_Pee);
		
		caculate();		
	})
	$("input[name='FA_New']").click(function(){
		var FA_New = $("input[name='FA_New']:checked").val();
		score2 = parseInt(FA_New);
		
		caculate();		
	})
	$("input[name='FA_View']").click(function(){
		var FA_View = $("input[name='FA_View']:checked").val();
		score3 = parseInt(FA_View);
		
		caculate();		
	})
	$("input[name='FA_Active']").click(function(){
		var FA_Active = $("input[name='FA_Active']:checked").val();
		score4 = parseInt(FA_Active);
		
		caculate();		
	})
	$("input[name='FA_History']").click(function(){
		var FA_History = $("input[name='FA_History']:checked").val();
		score5 = parseInt(FA_History);
		
		caculate();		
	})
	$("input[name='FA_Step']").click(function(){
		var FA_Step = $("input[name='FA_Step']:checked").val();
		score6 = parseInt(FA_Step);
		
		caculate();		
	})
	$("input[name='FA_Behavior']").click(function(){
		var FA_Behavior = $("input[name='FA_Behavior']:checked").val();
		score7 = parseInt(FA_Behavior);
		
		caculate();		
	})
	$("input[name='FA_Conscious']").click(function(){
		var FA_Conscious = $("input[name='FA_Conscious']:checked").val();
		score8 = parseInt(FA_Conscious);
		
		caculate();		
	})
	$("input[name='FA_Envir']").click(function(){
		var FA_Envir = $("input[name='FA_Envir']:checked").val();
		score9 = parseInt(FA_Envir);
		
		caculate();		
	})
	$("input[name='FA_Clothing']").click(function(){
		var FA_Clothing = $("input[name='FA_Clothing']:checked").val();
		score10 = parseInt(FA_Clothing);
		
		caculate();		
	})
	$("input[name='FA_Drug']").click(function(){
		var FA_Drug = $("input[name='FA_Drug']:checked").val();
		score11 = parseInt(FA_Drug);
		
		caculate();		
	})
	
	 $("input[name='FA_Disable']").click(function(){
		 
		$("input[name='FA_Age']").attr('checked',false);
		$("input[name='FA_Pee']").attr('checked',false);
		$("input[name='FA_New']").attr('checked',false);
		$("input[name='FA_View']").attr('checked',false);
		$("input[name='FA_Active']").attr('checked',false);
		$("input[name='FA_History']").attr('checked',false);
		$("input[name='FA_Step']").attr('checked',false);
		$("input[name='FA_Behavior']").attr('checked',false);
		$("input[name='FA_Conscious']").attr('checked',false);
		$("input[name='FA_Envir']").attr('checked',false);
		$("input[name='FA_Clothing']").attr('checked',false);
		$("input[name='FA_Drug']").attr('checked',false);
		$("input[name='FA_Age']").attr('checked',false);
		$("#FA_DrugName").val('');
		
		$("input[name='FA_Age']").attr('disabled',true);
		$("input[name='FA_Pee']").attr('disabled',true);
		$("input[name='FA_New']").attr('disabled',true);
		$("input[name='FA_View']").attr('disabled',true);
		$("input[name='FA_Active']").attr('disabled',true);
		$("input[name='FA_History']").attr('disabled',true);
		$("input[name='FA_Step']").attr('disabled',true);
		$("input[name='FA_Behavior']").attr('disabled',true);
		$("input[name='FA_Conscious']").attr('disabled',true);
		$("input[name='FA_Envir']").attr('disabled',true);
		$("input[name='FA_Clothing']").attr('disabled',true);
		$("input[name='FA_Drug']").attr('disabled',true);
		$("input[name='FA_Age']").attr('disabled',true);		
		$("#FA_DrugName").attr('disabled', true);
		
		$("#SCORE").text('');
		$("#LEVEL").text(''); 
	})
	$("#rebtn").click(function(){	
		$("input[name='FA_Age']").attr('disabled',false);
		$("input[name='FA_Pee']").attr('disabled',false);
		$("input[name='FA_New']").attr('disabled',false);
		$("input[name='FA_View']").attr('disabled',false);
		$("input[name='FA_Active']").attr('disabled',false);
		$("input[name='FA_History']").attr('disabled',false);
		$("input[name='FA_Step']").attr('disabled',false);
		$("input[name='FA_Behavior']").attr('disabled',false);
		$("input[name='FA_Conscious']").attr('disabled',false);
		$("input[name='FA_Envir']").attr('disabled',false);
		$("input[name='FA_Clothing']").attr('disabled',false);
		$("input[name='FA_Drug']").attr('disabled',false);
		$("input[name='FA_Age']").attr('disabled',false);
		$("#FA_DrugName").attr('disabled',false);	
				
	})
	 
 	$("#mybtn").click(function(){
		$("#FA_Score").val(total);
	
		$("form#form1").submit();
		
	})
 	
 })

</script>
<link href="../css/backend.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="1000" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
	<!-- header starting point -->
	<?php include("../include/header.php");?>
	<!-- header ending point -->    </td>
  </tr>
  <tr>
    <td valign="top"><table width="1000" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="160" valign="top" background="../images/bkline.jpg">
          	<!--menu starting point-->
            <?php include("../include/menu.php");?>
            <!--menu ending point-->          </td>
            
          <td width="10" valign="top"><img src="../images/spacer.gif" width="10" height="1" /></td>
          <td width="930" valign="top">
          <table width="830" border="0" cellpadding="1" cellspacing="1">
            <tr>
              <td height="20" class="content">專業照護 > 評估與記錄 &gt; 新增</td>
              </tr>
              <tr>
                  <td height="10"></td>
              </tr>                       
             <tr>
              	<td class="content_red_b" style="font-size: 16pt">住民姓名:<?php echo $row[0]['RS_Name'];?></td>   
             </tr>   
             <tr>
                <td height="15"></td>
              </tr>           
              
              <tr>
              		<td style="font-size: 13pt"><strong>跌倒危險因子評估</strong>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  分數:<label id="SCORE"> </label><label id="LEVEL"> </label>
                  </td>            
              </tr> 
              <tr>
                <td height="5"></td>
              </tr>
              <tr>
              	  <td ><img src="../images/blueline.jpg" /></td>
              </tr> 
               <tr>
                <td height="5"></td>
              </tr>   
            <tr>         
              <td>    
              <!--管理員管理startinging-->              
              <form name="form1" id="form1" method="post" action="fall_process.php" />
			   <input type="hidden" name="action" id="action" value="new"/>
              <input type="hidden" name="RS_ID" id="RS_ID" value="<?php echo $RS_ID;?>" />
              <input type="hidden" name="FA_Score" id="FA_Score" />
              <!--
              <table width="825" border="0">
              	  <tr>
                  <td width="110" align="right" class="content"><input name="FA_Disable" type="checkbox" class="form_fix" id="FA_Disable" value="Y"/>不適用&nbsp;&nbsp;&nbsp;原因</td>
                  <td width="705"><input name="FA_Reason" type="text" class="form_fix" id="FA_Reason" style="width:150px"/></td>    
                </tr>
                </table>
              -->  
                <table>
                <tr>
                  <td height="10"></td>
                </tr>
                
                <tr>
                  <td width="110" align="right"  class="content">年齡：</td>
                  <td width="705">
                 <input name="FA_Age" type="radio" class="form_fix" id="FA_Age1" value="1" />小於65歲
                 <input name="FA_Age" type="radio" class="form_fix" id="FA_Age2" value="2"/>大於65歲
                 <input name="FA_Age" type="radio" class="form_fix" id="FA_Age3" value="3"/>無法評估
                  </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">夜間頻尿：</td>
                  <td width="705">
                 <input name="FA_Pee" type="radio" class="form_fix" id="FA_Pee1" value="2"/>是
                 <input name="FA_Pee" type="radio" class="form_fix" id="FA_Pee2" value="1"/>否
                 <input name="FA_Pee" type="radio" class="form_fix" id="FA_Pee3" value="3"/>無法評估
                  </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">新入住或轉入：</td>
                  <td width="705">
                 <input name="FA_New" type="radio" class="form_fix" id="FA_New1" value="2"/>是
                 <input name="FA_New" type="radio" class="form_fix" id="FA_New2" value="1"/>否
                 <input name="FA_New" type="radio" class="form_fix" id="FA_New3" value="3"/>無法評估
                  </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">視覺障礙：</td>
                  <td width="705">
                 <input name="FA_View" type="radio" class="form_fix" id="FA_View1" value="2"/>是
                 <input name="FA_View" type="radio" class="form_fix" id="FA_View2" value="1"/>否
                 <input name="FA_View" type="radio" class="form_fix" id="FA_View3" value="3"/>無法評估
                  </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">活動能力障礙：</td>
                  <td width="705">
                 <input name="FA_Active" type="radio" class="form_fix" id="FA_Active1" value="2"/>是
                 <input name="FA_Active" type="radio" class="form_fix" id="FA_Active2" value="1"/>否
                 <input name="FA_Active" type="radio" class="form_fix" id="FA_Active3" value="3"/>無法評估
                  </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">跌倒病史：</td>
                  <td width="705">
                 <input name="FA_History" type="radio" class="form_fix" id="FA_History1" value="1"/>無
                 <input name="FA_History" type="radio" class="form_fix" id="FA_History2" value="2"/>有跌倒病史
                 <input name="FA_History" type="radio" class="form_fix" id="FA_History3" value="3"/>無法評估
                  </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">步態平衡：</td>
                  <td width="705">
                 <input name="FA_Step" type="radio" class="form_fix" id="FA_Step1" value="1"/>穩健平衡
                 <input name="FA_Step" type="radio" class="form_fix" id="FA_Step2" value="2"/>不穩健平衡
                 <input name="FA_Step" type="radio" class="form_fix" id="FA_Step3" value="3"/>無法評估
                  </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">行為：</td>
                  <td width="705">
                 <input name="FA_Behavior" type="radio" class="form_fix" id="FA_Behavior1" value="4"/>動盪不安
                 <input name="FA_Behavior" type="radio" class="form_fix" id="FA_Behavior2" value="3"/>沮喪
                 <input name="FA_Behavior" type="radio" class="form_fix" id="FA_Behavior3" value="1"/>正常
                 <input name="FA_Behavior" type="radio" class="form_fix" id="FA_Behavior4" value="2"/>無法評估
                  </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">意識狀態：</td>
                  <td width="705">
                 <input name="FA_Conscious" type="radio" class="form_fix" id="FA_Conscious1" value="1"/>清醒有定向感
                 <input name="FA_Conscious" type="radio" class="form_fix" id="FA_Conscious2" value="2"/>人、時、地混淆
                 <input name="FA_Conscious" type="radio" class="form_fix" id="FA_Conscious3" value="3"/>無法評估                 
                  </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">環境中熟悉物：</td>
                  <td width="705">
                 <input name="FA_Envir" type="radio" class="form_fix" id="FA_Envir1" value="1"/>擺放於健側或熟悉位置
                 <input name="FA_Envir" type="radio" class="form_fix" id="FA_Envir2" value="2"/>物件擺放被移動移位
                 <input name="FA_Envir" type="radio" class="form_fix" id="FA_Envir3" value="3"/>無法評估                 
                  </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">衣著(含鞋子)：</td>
                  <td width="705">
                 <input name="FA_Clothing" type="radio" class="form_fix" id="FA_Clothing1" value="1"/>衣服、褲子大小合宜、鞋子防滑
                 <input name="FA_Clothing" type="radio" class="form_fix" id="FA_Clothing2" value="2"/>衣服寬鬆、褲子太長、鞋子易滑
                 <input name="FA_Clothing" type="radio" class="form_fix" id="FA_Clothing3" value="3"/>無法評估                 
                  </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td width="110" align="right"  class="content">藥物：</td>
                  <td width="705">
                  ※1.抗組織胺 2.抗高血壓 3.鎮靜安眠藥 4.肌肉鬆弛劑 5.輕瀉劑 6.利尿劑 7.抗抑鬱藥 8.降血糖藥 <br />
                 <input name="FA_Drug" type="radio" class="form_fix" id="FA_Drug1" value="1"/>未使用以上藥物
                 <input name="FA_Drug" type="radio" class="form_fix" id="FA_Drug2" value="2"/>服用以上兩種藥物
                 <input name="FA_Drug" type="radio" class="form_fix" id="FA_Drug3" value="3"/>無法評估     
                 <br />            
                 藥名:<input name="FA_DrugName" id="FA_DrugName" class="form_fix" type="text"/> 
                  </td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                     
                <tr>
                  <td align="right" class="content">&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>
                      <input name="mybtn" type="button" class="form_fix" id="mybtn"  value="確定送出" />&nbsp; 
                      <!--<input type="reset" value="重填"  class="form_fix" id="rebtn" name="rebtn"/>-->
                   </td>
                </tr>                
              </table>
              </form>
              <!--管理員管理 ending-->              
              </td>
            </tr>
            
          </table>
          </td>
        </tr>
        
    </table></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><img src="../images/spacer.gif" width="1" height="1" /></td>
  </tr>
  <tr>
    <td>
       <div class="copyright">
          <!--footer starting point-->
          <?php include("../include/footer.php");?>
          <!--footer starting point-->
       </div>   
    </td>
  </tr>
</table>
</body>
</html>
