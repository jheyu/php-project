<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>留言板</title>
        <script type="text/javascript">
            function check_data()
            {
                if(document.myForm.author.value.length == 0)
                	alert("作者欄位不可以空白!!");
                else if(document.myForm.subject.value.length == 0)
                	alert("主題欄位不可以空白!!");
                else if(document.myForm.content.value.length == 0)
                	alert("內容欄位不可以空白!!");
                else
                	myForm.submit();
            }
        </script>
    </head>
    <body>
    	<a href="http://localhost/phpboard/login.php">登入管理</a>
        <?php
            require_once("connMysql.php");
            
            /*
            function create_connection()
            {
                $link = mysql_connect("localhost", "root", "67567500")
                    or die("Connect error<br><br>" . mysql_error());

                mysql_query("SET NAMES utf8");

                return $link;
            }
            */
            /*
            function execute_sql($database, $sql, $link)
            {
            	$db_selected = mysql_select_db($database, $link)
            	    or die("Connect error<br><br>" . mysql_error($link));
                echo $db_selected;
                $seldb = @mysql_select_db("phpboard", $link);
                if(!$seldb)
    	            die("failed!!");
                else
    	            echo "";

            	$result = mysql_query($sql);

            	return $result;
            }
            */

            //指定每頁顯示幾筆紀錄
            $records_per_page = 5;

            //取得要顯示第幾頁的紀錄
            if(isset($_GET["page"]))
            	$page = $_GET["page"];
            else
            	$page = 1;
            
            //本頁開始記錄筆數 = 每頁紀錄筆數 * (頁數-1)
            $started_record = $records_per_page * ($page - 1);

            //資料連接
            $link = mysql_connect("localhost", "root", "67567500");

            //未加限制顯示筆數的SQL敘述句
            $sql = "SELECT * FROM `board` ORDER BY `time` DESC";
            
            /*
            $result = execute_sql("phpboard", $sql, $link);
            */

            //加上限制顯示筆數的SQL敘述句,由本頁開始記錄筆數,每頁顯示預設筆數
            $query_limit_RecBoard = $sql. " LIMIT ".$started_record.", ".$records_per_page;
            
            //以加上限制筆數的SQL敘述句查詢資料到$RecBoard中
            $RecBoard = mysql_query($query_limit_RecBoard);

            //以未加上限制顯示筆數的SQL敘述句查詢資料到$all_RecBoard中
            $all_RecBoard = mysql_query($sql);

            //計算總筆數
            $total_records = mysql_num_rows($all_RecBoard);

            //計算總頁數=(總筆數/每頁筆數)
            $total_pages = ceil($total_records / $records_per_page);
            
            //-----------------------------------有問題----------------------------------------
            //mysql_data_seek($result, $started_record);
            

            echo "<table width='800' align='center' cellspacing='3'>";

            //顯示紀錄
            $j = 1;
            while($row = mysql_fetch_assoc($RecBoard) and $j <= $records_per_page)
            {
               echo "<td>作者: " . $row["author"] . "<br>";
               echo "主題: " . $row["subject"] . "<br>";
               echo "時間: " . $row["time"] . "<hr>";
               echo "<a href='show_news.php?id=".$row["id"]."'>閱讀與加入討論</a></td></tr>";
               //echo $row["content"] . "</td></tr>";
               $j++;
            }
            echo "</table>";

            //產生分頁導覽列
            echo "<p align='center'>";

            if($page > 1)
            	echo "<a href='index.php?page=".($page - 1)."'>上一頁</a>";

            for($i=1; $i<=$total_pages; $i++)
            {
                if($i == $page)
                	echo "$i";
                else
                	echo "<a href='index.php?page=$i'>$i</a>";
            }
            
            if($page < $total_pages)
            	echo "<a href='index.php?page=".($page + 1)."'>下一頁</a>";
            echo "</p>";

            //釋放空間
            mysql_free_result($RecBoard);
            mysql_close($link);
        ?>
        <form name="myForm" method="POST" action="post.php">
        	<table border="0" width="800" align="center" cellspacing="0">
        		<tr bgcolor="#OO84CA" align="center">
                    <td colspan="2">
                        <font color="#FFFFFF">請在此輸入新的討論</font>
                    </td>
        		</tr>
                <tr bgcolor="#D9F2FF">
                    <td width="15%">作者</td>
                    <td width="85%"><input name="author" type="text" size="50"></td>
                </tr>
                <tr bgcolor="#84D7FF">
                    <td width="15%">主題</td>
                    <td width="85%"><input name="subject" type="text" size="50"></td>
                </tr>
                <tr bgcolor="#D9F2FF">
                    <td width="15%">內容</td>
                    <td width="85%"><textarea name="content" cols="50" rows="5"></textarea></td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                    	<input type="button" value="張貼討論" onClick="check_data()">
                    	<input type="reset" value="重新輸入">
                    </td>
                </tr>
        	</table>
        </form>
    </body>
</html>
