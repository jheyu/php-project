<?php
    session_start();
?>
<html>
    <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>管理介面</title>
    </head>
    <body>
    	<a href="?logout=true">登出管理</a>
        <?php
            require_once("connMysql.php");

            //檢查是否經過登入          
            if(!isset($_SESSION["loginMember"])  || ($_SESSION["loginMember"]==""))
            {
            	echo "<script type='text/javascript'>location.href='http://localhost/phpboard/index.php'</script>;";
            }
            
            //執行登出動作
            if(isset($_GET["logout"])  && ($_GET["logout"]=="true"))
            {
            	unset($_SESSION["loginMember"]);
            	echo "<script type='text/javascript'>location.href='http://localhost/phpboard/index.php'</script>;";
            }

            //執行刪除動作
            if($row["id"] > 0)
            {
                echo "+++++++";
                $sql_query="DELETE FROM `board` WHERE `id`=".$_POST["id"];
                mysql_query($sql_query);
                echo "<script type='text/javascript'>location.href='http://localhost/phpboard/admin.php'</script>;";
            }
            
            /*
            function execute_sql($database, $sql, $link)
            {
            	$db_selected = mysql_select_db($database, $link)
            	    or die("Connect error<br><br>" . mysql_error($link));
                echo $db_selected;
                $seldb = @mysql_select_db("phpboard", $link);
                if(!$seldb)
    	            die("failed!!");
                else
    	            echo "";

            	$result = mysql_query($sql);

            	return $result;
            }
            */

            //指定每頁顯示幾筆紀錄
            $records_per_page = 5;

            //取得要顯示第幾頁的紀錄
            if(isset($_GET["page"]))
            	$page = $_GET["page"];
            else
            	$page = 1;

            //本頁開始記錄筆數 = 每頁紀錄筆數 * (頁數-1)
            $started_record = $records_per_page * ($page - 1);

            //資料連接
            $link = mysql_connect("localhost", "root", "67567500");

            //未加限制顯示筆數的SQL敘述句
            $sql = "SELECT * FROM `board` ORDER BY `time` DESC";
            
            /*
            $result = execute_sql("phpboard", $sql, $link);
            */

            //加上限制顯示筆數的SQL敘述句,由本頁開始記錄筆數,每頁顯示預設筆數
            $query_limit_RecBoard = $sql. " LIMIT ".$started_record.", ".$records_per_page;
            
            //以加上限制筆數的SQL敘述句查詢資料到$RecBoard中
            $RecBoard = mysql_query($query_limit_RecBoard);

            //以未加上限制顯示筆數的SQL敘述句查詢資料到$all_RecBoard中
            $all_RecBoard = mysql_query($sql);

            //計算總筆數
            $total_records = mysql_num_rows($all_RecBoard);

            //計算總頁數=(總筆數/每頁筆數)
            $total_pages = ceil($total_records / $records_per_page);
         
            //-----------------------------------有問題----------------------------------------
            //mysql_data_seek($result, $started_record);

            echo "<table width='800' align='center' cellspacing='3'>";
            //顯示紀錄
            $j = 1;
            while($row = mysql_fetch_assoc($RecBoard) and $j <= $records_per_page)
            {
                //輸出留言資訊
                echo "<td>作者: " . $row["author"];

                //每一筆留言加入修改與刪除連結
                echo "<a href='adminfix.php?id=".$row["id"]."'>[修改]</a>&nbsp";
                //echo "<a href='admindel.php?id=".$row["id"]."'>[刪除]</a>";
                echo "<a href='admin.php?id=".$row["id"]."'>[刪除]</a>";
                echo "<br>";
                echo "主題: " . $row["subject"] . "<br>";
                echo "時間: " . $row["time"] . "<hr>";
                echo $row["content"] . "</td></tr>";

                $j++;
            }
            echo "</table>";
            //echo $_GET["id"];

            //執行刪除動作
            if($_GET["id"] > 0)
            {
                $sql_query="DELETE FROM `board` WHERE `id`=".$_GET["id"];
                mysql_query($sql_query);
                echo "<script type='text/javascript'>location.href='http://localhost/phpboard/admin.php'</script>;";
            }

            //產生導覽列
            echo "<p align='center'>";

            if($page > 1)
            	echo "<a href='admin.php?page=".($page - 1)."'>上一頁</a>";

            for($i=1; $i<=$total_pages; $i++)
            {
                if($i == $page)
                	echo "$i";
                else
                	echo "<a href='admin.php?page=$i'>$i</a>";
            }
            
            if($page < $total_pages)
            	echo "<a href='admin.php?page=".($page + 1)."'>下一頁</a>";
            echo "</p>";

            //釋放空間
            mysql_free_result($RecBoard);
            mysql_close($link);
        ?>          
    </body>
</html>
