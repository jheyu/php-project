<?php
    session_start();
?>
<html>
    <head>
        <title>登入管理</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>
        <a href="http://localhost/phpboard/index.php">留言頁面</a>
        <?php
            if(!isset($_SESSION["loginMember"]) || ($_SESSION["loginMember"]==""))
            {
                if(isset($_POST["username"]) && isset($_POST["passwd"]))
                {
                    require_once("connMysql.php");

                    //選取儲存帳號密碼的資料表
                    $sql = "SELECT * FROM `admin`";
                    $result = mysql_query($sql);

                    //取出帳號密碼的值
                    $row_result = mysql_fetch_assoc($result);
                    $username = $row_result["username"];
                    $passwd = $row_result["passwd"];

                    //比對帳密，若登入成功則前往管理介面，否則就退回主畫面
                    if(($_POST["username"]==$username) && ($_POST["passwd"]==$passwd))
                    {
                        $_SESSION["loginMember"]=$username;
                        echo "<script type='text/javascript'>location.href='http://localhost/phpboard/admin.php'</script>;";
                    }
                    else
                    {
                    	//echo "<script type='text/javascript'>alert('"帳號或密碼錯誤!!"')</script>;";
                    	echo "<script type='text/javascript'>location.href='http://localhost/phpboard/index.php'</script>;";                    
                    }
                }
            }
            else
            {
            	//若已經有Session值則前往管理介面
            	echo "<script type='text/javascript'>location.href='http://localhost/phpboard/admin.php'</script>;";
            }
        ?>
        <form name="form1" method="POST" action="">
            <table border="0" align="center" cellpadding="4" cellspacing="0">
                <tr valign="top">
                    <td colspan="2" align="center">登入管理</td>
                </tr>
                <tr>
                    <td valign="middle">
                    	<p>管理帳號
                            <input type="text" name="username" id="username"> 
                    	</p>
                    	<p>管理密碼
                            <input type="password" name="passwd" id="passwd">
                    	</p>
                    	<p align="center">
                            <input type="submit" name="button" id="button" value="登入管理">
                            <input type="submit" name="button3" id="button3" value="回上一頁" onClick="window.history.back();">
                    	</p>
                    </td>    
                </tr>
            </table>
        </form>
    </body>
</html>