<?php
    session_start();
?>

<html>
    <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>管理介面</title>
    </head>
    <body>
        <?php
            require_once("connMysql.php");

            //檢查是否經過登入
            if(!isset($_SESSION["loginMember"])  || ($_SESSION["loginMember"]==""))
            {
            	echo "<script type='text/javascript'>location.href='http://localhost/phpboard/index.php'</script>;";
            }
            
            //執行登出動作
            if(isset($_GET["logout"])  && ($_GET["logout"]=="true"))
            {
            	unset($_SESSION["loginMember"]);
            	echo "<script type='text/javascript'>location.href='http://localhost/phpboard/index.php'</script>;";
            }
            
            //執行刪除動作
            if(isset($_POST["action"]) && ($_POST["action"]=="delete"))
            {
                $sql_query="DELETE FROM `board` WHERE `id`=".$_POST["id"];
                mysql_query($sql_query);
                echo "<script type='text/javascript'>location.href='http://localhost/phpboard/admin.php'</script>;";
            }
            $query_RecBoard = "SELECT * FROM `board` WHERE `id`=".$_GET["id"];
            $RecBoard = mysql_query($query_RecBoard);
            $row_RecBoard=mysql_fetch_assoc($RecBoard);
        ?>
        <form name="Form1" method="POST" action="">
            <table border="0" width="800" align="center" cellspacing="0">
                <tr valign="top">
                    <td colspan="2">
                        <font color="#FFFFFF" value=""></font>刪除留言板資料
                    </td>
                </tr>
                <tr bgcolor="#D9F2FF">
                    <td width="15%">作者</td>
                    <td width="85%"><input name="author" type="text" size="50"
                        value="<?php echo $row_RecBoard["author"]; ?>">
                    </td>
                </tr>
                <tr bgcolor="#84D7FF">
                    <td width="15%">主題</td>
                    <td width="85%"><input name="subject" type="text" size="50"
                         value="<?php echo $row_RecBoard["subject"]; ?>">
                    </td>
                </tr>
                <tr bgcolor="#D9F2FF">
                    <td width="15%">內容</td>
                    <td width="85%"><textarea name="content" cols="50" rows="5"><?php echo $row_RecBoard["content"]; ?></textarea>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input name="id" type="hidden" id="id"
                            value="<?php echo $row_RecBoard["id"]; ?>">
                        <input name="action" type="hidden" id="action" value="delete">
                        <input type="submit" name="button" id="button" value="刪除資料">
                        <input type="button" name="button3" id="button3" value="回上一頁"
                            onClick="window.history.back();">
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>    
