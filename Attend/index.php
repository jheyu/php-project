<?php
session_start();
error_reporting(0);
include_once("./conf.php");
echo "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>";

if($_POST['userid']){
	echo "檢測到已登入，正在轉入簽到退頁面";
	header("Location: sign.php");
	exit();
}
if(!$mode && !$_SESSION['admun_username']){

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="zh-TW">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>打卡系統 &rsaquo; 登入</title>
<link rel="stylesheet" id="login-css" href="./css/login.css" type="text/css" media="all" />

</head>
<body class="login">
<div id="login"><h1><a href="#上層網址" title="版權宣告">上層首頁</a></h1>

<form name="loginform" id="loginform" action='login_verify.php' method="post">
	<p>
		<label>帳號<br />
		<input name='userid' type='text' id='userid' class="input" value="" size="20" tabindex="10" /></label>
	</p>
	<p>
		<label>密碼<br />
		<input name='pw' type='password' id='pw' class="input" value="" size="20" tabindex="20" /></label>
	</p>
	<p class="submit">
		<input type="hidden" name="action" id="action" value="login">
		<input type="submit" name="submit" id="submit" class="button-primary" value="登入" tabindex="100" />
	</p>
</form>
<p id="nav">
<a href="signup.php" title="註冊帳號">沒有帳號？</a>
</p>
	</div>
</body>
</html>
<?php 
   }
?>