<?php
	header('Content-Type: application/json;charset=utf-8');

	include_once("DB_config.php");

	$hospital_p = $_POST['hospital'];
	$date = $_POST['date'];
	$jarray = array();//使用array儲存結果，再以json_encode回傳
									// 基隆             // 宜蘭         // 台北           // 桃園			  // 新竹    // 台中        // 彰化              // 雲林                           // 嘉義            // 台南      // 高雄                // 花蓮           
	$hospitalEnglishArray = array("Keelung_Chang_Gung","Lotung_Poh_Ai","National_Taiwan","Linkou_Chang_Gung","Ton_Yen","China_Medical","Changhua_Christian","National_Taiwan_Yun_Lin_Branch","Chia_Yi_Christian","Cheng_Kung","Kaohsiung_Chang_Gung","Hualien_Tzu_Chi");
	
	switch ($hospital_p)
	{
		case "基隆長庚紀念醫院暨情人湖院區":
		$hospital = $hospitalEnglishArray[0];
		break;  
		case "宜蘭羅東博愛醫院":
		$hospital = $hospitalEnglishArray[1];
		break;
		case "國立臺灣大學醫學院附設醫院":
		$hospital = $hospitalEnglishArray[2];
		break;  
		case "林口長庚紀念醫院":
		$hospital = $hospitalEnglishArray[3];
		break;  
		case "新竹東元綜合醫院":
		$hospital = $hospitalEnglishArray[4];
		break;  
		case "中國醫藥大學附設醫院":
		$hospital = $hospitalEnglishArray[5];
		break;  
		case "彰化基督教醫療財團法人彰化基督教醫院":
		$hospital = $hospitalEnglishArray[6];
		break;  
		case "國立臺灣大學醫學院附設醫院雲林分院":
		$hospital = $hospitalEnglishArray[7];	
		break;  
		case "戴德森醫療財團法人嘉義基督教醫院":
		$hospital = $hospitalEnglishArray[8];
		break;  
		case "國立成功大學醫學院附設醫院":
		$hospital = $hospitalEnglishArray[9];
		break;  
		case "高雄長庚紀念醫院":
		$hospital = $hospitalEnglishArray[10];
		break;  
		case "佛教慈濟醫療財團法人花蓮慈濟醫院":
		$hospital = $hospitalEnglishArray[11];
		break; 
		default:
		$hospital = "";
		break;
	}
	
	if($hospital != null && $date != null){
		
		// 醫院急診人數資料
		$queryHospitalData = "select * from ".$hospital." where DATE(curdate)='".$date."'";
        $resultHospitalData = mysql_query($queryHospitalData, $conn);
		$countHospitalData = mysql_num_rows($resultHospitalData);
		
		$jarray[0] = ["急診時間","急診人數"];
		for($i=0;$i<$countHospitalData;$i++){
			$row=mysql_fetch_assoc($resultHospitalData);
			$jarray[$i+1] = [$row['curdate'],(int)$row['visits']];
		
		}
		
	}else{
		echo 0;
		return;
	}
	mysql_close($conn);
	echo json_encode($jarray);
	return;
?>