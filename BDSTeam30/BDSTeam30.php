<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
    header("Content-Type:text/html; charset=utf-8");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
        <title>WaterDrop by TEMPLATED</title>
        <meta content="" name="keywords" />
        <meta content="" name="description" />
        <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700" rel="stylesheet" type="text/css" />
        <link href="default.css" media="all" rel="stylesheet" type="text/css" />
		<link type="text/css" href="jquery-ui-1.11.4.custom/jquery-ui.min.css" rel="stylesheet" />  
		<script type="text/javascript" src="jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<style type="text/css">
		.myButton {
		-moz-box-shadow:inset 0px 1px 3px 0px #91b8b3;
		-webkit-box-shadow:inset 0px 1px 3px 0px #91b8b3;
		box-shadow:inset 0px 1px 3px 0px #91b8b3;
		background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #768d87), color-stop(1, #6c7c7c));
		background:-moz-linear-gradient(top, #768d87 5%, #6c7c7c 100%);
		background:-webkit-linear-gradient(top, #768d87 5%, #6c7c7c 100%);
		background:-o-linear-gradient(top, #768d87 5%, #6c7c7c 100%);
		background:-ms-linear-gradient(top, #768d87 5%, #6c7c7c 100%);
		background:linear-gradient(to bottom, #768d87 5%, #6c7c7c 100%);
		filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#768d87', endColorstr='#6c7c7c',GradientType=0);
		background-color:#768d87;
		-moz-border-radius:5px;
		-webkit-border-radius:5px;
		border-radius:5px;
		border:1px solid #566963;
		display:inline-block;
		cursor:pointer;
		color:#ffffff;
		font-family:Arial;
		font-size:15px;
		font-weight:bold;
		padding:9px 15px;
		text-decoration:none;
		text-shadow:0px -1px 0px #2b665e;
		}
		.myButton:hover {
		background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #6c7c7c), color-stop(1, #768d87));
		background:-moz-linear-gradient(top, #6c7c7c 5%, #768d87 100%);
		background:-webkit-linear-gradient(top, #6c7c7c 5%, #768d87 100%);
		background:-o-linear-gradient(top, #6c7c7c 5%, #768d87 100%);
		background:-ms-linear-gradient(top, #6c7c7c 5%, #768d87 100%);
		background:linear-gradient(to bottom, #6c7c7c 5%, #768d87 100%);
		filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#6c7c7c', endColorstr='#768d87',GradientType=0);
		background-color:#6c7c7c;
		}
		.myButton:active {
		position:relative;
		top:1px;
		}
		</style>
        <script type="text/javascript">
           google.load("visualization", "1", {packages:["corechart"]});
		   google.load('visualization', '1', {packages: ['corechart', 'bar']});
        </script>
		<script type="text/javascript">
							$(document).ready(function(){  
								var hospitalData = [];
								var rainData = [];
								var result = [];
								var maxRain = new Array(24); // 存一小時雨量最大值
								var maxHospital = new Array(24); // 存一小時急診人數最大值
                                $('#AreaLocation').change(function(){ 
								    //$('#HospitalLocation').append("<option value="">"+123+"</option>");
								    //更動第一層時第二層清空
								    $('#HospitalLocation').empty().append("<option value=' '>請選擇</option>"); 
								    var str = $('#AreaLocation option:selected').val();
								    var str1 = str.substr(0,3);
								    var str2 = str.substr(3,3);
								    var i=0;
									
								    $.ajax({  
									   type:"POST",  
									   url:"update.php",  
									   data:{lv1:str1,lv2:str2},  
									   datatype:"json",  
									   success: function(result){  
									
									        //當第一層回到預設值時，第二層回到預設位置
									        if(result == ""){  
										       $('#HospitalLocation').val($('option:first').val());//pseudo selector   
									        }
									        //依據第一層回傳的值去改變第二層的內容
									        while(i<result.length){  
										       $("#HospitalLocation").append("<option value='"+result[i]+"'>"+result[i]+"</option>");  
										       i++;  
									        }
									    },  
									    error:  function(error){  
										   alert("error");  
									    }  
								    });
								});  
								
								$('#HospitalLocation').change(function(){
                                    var area = $('#AreaLocation option:selected').val();
									var hospital = $('#HospitalLocation option:selected').val();
									var county = area.substr(0,3);
									var townShip = area.substr(3,3);
					
									var date = new Date();
									//console.log(date);
									var currentDate = date.getFullYear().toString() + "/";		
									if((date.getMonth()+1) < 10)
										currentDate = currentDate + "0" + (date.getMonth()+1).toString() + "/";
									else
										currentDate = currentDate + date.getMonth().toString() + "/";
									if(date.getDate() < 10)
										currentDate = currentDate + "0" + date.getDate().toString();
									else
										currentDate = currentDate + date.getDate().toString();
	
									//console.log(county);
									//console.log(townShip);
									//console.log(hospital);
									//console.log(currentDate);
									if(hospital != " ")
									{
									$.ajax({  
                                        type:"POST",  
                                        url:"update2.php",  
                                        data:{county:county,townShip:townShip,date:currentDate},  
                                        datatype:"json",  
                                        success: function(rain){ // 取得雨量時間跟雨量 
											//console.log(rain);
											//var jsonArray = JSON.parse(rain);
                                    		//console.log( jsonArray );
											
											rainData = rain;
											if(rain.length != 1)
											{
												for(var i=0;i<maxRain.length;i++)
													maxRain[i] = 0;
												var max = 0; // 取1小時內最大值
												var count = 1; // 資料筆數
												var hour = 0;// 取得小時
												var split1 = new Array();
												var split2 = new Array();
												
												for(var i=0;i<maxRain.length;i++)
												{
													for(;count<rain.length;count++)
													{
														if(rain[count][0] != null)
														{
															split1 = rain[count][0].split(" ");
															split2 = split1[1].split(":");
															hour = parseInt(split2[0]);
														}
														//console.log(hour);
														
														if( hour == i && rain[count][0] != null && rain[count][1] > max)
															max = rain[count][1];
															//console.log(max);
														else if(hour > i)
															break;
													}
													maxRain[i] = max;
													max = 0;													
												}
												
												//console.log(maxRain);
												// column graph
												var data = new google.visualization.DataTable();
												data.addColumn('timeofday', 'Time of Day');
												data.addColumn('number', '降雨量');
												
												data.addRows([
												[{v: [0, 0, 0], f: '0 am'}, maxRain[0] ],
												[{v: [1, 0, 0], f: '1 am'}, maxRain[1] ],
												[{v: [2, 0, 0], f: '2 am'}, maxRain[2] ],
												[{v: [3, 0, 0], f: '3 am'}, maxRain[3] ],
												[{v: [4, 0, 0], f: '4 am'}, maxRain[4] ],
												[{v: [5, 0, 0], f: '5 am'}, maxRain[5] ],
												[{v: [6, 0, 0], f: '6 am'}, maxRain[6] ],
												[{v: [7, 0, 0], f: '7 am'}, maxRain[7] ],
												[{v: [8, 0, 0], f: '8 am'}, maxRain[8] ],
												[{v: [9, 0, 0], f: '9 am'}, maxRain[9] ],
												[{v: [10, 0, 0], f:'10 am'}, maxRain[10] ],
												[{v: [11, 0, 0], f: '11 am'}, maxRain[11] ],
												[{v: [12, 0, 0], f: '12 pm'}, maxRain[12] ],
												[{v: [13, 0, 0], f: '13 pm'}, maxRain[13] ],
												[{v: [14, 0, 0], f: '14 pm'}, maxRain[14] ],
												[{v: [15, 0, 0], f: '15 pm'}, maxRain[15] ],
												[{v: [16, 0, 0], f: '16 pm'}, maxRain[16] ],
												[{v: [17, 0, 0], f: '17 pm'}, maxRain[17] ],
												[{v: [18, 0, 0], f: '18 pm'}, maxRain[18] ],
												[{v: [19, 0, 0], f: '19 pm'}, maxRain[19] ],
												[{v: [20, 0, 0], f: '20 pm'}, maxRain[20] ],
												[{v: [21, 0, 0], f: '21 pm'}, maxRain[21] ],
												[{v: [22, 0, 0], f: '22 pm'}, maxRain[22] ],
												[{v: [23, 0, 0], f: '23 pm'}, maxRain[23] ]
												]);
												
												var options = {
													title: '降雨量及雨量時間',
													hAxis: {
														title: 'Time of Day',
														format: 'h:mm a',
														viewWindow: {
														min: [-1, 30, 0],
														max: [24, 30, 0]
														}
													},
													vAxis: {
														title: '降雨量'
													}
												};
	
												var chart = new google.visualization.ColumnChart(
												document.getElementById('chart_div'));
												chart.draw(data, options);
											}
											else
											{
												alert("查無雨量資料");
											}

                                        },  
                                        error: function(xhr, ajaxOptions, thrownError)
                                    	{
                                    		console.log("readyState: " + xhr.readyState);
                                    		console.log("responseText: "+ xhr.responseText);
                                    		console.log("status: " + xhr.status);
                                    		console.log("text status: " + ajaxOptions);
                                    		console.log("error: " + thrownError);
                                     	}
                                        
                                    });
									
									$.ajax({  
										type:"POST",  
										url:"update3.php",  
										data:{hospital:hospital,date:currentDate},  
										datatype:"json",  
										success: function(hospital_r){ // 取得急診時間及急診人數 
										
											//var jsonArray = JSON.parse(hospital_r);
                                    		//console.log( jsonArray );
											hospitalData = hospital_r;
											if(hospital_r.length != 1)
											{
												/*
												console.log(hospital_r);
												for(var i=0;i<maxHospital.length;i++)
													maxHospital[i] = 0;
												var max = 0; // 取1小時內最大值
												var hourIndex = 0;
												var count = 1; // 資料筆數
												var hour = 0;// 取得小時
												var split1 = new Array();
												var split2 = new Array();
												 
												if(hospital_r[count][0] != null)
												{
													split1 = hospital_r[count][0].split(" ");
													split2 = split1[1].split(":");
													hour = parseInt(split2[0]);
												}
												console.log(hour);
												if(hour == 12)
												{
													for(;count<hospital_r.length;count++)
													{
														if(hospital_r[count][0] != null)
														{
															split1 = hospital_r[count][0].split(" ");
															split2 = split1[1].split(":");
															hour = parseInt(split2[0]);
														}
														//console.log(hour);
														console.log(hospital_r[count][1]);
														if( hour == i && hospital_r[count][0] != null && hospital_r[count][1] > max)
														{
															max = hospital_r[count][1];
															console.log(max);
														}
														else if(hour != 12)
															break;
													}
													maxHospital[hourIndex] = max;
													max = 0;
												}
												console.log(hour);
												for(var i=1;i<=12;i++)
												{
													for(;count<hospital_r.length;count++)
													{
														if(hospital_r[count][0] != null)
														{
															split1 = hospital_r[count][0].split(" ");
															split2 = split1[1].split(":");
															hour = parseInt(split2[0]);
														}
														//console.log(hour);
														console.log(hospital_r[count][1]);
														if( hour == i && hospital_r[count][0] != null && hospital_r[count][1] > max)
														{
															max = hospital_r[count][1];
															console.log(max);
														}
														else if(hour > i)
															break;
													}
													maxHospital[++hourIndex] = max;
													max = 0;
												}
												console.log(hour);
												for(var i=1;i<12;i++)
												{
													for(;count<hospital_r.length;count++)
													{
														if(hospital_r[count][0] != null)
														{
															split1 = hospital_r[count][0].split(" ");
															split2 = split1[1].split(":");
															hour = parseInt(split2[0]);
														}
														//console.log(hour);
														console.log(hospital_r[count][1]);
														if( hour == i && hospital_r[count][0] != null && hospital_r[count][1] > max)
														{
															max = hospital_r[count][1];
															//console.log(max);
														}
														else if(hour > i)
															break;
													}
													maxHospital[++hourIndex] = max;
													max = 0;
												}
												console.log(maxHospital);
												*/
												// Line graph
												var data = google.visualization.arrayToDataTable(hospital_r);

												var options = {
													title: '急診人數及急診更新時間',
													curveType: 'function',
													legend: { position: 'bottom' },
													hAxis: {
															title: 'Time of Day'
													},
													vAxis: {
														title: '急診人數'
													}
												};

												var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));

												chart.draw(data, options);
			
											}	
											else
											{
												alert("查無急診資料");
											}
										}, 
										error:function(xhr, ajaxOptions, thrownError)
                                    	{
                                    		console.log("readyState: " + xhr.readyState);
                                    		console.log("responseText: "+ xhr.responseText);
                                    		console.log("status: " + xhr.status);
                                    		console.log("text status: " + ajaxOptions);
                                    		console.log("error: " + thrownError);
                                     	} 
									});
									}
                                });
									
								
                                $('#datepicker').change(function(){
                                    var area = $('#AreaLocation option:selected').val();
                                    var hospital = $('#HospitalLocation option:selected').val();
                                    var date = $('#datepicker').val();
                                    var county = area.substr(0,3);
                                    var townShip = area.substr(3,3);
                                    var i = 0;
									//console.log(date);
									if(hospital != " ")
									{
                                    $.ajax({  
                                       type:"POST",  
                                       url:"update2.php",  
                                       data:{county:county,townShip:townShip,date:date},  
                                       datatype:"json",  
                                       success: function(rain){ // 取得雨量時間跟雨量 
											rainData = rain;
											if(rain.length != 1)
											{
												for(var i=0;i<maxRain.length;i++)
													maxRain[i] = 0;
												var max = 0; // 取1小時內最大值
												var count = 1; // 資料筆數
												var hour = 0;// 取得小時
												var split1 = new Array();
												var split2 = new Array();
												
												for(var i=0;i<maxRain.length;i++)
												{
													for(;count<rain.length;count++)
													{
														if(rain[count][0] != null)
														{
															split1 = rain[count][0].split(" ");
															split2 = split1[1].split(":");
															hour = parseInt(split2[0]);
														}
														//console.log(hour);
														
														if( hour == i && rain[count][0] != null && rain[count][1] > max)
															max = rain[count][1];
															//console.log(max);
														else if(hour > i)
															break;
													}
													maxRain[i] = max;
													max = 0;													
												}
												
												//console.log(maxRain);
												// column graph
												var data = new google.visualization.DataTable();
												data.addColumn('timeofday', 'Time of Day');
												data.addColumn('number', '降雨量');
												
												data.addRows([
												[{v: [0, 0, 0], f: '0 am'}, maxRain[0] ],
												[{v: [1, 0, 0], f: '1 am'}, maxRain[1] ],
												[{v: [2, 0, 0], f: '2 am'}, maxRain[2] ],
												[{v: [3, 0, 0], f: '3 am'}, maxRain[3] ],
												[{v: [4, 0, 0], f: '4 am'}, maxRain[4] ],
												[{v: [5, 0, 0], f: '5 am'}, maxRain[5] ],
												[{v: [6, 0, 0], f: '6 am'}, maxRain[6] ],
												[{v: [7, 0, 0], f: '7 am'}, maxRain[7] ],
												[{v: [8, 0, 0], f: '8 am'}, maxRain[8] ],
												[{v: [9, 0, 0], f: '9 am'}, maxRain[9] ],
												[{v: [10, 0, 0], f:'10 am'}, maxRain[10] ],
												[{v: [11, 0, 0], f: '11 am'}, maxRain[11] ],
												[{v: [12, 0, 0], f: '12 pm'}, maxRain[12] ],
												[{v: [13, 0, 0], f: '13 pm'}, maxRain[13] ],
												[{v: [14, 0, 0], f: '14 pm'}, maxRain[14] ],
												[{v: [15, 0, 0], f: '15 pm'}, maxRain[15] ],
												[{v: [16, 0, 0], f: '16 pm'}, maxRain[16] ],
												[{v: [17, 0, 0], f: '17 pm'}, maxRain[17] ],
												[{v: [18, 0, 0], f: '18 pm'}, maxRain[18] ],
												[{v: [19, 0, 0], f: '19 pm'}, maxRain[19] ],
												[{v: [20, 0, 0], f: '20 pm'}, maxRain[20] ],
												[{v: [21, 0, 0], f: '21 pm'}, maxRain[21] ],
												[{v: [22, 0, 0], f: '22 pm'}, maxRain[22] ],
												[{v: [23, 0, 0], f: '23 pm'}, maxRain[23] ]
												]);
												
												var options = {
													title: '降雨量及雨量時間',
													hAxis: {
														title: 'Time of Day',
														format: 'h:mm a',
														viewWindow: {
														min: [-1, 30, 0],
														max: [24, 30, 0]
														}
													},
													vAxis: {
														title: '降雨量'
													}
												};
	
												var chart = new google.visualization.ColumnChart(
												document.getElementById('chart_div'));
												chart.draw(data, options);
											}
											else
											{
												alert("查無雨量資料");
											}

                                        },  
                                        error:  function(error){  
                                           alert("error");  
                                        }  
                                        
                                    });
									
									$.ajax({  
										type:"POST",  
										url:"update3.php",  
										data:{hospital:hospital,date:date},  
										datatype:"json",  
										success: function(hospital_r){ // 取得急診時間及急診人數
											hospitalData = hospital_r;
											if(hospital_r.length != 1)
											{	
												/*
												console.log(hospital_r);
												for(var i=0;i<maxHospital.length;i++)
													maxHospital[i] = 0;
												var max = 0; // 取1小時內最大值
												var hourIndex = 0;
												var count = 1; // 資料筆數
												var hour = 0;// 取得小時
												var split1 = new Array();
												var split2 = new Array();
												 
												//for(var i=0;i<maxHospital.length;i++)
												//{
												if(hospital_r[count][0] != null)
												{
													split1 = hospital_r[count][0].split(" ");
													split2 = split1[1].split(":");
													hour = parseInt(split2[0]);
												}
												console.log(hour);
												if(hour == 12)
												{
													for(;count<hospital_r.length;count++)
													{
														if(hospital_r[count][0] != null)
														{
															split1 = hospital_r[count][0].split(" ");
															split2 = split1[1].split(":");
															hour = parseInt(split2[0]);
														}
														//console.log(hour);
														console.log(hospital_r[count][1]);
														if( hour == i && hospital_r[count][0] != null && hospital_r[count][1] > max)
														{
															max = hospital_r[count][1];
															console.log(max);
														}
														else if(hour != 12)
															break;
													}
													maxHospital[hourIndex] = max;
													max = 0;
												}
												console.log(hour);
												for(var i=1;i<=12;i++)
												{
													for(;count<hospital_r.length;count++)
													{
														if(hospital_r[count][0] != null)
														{
															split1 = hospital_r[count][0].split(" ");
															split2 = split1[1].split(":");
															hour = parseInt(split2[0]);
														}
														//console.log(hour);
														console.log(hospital_r[count][1]);
														if( hour == i && hospital_r[count][0] != null && hospital_r[count][1] > max)
														{
															max = hospital_r[count][1];
															console.log(max);
														}
														else if(hour > i)
															break;
													}
													maxHospital[++hourIndex] = max;
													max = 0;
												}
												console.log(hour);
												for(var i=1;i<12;i++)
												{
													for(;count<hospital_r.length;count++)
													{
														if(hospital_r[count][0] != null)
														{
															split1 = hospital_r[count][0].split(" ");
															split2 = split1[1].split(":");
															hour = parseInt(split2[0]);
														}
														//console.log(hour);
														console.log(hospital_r[count][1]);
														if( hour == i && hospital_r[count][0] != null && hospital_r[count][1] > max)
														{
															max = hospital_r[count][1];
															//console.log(max);
														}
														else if(hour > i)
															break;
													}
													maxHospital[++hourIndex] = max;
													max = 0;
												}
												//}
												console.log(hour);
												console.log(maxHospital);
												*/
												// Line graph
												var data = google.visualization.arrayToDataTable(hospital_r);

												var options = {
													title: '急診人數及急診更新時間',
													curveType: 'function',
													legend: { position: 'bottom' },
													hAxis: {
															title: 'Time of Day'
													},
													vAxis: {
														title: '急診人數'
													}
												};

												var chart = new google.visualization.LineChart(document.getElementById('chart_div1'));

												chart.draw(data, options);
			
											}
											else
											{
												alert("查無急診資料");
											}
										}, 
										error:  function(error){  
											alert("error");  
										}  
									
									});
									
									}
                                });
								/*
								$('.myButton').click(function(){

									result.push(["降雨量","急診人數"]);
									for(var i=0;i<24;i++)
										result.push([maxRain[i],maxHospital[i]]);
									console.log(maxRain);
									console.log(maxHospital);
									var data = new google.visualization.arrayToDataTable(result);
									var options = {
										title: '急診人數 vs. 降雨量',
										hAxis: {title: '降雨量', minValue: 0, maxValue: 50},
										vAxis: {title: '急診人數', minValue: 0, maxValue: 20},
										legend: 'none'
									};
										
									var chart = new google.visualization.ScatterChart(document.getElementById('chart_div2'));
									chart.draw(data, options);
									result.length = 0;
								});
								*/
								$(function() {  
									$("#datepicker").datepicker({dateFormat: 'yy/mm/dd', showOn: 'both', showOtherMonths: true, 
										showWeeks: true, firstDay: 1, changeFirstDay: false,
										buttonImageOnly: true, buttonImage: 'calendar.gif'});
								});
							});  
		</script> 

        <!--[if IE 6]>
<link href="default_ie6.css" rel="stylesheet" type="text/css" />
<![endif]-->
    </head>
    <body>
        <div id="wrapper">
            <div id="header">
                <div id="logo">
                    <h1>
                        急診人數與雨量分析</h1>
                    <p>
                        Design by 劉哲宇 and 李岳庭</p>
                </div>
            </div>
            <div id="banner">
                <img alt="" height="400" src="images/header-image-bg.jpg" width="1200" /></div>
            <div id="three-columns">
                <div class="content">
                    <div id="column1">
                        <h2>
                            期末專題簡介</h2>
                        <ul class="list-style2">
                            <li class="first">
                                <a href="#introduce">研究動機及目的</a></li>
                            <li>
                                <a href="#skill">技術架構</a></li>
                            <li>
                                <a href="#result">成果展示</a></li>
                        </ul>
                        <a class="link-style" href="#">Read More</a>
                        <p>
                            &nbsp;</p>
                    </div>
                    <div id="column2">
                        <h2>急診人數及雨量分析圖</h2>
                        <p><div id="chart_div" style="width: 430px; height: 300px;"></div></p>
						<p><div id="chart_div1" style="width: 430px; height: 300px;"></div></p>
						<p><div id="chart_div2" style="width: 430px; height: 300px;"></div></p>
                        <!--p>文字描述</p>
                        <p>&nbsp;</p>
                        <h2>急診人數圖</h2>
                        <p><img alt="" height="300" src="images/pics01.jpg" width="430" /></p>
                        <!--p>文字描述</p-->
                        <p>&nbsp;</p>
                        <a name="introduce"> 專題簡介: </a>
						<br>
                        <p>
                                透過開放政府的雨量資料，和全臺灣某些地區的
							醫院，不同格式的急診即時就醫人數資料，利用網頁
							來呈現、比較、分析各地區醫院的急診人數和當地的
							雨量大小，由實驗結果來探討雨量是否會間接影響到
							，各家醫院急診的人數增加?還是人數會有所減少?與
							我們所預期的結果是否相同。
						<br>
							    現今交通四通八達，相對的交通會出現一些意外
							事故，想去調查說除了天氣好壞無關的諸類原因外，
							了解醫院人數在哪段時間可能因天氣變壞導致看診人
							數爆增，去探討雨量的多寡是否導致意外，而紛紛送
							往醫院。所以為了能夠知道雨量大小跟醫院看急診的
							人是否有相關，而想去探討的一項研究，雖然可能會
							發現結果與預期會有所不同，但還是值得去觀察分析
							。</p>
                        <a name="skill"> 技術架構: </a>
						<br>
						<img src="123.gif" width=600 height=400>
						<img src="1234.gif" width=600 height=400>
                        <p>
                            &nbsp;</p>
                        <a name="result"> 成果展示: </a>
						<br>
                        <p>
                            &nbsp;</p>
                    </div>
                    <div id="column3">
						<p>選擇地區</p>
                        <form name="form1">
                            <select name="area" id="AreaLocation">
							<option value=" ">請選擇</option> 
                            <?php
								include_once("DB_config.php");

								$str = "select * from hospital_info";
								$result = mysql_query($str,$conn);
								//$row = mysql_fetch_array($result);
								$count = mysql_num_rows($result); 
								
                                for($i=0;$i<$count;$i++){
									$row=mysql_fetch_assoc($result);
                            ?>
                            <?php
							echo '<option value ="'.$row['area'].$row['localarea'].'">'.$row['area']." ".$row['localarea'].'</option>';
							?>
							<?php
								}
								mysql_close($conn);
							?>
                            </select>
                        </form>
						<p>選擇醫院</p>
                        <form>
                            <select id="HospitalLocation">
							<option value="">請選擇</option>  
							</select>
                        </form>
                        <p>選擇時間</p>
						<input type="text" id="datepicker"/>
                    </div>
                </div>
            </div>
        </div>
        <div id="footer">
            <p>
                &copy; Untitled. All rights reserved. Design by <a href="http://templated.co" rel="nofollow">TEMPLATED</a>. Photos by <a href="http://fotogrph.com/">Fotogrph</a></p>
        </div>
        <p>
            &nbsp;</p>
    </body>
</html>

