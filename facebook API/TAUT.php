<html>
<head>
<title>WebSpeaks.in | Upload images to Facebook</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
<?php
require_once 'src/facebook.php';

$facebook = new Facebook(array(
 'appId'  => '575425439136203',
 'secret' => '7fcb0d0a43dc4bfc6d25d11317110d78',
 'fileUpload' => true,
 'allowSignedRequest' => false
));

$actions = $_REQUEST["actions"];


// Get User ID
$user = $facebook->getUser();

// Login or logout url will be needed depending on current user state.
if ($user) {
  $logoutUrl = $facebook->getLogoutUrl();
} else {
   $statusUrl = $facebook->getLoginStatusUrl();
   //$loginUrl = $facebook->getLoginUrl();
   $loginUrl = $facebook->getLoginUrl(array('scope' => 'read_stream, publish_stream, photo_upload, user_photos, manage_pages'));
   echo "<script>top.location.href = '" . $loginUrl . "';</script>";
}

if ($user) {
    $access_token = $facebook->getAccessToken();       
    $facebook->setFileUploadSupport(true);
    $params = array('access_token' => $access_token);
}

//The id of the fanpage
$fanpage = '303771766330648';

//Replace arvind07 with your Facebook ID
$accounts = $facebook->api('/'.$user.'/accounts', 'GET', $params);
/*
//Replace arvind07 with your Facebook ID
$accounts = $facebook->api('/100000104561158/accounts', 'GET', $params);
*/

$data = $accounts['data'];
foreach($accounts['data'] as $account) {
if( $account['id'] == $fanpage || $account['name'] == $fanpage ){
      $fanpage_token = $account['access_token'];
  }
}

//The id of the album
$album_id = '303912136316611'; //已存在的相簿id

$valid_files = array('image/jpeg', 'image/png', 'image/gif');

if(isset($_FILES) && !empty($_FILES)){
   if( !in_array($_FILES['pic']['type'], $valid_files ) ){
          echo 'Only jpg, png and gif image types are supported!';
 }else{
  #Upload photo here
  $img = realpath($_FILES["pic"]["tmp_name"]);
 
  $args = array(
   'message' => 'This photo was uploaded via WebSpeaks.in',
   'image' => '@' . $img,
   'aid' => $album_id,
   'no_story' => 0,
   'access_token' => $fanpage_token
  );

  echo 'album_id : '.$album_id;
  $photo = $facebook->api($album_id . '/photos?access_token='.$access_token, 'post', $args);
  echo "photoid".$photo;

  if( is_array( $photo ) && !empty( $photo['id'] ) ){
   echo '<p><a target="_blank" href="http://www.facebook.com/photo.php?fbid='.$photo['id'].'">Click here to watch this photo on Facebook.</a></p>';
  }
 }
}

?>
<!-- Form for uploading the photo -->
<div class="main">
  <p>Select a photo to upload on Facebook Fan Page</p>
  <form method="post" action="TAUT.php" enctype="multipart/form-data">
    <input type="hidden" name="actions" value="postphoto">
    Select the image: 
    <input name="pic" id="pic" type="file"><br/><br/>
    <input type="submit" value="Upload"><br/>
  </form>
</div>
</body>
</html>
