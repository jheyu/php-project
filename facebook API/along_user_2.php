<?php
require 'src/facebook.php';

// Create our Application instance (replace this with your appId and secret).
$facebook = new Facebook(array(
    'appId'  => '575425439136203',
    'secret' => '7fcb0d0a43dc4bfc6d25d11317110d78',
    'fileUpload' => true,
    'allowSignedRequest' => false // optional but should be set to false for non-canvas apps
    //'cookie' => true
));

//Get User ID
$user = $facebook->getUser();

// Login or logout url will be needed depending on current user state.
if($user) {
    $logoutUrl = $facebook->getLogoutUrl();
}else {
    $statusUrl = $facebook->getLoginStatusUrl();
    //$loginUrl = $facebook->getLoginUrl();
    $loginUrl = $facebook->getLoginUrl(array('scope' => 'read_stream, publish_stream, photo_upload, user_photos, manage_pages'));
    echo "<script>top.location.href = '" . $loginUrl . "';</script>";
}

/*
if(is_null($facebook->getUser()))
{
    $dialog_url = "http://www.facebook.com/dialog/oauth?"
            . "client_id=" .  $app_id 
            . "&redirect_uri=" . urlencode( $post_login_url)
            . "&scope=publish_stream";
    echo("<script>top.location.href='" . $dialog_url . "'</script>");
}
*/

$actions = $_REQUEST['actions'];

if ($user) {

    if($actions == 'postphoto') {

        //Get Access_token
    	$access_token = $facebook->getAccessToken(); 

        //allow uploads
	    $facebook->setFileUploadSupport(true);

        $valid_files = array('image/jpeg', 'image/png', 'image/gif');
        
        if(isset($_FILES) && !empty($_FILES)) {
					
				if(!in_array($_FILES['pic']['type'], $valid_files)){
				    echo 'Only jpg, png and gif image types are supported!';
			    }else{
			    	//Upload photo here
                    $img = realpath($_FILES["pic"]["tmp_name"]);
	   
	                //add a status message
	                $args = array(
			            'message' => $_REQUEST['msg'],
			            'image' => '@' . $img,
			            //'aid' => $album_id,
			            //'no_story' => 0,
			            //'access_token' => $fanpage_token
	                );
						
	                $photo = $facebook->api('me/photos', 'post', $args);
	                echo '<p><a target="_blank" href="http://www.facebook.com/photo.php?fbid='.$photo['id'].'">Click here to watch this photo on Facebook.</a></p>';
                }
        }
    }
}

?>
<form enctype="multipart/form-data" action="<?php echo $graph_url ?>" method="POST">
	<input type="hidden" name="actions" value="postphoto">    
    Please choose a photo:
    <input name="pic" id="pic" type="file"><br/><br/>
    Please input sentence:
    <input name="msg" id="msg" type="text"><br/><br/>
    <input type="submit"  value="Upload"/><br/>
</form>
