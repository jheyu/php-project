<?php
require 'src/facebook.php';

// Create our Application instance (replace this with your appId and secret).
$facebook = new Facebook(array(
    'appId'  => '575425439136203',
    'secret' => '7fcb0d0a43dc4bfc6d25d11317110d78',
    'fileUpload' => true,
    'allowSignedRequest' => false // optional but should be set to false for non-canvas apps
));

//$post_login_url = 'http://www.moveinpocket.com/demo/saboten/along_user.php';    
$code = $_REQUEST['code'];
$actions = $_REQUEST['actions'];

if($actions == 'postphoto'){
    //Obtain the access_token with publish_stream permission 
    if(empty($code)){
        $dialog_url = "http://www.facebook.com/dialog/oauth?"
            . "client_id=" .  $app_id 
            . "&redirect_uri=" . urlencode( $post_login_url)
            . "&scope=publish_stream";
        echo("<script>top.location.href='" . $dialog_url . "'</script>");
    }else{
    	$token_url="https://graph.facebook.com/oauth/access_token?"
            . "client_id=" . $app_id 
            . "&redirect_uri=" . urlencode( $post_login_url)
            . "&client_secret=" . $app_secret
            . "&code=" . $code;
          
        $response = file_get_contents($token_url);
        $params = null;
        parse_str($response, $params);
        $access_token = $params['access_token'];

        // Show photo upload form to user and post to the Graph URL
        $graph_url= "https://graph.facebook.com/me/photos?" . "access_token=" .$access_token;
    }
}

?>
<form enctype="multipart/form-data" action="<?php echo $graph_url ?>" method="POST">
	<input type="hidden" name="actions" value="postphoto">    
    Please choose a photo:
    <input name="pic" id="pic" type="file"><br/><br/>
    Please input sentence:
    <input name="msg" id="msg" type="text"><br/><br/>
    <input type="submit"  value="Upload"/><br/>
</form>