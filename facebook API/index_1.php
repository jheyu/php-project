<?php
require_once 'library/facebook.php';


$facebook = new Facebook(array(
 'appId'  => '294175142205',
 'secret' => '87df9b29d3dffaa24fd6036d75c1f668',
 'fileUpload' => true
));
?>
<html>
<head>
<title>WebSpeaks.in | Create album and upload photos to Facebook Fan Page</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<?php
//It can be found at https://developers.facebook.com/tools/access_token/
$access_token = 'AAAAARH40cT0BANFh5V7Cn8dlXciZCbonSmGQZCeX3kUKIx2OMmvrqKUsHIBBj6bYi2vV25lMj6ZA5UDOaJpAxJXBEMOjZAcZD';

$params = array('access_token' => $access_token);

//The id of the fanpage
$fanpage = '353686361320061';

/*
 * Go to https://developers.facebook.com/tools/explorer?method=GET&path=me%2Faccounts
 * Enter https://graph.facebook.com/me/accounts in the second text box
 * Click on submit
 * Json data will be returned
 * From that select the access token of your Fan page
*/
$page_access_token = 'AAACEdEose0cBANZASZChz2ygVvPvdGaZB1g1CD7FUqGf08XgGr4byZAPITZAAtTAFsq8HPMwsM7rGpIhZAXzSjxVCBU9AZCZAGimaRfCueeLTuHuj2OqHrm7gsgNlpRHrbwZD';
$facebook->setAccessToken($page_access_token);

$facebook->setFileUploadSupport(true);

//Create an album
$album_details = array(
        'message'=> 'Test album',
        'name'=> 'Album name'.date('Y-m-d H:i:s') //should be unique each time
);
$album = $facebook->api('/'.$fanpage.'/albums', 'post', $album_details);

//The id of the album
$album_id =$album['id'];

//Replace arvind07 with your Facebook ID
$accounts = $facebook->api('/arvind07/accounts', 'GET', $params);

foreach($accounts['data'] as $account) {
	if( $account['id'] == $fanpage || $account['name'] == $fanpage ){
		$fanpage_token = $account['access_token'];
	}
}

$valid_files = array('image/jpeg', 'image/png', 'image/gif');

if(isset($_FILES) && !empty($_FILES)){
	if( !in_array($_FILES['pic']['type'], $valid_files ) ){
		echo 'Only jpg, png and gif image types are supported!';
	}else{
		#Upload photo here
		$img = realpath($_FILES["pic"]["tmp_name"]);

		$args = array(
			'message' => 'This photo was uploaded via WebSpeaks.in',
			'image' => '@' . $img,
			'aid' => $album_id,
			'no_story' => 0,
			'access_token' => $fanpage_token
		);

		$photo = $facebook->api($album_id . '/photos', 'post', $args);
		if( is_array( $photo ) && !empty( $photo['id'] ) ){
			echo '<p><a target="_blank" href="http://www.facebook.com/photo.php?fbid='.$photo['id'].'">Click here to watch this photo on Facebook.</a></p>';
		}
	}
}

?>
<!-- Form for uploading the photo -->
<div class="main">
  <p>Select a photo to upload on Facebook Fan Page</p>
  <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
    <p>Select the image: <input type="file" name="pic" /></p>
    <p><input class="post_but" type="submit" value="Create album and Upload" /></p>
  </form>
</div>
</body>
</html>
